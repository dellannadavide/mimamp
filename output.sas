begin_variables
107
var0 7 -1
var1 6 -1
var2 2 -1
var3 2 -1
var4 2 -1
var5 2 -1
var6 2 -1
var7 2 -1
var8 2 -1
var9 2 -1
var10 2 -1
var11 2 -1
var12 2 -1
var13 2 -1
var14 2 -1
var15 2 -1
var16 2 -1
var17 2 -1
var18 2 -1
var19 2 -1
var20 2 -1
var21 2 -1
var22 2 -1
var23 2 -1
var24 2 -1
var25 2 -1
var26 2 -1
var27 2 -1
var28 2 -1
var29 2 -1
var30 2 -1
var31 2 -1
var32 2 -1
var33 2 -1
var34 2 -1
var35 2 -1
var36 -1 -1
var37 -1 -1
var38 -1 -1
var39 -1 -1
var40 -1 -1
var41 -1 -1
var42 -1 -1
var43 -1 -1
var44 -1 -1
var45 -1 -1
var46 -1 -1
var47 -1 0
var48 -1 1
var49 -1 -1
var50 -1 -1
var51 -1 -1
var52 -1 -1
var53 -1 -1
var54 -1 -1
var55 -1 -1
var56 -1 -1
var57 -1 -1
var58 -1 -1
var59 -1 -1
var60 -1 -1
var61 -1 -1
var62 -1 -1
var63 -1 -1
var64 -1 -1
var65 -1 -1
var66 -1 -1
var67 -1 -1
var68 -1 -1
var69 -1 -1
var70 -1 -1
var71 -1 -1
var72 -1 -1
var73 -1 -1
var74 -1 -1
var75 -1 -1
var76 -1 -1
var77 -1 -1
var78 -1 -1
var79 3 2
var80 3 2
var81 3 2
var82 3 2
var83 3 2
var84 3 2
var85 3 2
var86 3 2
var87 3 2
var88 3 2
var89 3 2
var90 3 2
var91 3 2
var92 3 2
var93 3 2
var94 3 2
var95 3 2
var96 3 2
var97 3 2
var98 3 2
var99 3 2
var100 3 2
var101 3 2
var102 3 2
var103 3 2
var104 3 2
var105 3 2
var106 3 2
end_variables
begin_state
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
0
0
1
1
0
1
1
1
1
1
1
1
1
0
0
1
1
1
1
0
1863.0
247.0
241.0
1908.8
1037.1
2277.7
95.8
953.8
1284.4
1282.6
-2
-2
2638.7
88.1
999.0
1167.3
1810.0
2004.5
1127.0
2361.5
305.0
360.3
10.0
372.2
1803.8
36.3
1671.2
1702.0
1036.2
1362.1
1359.2
2726.8
310.0
882.0
1873.0
1.0
2.0
300.0
872.0
1800.0
0.0
0.0
2
2
2
2
2
2
2
2
2
2
2
2
2
2
2
2
2
2
2
2
2
2
2
2
2
2
2
2
end_state
begin_goal
6
9 0
10 0
11 0
12 0
15 0
16 0
end_goal
67
begin_operator
landing uav5 airport-cuf-site-begin-landing airport-cuf-site airport-cuf
= 57
1
28 0
0
0
1
0 0 0 1 0 5
2
0 0 0 1 5 1
0 0 0 15 -1 0
end_operator
begin_operator
landing uav7 airport-cuf-site-begin-landing airport-cuf-site airport-cuf
= 74
1
29 0
0
0
1
0 0 0 0 0 6
2
0 0 0 16 -1 0
0 0 0 0 6 1
end_operator
begin_operator
monitor uav5 obsreq110
= 74
2
24 0
17 0
0
1
24 0
1
0 0 0 9 1 0
1
0 0 0 77 + 72
end_operator
begin_operator
monitor uav5 obsreq113
= 37
2
25 0
21 0
0
1
25 0
1
0 0 0 10 1 0
1
0 0 0 77 + 72
end_operator
begin_operator
monitor uav7 obsreq111
= 75
2
26 0
18 0
0
1
26 0
1
0 0 0 11 1 0
1
0 0 0 78 + 72
end_operator
begin_operator
monitor uav7 obsreq112
= 76
2
20 0
27 0
0
1
27 0
1
0 0 0 12 1 0
1
0 0 0 78 + 72
end_operator
begin_operator
monitor_before uav7 uav5 obsreq111 obsreq112
= 75
2
26 0
18 0
0
1
26 0
2
0 0 0 19 -1 0
0 0 0 11 1 0
1
0 0 0 78 + 72
end_operator
begin_operator
monitor_before uav7 uav7 obsreq111 obsreq112
= 75
2
26 0
18 0
0
1
26 0
2
0 0 0 11 1 0
0 0 0 20 -1 0
1
0 0 0 78 + 72
end_operator
begin_operator
stay_on_trg uav5 target109 obsreq113
= 71
3
1 2
2 0
33 1
0
0
1
0 0 0 25 -1 0
3
0 0 0 33 -1 0
0 0 1 2 0 2 -1 1
0 0 1 25 0 25 0 1
end_operator
begin_operator
stay_on_trg uav5 target88 obsreq110
= 69
3
32 1
1 3
3 0
0
0
1
0 0 0 24 -1 0
3
0 0 0 32 -1 0
0 0 1 24 0 24 0 1
0 0 1 3 0 3 -1 1
end_operator
begin_operator
stay_on_trg uav5 targetend109 obsreq113
= 71
3
1 4
4 0
33 1
0
0
1
0 0 0 25 -1 0
3
0 0 0 33 -1 0
0 0 1 4 0 4 -1 1
0 0 1 25 0 25 0 1
end_operator
begin_operator
stay_on_trg uav7 target89 obsreq111
= 70
3
0 2
34 1
5 0
0
0
1
0 0 0 26 -1 0
3
0 0 0 34 -1 0
0 0 1 26 0 26 0 1
0 0 1 5 0 5 -1 1
end_operator
begin_operator
stay_on_trg uav7 target90 obsreq112
= 53
3
0 3
35 1
6 0
0
0
1
0 0 0 27 -1 0
3
0 0 1 27 0 27 0 1
0 0 0 35 -1 0
0 0 1 6 0 6 -1 1
end_operator
begin_operator
stay_on_trg uav7 targetend89 obsreq111
= 70
3
0 4
34 1
7 0
0
0
1
0 0 0 26 -1 0
3
0 0 0 34 -1 0
0 0 1 26 0 26 0 1
0 0 1 7 0 7 -1 1
end_operator
begin_operator
stay_on_trg uav7 targetend90 obsreq112
= 53
3
0 5
8 0
35 1
0
0
1
0 0 0 27 -1 0
3
0 0 1 27 0 27 0 1
0 0 1 8 0 8 -1 1
0 0 0 35 -1 0
end_operator
begin_operator
take_off uav5 airport-cuf-site airport-cuf-site-end-takeoff airport-cuf
= 38
2
1 1
30 0
0
0
0
2
0 0 1 30 0 30 -1 1
0 0 0 22 -1 0
end_operator
begin_operator
take_off uav7 airport-cuf-site airport-cuf-site-end-takeoff airport-cuf
= 39
2
0 1
31 0
0
0
0
2
0 0 1 31 0 31 -1 1
0 0 0 23 -1 0
end_operator
begin_operator
transfer_to_land uav5 airport-cuf-site airport-cuf-site-begin-landing airport-cuf
= 43
2
80 0
79 0
0
0
2
0 0 0 1 1 5
0 0 0 22 0 1
2
0 0 0 1 5 0
0 0 0 28 -1 0
end_operator
begin_operator
transfer_to_land uav5 airport-cuf-site-begin-landing airport-cuf-site-begin-landing airport-cuf
= 36
2
79 0
81 0
0
0
2
0 0 0 1 0 5
0 0 0 22 0 1
2
0 0 0 1 5 0
0 0 0 28 -1 0
end_operator
begin_operator
transfer_to_land uav5 target109 airport-cuf-site-begin-landing airport-cuf
= 55
2
82 0
79 0
0
0
2
0 0 0 1 2 5
0 0 0 22 0 1
2
0 0 0 1 5 0
0 0 0 28 -1 0
end_operator
begin_operator
transfer_to_land uav5 target88 airport-cuf-site-begin-landing airport-cuf
= 54
2
83 0
79 0
0
0
2
0 0 0 1 3 5
0 0 0 22 0 1
2
0 0 0 1 5 0
0 0 0 28 -1 0
end_operator
begin_operator
transfer_to_land uav5 targetend109 airport-cuf-site-begin-landing airport-cuf
= 56
2
84 0
79 0
0
0
2
0 0 0 1 4 5
0 0 0 22 0 1
2
0 0 0 1 5 0
0 0 0 28 -1 0
end_operator
begin_operator
transfer_to_land uav7 airport-cuf-site airport-cuf-site-begin-landing airport-cuf
= 50
2
85 0
86 0
0
0
2
0 0 0 0 1 6
0 0 0 23 0 1
2
0 0 0 0 6 0
0 0 0 29 -1 0
end_operator
begin_operator
transfer_to_land uav7 airport-cuf-site-begin-landing airport-cuf-site-begin-landing airport-cuf
= 36
2
81 0
85 0
0
0
2
0 0 0 0 0 6
0 0 0 23 0 1
2
0 0 0 0 6 0
0 0 0 29 -1 0
end_operator
begin_operator
transfer_to_land uav7 target89 airport-cuf-site-begin-landing airport-cuf
= 65
2
85 0
87 0
0
0
2
0 0 0 0 2 6
0 0 0 23 0 1
2
0 0 0 0 6 0
0 0 0 29 -1 0
end_operator
begin_operator
transfer_to_land uav7 target90 airport-cuf-site-begin-landing airport-cuf
= 67
2
88 0
85 0
0
0
2
0 0 0 0 3 6
0 0 0 23 0 1
2
0 0 0 0 6 0
0 0 0 29 -1 0
end_operator
begin_operator
transfer_to_land uav7 targetend89 airport-cuf-site-begin-landing airport-cuf
= 66
2
89 0
85 0
0
0
2
0 0 0 0 4 6
0 0 0 23 0 1
2
0 0 0 0 6 0
0 0 0 29 -1 0
end_operator
begin_operator
transfer_to_land uav7 targetend90 airport-cuf-site-begin-landing airport-cuf
= 68
2
90 0
85 0
0
0
2
0 0 0 0 5 6
0 0 0 23 0 1
2
0 0 0 0 6 0
0 0 0 29 -1 0
end_operator
begin_operator
transfer_to_trg uav5 airport-cuf-site target109 obsreq113
= 41
2
91 0
22 0
0
2
10 1
21 0
2
0 0 0 1 1 5
0 0 0 2 1 0
1
0 0 0 1 5 2
end_operator
begin_operator
transfer_to_trg uav5 airport-cuf-site target88 obsreq110
= 40
2
92 0
22 0
0
2
17 0
9 1
2
0 0 0 1 1 5
0 0 0 3 1 0
1
0 0 0 1 5 3
end_operator
begin_operator
transfer_to_trg uav5 airport-cuf-site targetend109 obsreq113
= 42
2
93 0
22 0
0
2
10 1
21 0
2
0 0 0 1 1 5
0 0 0 4 1 0
1
0 0 0 1 5 4
end_operator
begin_operator
transfer_to_trg uav5 airport-cuf-site-begin-landing target109 obsreq113
= 55
2
82 0
22 0
0
2
10 1
21 0
2
0 0 0 1 0 5
0 0 0 2 1 0
1
0 0 0 1 5 2
end_operator
begin_operator
transfer_to_trg uav5 airport-cuf-site-begin-landing target88 obsreq110
= 54
2
83 0
22 0
0
2
17 0
9 1
2
0 0 0 1 0 5
0 0 0 3 1 0
1
0 0 0 1 5 3
end_operator
begin_operator
transfer_to_trg uav5 airport-cuf-site-begin-landing targetend109 obsreq113
= 56
2
84 0
22 0
0
2
10 1
21 0
2
0 0 0 1 0 5
0 0 0 4 1 0
1
0 0 0 1 5 4
end_operator
begin_operator
transfer_to_trg uav5 target109 target109 obsreq113
= 36
2
22 0
81 0
0
2
10 1
21 0
2
0 0 0 1 2 5
0 0 0 2 1 0
1
0 0 0 1 5 2
end_operator
begin_operator
transfer_to_trg uav5 target109 target88 obsreq110
= 51
2
22 0
94 0
0
2
17 0
9 1
2
0 0 0 1 2 5
0 0 0 3 1 0
1
0 0 0 1 5 3
end_operator
begin_operator
transfer_to_trg uav5 target109 targetend109 obsreq113
= 53
2
22 0
95 0
0
2
10 1
21 0
2
0 0 0 1 2 5
0 0 0 4 1 0
1
0 0 0 1 5 4
end_operator
begin_operator
transfer_to_trg uav5 target88 target109 obsreq113
= 51
2
22 0
94 0
0
2
10 1
21 0
2
0 0 0 1 3 5
0 0 0 2 1 0
1
0 0 0 1 5 2
end_operator
begin_operator
transfer_to_trg uav5 target88 target88 obsreq110
= 36
2
22 0
81 0
0
2
17 0
9 1
2
0 0 0 1 3 5
0 0 0 3 1 0
1
0 0 0 1 5 3
end_operator
begin_operator
transfer_to_trg uav5 target88 targetend109 obsreq113
= 52
2
96 0
22 0
0
2
10 1
21 0
2
0 0 0 1 3 5
0 0 0 4 1 0
1
0 0 0 1 5 4
end_operator
begin_operator
transfer_to_trg uav5 targetend109 target109 obsreq113
= 53
2
22 0
95 0
0
2
10 1
21 0
2
0 0 0 1 4 5
0 0 0 2 1 0
1
0 0 0 1 5 2
end_operator
begin_operator
transfer_to_trg uav5 targetend109 target88 obsreq110
= 52
2
96 0
22 0
0
2
17 0
9 1
2
0 0 0 1 4 5
0 0 0 3 1 0
1
0 0 0 1 5 3
end_operator
begin_operator
transfer_to_trg uav5 targetend109 targetend109 obsreq113
= 36
2
22 0
81 0
0
2
10 1
21 0
2
0 0 0 1 4 5
0 0 0 4 1 0
1
0 0 0 1 5 4
end_operator
begin_operator
transfer_to_trg uav7 airport-cuf-site target89 obsreq111
= 44
2
97 0
23 0
0
2
18 0
11 1
2
0 0 0 0 1 6
0 0 0 5 1 0
1
0 0 0 0 6 2
end_operator
begin_operator
transfer_to_trg uav7 airport-cuf-site target90 obsreq112
= 46
2
98 0
23 0
0
2
20 0
12 1
2
0 0 0 0 1 6
0 0 0 6 1 0
1
0 0 0 0 6 3
end_operator
begin_operator
transfer_to_trg uav7 airport-cuf-site targetend89 obsreq111
= 45
2
23 0
99 0
0
2
18 0
11 1
2
0 0 0 0 1 6
0 0 0 7 1 0
1
0 0 0 0 6 4
end_operator
begin_operator
transfer_to_trg uav7 airport-cuf-site targetend90 obsreq112
= 49
2
100 0
23 0
0
2
20 0
12 1
2
0 0 0 8 1 0
0 0 0 0 1 6
1
0 0 0 0 6 5
end_operator
begin_operator
transfer_to_trg uav7 airport-cuf-site-begin-landing target89 obsreq111
= 65
2
23 0
87 0
0
2
18 0
11 1
2
0 0 0 0 0 6
0 0 0 5 1 0
1
0 0 0 0 6 2
end_operator
begin_operator
transfer_to_trg uav7 airport-cuf-site-begin-landing target90 obsreq112
= 67
2
88 0
23 0
0
2
20 0
12 1
2
0 0 0 0 0 6
0 0 0 6 1 0
1
0 0 0 0 6 3
end_operator
begin_operator
transfer_to_trg uav7 airport-cuf-site-begin-landing targetend89 obsreq111
= 66
2
89 0
23 0
0
2
18 0
11 1
2
0 0 0 0 0 6
0 0 0 7 1 0
1
0 0 0 0 6 4
end_operator
begin_operator
transfer_to_trg uav7 airport-cuf-site-begin-landing targetend90 obsreq112
= 68
2
90 0
23 0
0
2
20 0
12 1
2
0 0 0 8 1 0
0 0 0 0 0 6
1
0 0 0 0 6 5
end_operator
begin_operator
transfer_to_trg uav7 target89 target89 obsreq111
= 36
2
81 0
23 0
0
2
18 0
11 1
2
0 0 0 0 2 6
0 0 0 5 1 0
1
0 0 0 0 6 2
end_operator
begin_operator
transfer_to_trg uav7 target89 target90 obsreq112
= 60
2
101 0
23 0
0
2
20 0
12 1
2
0 0 0 0 2 6
0 0 0 6 1 0
1
0 0 0 0 6 3
end_operator
begin_operator
transfer_to_trg uav7 target89 targetend89 obsreq111
= 58
2
23 0
102 0
0
2
18 0
11 1
2
0 0 0 0 2 6
0 0 0 7 1 0
1
0 0 0 0 6 4
end_operator
begin_operator
transfer_to_trg uav7 target89 targetend90 obsreq112
= 61
2
23 0
103 0
0
2
20 0
12 1
2
0 0 0 8 1 0
0 0 0 0 2 6
1
0 0 0 0 6 5
end_operator
begin_operator
transfer_to_trg uav7 target90 target89 obsreq111
= 60
2
23 0
101 0
0
2
18 0
11 1
2
0 0 0 0 3 6
0 0 0 5 1 0
1
0 0 0 0 6 2
end_operator
begin_operator
transfer_to_trg uav7 target90 target90 obsreq112
= 36
2
81 0
23 0
0
2
20 0
12 1
2
0 0 0 0 3 6
0 0 0 6 1 0
1
0 0 0 0 6 3
end_operator
begin_operator
transfer_to_trg uav7 target90 targetend89 obsreq111
= 62
2
104 0
23 0
0
2
18 0
11 1
2
0 0 0 0 3 6
0 0 0 7 1 0
1
0 0 0 0 6 4
end_operator
begin_operator
transfer_to_trg uav7 target90 targetend90 obsreq112
= 64
2
105 0
23 0
0
2
20 0
12 1
2
0 0 0 8 1 0
0 0 0 0 3 6
1
0 0 0 0 6 5
end_operator
begin_operator
transfer_to_trg uav7 targetend89 target89 obsreq111
= 58
2
102 0
23 0
0
2
18 0
11 1
2
0 0 0 0 4 6
0 0 0 5 1 0
1
0 0 0 0 6 2
end_operator
begin_operator
transfer_to_trg uav7 targetend89 target90 obsreq112
= 62
2
104 0
23 0
0
2
20 0
12 1
2
0 0 0 0 4 6
0 0 0 6 1 0
1
0 0 0 0 6 3
end_operator
begin_operator
transfer_to_trg uav7 targetend89 targetend89 obsreq111
= 36
2
81 0
23 0
0
2
18 0
11 1
2
0 0 0 0 4 6
0 0 0 7 1 0
1
0 0 0 0 6 4
end_operator
begin_operator
transfer_to_trg uav7 targetend89 targetend90 obsreq112
= 63
2
106 0
23 0
0
2
20 0
12 1
2
0 0 0 8 1 0
0 0 0 0 4 6
1
0 0 0 0 6 5
end_operator
begin_operator
transfer_to_trg uav7 targetend90 target89 obsreq111
= 61
2
23 0
103 0
0
2
18 0
11 1
2
0 0 0 0 5 6
0 0 0 5 1 0
1
0 0 0 0 6 2
end_operator
begin_operator
transfer_to_trg uav7 targetend90 target90 obsreq112
= 64
2
105 0
23 0
0
2
20 0
12 1
2
0 0 0 0 5 6
0 0 0 6 1 0
1
0 0 0 0 6 3
end_operator
begin_operator
transfer_to_trg uav7 targetend90 targetend89 obsreq111
= 63
2
106 0
23 0
0
2
18 0
11 1
2
0 0 0 0 5 6
0 0 0 7 1 0
1
0 0 0 0 6 4
end_operator
begin_operator
transfer_to_trg uav7 targetend90 targetend90 obsreq112
= 36
2
81 0
23 0
0
2
20 0
12 1
2
0 0 0 8 1 0
0 0 0 0 5 6
1
0 0 0 0 6 5
end_operator
0
28
79 = 47 36
80 > 43 36
81 > 36 36
82 > 55 36
83 > 54 36
84 > 56 36
85 = 48 36
86 > 50 36
87 > 65 36
88 > 67 36
89 > 66 36
90 > 68 36
91 > 41 36
92 > 40 36
93 > 42 36
94 > 51 36
95 > 53 36
96 > 52 36
97 > 44 36
98 > 46 36
99 > 45 36
100 > 49 36
101 > 60 36
102 > 58 36
103 > 61 36
104 > 62 36
105 > 64 36
106 > 63 36
2
47 - 77 73
48 - 78 73
0
