(define (domain SmatF2)
(:types 
          Uav Airport Target Site2D SensorSuite  - object
          Point - Target
)
(:predicates
        ;; Predicato per sapere quale sia la posizione corrente dell'Uav (coordinate x,y del punto "site" proiettato a terra):
        (current_site ?uav - Uav ?l - Site2D)
        
        ;; Predicati di TAKEOFF dell'Uav da un aeroporto:
        ;; dato che nella modellazione dell'oggetto aeroporto su DB, 
        ;; esso è individuato da un segmento individuato da 2 punti nel piano (Site2D),
        ;; assumiamo che il primo sia quello "di inizio pista" da cui comincia a rullare l'Uav
        ;; e che il secondo rappresenti una approssimazione del punto di fine pista 
        ;; (in cui immaginiamo già l'Uav in volo, quindi con una Altitudine diversa da zero):

        ;; 1.
        (ready_to_takeoff ?uav - Uav ?a - Airport)
        (site_for_begin_takeOff ?a - Airport ?l - Site2D) ;; qui riportiamo proprio il punto 2D di inizio pista
                                                       ;; in cui l'Uav deve trovarsi per decollare

        ;; 2. 
        (site_for_end_takeOff ?a - Airport ?l - Site2D) ;; qui riportiamo proprio il punto 2D che approssima quello di fine pista
                                                        ;; in cui l'Uav si troverà dopo il decollo

        ;; Predicati di LANDING dell'Uav in un aeroporto:
        ;; 1. 
        (site_for_landing ?a - Airport ?l - Site2D) ;; qui riportiamo il punto 2D su cui deve trovarsi l'Uav per atterrare
        ;; 2.
        (site_after_landing ?a - Airport ?l - Site2D) ;; qui riportiamo il punto 2D su cui deve trovarsi 
                                                      ;; l'Uav dopo essere atterrato sulla pista
        ;; 3.
        (landed ?uav - Uav  ?a - Airport) ;; qui diamo conferma dell'atterraggio avvenuto

        ;; Predicato di FLY dell'Uav in volo:
        (on_fly ?uav - Uav) ;; qui diciamo se o meno l'Uav è in volo

        ;; Predicati per la MONITORING TARGET da parte di un Uav con un certo sensore:
        
        ;; 1. Dico che il Target ?t deve essere coperto dall'Uav ?uav con una sensor suite ?ss
        (to_cover ?t - Target ?uav - Uav ?ss - SensorSuite)
 
        ;; 2. Dico se l'Uav ?uav ha coperto il target ?t:
        (covered ?uav - Uav ?t  - Target)

        ;; 3. Dico se l'Uav ?uav non ha ancora coperto il target ?t:
        (uncovered ?uav - Uav ?t - Target)

        (unvisited_t ?t - Target)


        ;; 4. Dico quale deve essere la posizione (punto di proiezione 2d al suolo) per l'inizio dell'osservazione di un target:
        (site_target ?l_target - Site2D ?t - Target)

        ;target t1 deve essere monitorato prima del target t2
        (before ?t1 - Target ?t2 - Target ?uav1 - Uav ?uav2 - Uav)
        (overlap ?t1 - Target ?t2 - Target ?uav1 - Uav ?uav2 - Uav)        
        ;(covering_order ?t1 - Target ?t2 - Target)
        (already-transfered ?uav - Uav) ;questo serve per evitare che una volta trasferiti sul target non si decida di trasferirsi su un altro target
        (not_same ?l1 - Site2D ?l2 - Site2D)
        (not-already-transfered ?uav - Uav)

        (osservabile ?t1 - Target ?uav - Uav)

        (choosen_start_site_target ?uav - Uav ?l_start_target - Site2D ?t - Target)
)
(:functions 
            ;; Mi servono i seguenti fluenti per consentire a ciascuno uav di cominciare le azioni relative al landing
            ;; solo dopo aver monitorato i target che gli erano stati assegnati:
            (NumberTrgAssigned ?uav - Uav)
            (NumberTrgCovered ?uav - Uav)

            ;; Fluenti che calcolano distanze tra punti (misurate in METRI):
            (Distance2D ?l1 ?l2 - Site2D) ;; Funzione che fornisce la distanza euclidea di due punti sul piano
            (Distance-waiting-flight ?uav - Uav) ;; Fluente che fornisce la distanza che un Uav può percorrere al fine 
                                                 ;; di far trascorrere un lasso di tempo strategico per sincronizzare con altri uav          
            ;; Funzioni che calcolano il tempo (misurato in MILLISECONDI):
            (Time-mission ?uav - Uav) ;; Fluente che restituisce il tempo di missione di ciascun Uav
            (Delta-Time ?uav - Uav) ;; Fluente rappresentante il tempo che è passato da quando è partito il beginner
                                    ;; rispetto al momento in cui parte tale uav.
                                    ;; Mi serve per ogni uav memorizzare tale informazione, perché questo lasso di tempo 
                                    ;; deve essere sottratto al tempo di missione dell'uav quando definiamo la funzione di minimizzazione.
            (Time-mission-start-obs-target ?t - Target ?uav - Uav) ;; Fluente che restituisce il momento 
                                                      ;; della missione in cui è iniziato il monitoring del target ?t
            (Time-mission-end-obs-target ?t - Target ?uav - Uav) ;; Fluente che restituisce il momento 
                                                      ;; della missione in cui è terminato il monitoring del target ?t
            (Time-landing ?uav - Uav ?a - Airport) ;; Funzione che restituisce il tempo 
                                                  ;; che impiega l'Uav ?uav per atterrare sull'aeroporto ?a
            (Time-take-off ?uav - Uav ?a - Airport) ;; Funzione che restituisce il tempo 
                                                   ;; che impiega l'Uav ?uav per decollare dall'aeroporto ?a
            (Time-min-obs ?t - Target) ;; Funzione che restituisce il tempo minimo 
                                       ;; di osservazione richiesto per il monitoraggio di un target ?t
            (Earliest-time-start-obs-target ?t - Target) ;; Funzione che restituisce il numero di millisecondi minimo 
                                       ;; che deve passare dall'inizio della missione per il monitoraggio del target ?t.
                                       ;; E' calcolato basandosi sull'earliest time associato al targetobs e sul tempo
                                       ;; di inizio prefissato della missione.
            (Latest-time-end-obs-target ?t - Target) ;; ;; Funzione che restituisce il numero di millisecondi massimo 
                                       ;; che deve passare dall'inizio della missione per il monitoraggio del target ?t.
                                       ;; E' calcolato basandosi sul latest time associato al targetobs e sul tempo
                                       ;; di inizio prefissato della missione.
            ;; (Time-end-takeoff-begins-uav ?uav - Uav)
            ;; Funzioni che restituiscono parametri specifici dell'Uav considerato:
            (Cruise-speed ?uav - Uav) ;; Funzione che restituisce la velocità media di crociera (metri/millisecondi) di un ?uav
            (Time-unit-distance ?uav - Uav) ;; Funzione che mi restituisce il reciproco della Cruise-speed, 
                                            ;; ovvero quanti millisecondi sono necessari per percorrere un metro
            (Rate-Consumption ?uav - Uav) ;; Funzione che restituisce il consumo medio di carburante (Litri/metro) di un ben preciso ?uav
            (Fuel-Level ?Uav - Uav) ;; Funzione che restituisce il consumo di carburante ?uav durante la missione

            (observable ?t1 - Target ?uav - Uav)

            (total-fly-time)

            ; servono per calcolare la durata effettiva della missione
            (min_delta_time) ;dice il tempo di partenza del primo uav.
            (max_time_mission) ;dice il tempo di lanting dell'ultimo uav

            (time-wait-on-ground)
            (time-loiter)

            (time_between_take_off)
            (time-last-take-off ?a - Airport)

            (fuel_required_monitor ?t - Target ?uav - Uav)
            (time_required ?l1 ?l2 - Site2D ?uav - Uav)
            (fuel_required_transfer ?l1 ?l2 - Site2D ?uav - Uav)
            (fuel_required_loiter  ?uav - Uav)
)

(:action wait_on_ground
  :parameters (?uav - Uav ?a - Airport)
  :precondition (and  (ready_to_takeoff ?uav ?a))
  :effect (and (increase (Delta-Time ?uav) (time-wait-on-ground))
                (increase (Time-mission ?uav) (time-wait-on-ground))
          )
)

(:action take_off_prev
  :parameters (?uav - Uav ?l_start ?l_end - Site2D ?a - Airport)
  :precondition (and 
                    (ready_to_takeoff ?uav ?a)
                    (site_for_begin_takeOff ?a ?l_start)
                    (site_for_end_takeOff ?a ?l_end)
                    (current_site ?uav ?l_start)
                    ;(>= (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))
                    (>= (Time-mission ?uav) (+ (time-last-take-off ?a) (time_between_take_off)))

                    (< (Delta-Time ?uav) (min_delta_time))
                )
  :effect (and 
             (not (ready_to_takeoff ?uav ?a))
             (current_site ?uav ?l_end)
             (not (current_site ?uav ?l_start))
             (on_fly ?uav)

             (assign (time-last-take-off ?a) (Time-mission ?uav))

             (increase (Time-mission ?uav) (Time-take-off ?uav ?a))
             (increase (total-fly-time) (Time-take-off ?uav ?a))
             ;(decrease (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))

             (assign (min_delta_time) (Delta-Time ?uav))
        )
)

(:action take_off
:parameters (?uav - Uav ?l_start ?l_end - Site2D ?a - Airport)
:precondition (and 
                  (ready_to_takeoff ?uav ?a)
                  (site_for_begin_takeOff ?a ?l_start)
                  (site_for_end_takeOff ?a ?l_end)
                  (current_site ?uav ?l_start)
                  ;(>= (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))
                  (>= (Time-mission ?uav) (+ (time-last-take-off ?a) (time_between_take_off)))

                  (>= (Delta-Time ?uav) (min_delta_time))
              )
:effect (and 
             (not (ready_to_takeoff ?uav ?a))
             (current_site ?uav ?l_end)
             (not (current_site ?uav ?l_start))
             (on_fly ?uav)

             (assign (time-last-take-off ?a) (Time-mission ?uav))

             (increase (Time-mission ?uav) (Time-take-off ?uav ?a))
             (increase (total-fly-time) (Time-take-off ?uav ?a))
             ;(decrease (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))

        )
)

(:action landing_succ
:parameters (?uav - Uav ?l_start ?l_end - Site2D ?a - Airport)
:precondition (and (on_fly ?uav)
                   (current_site ?uav ?l_start)
                   (site_for_landing ?a ?l_start)
                   (site_after_landing ?a ?l_end)
                   ;(>= (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))
                   (< (max_time_mission) (+ (Time-mission ?uav) (Time-landing ?uav ?a)))
              )
:effect (and  (landed ?uav ?a)
              (not (on_fly ?uav))
              (not (current_site ?uav ?l_start))
              (current_site ?uav ?l_end)
              (increase (Time-mission ?uav) (Time-landing ?uav ?a))
              (increase (total-fly-time) (Time-landing ?uav ?a))
              ;(decrease (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))
              (assign (max_time_mission) (+ (Time-mission ?uav) (Time-landing ?uav ?a)))
        )
)
(:action landing
:parameters (?uav - Uav ?l_start ?l_end - Site2D ?a - Airport)
:precondition (and (on_fly ?uav)
                   (current_site ?uav ?l_start)
                   (site_for_landing ?a ?l_start)
                   (site_after_landing ?a ?l_end)
                   ;(>= (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))
                   (>= (max_time_mission) (+ (Time-mission ?uav) (Time-landing ?uav ?a)))
              )
:effect (and  (landed ?uav ?a)
              (not (on_fly ?uav))
              (not (current_site ?uav ?l_start))
              (current_site ?uav ?l_end)
              (increase (Time-mission ?uav) (Time-landing ?uav ?a))
              (increase (total-fly-time) (Time-landing ?uav ?a))
              ;(decrease (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))
        )
)

(:action transfer_to_trg
:parameters  (?uav - Uav ?l_start ?l_end - Site2D ?t - Target)
:precondition  (and (on_fly ?uav)
                    (current_site ?uav ?l_start)
                    (site_target ?l_end ?t)
                    (uncovered ?uav ?t)
                    (>= (Distance2D ?l_start ?l_end) 0)
                    ;(>= (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))
                    (not-already-transfered ?uav)

               )
:effect (and (current_site ?uav ?l_end)
              (not (current_site ?uav ?l_start))

              (choosen_start_site_target ?uav ?l_end ?t)

              (increase (Time-mission ?uav)  (time_required ?l_start ?l_end ?uav))
              (increase (total-fly-time)  (time_required ?l_start ?l_end ?uav))
              ;(decrease (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))
              (not (not-already-transfered ?uav))
        )
)

(:action transfer_to_land
:parameters (?uav - Uav ?l_start ?l_end - Site2D ?a - Airport)
:precondition  (and (on_fly ?uav)
                    (current_site ?uav ?l_start)
                    (site_for_landing ?a ?l_end)
                    (> (Distance2D ?l_start ?l_end) 0)
                    (>= (NumberTrgCovered ?uav) (NumberTrgAssigned ?uav))  
                    ;(>= (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))
               )               
:effect  (and (current_site ?uav ?l_end)
              (not (current_site ?uav ?l_start))
              (increase (Time-mission ?uav)  (time_required ?l_start ?l_end ?uav))
              (increase (total-fly-time)  (time_required ?l_start ?l_end ?uav))
              ;(decrease (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))
         )
)

(:action monitor_target
:parameters (?uav - Uav ?uav_l_start ?uav_l_end - Site2D ?t - Target ?ss - SensorSuite) 
:precondition  (and (on_fly ?uav)
                    (current_site ?uav ?uav_l_start)
                    (to_cover ?t ?uav ?ss)
                    (uncovered ?uav ?t)
                    (osservabile ?t ?uav)
                    (<= (observable ?t ?uav) (Time-mission ?uav))

                    (choosen_start_site_target ?uav ?uav_l_start ?t)
                    (site_target ?uav_l_end ?t)
                    (not_same ?uav_l_end ?uav_l_start)

                    (>= (Time-mission ?uav) (Earliest-time-start-obs-target ?t))
                    (>= (Latest-time-end-obs-target ?t) (+ (Time-mission ?uav) (Time-min-obs ?t)))     
                    ;(>= (Fuel-Level ?uav) (* (* (Time-min-obs ?t) (Cruise-speed ?uav)) (Rate-Consumption ?uav)))
               )                  
:effect  (and (covered ?uav ?t)
              (not (uncovered ?uav ?t))

              (not (choosen_start_site_target ?uav ?uav_l_start ?t))

              (assign (Time-mission-start-obs-target ?t ?uav) (Time-mission ?uav))
              (assign (Time-mission-end-obs-target ?t ?uav) (+ (Time-mission ?uav) (Time-min-obs ?t)))
              (not (current_site ?uav ?uav_l_start))
              (current_site ?uav ?uav_l_end)
              (increase (Time-mission ?uav)  (Time-min-obs ?t))
              (increase (total-fly-time)  (Time-min-obs ?t))
              (increase (NumberTrgCovered ?uav) 1)
              ;(decrease (Fuel-Level ?uav) (* (* (Time-min-obs ?t) (Cruise-speed ?uav)) (Rate-Consumption ?uav)))
              (not-already-transfered ?uav)
         )
)

(:action monitor_point_target
:parameters (?uav - Uav ?uav_l_start - Site2D ?t - Point ?ss - SensorSuite) 
                                                                                        ;; in tal caso abbiamo un monitoraggio di un target (che può essere puntuale o lineare, quindi anche area...perché assumiamo che
                                                                                        ;; anche un target di tipo area possa essere rappresentato da un segmento: diagonale del box2d che circoscrive l'area)
                                                                                        ;; la leg come proiezione 2D della traiettoria aerea dell'uav corrisponde proprio al segmento al suolo che identifica
                                                                                        ;; il target (puntuale se i due estremi coincidono, oppure lineare o areoforme)
:precondition  (and (on_fly ?uav)
                    (current_site ?uav ?uav_l_start)
                    (to_cover ?t ?uav ?ss)
                    (uncovered ?uav ?t)
                    (osservabile ?t ?uav)
                    (<= (observable ?t ?uav) (Time-mission ?uav))

                    (choosen_start_site_target ?uav ?uav_l_start ?t)
                    (site_target ?uav_l_start ?t)

                    (>= (Time-mission ?uav) (Earliest-time-start-obs-target ?t))
                    (>= (Latest-time-end-obs-target ?t) (+ (Time-mission ?uav) (Time-min-obs ?t)))        
                    ;(>= (Fuel-Level ?uav) (* (* (Time-min-obs ?t) (Cruise-speed ?uav)) (Rate-Consumption ?uav)))
               )                  
:effect  (and (covered ?uav ?t)
              (not (uncovered ?uav ?t))

              (not (choosen_start_site_target ?uav ?uav_l_start ?t))

              (assign (Time-mission-start-obs-target ?t ?uav) (Time-mission ?uav))
              (assign (Time-mission-end-obs-target ?t ?uav) (+ (Time-mission ?uav) (Time-min-obs ?t)))
              (increase (Time-mission ?uav)  (Time-min-obs ?t))
              (increase (total-fly-time)  (Time-min-obs ?t))
              (increase (NumberTrgCovered ?uav) 1)
              ;(decrease (Fuel-Level ?uav) (* (* (Time-min-obs ?t) (Cruise-speed ?uav)) (Rate-Consumption ?uav)))
              (not-already-transfered ?uav)
         )
)

(:action enable_target_before
  :parameters (?t1 ?t2 - Target ?uav1 ?uav2 - Uav) 
  :precondition (and (before ?t1 ?t2 ?uav1 ?uav2) (covered  ?uav1 ?t1))
  :effect (and (assign (observable ?t2 ?uav2) (Time-mission-end-obs-target ?t1 ?uav1))
               (osservabile ?t2 ?uav2))
)
(:action enable_target_overlap
  :parameters (?t1 ?t2 - Target ?uav1 ?uav2 - Uav) 
  :precondition (and (overlap ?t1 ?t2 ?uav1 ?uav2) (covered  ?uav1 ?t1))
  :effect (and (assign (observable ?t2 ?uav2) (Time-mission-start-obs-target ?t1 ?uav1))
               (osservabile ?t2 ?uav2))
)


(:action loiter 
:parameters  (?uav - Uav ?l_site - Site2D ?t - Target) 
:precondition  (and (on_fly ?uav)
                    (current_site ?uav ?l_site)

                    (choosen_start_site_target ?uav ?l_site ?t)
                    (> (observable ?t ?uav) (Time-mission ?uav))
                    (< (observable ?t ?uav) 999999995904)
                    ;(>= (Fuel-Level ?uav) (* (Distance-waiting-flight ?uav) (Rate-Consumption ?uav)))
                    (> (NumberTrgCovered ?uav) 0)
               )
:effect (and
              
              (increase (Time-mission ?uav)  (time-loiter))
              (increase (total-fly-time)  (time-loiter))
              ;(decrease (Fuel-Level ?uav) (* (* (time-loiter) (Cruise-speed ?uav)) (Rate-Consumption ?uav)))
        )
)

)
