(define (domain SmatF2)
(:requirements :equality :typing :fluents :disjunctive-preconditions :negative-preconditions :durative-actions :timed-initial-literals :duration-inequalities )
(:types 
          Uav Airport Target Site2D SensorSuite  - object
          Point - Target
)
(:predicates
        ;; Predicato per sapere quale sia la posizione corrente dell'Uav (coordinate x,y del punto "site" proiettato a terra):
        (current_site ?uav - Uav ?l - Site2D)
        
        ;; Predicati di TAKEOFF dell'Uav da un aeroporto:
        ;; dato che nella modellazione dell'oggetto aeroporto su DB, 
        ;; esso è individuato da un segmento individuato da 2 punti nel piano (Site2D),
        ;; assumiamo che il primo sia quello "di inizio pista" da cui comincia a rullare l'Uav
        ;; e che il secondo rappresenti una approssimazione del punto di fine pista 
        ;; (in cui immaginiamo già l'Uav in volo, quindi con una Altitudine diversa da zero):

        ;; 1.
        (ready_to_takeoff ?uav - Uav ?a - Airport)
        (site_for_begin_takeOff ?a - Airport ?l - Site2D) ;; qui riportiamo proprio il punto 2D di inizio pista
                                                       ;; in cui l'Uav deve trovarsi per decollare

        ;; 2. 
        (site_for_end_takeOff ?a - Airport ?l - Site2D) ;; qui riportiamo proprio il punto 2D che approssima quello di fine pista
                                                        ;; in cui l'Uav si troverà dopo il decollo

        ;; Predicati di LANDING dell'Uav in un aeroporto:
        ;; 1. 
        (site_for_landing ?a - Airport ?l - Site2D) ;; qui riportiamo il punto 2D su cui deve trovarsi l'Uav per atterrare
        ;; 2.
        (site_after_landing ?a - Airport ?l - Site2D) ;; qui riportiamo il punto 2D su cui deve trovarsi 
                                                      ;; l'Uav dopo essere atterrato sulla pista
        ;; 3.
        (landed ?uav - Uav  ?a - Airport) ;; qui diamo conferma dell'atterraggio avvenuto

        ;; Predicato di FLY dell'Uav in volo:
        (on_fly ?uav - Uav) ;; qui diciamo se o meno l'Uav è in volo

        ;; Predicati per la MONITORING TARGET da parte di un Uav con un certo sensore:
        
        ;; 1. Dico che il Target ?t deve essere coperto dall'Uav ?uav con una sensor suite ?ss
        (to_cover ?t - Target ?uav - Uav)
 
        ;; 2. Dico se l'Uav ?uav ha coperto il target ?t:
        (covered ?uav - Uav ?t  - Target)

        ;; 3. Dico se l'Uav ?uav non ha ancora coperto il target ?t:
        (uncovered ?uav - Uav ?t - Target)

        ;; 4. Dico quale deve essere la posizione (punto di proiezione 2d al suolo) per l'inizio dell'osservazione di un target:
        (site_target ?l_target - Site2D ?t - Target)

        ;target t1 deve essere monitorato prima del target t2
        (not_constrained ?t1 - Target)
        (before ?t1 - Target ?t2 - Target)
        (overlap ?t1 - Target ?t2 - Target ?uav1 - Uav ?uav2 - Uav)        
        ;(covering_order ?t1 - Target ?t2 - Target)
        (already-transfered ?uav - Uav) ;questo serve per evitare che una volta trasferiti sul target non si decida di trasferirsi su un altro target
        (not-already-transfered ?uav - Uav)
        (osservabile ?t1 - Target)
        (observable ?t1 - Target ?uav - Uav)


        (choosen_start_site_target ?uav - Uav ?l_start_target - Site2D ?t - Target)

        (next_action ?uav - Uav)

        (visited ?uav - Uav ?loc - Site2D)
        (visited_t ?uav - Uav ?t - Target)
        (unvisited_t ?t - Target)

        (on_trg ?uav - Uav ?t - Target)
        (ready_to_land ?uav - Uav)
)
(:functions 
            ;; Mi servono i seguenti fluenti per consentire a ciascuno uav di cominciare le azioni relative al landing
            ;; solo dopo aver monitorato i target che gli erano stati assegnati:
            (NumberTrgAssigned ?uav - Uav)
            (NumberTrgCovered ?uav - Uav)

            ;; Fluenti che calcolano distanze tra punti (misurate in METRI):
            (Distance2D ?l1 ?l2 - Site2D) ;; Funzione che fornisce la distanza euclidea di due punti sul piano
            (Distance-waiting-flight ?uav - Uav) ;; Fluente che fornisce la distanza che un Uav può percorrere al fine 
                                                 ;; di far trascorrere un lasso di tempo strategico per sincronizzare con altri uav 
                                                 ;; eventuali monitoraggi           
            ;; Funzioni che calcolano il tempo (misurato in MILLISECONDI):
            (Time-mission ?uav - Uav) ;; Fluente che restituisce il tempo di missione di ciascun Uav
            (Delta-Time ?uav - Uav) ;; Fluente rappresentante il tempo che è passato da quando è partito il beginner
                                    ;; rispetto al momento in cui parte tale uav.
                                    ;; Mi serve per ogni uav memorizzare tale informazione, perché questo lasso di tempo 
                                    ;; deve essere sottratto al tempo di missione dell'uav quando definiamo la funzione di minimizzazione.
            (Time-mission-start-obs-target ?t - Target ?uav - Uav) ;; Fluente che restituisce il momento 
                                                      ;; della missione in cui è iniziato il monitoring del target ?t
            (Time-mission-end-obs-target ?t - Target ?uav - Uav) ;; Fluente che restituisce il momento 
                                                      ;; della missione in cui è terminato il monitoring del target ?t
            (Time-landing ?uav - Uav ?a - Airport) ;; Funzione che restituisce il tempo 
                                                  ;; che impiega l'Uav ?uav per atterrare sull'aeroporto ?a
            (Time-take-off ?uav - Uav ?a - Airport) ;; Funzione che restituisce il tempo 
                                                   ;; che impiega l'Uav ?uav per decollare dall'aeroporto ?a
            (Time-min-obs ?t - Target) ;; Funzione che restituisce il tempo minimo 
                                       ;; di osservazione richiesto per il monitoraggio di un target ?t
            (Earliest-time-start-obs-target ?t - Target) ;; Funzione che restituisce il numero di millisecondi minimo 
                                       ;; che deve passare dall'inizio della missione per il monitoraggio del target ?t.
                                       ;; E' calcolato basandosi sull'earliest time associato al targetobs e sul tempo
                                       ;; di inizio prefissato della missione.
            (Latest-time-end-obs-target ?t - Target) ;; ;; Funzione che restituisce il numero di millisecondi massimo 
                                       ;; che deve passare dall'inizio della missione per il monitoraggio del target ?t.
                                       ;; E' calcolato basandosi sul latest time associato al targetobs e sul tempo
                                       ;; di inizio prefissato della missione.
            ;; (Time-end-takeoff-begins-uav ?uav - Uav)
            ;; Funzioni che restituiscono parametri specifici dell'Uav considerato:
            (Cruise-speed ?uav - Uav) ;; Funzione che restituisce la velocità media di crociera (metri/millisecondi) di un ?uav
            (Time-unit-distance ?uav - Uav) ;; Funzione che mi restituisce il reciproco della Cruise-speed, 
                                            ;; ovvero quanti millisecondi sono necessari per percorrere un metro
            (Rate-Consumption ?uav - Uav) ;; Funzione che restituisce il consumo medio di carburante (Litri/metro) di un ben preciso ?uav
            (Fuel-Level ?Uav - Uav) ;; Funzione che restituisce il consumo di carburante ?uav durante la missione


            (total-fly-time)

            ; servono per calcolare la durata effettiva della missione
            (min_delta_time) ;dice il tempo di partenza del primo uav.
            (max_time_mission) ;dice il tempo di lanting dell'ultimo uav

            (time-wait-on-ground)
            (time-loiter)

            (time_between_take_off)
            (time-last-take-off ?a - Airport)

            (start_next ?uav - Uav)

            (fuel_required_monitor ?t - Target ?uav - Uav)
            (time_required ?l1 ?l2 - Site2D ?uav - Uav)
            (fuel_required_transfer ?l1 ?l2 - Site2D ?uav - Uav)
            (fuel_required_loiter  ?uav - Uav)

)
(:durative-action take_off
:parameters (?uav - Uav ?l_start ?l_end - Site2D ?a - Airport)
:duration (= ?duration (Time-take-off ?uav ?a))
:condition (and 
                (at start (ready_to_takeoff ?uav ?a))
                (at start (site_for_begin_takeOff ?a ?l_start))
                (at start (site_for_end_takeOff ?a ?l_end))
                (at start (current_site ?uav ?l_start))
              
                (at start (> (start_next ?uav) 0)) 
(at start (< (start_next ?uav) 0.1))
            )
  :effect (and 
                (at start (assign (start_next ?uav) (Time-take-off ?uav ?a)))
                (decrease (start_next ?uav) #t)

                (at start (on_fly ?uav))
                (at start (not (ready_to_takeoff ?uav ?a)))


                ;(at end (increase (Time-mission ?uav) (Time-take-off ?uav ?a)))
                ;(at end (increase (total-fly-time) (Time-take-off ?uav ?a)))            
        )
)


(:durative-action transfer_to_trg
:parameters (?uav - Uav ?l_start ?l_end - Site2D ?t - Target)
:duration (= ?duration (time_required ?l_start ?l_end ?uav))
:condition (and 
                (at start (on_fly ?uav))
                ;(at start (not (already-transfered ?uav)))
                (at start (site_target ?l_end ?t))

                (at start (current_site ?uav ?l_start))
                (at start (> (time_required ?l_start ?l_end ?uav) 0))

                (at end (observable ?t ?uav))
                (at end (uncovered ?uav ?t))
                (at start (to_cover ?t ?uav))

                (at start (> (start_next ?uav) 0)) 
(at start (< (start_next ?uav) 0.1))
             )
:effect (and 
                (at start (assign (start_next ?uav) (time_required ?l_start ?l_end ?uav)))
                (decrease (start_next ?uav) #t)

                (at start (not (current_site ?uav ?l_start)))
                (at start (current_site ?uav ?l_end))
                ;(at start (already-transfered ?uav))


                (at start (choosen_start_site_target ?uav ?l_end ?t))


                ;(at end (visited ?uav ?l_end))


                ;(at end (increase (Time-mission ?uav)  (* (Distance2D ?l_start ?l_end) (Time-unit-distance ?uav))))
                ;(at end (increase (total-fly-time)  (* (Distance2D ?l_start ?l_end) (Time-unit-distance ?uav))))
        )
)

(:durative-action stay_on_trg
:parameters (?uav - Uav ?l_start - Site2D ?t - Target)
:duration (= ?duration (+ (Time-min-obs ?t) 10))
:condition (and 

                (at start (current_site ?uav ?l_start))
                (at start (site_target ?l_start ?t))

                (at start (unvisited_t ?t))              


                
                (at start (> (start_next ?uav) 0)) 
(at start (< (start_next ?uav) 0.1))
                (at start (choosen_start_site_target ?uav ?l_start ?t))

             )
:effect (and 
                (at start (assign (start_next ?uav) (+ (Time-min-obs ?t) 10)))
                (at start (on_trg ?uav ?t))
                (at start (not (unvisited_t ?t)))
                (decrease (start_next ?uav) #t)         

                (at end (not (on_trg ?uav ?t)))
                ;(at start (not (already-transfered ?uav)))


                (at end (not (choosen_start_site_target ?uav ?l_start ?t)))



                ;(at end (increase (Time-mission ?uav)  (* (Distance2D ?l_start ?l_end) (Time-unit-distance ?uav))))
                ;(at end (increase (total-fly-time)  (* (Distance2D ?l_start ?l_end) (Time-unit-distance ?uav))))
        )
)


(:durative-action transfer_to_land
:parameters (?uav - Uav ?l_start ?l_end - Site2D ?a - Airport)
:duration (= ?duration (time_required ?l_start ?l_end ?uav))
:condition (and     
                (at start (= (NumberTrgCovered ?uav) (NumberTrgAssigned ?uav)))
                (at start (on_fly ?uav))
                (at start (current_site ?uav ?l_start))
                (at start (site_for_landing ?a ?l_end))
                (at start (> (time_required ?l_start ?l_end ?uav) 0))
                (at start (> (start_next ?uav) 0)) 
(at start (< (start_next ?uav) 0.1))

           )               
  :effect  (and           
                (at start (assign (start_next ?uav) (time_required ?l_start ?l_end ?uav)))
                (decrease (start_next ?uav) #t)

                (at start (not (current_site ?uav ?l_start)))
                (at start (current_site ?uav ?l_end))

                (at start (ready_to_land ?uav))
                (at start (not (on_fly ?uav)))


                
         )
)

(:durative-action landing
:parameters (?uav - Uav ?l_start ?l_end - Site2D ?a - Airport)
:duration (= ?duration (Time-landing ?uav ?a))
:condition (and     
                (at start (ready_to_land ?uav))

                (at start (current_site ?uav ?l_start))
                (at start (site_for_landing ?a ?l_start))
                (at start (site_after_landing ?a ?l_end))

                (at start (> (start_next ?uav) 0)) 
(at start (< (start_next ?uav) 0.1))
            )
:effect (and                      
                (at end (landed ?uav ?a))
                (at start (not (current_site ?uav ?l_start)))
                (at end (current_site ?uav ?l_end))
                ;(at end (increase (Time-mission ?uav) (Time-landing ?uav ?a)))
                ;(at end (increase (total-fly-time) (Time-landing ?uav ?a)))
                
        )
)


(:durative-action monitor
:parameters (?uav - Uav ?t - Target)
:duration (= ?duration (Time-min-obs ?t))
:condition (and 
                (at start (on_trg ?uav ?t))
                (at end (on_trg ?uav ?t))

                ;(at start (not_constrained ?t))
                (at start (observable ?t ?uav))


                (at start (uncovered ?uav ?t))

                (at start (osservabile ?t))
                (over all (osservabile ?t))
                (at end (osservabile ?t))
             )
:effect (and 
                (at start (covered ?uav ?t))
                (at start (not (uncovered ?uav ?t)))

                (at end (increase (NumberTrgCovered ?uav) 1))

                ;(at end (increase (Time-mission ?uav)  (* (Distance2D ?l_start ?l_end) (Time-unit-distance ?uav))))
                ;(at end (increase (total-fly-time)  (* (Distance2D ?l_start ?l_end) (Time-unit-distance ?uav))))
        )
)

(:durative-action monitor_before
:parameters (?uav ?uav2 - Uav ?t ?t2 - Target)
:duration (= ?duration (Time-min-obs ?t))
:condition (and 
                (at start (on_trg ?uav ?t))
                (at end (on_trg ?uav ?t))

                ;(at start (before ?t2 ?t))
                (at start (before ?t ?t2))

                (at start (observable ?t ?uav))
                ;(at start (covered ?uav2 ?t2))



                (at start (uncovered ?uav ?t))


                (at start (osservabile ?t))
                (over all (osservabile ?t))
                (at end (osservabile ?t))
             )
:effect (and 
                (at start (covered ?uav ?t))
                (at start (not (uncovered ?uav ?t)))
                (at start (observable ?t2 ?uav2))


                (at end (increase (NumberTrgCovered ?uav) 1))

                ;(at end (increase (Time-mission ?uav)  (* (Distance2D ?l_start ?l_end) (Time-unit-distance ?uav))))
                ;(at end (increase (total-fly-time)  (* (Distance2D ?l_start ?l_end) (Time-unit-distance ?uav))))
        )
)


)
