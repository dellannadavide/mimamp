(define (domain SmatF2)
;(:requirements :adl :strips :equality :typing :fluents :disjunctive-preconditions :negative-preconditions)
(:types 
          Uav Airport Target Site2D SensorSuite  - object
          Point - Target
)
(:predicates
        ;; Predicato per sapere quale sia la posizione corrente dell'Uav (coordinate x,y del punto "site" proiettato a terra):
        (current_site ?uav - Uav ?l - Site2D)
        
        ;; Predicati di TAKEOFF dell'Uav da un aeroporto:
        ;; dato che nella modellazione dell'oggetto aeroporto su DB, 
        ;; esso è individuato da un segmento individuato da 2 punti nel piano (Site2D),
        ;; assumiamo che il primo sia quello "di inizio pista" da cui comincia a rullare l'Uav
        ;; e che il secondo rappresenti una approssimazione del punto di fine pista 
        ;; (in cui immaginiamo già l'Uav in volo, quindi con una Altitudine diversa da zero):

        ;; 1.
        (ready_to_takeoff ?uav - Uav ?a - Airport)
        (site_for_begin_takeOff ?a - Airport ?l - Site2D) ;; qui riportiamo proprio il punto 2D di inizio pista
                                                       ;; in cui l'Uav deve trovarsi per decollare

        ;; 2. 
        (site_for_end_takeOff ?a - Airport ?l - Site2D) ;; qui riportiamo proprio il punto 2D che approssima quello di fine pista
                                                        ;; in cui l'Uav si troverà dopo il decollo

        ;; Predicati di LANDING dell'Uav in un aeroporto:
        ;; 1. 
        (site_for_landing ?a - Airport ?l - Site2D) ;; qui riportiamo il punto 2D su cui deve trovarsi l'Uav per atterrare
        ;; 2.
        (site_after_landing ?a - Airport ?l - Site2D) ;; qui riportiamo il punto 2D su cui deve trovarsi 
                                                      ;; l'Uav dopo essere atterrato sulla pista
        ;; 3.
        (landed ?uav - Uav  ?a - Airport) ;; qui diamo conferma dell'atterraggio avvenuto

        ;; Predicato di FLY dell'Uav in volo:
        (on_fly ?uav - Uav) ;; qui diciamo se o meno l'Uav è in volo

        ;; Predicati per la MONITORING TARGET da parte di un Uav con un certo sensore:
        
        ;; 1. Dico che il Target ?t deve essere coperto dall'Uav ?uav con una sensor suite ?ss
        (to_cover ?t - Target ?uav - Uav ?ss - SensorSuite)
 
        ;; 2. Dico se l'Uav ?uav ha coperto il target ?t:
        (covered ?uav - Uav ?t  - Target)

        ;; 3. Dico se l'Uav ?uav non ha ancora coperto il target ?t:
        (uncovered ?uav - Uav ?t - Target)

        ;; 4. Dico quale deve essere la posizione (punto di proiezione 2d al suolo) per l'inizio dell'osservazione di un target:
        (site_target ?l_target - Site2D ?t - Target)
        
        ;; ...se il target è puntuale ?l_end_target corrisponde a ?l_start_target.

        ;; Predicati relativi alle questioni "multiobs" tra targets:

        ;; 1. Così dico se il target ?t1 è un target fittizio che sarà "composto" da un target reale (?t2) 
        ;; (mi serve nelle situazioni di overlap
        ;; in cui creo un target fittizio ?t1 le cui coordinate sono state ottenute dalla media dei target in overlap tra loro, tipicamente
        ;; due target, quindi che spazialmente sta in mezzo ai due target reali, 
        ;; che vado a coprire, ottenendo così una leg che permette all'Uav di eseguire le osservazioni di tali target)
        (target_composed_by ?t1 - Target ?t2 - Target)

        ;; Predicato che stabilisce quale Uav partirà per primo nella missione
        ;; E' importante al fine di sincronizzare gli orologi (i tempi di missione) degli altri Uav
        ;; con il suo orologio
        (IsFirstUav ?uav - Uav)
        (NotFirstUav ?uav - Uav)

        ;; Il seguente predicato indica che i due target che devono essere monitorati contemporaneamente dallo stesso uav,
        ;; e che compongono il target fittizio, devono essere distinti:
        (not_same ?t1 - Target ?t2 - Target)

        ;target t1 deve essere monitorato prima del target t2
        (before ?t1 - Target ?t2 - Target ?uav1 - Uav ?uav2 - Uav)
        (overlap ?t1 - Target ?t2 - Target ?uav1 - Uav ?uav2 - Uav)        
        ;(covering_order ?t1 - Target ?t2 - Target)
        (already-transfered ?uav - Uav)

        (osservabile ?t1 - Target ?uav - Uav)

        (choosen_start_site_target ?uav - Uav ?l_start_target - Site2D ?t - Target)

        (first_covered ?uav - Uav)

)
(:functions 
            ;; Mi serve il seguente per sincronizzare la partenza del beginner uav con tutti gli altri:
            (startedMission)
            ;; Mi servono i seguenti fluenti per consentire a ciascuno uav di cominciare le azioni relative al landing
            ;; solo dopo aver monitorato i target che gli erano stati assegnati:
            (NumberTrgAssigned ?uav - Uav)
            (NumberTrgCovered ?uav - Uav)

            ;; I seguenti fluenti sono pensati per controllare il massimo numero di waiting flight consentiti per ciascun uav 
            ;; (attualmente sono delle costanti:
            (MaxWaitingFlight ?uav - Uav)
            (NumberWaitingFlighted ?uav - Uav)

            ;; Fluenti che calcolano distanze tra punti (misurate in METRI):
            (Distance2D ?l1 ?l2 - Site2D) ;; Funzione che fornisce la distanza euclidea di due punti sul piano
            (Distance-waiting-flight ?uav - Uav) ;; Fluente che fornisce la distanza che un Uav può percorrere al fine 
                                                 ;; di far trascorrere un lasso di tempo strategico per sincronizzare con altri uav 
                                                 ;; eventuali monitoraggi   
            (Global-Init-Time-Mission)         
            ;; Funzioni che calcolano il tempo (misurato in MILLISECONDI):
            (Time-mission ?uav - Uav) ;; Fluente che restituisce il tempo di missione di ciascun Uav
            (Delta-Time ?uav - Uav) ;; Fluente rappresentante il tempo che è passato da quando è partito il beginner
                                    ;; rispetto al momento in cui parte tale uav.
                                    ;; Mi serve per ogni uav memorizzare tale informazione, perché questo lasso di tempo 
                                    ;; deve essere sottratto al tempo di missione dell'uav quando definiamo la funzione di minimizzazione.
            (Time-mission-start-obs-target ?t - Target ?uav - Uav) ;; Fluente che restituisce il momento 
                                                      ;; della missione in cui è iniziato il monitoring del target ?t
            (Time-mission-end-obs-target ?t - Target ?uav - Uav) ;; Fluente che restituisce il momento 
                                                      ;; della missione in cui è terminato il monitoring del target ?t
            (Time-landing ?uav - Uav ?a - Airport) ;; Funzione che restituisce il tempo 
                                                  ;; che impiega l'Uav ?uav per atterrare sull'aeroporto ?a
            (Time-take-off ?uav - Uav ?a - Airport) ;; Funzione che restituisce il tempo 
                                                   ;; che impiega l'Uav ?uav per decollare dall'aeroporto ?a
            (Time-min-obs ?t - Target) ;; Funzione che restituisce il tempo minimo 
                                       ;; di osservazione richiesto per il monitoraggio di un target ?t
            (Earliest-time-start-obs-target ?t - Target) ;; Funzione che restituisce il numero di millisecondi minimo 
                                       ;; che deve passare dall'inizio della missione per il monitoraggio del target ?t.
                                       ;; E' calcolato basandosi sull'earliest time associato al targetobs e sul tempo
                                       ;; di inizio prefissato della missione.
            (Latest-time-end-obs-target ?t - Target) ;; ;; Funzione che restituisce il numero di millisecondi massimo 
                                       ;; che deve passare dall'inizio della missione per il monitoraggio del target ?t.
                                       ;; E' calcolato basandosi sul latest time associato al targetobs e sul tempo
                                       ;; di inizio prefissato della missione.
            (Time-overlap-targets ?uav - Uav ?t1 ?t2 - Target) ;; Funzione che restituisce il tempo necessario al monitoraggio dei due target
                                                               ;; in simultanea
            ;; (Time-end-takeoff-begins-uav ?uav - Uav)
            ;; Funzioni che restituiscono parametri specifici dell'Uav considerato:
            (Cruise-speed ?uav - Uav) ;; Funzione che restituisce la velocità media di crociera (metri/millisecondi) di un ?uav
            (Time-unit-distance ?uav - Uav) ;; Funzione che mi restituisce il reciproco della Cruise-speed, 
                                            ;; ovvero quanti millisecondi sono necessari per percorrere un metro
            (Rate-Consumption ?uav - Uav) ;; Funzione che restituisce il consumo medio di carburante (Litri/metro) di un ben preciso ?uav
            (Fuel-Level ?Uav - Uav) ;; Funzione che restituisce il consumo di carburante ?uav durante la missione

            (observable ?t1 - Target ?uav - Uav)

            (total-fly-time)

            (min_delta_time)
            (max_time_mission)

            (time-wait-on-ground)
            (time-loiter)

            (time_between_take_off)
            (time-last-take-off ?a - Airport)
)

(:action wait_on_ground
  :parameters(?uav - Uav ?a - Airport)
  :precondition (and  (ready_to_takeoff ?uav ?a)
    )
  :effect (and (increase (Delta-Time ?uav) (time-wait-on-ground))
                (increase (Time-mission ?uav) (time-wait-on-ground))
    )
)

(:action take_off_prev
:parameters (?uav - Uav ?l_start ?l_end - Site2D ?a - Airport) ;; per semplicità assumo che l_start sia la proiezione al suolo 
                                                              ;; del punto di inizio dell'aeroporto 
                                                              ;; (del segmento che rappresenta l'aeroporto), 
                                                              ;; mentre l_end avrà coordinate (x+DeltaX, y+DeltaY), 
                                                              ;; dove (x,y) è la proiezione al suolo del punto di fine dell'aeroporto
                                                              ;; e deltaX e deltaY sono delle variazioni di distanza che 
                                                              ;; determinano lo spostamento più in là, rispetto all'aeroporto.
                                                              ;; Come dire che una volta decollato, l'Uav dovrà raggiungere 
                                                              ;; una posizione in quota, la cui proiezione a terra è un punto
                                                              ;; "più avanti" di un qualche delta, rispetto alla fine dell'aeroporto. 
:precondition (and 
                    (ready_to_takeoff ?uav ?a)
                    (site_for_begin_takeOff ?a ?l_start)
                    (site_for_end_takeOff ?a ?l_end)
                    (current_site ?uav ?l_start)
                    (>= (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))
                    (< (Delta-Time ?uav) (min_delta_time))

                    (>= (Time-mission ?uav) (+ (time-last-take-off ?a) (time_between_take_off)))

              )
:effect (and 
             (not (ready_to_takeoff ?uav ?a))
             (current_site ?uav ?l_end)
             (not (current_site ?uav ?l_start))
             (on_fly ?uav)

             (assign (time-last-take-off ?a) (Time-mission ?uav))

             (increase (Time-mission ?uav) (Time-take-off ?uav ?a))
             (increase (total-fly-time) (Time-take-off ?uav ?a))
             (decrease (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))
             (assign (min_delta_time) (Delta-Time ?uav))
        )
)
(:action take_off
:parameters (?uav - Uav ?l_start ?l_end - Site2D ?a - Airport) ;; per semplicità assumo che l_start sia la proiezione al suolo 
                                                              ;; del punto di inizio dell'aeroporto 
                                                              ;; (del segmento che rappresenta l'aeroporto), 
                                                              ;; mentre l_end avrà coordinate (x+DeltaX, y+DeltaY), 
                                                              ;; dove (x,y) è la proiezione al suolo del punto di fine dell'aeroporto
                                                              ;; e deltaX e deltaY sono delle variazioni di distanza che 
                                                              ;; determinano lo spostamento più in là, rispetto all'aeroporto.
                                                              ;; Come dire che una volta decollato, l'Uav dovrà raggiungere 
                                                              ;; una posizione in quota, la cui proiezione a terra è un punto
                                                              ;; "più avanti" di un qualche delta, rispetto alla fine dell'aeroporto. 
:precondition (and 
                    (ready_to_takeoff ?uav ?a)
                    (site_for_begin_takeOff ?a ?l_start)
                    (site_for_end_takeOff ?a ?l_end)
                    (current_site ?uav ?l_start)
                    (>= (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))
                    (>= (Delta-Time ?uav) (min_delta_time))

                    (>= (Time-mission ?uav) (+ (time-last-take-off ?a) (time_between_take_off)))
              )
:effect (and 
             (not (ready_to_takeoff ?uav ?a))
             (current_site ?uav ?l_end)
             (not (current_site ?uav ?l_start))
             (on_fly ?uav)

             (assign (time-last-take-off ?a) (Time-mission ?uav))

             (increase (Time-mission ?uav) (Time-take-off ?uav ?a))
             (increase (total-fly-time) (Time-take-off ?uav ?a))
             (decrease (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))

        )
)


(:action landing_succ
:parameters (?uav - Uav ?l_start ?l_end - Site2D ?a - Airport) 
                                                               ;; per semplicità assumo che l_start e l_end siano le proeizioni al suolo
                                                               ;; rispettivamente di un punto prossimo ad uno dei punti estremi
                                                               ;; del segmento che rappresenta l'aeroporto, mentre l'altro coindiderà
                                                               ;; proprio con l'altro estremo.
:precondition (and (on_fly ?uav)
                   (current_site ?uav ?l_start)
                   (site_for_landing ?a ?l_start)
                   (site_after_landing ?a ?l_end)
                   ;(= (NumberTrgCovered ?uav) (NumberTrgAssigned ?uav))  
                   (>= (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))
                   (> (+ (Time-mission ?uav) (Time-landing ?uav ?a)) (max_time_mission))
              )
:effect (and  (landed ?uav ?a)
              (not (on_fly ?uav))
              (not (current_site ?uav ?l_start))
              (current_site ?uav ?l_end)
              (increase (Time-mission ?uav) (Time-landing ?uav ?a))
              (increase (total-fly-time) (Time-landing ?uav ?a))
              (decrease (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))
              (assign (max_time_mission) (+ (Time-mission ?uav) (Time-landing ?uav ?a)))
        )
)

(:action landing
:parameters (?uav - Uav ?l_start ?l_end - Site2D ?a - Airport) 
                                                               ;; per semplicità assumo che l_start e l_end siano le proeizioni al suolo
                                                               ;; rispettivamente di un punto prossimo ad uno dei punti estremi
                                                               ;; del segmento che rappresenta l'aeroporto, mentre l'altro coindiderà
                                                               ;; proprio con l'altro estremo.
:precondition (and (on_fly ?uav)
                   (current_site ?uav ?l_start)
                   (site_for_landing ?a ?l_start)
                   (site_after_landing ?a ?l_end)
                   ;(= (NumberTrgCovered ?uav) (NumberTrgAssigned ?uav))  
                   (>= (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))
                   (<= (+ (Time-mission ?uav) (Time-landing ?uav ?a)) (max_time_mission))
              )
:effect (and  (landed ?uav ?a)
              (not (on_fly ?uav))
              (not (current_site ?uav ?l_start))
              (current_site ?uav ?l_end)
              (increase (Time-mission ?uav) (Time-landing ?uav ?a))
              (increase (total-fly-time) (Time-landing ?uav ?a))
              (decrease (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))
        )
)

(:action transfer_to_trg
:parameters  (?uav - Uav ?l_start ?l_end - Site2D ?t - Target) 
                                                               ;; per semplicità  assumo che l_end sia il punto di proiezione al suolo
                                                               ;; che rappresenta l'inizio del target 
                                                               ;; (poi se il target nello specifico è puntuale, 
                                                               ;; allora l'unico punto rappresentante il target), 
                                                               ;; a cui, partendo da l_start, l'Uav deve giungere.
                                                               ;; Infatti, in tal caso abbiamo un trasferimento 
                                                               ;; che descrive in proiezione 2d una leg 
                                                               ;; (segmento con l_start diverso da l_end). 
:precondition  (and (on_fly ?uav)
                    (current_site ?uav ?l_start)
                    (site_target ?l_end ?t)
                    (uncovered ?uav ?t)
                    (>= (Distance2D ?l_start ?l_end) 0)
                    (>= (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))
                    (not (already-transfered ?uav))

               )
:effect (and (current_site ?uav ?l_end)
              (not (current_site ?uav ?l_start))

              (choosen_start_site_target ?uav ?l_end ?t)

              (increase (Time-mission ?uav)  (* (Distance2D ?l_start ?l_end) (Time-unit-distance ?uav)))
              (increase (total-fly-time)  (* (Distance2D ?l_start ?l_end) (Time-unit-distance ?uav)))
              (decrease (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))
              (already-transfered ?uav)
        )
)

(:action transfer_to_land
:parameters (?uav - Uav ?l_start ?l_end - Site2D ?a - Airport) 
                                                               ;; per semplicità assumo che l_end si riferisca alla proiezione al suolo 
                                                               ;; di un punto prossimo ad uno dei punti che delineano 
                                                               ;; il segmento rappresentante l'aeroporto.
:precondition  (and (on_fly ?uav)
                    (current_site ?uav ?l_start)
                    (site_for_landing ?a ?l_end)
                    (> (Distance2D ?l_start ?l_end) 0)
                    (= (NumberTrgCovered ?uav) (NumberTrgAssigned ?uav))  
                    (>= (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))
               )               
:effect  (and (current_site ?uav ?l_end)
              (not (current_site ?uav ?l_start))
              (increase (Time-mission ?uav)  (* (Distance2D ?l_start ?l_end) (Time-unit-distance ?uav)))
              (increase (total-fly-time)  (* (Distance2D ?l_start ?l_end) (Time-unit-distance ?uav)))
              (decrease (Fuel-Level ?uav) (* (Distance2D ?l_start ?l_end) (Rate-Consumption ?uav)))
         )
)

(:action monitor_point_target
:parameters (?uav - Uav ?uav_l_start - Site2D ?t - Point ?ss - SensorSuite) 
            ;; in tal caso abbiamo un monitoraggio di un target (che può essere puntuale o lineare, quindi anche area...perché assumiamo che
            ;; anche un target di tipo area possa essere rappresentato da un segmento: diagonale del box2d che circoscrive l'area)
            ;; la leg come proiezione 2D della traiettoria aerea dell'uav corrisponde proprio al segmento al suolo che identifica
            ;; il target (puntuale se i due estremi coincidono, oppure lineare o areoforme)
:precondition  (and (on_fly ?uav)
                    (current_site ?uav ?uav_l_start)
                    (to_cover ?t ?uav ?ss)
                    (uncovered ?uav ?t)
                    (osservabile ?t ?uav)
                    (<= (observable ?t ?uav) (Time-mission ?uav))

                    (choosen_start_site_target ?uav ?uav_l_start ?t)

                    (>= (Time-mission ?uav) (Earliest-time-start-obs-target ?t))
                    (<= (+ (Time-mission ?uav) (Time-min-obs ?t)) (Latest-time-end-obs-target ?t))        
                    (>= (Fuel-Level ?uav) (* (* (Time-min-obs ?t) (Cruise-speed ?uav)) (Rate-Consumption ?uav)))
               )                  
:effect  (and (covered ?uav ?t)
              (not (uncovered ?uav ?t))

              (not (choosen_start_site_target ?uav ?uav_l_start ?t))

              (assign (Time-mission-start-obs-target ?t ?uav) (Time-mission ?uav))
              (assign (Time-mission-end-obs-target ?t ?uav) (+ (Time-mission ?uav) (Time-min-obs ?t)))
              (increase (Time-mission ?uav)  (Time-min-obs ?t))
              (increase (total-fly-time)  (Time-min-obs ?t))
              (increase (NumberTrgCovered ?uav) 1)
              (decrease (Fuel-Level ?uav) (* (* (Time-min-obs ?t) (Cruise-speed ?uav)) (Rate-Consumption ?uav)))
              (not (already-transfered ?uav))
              ;(first_covered ?uav)
         )
)

(:action monitor_target
:parameters (?uav - Uav ?uav_l_start ?uav_l_end - Site2D ?t - Target ?ss - SensorSuite) 
                                                                                        ;; in tal caso abbiamo un monitoraggio di un target (che può essere puntuale o lineare, quindi anche area...perché assumiamo che
                                                                                        ;; anche un target di tipo area possa essere rappresentato da un segmento: diagonale del box2d che circoscrive l'area)
                                                                                        ;; la leg come proiezione 2D della traiettoria aerea dell'uav corrisponde proprio al segmento al suolo che identifica
                                                                                        ;; il target (puntuale se i due estremi coincidono, oppure lineare o areoforme)
:precondition  (and (on_fly ?uav)
                    (current_site ?uav ?uav_l_start)
                    (to_cover ?t ?uav ?ss)
                    (uncovered ?uav ?t)
                    (osservabile ?t ?uav)
                    (<= (observable ?t ?uav) (Time-mission ?uav))

                    (choosen_start_site_target ?uav ?uav_l_start ?t)
                    (site_target ?uav_l_end ?t)
                    (not (= ?uav_l_end ?uav_l_start))

                    (>= (Time-mission ?uav) (Earliest-time-start-obs-target ?t))
                    (<= (+ (Time-mission ?uav) (Time-min-obs ?t)) (Latest-time-end-obs-target ?t))        
                    (>= (Fuel-Level ?uav) (* (* (Time-min-obs ?t) (Cruise-speed ?uav)) (Rate-Consumption ?uav)))
               )                  
:effect  (and (covered ?uav ?t)
              (not (uncovered ?uav ?t))

              (not (choosen_start_site_target ?uav ?uav_l_start ?t))

              (assign (Time-mission-start-obs-target ?t ?uav) (Time-mission ?uav))
              (assign (Time-mission-end-obs-target ?t ?uav) (+ (Time-mission ?uav) (Time-min-obs ?t)))
              (not (current_site ?uav ?uav_l_start))
              (current_site ?uav ?uav_l_end)
              (increase (Time-mission ?uav)  (Time-min-obs ?t))
              (increase (total-fly-time)  (Time-min-obs ?t))
              (increase (NumberTrgCovered ?uav) 1)
              (decrease (Fuel-Level ?uav) (* (* (Time-min-obs ?t) (Cruise-speed ?uav)) (Rate-Consumption ?uav)))
              (not (already-transfered ?uav))
              ;(first_covered ?uav)
         )
)

(:action enable_target_before
  :parameters (?t1 ?t2 - Target ?uav1 ?uav2 - Uav) 
  :precondition (and (before ?t1 ?t2 ?uav1 ?uav2) (covered  ?uav1 ?t1))
  :effect (and (assign (observable ?t2 ?uav2) (Time-mission-end-obs-target ?t1 ?uav1))
               (osservabile ?t2 ?uav2))
)
(:action enable_target_overlap
  :parameters (?t1 ?t2 - Target ?uav1 ?uav2 - Uav) 
  :precondition (and (overlap ?t1 ?t2 ?uav1 ?uav2) (covered  ?uav1 ?t1))
  :effect (and (assign (observable ?t2 ?uav2) (Time-mission-start-obs-target ?t1 ?uav1))
               (osservabile ?t2 ?uav2))
)

(:action loiter_no 
                        ;; Si tratta di una azione "fittizia" che consente ad un UAV di "prendere tempo" rispetto ad un altro UAV se
                        ;; la sua azione di monitoring deve avvenire necessariamente dopo quella dell'altro UAV
:parameters  (?uav - Uav ?l_site - Site2D ?t - Target) 
:precondition  (and (on_fly ?uav)
                    (current_site ?uav ?l_site)

                    (choosen_start_site_target ?uav ?l_site ?t)
                    (> (observable ?t ?uav) (Time-mission ?uav))
                    (< (observable ?t ?uav) 999999995904)
                    ;(<= (NumberWaitingFlighted ?uav) (MaxWaitingFlight ?uav))
                    (>= (Fuel-Level ?uav) (* (Distance-waiting-flight ?uav) (Rate-Consumption ?uav)))
                    (= (NumberTrgCovered ?uav) 0)
                    ;(first_covered ?uav)
               )
:effect (and
              
              (increase (Time-mission ?uav)  (time-loiter))
              (increase (total-fly-time)  (time-loiter))
              ;(increase (Time-mission ?uav)  (* (Distance-waiting-flight ?uav) (Time-unit-distance ?uav)))
              ;(assign (Time-mission ?uav) (observable ?t ?uav))
              (decrease (Fuel-Level ?uav) (* (* (time-loiter) (Cruise-speed ?uav)) (Rate-Consumption ?uav)))
              ;(decrease (Fuel-Level ?uav) (* (Distance-waiting-flight ?uav) (Rate-Consumption ?uav)))
              ;(increase (NumberWaitingFlighted ?uav) 1)

        )
)
(:action loiter 
                        ;; Si tratta di una azione "fittizia" che consente ad un UAV di "prendere tempo" rispetto ad un altro UAV se
                        ;; la sua azione di monitoring deve avvenire necessariamente dopo quella dell'altro UAV
:parameters  (?uav - Uav ?l_site - Site2D ?t - Target) 
:precondition  (and (on_fly ?uav)
                    (current_site ?uav ?l_site)

                    (choosen_start_site_target ?uav ?l_site ?t)
                    (> (observable ?t ?uav) (Time-mission ?uav))
                    (< (observable ?t ?uav) 999999995904)
                    ;(<= (NumberWaitingFlighted ?uav) (MaxWaitingFlight ?uav))
                    (>= (Fuel-Level ?uav) (* (Distance-waiting-flight ?uav) (Rate-Consumption ?uav)))
                    (> (NumberTrgCovered ?uav) 0)
                    ;(first_covered ?uav)
               )
:effect (and
              
              (increase (Time-mission ?uav)  (time-loiter))
              (increase (total-fly-time)  (time-loiter))
              ;(increase (Time-mission ?uav)  (* (Distance-waiting-flight ?uav) (Time-unit-distance ?uav)))
              ;(assign (Time-mission ?uav) (observable ?t ?uav))
              (decrease (Fuel-Level ?uav) (* (* (time-loiter) (Cruise-speed ?uav)) (Rate-Consumption ?uav)))
              ;(decrease (Fuel-Level ?uav) (* (Distance-waiting-flight ?uav) (Rate-Consumption ?uav)))
              ;(increase (NumberWaitingFlighted ?uav) 1)

        )
)

)
