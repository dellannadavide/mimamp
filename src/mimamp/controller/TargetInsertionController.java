/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.controller;

import com.lynden.gmapsfx.javascript.JavascriptObject;
import java.sql.SQLException;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import mimamp.model.Coordinates;
import mimamp.model.Database;
import mimamp.model.ObservationRequest;
import mimamp.model.Target;

/**
 *
 * @author marco
 */
public class TargetInsertionController {

    private ObservableList<ObservationRequest> requests;
    private ObservableList<Target> targets;
    private Target tmpTarget;

    public TargetInsertionController() {
        try (Database db = new Database()) {
            targets = FXCollections.observableList(db.getAllTargets());
            requests = FXCollections.observableList(db.getAllRequests(targets));
        } catch (SQLException e) {
            System.out.println("ERROR: couldn't get data from database: " + e.getMessage());
            targets = FXCollections.observableList(new ArrayList<>());
            requests = FXCollections.observableList(new ArrayList<>());
        }
    }

    public void addObservationRequest(ObservationRequest newReq) {
        requests.add(newReq);
    }

    public void deleteObservationRequest(ObservationRequest req) {
        try (Database db = new Database()) {
            db.deleteRequest(req);
            requests.remove(req);
        } catch (SQLException e) {
            System.out.println("Error deleting request: " + e.getMessage());
        }
    }

    public void deleteTarget(Target target) {
        try (Database db = new Database()) {
            db.deleteTarget(target);
            requests.removeIf(req -> req.getTarget().equals(target));
            targets.remove(target);
        } catch (SQLException e) {
            System.out.println("Error deleting target: " + e.getMessage());
        }
    }

    public ObservationRequest getObservationRequest(int index) {
        return requests.get(index);
    }

    public JavascriptObject getObservationRequestShape(int index) {
        return requests.get(index).getTargetShape();
    }

    public ObservableList<ObservationRequest> getObservationRequests() {
        return requests;
    }

    public JavascriptObject getTargetShape(int index) {
        return targets.get(index).getShape();
    }

    public Target getTarget(int index) {
        return targets.get(index);
    }

    public ObservableList<Target> getTargets() {
        return targets;
    }

    public void saveObservationRequest(ObservationRequest request) {
        try (Database db = new Database()) {
            if (request.getId() == -1) {
                db.addRequest(request);
                requests.add(request);
            } else {
                db.editRequest(request);
            }
        } catch (SQLException e) {
            System.out.println("Error saving request: " + e.getMessage());
        }
    }

    public void saveTarget(Target target) {
        if (!targets.contains(target)) {
            targets.add(target);
        }
        if (target.equals(tmpTarget)) {
            tmpTarget = null;
        }
        try (Database db = new Database()) {
            if (target.getId() == -1) {
                db.addTarget(target);
            } else {
                db.renameTarget(target);
            }
        } catch (SQLException e) {
            System.out.println("Error saving target: " + e.getMessage());
        }
    }

    public Target getTmpTarget() {
        tmpTarget.calculateLength();
        return tmpTarget;
    }

    public Target getTmpTarget(Coordinates newPosition) {
        if (tmpTarget == null) {
            tmpTarget = new Target(newPosition);
        } else {
            tmpTarget.addPosition(newPosition);
        }
        return tmpTarget;
    }
}
