package mimamp.controller;

import com.lynden.gmapsfx.javascript.object.LatLong;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import mimamp.model.Airport;
import mimamp.model.CoordBounds;
import mimamp.model.Database;
import mimamp.model.ObservationRequest;
import mimamp.model.UAV;

/**
 * Rivedere l'interazione tra questa roba ed il database...
 */
public class MissionDefiner {

    private LocalDate date;
    private CoordBounds area;
    private List<UAV> uavs;
    private ObservableList<ObservationRequest> requests;
    private final List<ObservationRequest> selectedRequests;

    public MissionDefiner() {
        selectedRequests = new ArrayList<>();
        try (Database db = new Database()) {
            this.uavs = db.getAllUAVs();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public boolean checkCompatibilityOfSelectedRequests() {
        return selectedRequests.stream().allMatch(
                request -> uavs.stream().anyMatch(
                        uav -> uav.canHandle(request)));
    }

    public List<Airport> getAirports() {
        try (Database db = new Database()) {
            return db.getAllAirports();
        } catch (SQLException ex) {
            return new ArrayList<>();
        }
    }

    public String getAvailabilitiesString() {
        StringBuilder s = new StringBuilder("UAVs available on this date:\n");
        uavs.stream().forEach(uav
                -> uav.getAvailabilities().stream()
                .filter(uavAv -> uavAv.containsDate(date))
                .forEach(uavAv -> s.append(uavAv).append("\n")));
        return s.toString();
    }

    public ObservableList<ObservationRequest> getFilteredRequests() {
        if (requests == null) {
            return FXCollections.emptyObservableList();
        }
        return requests;
    }

    public List<ObservationRequest> getSelectedRequests() {
        return selectedRequests;
    }

    public int getSelectedRequestsCount() {
        return selectedRequests.size();
    }

    public boolean isSelected(ObservationRequest req) {
        return selectedRequests.contains(req);
    }

    public void selectObservationRequest(ObservationRequest req) {
        selectedRequests.add(req);
    }

    public void setArea(LatLong northWest, LatLong southEast) {
        this.area = new CoordBounds(northWest, southEast);
        updateRequests();
    }

    public void setDate(LocalDate date) {
        this.date = date;
        if (requests != null) {
            updateRequests();
        }
    }

    public void unselectObservationRequest(ObservationRequest req) {
        selectedRequests.remove(req);
    }

    private void updateRequests() {
        try (Database db = new Database()) {
            selectedRequests.clear();
            requests = FXCollections.observableList(
                    db.getFilteredRequests(date, area));
        } catch (SQLException e) {
            if (requests != null) {
                System.out.println("Error updating requests: " + e.getMessage());
            } else {
                throw new RuntimeException(e.getMessage());
            }
        }
    }
}
