package mimamp.controller.encoding;

import mimamp.model.*;

import java.text.DecimalFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * Created by davide on 21/10/16.
 */
public class NumericEncoder extends Encoder {

    public NumericEncoder(Mission m, String domain, boolean millisec) {
        super(m, domain, millisec);
    }

    @Override
    String getPDDLGoal() {
        Mission m = getMission();
        boolean millis = toMillis();
        boolean isSmatDomain = Domains.isSMATDomain(getDomain());
        boolean isFreeDomain = Domains.isFreeDomain(getDomain());

        StringBuilder s = new StringBuilder();
        s.append("\t(and\n");

        /*covered goals are written only for SMAT domain and FREE TRG domains (without expressing the agent)*/
        if(isSmatDomain) {
            // Covered Requests
            m.getAssignments().keySet().stream()
                    .forEach((ObservationRequest req) -> {
                        UAV uav = m.getAssignments().get(req).getUAV();
                        s.append(String.format(Locale.ENGLISH,
                                "\t  (covered %s %s)\n",
                                uav.pddl(), req.pddl()));
                    });
        }
        if(isFreeDomain) {
            // Covered Requests
            m.getRequests().forEach((ObservationRequest req) -> {
                        s.append(String.format(Locale.ENGLISH,
                                "\t  (covered %s)\n",
                                req.pddl()));
                    });
        }

        // Landed UAVs
        m.getUAVsAvailability().stream()
                .distinct()
                .forEach((UAVAvailability uavAv) -> {
                    UAV uav = uavAv.getUAV();
                    Airport airport = uavAv.getAirport();
                    s.append(String.format(Locale.ENGLISH,
                            "\t  (landed %s %s)\n",
                            uav.pddl(), airport.pddl()));
                });

        // Timebound
        long maxdur = millis ? m.getMaxDuration().toMillis() : m.getMaxDuration().getSeconds();
        s.append(String.format(Locale.ENGLISH,
                "\t  (< (- (max_time_mission) (min_delta_time)) %d)\n", maxdur));

        s.append("\t)\n");
        return s.toString();
    }

    @Override
    String getPDDLInit() {
        Mission m = getMission();
        Set<ObservationRequest> observable = getObservable();
        Set<Target> targetSites = getTargetSites();
        Map<UAV, Integer> numTrgAssigned = getNumTrgAssigned();
        DecimalFormat df = getDf();

        boolean millis = toMillis();
        boolean isSmatDomain = Domains.isSMATDomain(getDomain());
        boolean isFreeDomain = Domains.isFreeDomain(getDomain());

        StringBuilder s = new StringBuilder();

        // Global
        s.append(String.format(Locale.ENGLISH,
                    "\t(= (time-wait-on-ground) %d)\n", getTimeWaitOnGround(millis)))
            .append(String.format(Locale.ENGLISH,
                    "\t(= (time-loiter) %d)\n", getTimeLoiter(millis)))
            .append(String.format(Locale.ENGLISH,
                    "\t(= (time_between_take_off) %d)\n", getTimeBetweenTakeOff(millis)))
            .append("\t(= (total-fly-time) 0)\n")
            .append(String.format(Locale.ENGLISH,
                    "\t(= (min_delta_time) %s)\n", PDDL_MAX_INTEGER))
            .append("\t(= (max_time_mission) 0)\n")
            .append("\n");

        if(isFreeDomain)
            s.append("\t(= (NumberTrgCovered) 0)\n")
                .append("\t(= (MinNumTrgt) 0)\n")
                    .append("\n");

        // Requests
        m.getObservationRequests().stream()
                .forEach((ObservationRequest req) -> {
                    String pddlReq = req.pddl();
                    s.append(String.format(Locale.ENGLISH,
                            "\t(site_target %s %s)\n",
                            req.pddlSite(), req.pddl()))
                            .append(String.format(Locale.ENGLISH,
                                    "\t(= (Time-min-obs %s) %d)\n",
                                    pddlReq, req.getMinDuration(millis)))
                            .append(String.format(Locale.ENGLISH,
                                    "\t(= (Earliest-time-start-obs-target %s) %d)\n",
                                    pddlReq, millis ? Duration.between(m.getEarliestTime(), req.getEarliestTime()).toMillis(): Duration.between(m.getEarliestTime(), req.getEarliestTime()).getSeconds()))
                            .append(String.format(Locale.ENGLISH,
                                    "\t(= (Latest-time-end-obs-target %s) %d)\n",
                                    pddlReq, millis ? Duration.between(m.getEarliestTime(), req.getLatestTime()).toMillis() : Duration.between(m.getEarliestTime(), req.getLatestTime()).getSeconds()));
                    if(!isSmatDomain) {
                        s.append(String.format(Locale.ENGLISH,
                                "\t(unvisited_t %s)\n",
                                pddlReq));
                    }
                    if(!isFreeDomain) {
                        UAVAvailability uavAv = m.getAssignments().get(req);
                        String pddlUav = uavAv.getUAV().pddl();
                        s.append(String.format(Locale.ENGLISH,
                                "\t(to_cover %s %s %s)\n",
                                pddlReq, pddlUav, uavAv.pddlSensorSuite()))
                                .append(String.format(Locale.ENGLISH,
                                        "\t(uncovered %s %s)\n",
                                        pddlUav, pddlReq))
                                .append(String.format(Locale.ENGLISH,
                                        "\t(= (Time-mission-start-obs-target %s %s) 0)\n",
                                        pddlReq, pddlUav))
                                .append(String.format(Locale.ENGLISH,
                                        "\t(= (Time-mission-end-obs-target %s %s) 0)\n",
                                        pddlReq, pddlUav));
                    }
                    else {
                        s.append(String.format(Locale.ENGLISH,
                                    "\t(uncovered %s)\n",
                                    pddlReq))
                            .append(String.format(Locale.ENGLISH,
                                    "\t(= (Time-mission-start-obs-target %s) 0)\n",
                                    pddlReq))
                            .append(String.format(Locale.ENGLISH,
                                    "\t(= (Time-mission-end-obs-target %s) 0)\n",
                                    pddlReq));
                    }
                    if (!req.targetIsPoint()) {
                        s.append(String.format(Locale.ENGLISH,
                                "\t(site_target %s %s)\n",
                                req.pddlLastSite(), req.pddl()));
                        if(!isSmatDomain) {
                            s.append(String.format(Locale.ENGLISH,
                                    "\t(not_same %s %s)\n",
                                    req.pddlSite(), req.pddlLastSite()));
                        }
                    }
                });

        s.append("\n");

        // Observable requests
        observable.stream().forEach((ObservationRequest obsReq) -> {
            if(!isFreeDomain) {
                UAV uav = m.getAssignments().get(obsReq).getUAV();
                s.append(String.format(Locale.ENGLISH,
                        "\t(osservabile %s %s)\n", obsReq.pddl(), uav.pddl()))
                        .append(String.format(Locale.ENGLISH,
                                "\t(= (observable %s %s) %d)\n",
                                obsReq.pddl(), uav.pddl(),
                                millis ? Duration.between(m.getEarliestTime(), obsReq.getEarliestTime()).toMillis() : Duration.between(m.getEarliestTime(), obsReq.getEarliestTime()).getSeconds()));
            }
            else {
                s.append(String.format(Locale.ENGLISH,
                        "\t(osservabile %s)\n", obsReq.pddl()))
                        .append(String.format(Locale.ENGLISH,
                                "\t(= (observable %s) %d)\n",
                                obsReq.pddl(),
                                millis ? Duration.between(m.getEarliestTime(), obsReq.getEarliestTime()).toMillis() : Duration.between(m.getEarliestTime(), obsReq.getEarliestTime()).getSeconds()));

            }

        });

        s.append("\n");

        // Airports
        m.getUAVsAvailability().stream()
                .map(uavAv -> uavAv.getAirport())
                .distinct()
                .forEach((Airport airport) -> {
                            String pddl = airport.pddl();
                            String pddlBeginLanding = airport.pddlBeginLanding();
                            String pddlEndTakeoff = airport.pddlEndTakeoff();
                            String pddlCenter = airport.pddlCenter();

                            s.append(String.format(Locale.ENGLISH,
                                    "\t(site_for_begin_takeOff %s %s)\n",
                                    pddl, airport.pddlCenter()))
                                    .append(String.format(Locale.ENGLISH,
                                            "\t(site_for_end_takeOff %s %s)\n",
                                            pddl, airport.pddlEndTakeoff()))
                                    .append(String.format(Locale.ENGLISH,
                                            "\t(site_for_landing %s %s)\n",
                                            pddl, airport.pddlBeginLanding()))
                                    .append(String.format(Locale.ENGLISH,
                                            "\t(site_after_landing %s %s)\n",
                                            pddl, airport.pddlCenter()))
                                    .append(String.format(Locale.ENGLISH,
                                            "\t(= (time-last-take-off %s) 0)\n",
                                            pddl));

                            // Distances between airports
                            m.getUAVsAvailability().stream()
                                    .map(uavAv -> uavAv.getAirport())
                                    .distinct()
                                    .forEach((Airport airport2) -> {
                                        s.append(String.format(Locale.ENGLISH,
                                                "\t(= (Distance2D %s %s) %d)\n",
                                                pddlCenter, airport2.pddlCenter(),
                                                airport.getDistanceFromCenter(airport2.getPosition())))
                                                .append(String.format(Locale.ENGLISH,
                                                        "\t(= (Distance2D %s %s) %d)\n",
                                                        pddlCenter, airport2.pddlBeginLanding(),
                                                        airport.getDistanceFromCenter(airport2.getBeginOfLanding())))
                                                .append(String.format(Locale.ENGLISH,
                                                        "\t(= (Distance2D %s %s) %d)\n",
                                                        pddlCenter, airport2.pddlEndTakeoff(),
                                                        airport.getDistanceFromCenter(airport2.getEndOfTakeoff())))
                                                .append(String.format(Locale.ENGLISH,
                                                        "\t(= (Distance2D %s %s) %d)\n",
                                                        pddlBeginLanding, airport2.pddlCenter(),
                                                        airport.getDistanceFromBeginOfLanding(airport2.getPosition())))
                                                .append(String.format(Locale.ENGLISH,
                                                        "\t(= (Distance2D %s %s) %d)\n",
                                                        pddlBeginLanding, airport2.pddlBeginLanding(),
                                                        airport.getDistanceFromBeginOfLanding(airport2.getBeginOfLanding())))
                                                .append(String.format(Locale.ENGLISH,
                                                        "\t(= (Distance2D %s %s) %d)\n",
                                                        pddlBeginLanding, airport2.pddlEndTakeoff(),
                                                        airport.getDistanceFromBeginOfLanding(airport2.getEndOfTakeoff())))
                                                .append(String.format(Locale.ENGLISH,
                                                        "\t(= (Distance2D %s %s) %d)\n",
                                                        pddlEndTakeoff, airport2.pddlCenter(),
                                                        airport.getDistanceFromEndOfTakeoff(airport2.getPosition())))
                                                .append(String.format(Locale.ENGLISH,
                                                        "\t(= (Distance2D %s %s) %d)\n",
                                                        pddlEndTakeoff, airport2.pddlBeginLanding(),
                                                        airport.getDistanceFromEndOfTakeoff(airport2.getBeginOfLanding())))
                                                .append(String.format(Locale.ENGLISH,
                                                        "\t(= (Distance2D %s %s) %d)\n",
                                                        pddlEndTakeoff, airport2.pddlEndTakeoff(),
                                                        airport.getDistanceFromEndOfTakeoff(airport2.getEndOfTakeoff())));
                                        if(!isSmatDomain) {
                                            m.getUAVsAvailability().stream()
                                                    .distinct()
                                                    .forEach((UAVAvailability uavAv) -> {
                                                        UAV uav = uavAv.getUAV();
                                                        double uav_speed = uav.getCruisingSpeed(millis);// in m/ms se millis, in m/s altrimenti
                                                        String pddlUav = uav.pddl();
                                                        s.append(String.format(Locale.ENGLISH,
                                                                "\t(= (time_required %s %s %s) %.1f)\n",
                                                                pddlCenter, airport2.pddlCenter(), pddlUav,
                                                                airport.getDistanceFromCenter(airport2.getPosition()) / uav_speed))
                                                                .append(String.format(Locale.ENGLISH,
                                                                        "\t(= (time_required %s %s %s) %.1f)\n",
                                                                        pddlCenter, airport2.pddlBeginLanding(), pddlUav,
                                                                        airport.getDistanceFromCenter(airport2.getBeginOfLanding()) / uav_speed))
                                                                .append(String.format(Locale.ENGLISH,
                                                                        "\t(= (time_required %s %s %s) %.1f)\n",
                                                                        pddlCenter, airport2.pddlEndTakeoff(), pddlUav,
                                                                        airport.getDistanceFromCenter(airport2.getEndOfTakeoff()) / uav_speed))
                                                                .append(String.format(Locale.ENGLISH,
                                                                        "\t(= (time_required %s %s %s) %.1f)\n",
                                                                        pddlBeginLanding, airport2.pddlCenter(), pddlUav,
                                                                        airport.getDistanceFromBeginOfLanding(airport2.getPosition()) / uav_speed))
                                                                .append(String.format(Locale.ENGLISH,
                                                                        "\t(= (time_required %s %s %s) %.1f)\n",
                                                                        pddlBeginLanding, airport2.pddlBeginLanding(), pddlUav,
                                                                        airport.getDistanceFromBeginOfLanding(airport2.getBeginOfLanding()) / uav_speed))
                                                                .append(String.format(Locale.ENGLISH,
                                                                        "\t(= (time_required %s %s %s) %.1f)\n",
                                                                        pddlBeginLanding, airport2.pddlEndTakeoff(), pddlUav,
                                                                        airport.getDistanceFromBeginOfLanding(airport2.getEndOfTakeoff()) / uav_speed))
                                                                .append(String.format(Locale.ENGLISH,
                                                                        "\t(= (time_required %s %s %s) %.1f)\n",
                                                                        pddlEndTakeoff, airport2.pddlCenter(), pddlUav,
                                                                        airport.getDistanceFromEndOfTakeoff(airport2.getPosition()) / uav_speed))
                                                                .append(String.format(Locale.ENGLISH,
                                                                        "\t(= (time_required %s %s %s) %.1f)\n",
                                                                        pddlEndTakeoff, airport2.pddlBeginLanding(), pddlUav,
                                                                        airport.getDistanceFromEndOfTakeoff(airport2.getBeginOfLanding()) / uav_speed))
                                                                .append(String.format(Locale.ENGLISH,
                                                                        "\t(= (time_required %s %s %s) %.1f)\n",
                                                                        pddlEndTakeoff, airport2.pddlEndTakeoff(), pddlUav,
                                                                        airport.getDistanceFromEndOfTakeoff(airport2.getEndOfTakeoff()) / uav_speed));
                                                    });
                                        }
                                    });
                            targetSites.stream().forEach(trg -> {
                                String targetPddl = trg.pddl();
                                Coordinates trgPosition = trg.getFirstPosition();
                                s.append(String.format(Locale.ENGLISH,
                                        "\t(= (Distance2D %s %s) %d)\n",
                                        pddlBeginLanding, targetPddl,
                                        airport.getDistanceFromBeginOfLanding(trgPosition)))
                                        .append(String.format(Locale.ENGLISH,
                                                "\t(= (Distance2D %s %s) %d)\n",
                                                targetPddl, pddlBeginLanding,
                                                airport.getDistanceFromBeginOfLanding(trgPosition)))
                                        .append(String.format(Locale.ENGLISH,
                                                "\t(= (Distance2D %s %s) %d)\n",
                                                pddlEndTakeoff, targetPddl,
                                                airport.getDistanceFromEndOfTakeoff(trgPosition)))
                                        .append(String.format(Locale.ENGLISH,
                                                "\t(= (Distance2D %s %s) %d)\n",
                                                targetPddl, pddlEndTakeoff,
                                                airport.getDistanceFromEndOfTakeoff(trgPosition)))
                                        .append(String.format(Locale.ENGLISH,
                                                "\t(= (Distance2D %s %s) %d)\n",
                                                pddlCenter, targetPddl,
                                                airport.getDistanceFromCenter(trgPosition)))
                                        .append(String.format(Locale.ENGLISH,
                                                "\t(= (Distance2D %s %s) %d)\n",
                                                targetPddl, pddlCenter,
                                                airport.getDistanceFromCenter(trgPosition)));
                                if(!isSmatDomain) {
                                    m.getUAVsAvailability().stream()
                                            .distinct()
                                            .forEach((UAVAvailability uavAv) -> {
                                                String targetPddl_inn = trg.pddl();
                                                Coordinates trgPosition_inn = trg.getFirstPosition();
                                                UAV uav = uavAv.getUAV();
                                                double uav_speed = uav.getCruisingSpeed(millis);// in m/ms se millis, in m/s altrimenti
                                                String pddlUav = uav.pddl();
                                                s.append(String.format(Locale.ENGLISH,
                                                        "\t(= (time_required %s %s %s) %.1f)\n",
                                                        pddlBeginLanding, targetPddl_inn, pddlUav,
                                                        airport.getDistanceFromBeginOfLanding(trgPosition_inn) / uav_speed))
                                                        .append(String.format(Locale.ENGLISH,
                                                                "\t(= (time_required %s %s %s) %.1f)\n",
                                                                targetPddl_inn, pddlBeginLanding, pddlUav,
                                                                airport.getDistanceFromBeginOfLanding(trgPosition_inn) / uav_speed))
                                                        .append(String.format(Locale.ENGLISH,
                                                                "\t(= (time_required %s %s %s) %.1f)\n",
                                                                pddlEndTakeoff, targetPddl_inn, pddlUav,
                                                                airport.getDistanceFromEndOfTakeoff(trgPosition_inn) / uav_speed))
                                                        .append(String.format(Locale.ENGLISH,
                                                                "\t(= (time_required %s %s %s) %.1f)\n",
                                                                targetPddl_inn, pddlEndTakeoff, pddlUav,
                                                                airport.getDistanceFromEndOfTakeoff(trgPosition_inn) / uav_speed))
                                                        .append(String.format(Locale.ENGLISH,
                                                                "\t(= (time_required %s %s %s) %.1f)\n",
                                                                pddlCenter, targetPddl_inn, pddlUav,
                                                                airport.getDistanceFromCenter(trgPosition_inn) / uav_speed))
                                                        .append(String.format(Locale.ENGLISH,
                                                                "\t(= (time_required %s %s %s) %.1f)\n",
                                                                targetPddl_inn, pddlCenter, pddlUav,
                                                                airport.getDistanceFromCenter(trgPosition_inn) / uav_speed));
                                            });
                                }

                                if (!trg.isPoint()) {
                                    targetPddl = trg.pddlLast();
                                    trgPosition = trg.getLastPosition();
                                    s.append(String.format(Locale.ENGLISH,
                                            "\t(= (Distance2D %s %s) %d)\n",
                                            pddlBeginLanding, targetPddl,
                                            airport.getDistanceFromBeginOfLanding(trgPosition)))
                                            .append(String.format(Locale.ENGLISH,
                                                    "\t(= (Distance2D %s %s) %d)\n",
                                                    targetPddl, pddlBeginLanding,
                                                    airport.getDistanceFromBeginOfLanding(trgPosition)))
                                            .append(String.format(Locale.ENGLISH,
                                                    "\t(= (Distance2D %s %s) %d)\n",
                                                    pddlEndTakeoff, targetPddl,
                                                    airport.getDistanceFromEndOfTakeoff(trgPosition)))
                                            .append(String.format(Locale.ENGLISH,
                                                    "\t(= (Distance2D %s %s) %d)\n",
                                                    targetPddl, pddlEndTakeoff,
                                                    airport.getDistanceFromEndOfTakeoff(trgPosition)))
                                            .append(String.format(Locale.ENGLISH,
                                                    "\t(= (Distance2D %s %s) %d)\n",
                                                    pddlCenter, targetPddl,
                                                    airport.getDistanceFromCenter(trgPosition)))
                                            .append(String.format(Locale.ENGLISH,
                                                    "\t(= (Distance2D %s %s) %d)\n",
                                                    targetPddl, pddlCenter,
                                                    airport.getDistanceFromCenter(trgPosition)));

                                    if(!isSmatDomain) {
                                        m.getUAVsAvailability().stream()
                                                .distinct()
                                                .forEach((UAVAvailability uavAv) -> {
                                                    String targetPddl_inn = trg.pddlLast();
                                                    Coordinates trgPosition_inn = trg.getLastPosition();
                                                    UAV uav = uavAv.getUAV();
                                                    double uav_speed = uav.getCruisingSpeed(millis);// in m/ms se millis, in m/s altrimenti
                                                    String pddlUav = uav.pddl();
                                                    s.append(String.format(Locale.ENGLISH,
                                                            "\t(= (time_required %s %s %s) %.1f)\n",
                                                            pddlBeginLanding, targetPddl_inn, pddlUav,
                                                            airport.getDistanceFromBeginOfLanding(trgPosition_inn) / uav_speed))
                                                            .append(String.format(Locale.ENGLISH,
                                                                    "\t(= (time_required %s %s %s) %.1f)\n",
                                                                    targetPddl_inn, pddlBeginLanding, pddlUav,
                                                                    airport.getDistanceFromBeginOfLanding(trgPosition_inn) / uav_speed))
                                                            .append(String.format(Locale.ENGLISH,
                                                                    "\t(= (time_required %s %s %s) %.1f)\n",
                                                                    pddlEndTakeoff, targetPddl_inn, pddlUav,
                                                                    airport.getDistanceFromEndOfTakeoff(trgPosition_inn) / uav_speed))
                                                            .append(String.format(Locale.ENGLISH,
                                                                    "\t(= (time_required %s %s %s) %.1f)\n",
                                                                    targetPddl_inn, pddlEndTakeoff, pddlUav,
                                                                    airport.getDistanceFromEndOfTakeoff(trgPosition_inn) / uav_speed))
                                                            .append(String.format(Locale.ENGLISH,
                                                                    "\t(= (time_required %s %s %s) %.1f)\n",
                                                                    pddlCenter, targetPddl_inn, pddlUav,
                                                                    airport.getDistanceFromCenter(trgPosition_inn) / uav_speed))
                                                            .append(String.format(Locale.ENGLISH,
                                                                    "\t(= (time_required %s %s %s) %.1f)\n",
                                                                    targetPddl_inn, pddlCenter, pddlUav,
                                                                    airport.getDistanceFromCenter(trgPosition_inn) / uav_speed));
                                                });
                                    }
                                }
                            });
                        }
                );

        s.append("\n");

        // UAVs
        m.getUAVsAvailability().stream()
                .distinct()
                .forEach((UAVAvailability uavAv) -> {
                            UAV uav = uavAv.getUAV();
                            Airport airport = uavAv.getAirport();
                            String pddlUav = uav.pddl();
                            String pddlArpt = airport.pddl();
                            s.append(String.format(Locale.ENGLISH,
                                    "\t(current_site %s %s)\n",
                                    pddlUav, airport.pddlCenter()))
                                    .append(String.format(Locale.ENGLISH,
                                            "\t(ready_to_takeoff %s %s)\n",
                                            pddlUav, pddlArpt))
                                    .append(String.format(Locale.ENGLISH,
                                            "\t(= (Time-take-off %s %s) %d)\n",
                                            pddlUav, pddlArpt, airport.getTakeoffDuration(millis)))
                                    .append(String.format(Locale.ENGLISH,
                                            "\t(= (Time-landing %s %s) %d)\n",
                                            pddlUav, pddlArpt, airport.getLandingDuration(millis)));
                            if(isSmatDomain) {
                                s.append(String.format(Locale.ENGLISH,
                                        "\t(= (Cruise-speed %s) %s)\n",
                                        pddlUav, df.format(uav.getCruisingSpeed(millis))))
                                        .append(String.format(Locale.ENGLISH,
                                                "\t(= (Time-unit-distance %s) %d)\n",
                                                pddlUav, uav.getTimeUnitDistance()))
                                        .append(String.format(Locale.ENGLISH,
                                                "\t(= (Rate-Consumption %s) %s)\n",
                                                pddlUav, df.format(uav.getConsumption())))
                                        .append(String.format(Locale.ENGLISH,
                                                "\t(= (Fuel-Level %s) %s)\n",
                                                pddlUav, df.format(uav.getFuelLiters())))
                                        .append(String.format(Locale.ENGLISH,
                                                "\t(= (Distance-waiting-flight %s) %d)\n",
                                                pddlUav, getDistanceWaitingFlight(millis)));
                            }
                            else {
                                s.append(String.format(Locale.ENGLISH,
                                        "\t(not-already-transfered %s)\n",
                                        pddlUav))
                                .append(String.format(Locale.ENGLISH,
                                        "\t(= (fuel_required_loiter %s) 1)\n",
                                        pddlUav));
                            }
                            if(!isFreeDomain) {
                                s.append(String.format("\t(= (NumberTrgAssigned %s) %d)\n",
                                            pddlUav, numTrgAssigned.get(uav)))
                                            .append(String.format("\t(= (NumberTrgCovered %s) 0)\n",
                                                    pddlUav));
                            }
                            else {
                                s.append(String.format("\t(= (NumberTrgCoveredUav %s) 0)\n",
                                                pddlUav));
                            }

                            LocalDateTime initTime = m.getFirstUsefulTakeoffTime(uavAv);
                            long initTimeDelta = millis ? Duration.between(m.getEarliestTime(), initTime).toMillis() : Duration.between(m.getEarliestTime(), initTime).getSeconds();
                            s.append(String.format(Locale.ENGLISH,
                                    "\t(= (Time-mission %s) %d)\n",
                                    pddlUav, initTimeDelta))
                                    .append(String.format(Locale.ENGLISH,
                                            "\t(= (Delta-Time %s) %d)\n",
                                            pddlUav, initTimeDelta));
                        }
                );

        s.append("\n");

        // Distances between targets
        targetSites.stream().forEach(trg1 -> {
            targetSites.stream().forEach(trg2 -> {
                s.append(String.format(Locale.ENGLISH,
                        "\t(= (Distance2D %s %s) %d)\n",
                        trg1.pddl(), trg2.pddl(),
                        trg1.getFirstPosition().distanceFrom(
                                trg2.getFirstPosition())));
                if(!isSmatDomain) {
                    m.getUAVsAvailability().stream()
                            .distinct()
                            .forEach((UAVAvailability uavAv) -> {
                                UAV uav = uavAv.getUAV();
                                double uav_speed = uav.getCruisingSpeed(millis);// in m/ms se millis, in m/s altrimenti
                                String pddlUav = uav.pddl();
                                s.append(String.format(Locale.ENGLISH,
                                        "\t(= (time_required %s %s %s) %.1f)\n",
                                        trg1.pddl(), trg2.pddl(), pddlUav,
                                        trg1.getFirstPosition().distanceFrom(
                                                trg2.getFirstPosition()) / uav_speed));
                            });
                }
                if (!trg2.isPoint()) {
                    s.append(String.format(Locale.ENGLISH,
                            "\t(= (Distance2D %s %s) %d)\n",
                            trg1.pddl(), trg2.pddlLast(),
                            trg1.getFirstPosition().distanceFrom(
                                    trg2.getLastPosition())));
                    if(!isSmatDomain) {
                        m.getUAVsAvailability().stream()
                                .distinct()
                                .forEach((UAVAvailability uavAv) -> {
                                    UAV uav = uavAv.getUAV();
                                    double uav_speed = uav.getCruisingSpeed(millis);// in m/ms se millis, in m/s altrimenti
                                    String pddlUav = uav.pddl();
                                    s.append(String.format(Locale.ENGLISH,
                                            "\t(= (time_required %s %s %s) %.1f)\n",
                                            trg1.pddl(), trg2.pddlLast(), pddlUav,
                                            trg1.getFirstPosition().distanceFrom(
                                                    trg2.getLastPosition()) / uav_speed));
                                });
                    }
                }
            });
        });
        targetSites.stream()
                .filter(trg -> !trg.isPoint())
                .forEach(trg1 -> {
                    targetSites.stream().forEach(trg2 -> {
                        s.append(String.format(Locale.ENGLISH,
                                "\t(= (Distance2D %s %s) %d)\n",
                                trg1.pddlLast(), trg2.pddl(),
                                trg1.getLastPosition().distanceFrom(
                                        trg2.getFirstPosition())));
                        if(!isSmatDomain) {
                            m.getUAVsAvailability().stream()
                                    .distinct()
                                    .forEach((UAVAvailability uavAv) -> {
                                        UAV uav = uavAv.getUAV();
                                        double uav_speed = uav.getCruisingSpeed(millis);// in m/ms se millis, in m/s altrimenti
                                        String pddlUav = uav.pddl();
                                        s.append(String.format(Locale.ENGLISH,
                                                "\t(= (time_required %s %s %s) %.1f)\n",
                                                trg1.pddlLast(), trg2.pddl(), pddlUav,
                                                trg1.getLastPosition().distanceFrom(
                                                        trg2.getFirstPosition()) / uav_speed));
                                    });
                        }
                        if (!trg2.isPoint()) {
                            s.append(String.format(Locale.ENGLISH,
                                    "\t(= (Distance2D %s %s) %d)\n",
                                    trg1.pddlLast(), trg2.pddlLast(),
                                    trg1.getLastPosition().distanceFrom(
                                            trg2.getLastPosition())));
                            if(!isSmatDomain) {
                                m.getUAVsAvailability().stream()
                                        .distinct()
                                        .forEach((UAVAvailability uavAv) -> {
                                            UAV uav = uavAv.getUAV();
                                            double uav_speed = uav.getCruisingSpeed(millis);// in m/ms se millis, in m/s altrimenti
                                            String pddlUav = uav.pddl();
                                            s.append(String.format(Locale.ENGLISH,
                                                    "\t(= (time_required %s %s %s) %.1f)\n",
                                                    trg1.pddlLast(), trg2.pddlLast(), pddlUav,
                                                    trg1.getLastPosition().distanceFrom(
                                                            trg2.getLastPosition()) / uav_speed));
                                        });
                            }
                        }
                    });
                });

        s.append("\n");

        // MultiObs
        m.getMultiObs()
                .stream().forEach((MultiObs mOb) -> {
                    if(!isFreeDomain) {
                        UAV uav1 = m.getAssignments().get(mOb.getRequest1()).getUAV();
                        UAV uav2 = m.getAssignments().get(mOb.getRequest2()).getUAV();
                        s.append(String.format(Locale.ENGLISH,
                                "\t(%s %s %s %s %s)\n",
                                mOb.getConstraint(), mOb.pddlRequest1(), mOb.pddlRequest2(),
                                uav1.pddl(), uav2.pddl()))
                                .append(String.format(Locale.ENGLISH,
                                        "\t(= (observable %s %s) %s)\n",
                                        mOb.pddlRequest2(), uav2.pddl(), PDDL_MAX_INTEGER));
                    }
                    else {
                        s.append(String.format(Locale.ENGLISH,
                                "\t(%s %s %s)\n",
                                mOb.getConstraint(), mOb.pddlRequest1(), mOb.pddlRequest2()))
                                .append(String.format(Locale.ENGLISH,
                                        "\t(= (observable %s) %s)\n",
                                        mOb.pddlRequest2(), PDDL_MAX_INTEGER));
                    }
                }
        );

        return s.toString();
    }


    @Override
    String getPDDLMetric() {
        return "minimize (total-fly-time)";
    }
}
