package mimamp.controller.encoding;

import mimamp.model.Mission;

/**
 * Created by davide on 21/10/16.
 */
public class EncoderFactory {

    public Encoder getEncoder(String encoderType, Mission mission, String domain, boolean millis) {
        if (encoderType == null) {
            return null;
        }
        if (encoderType.equalsIgnoreCase("NUMERIC")) {
            return new NumericEncoder(mission, domain, millis);

        } else if (encoderType.equalsIgnoreCase("TEMPORAL")) {
            return new TemporalEncoder(mission, domain, millis);
        }

        return null;
    }

}
