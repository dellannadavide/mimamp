package mimamp.controller.encoding;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import mimamp.model.Airport;
import mimamp.model.Coordinates;
import mimamp.model.Mission;
import mimamp.model.MultiObs;
import mimamp.model.ObservationRequest;
import mimamp.model.SensorSuite;
import mimamp.model.Target;
import mimamp.model.UAV;
import mimamp.model.UAVAvailability;

/**
 *
 * @author marco
 */
public abstract class Encoder {
    public static final String PDDL_MAX_INTEGER = "999999995904";
    /**
     * values are in seconds
     */
    private static final int DISTANCE_WAITING_FLIGHT = 100;
    private static final int TIME_BETWEEN_TAKE_OFF = 300;
    private static final int TIME_LOITER = 100;
    private static final int TIME_WAIT_ON_GROUND = 100;


    private final DecimalFormat df;
    private final Mission m;
    private final Map<UAV, Integer> numTrgAssigned;
    private final Set<ObservationRequest> observable;
    private final Set<Target> targetSites;

    private final boolean toMillis;
    private final String domain;

    public Encoder(Mission m, String domain, boolean millis) {
        this.df = new DecimalFormat("0.#");
        df.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.ENGLISH));
        df.setMaximumFractionDigits(4);
        df.setRoundingMode(RoundingMode.CEILING);
        
        this.m = m;
        m.QTClustering();

        this.domain = domain;
        this.toMillis = millis;

        this.numTrgAssigned = m.createNumTrgMap();

        this.targetSites = new HashSet<>();
        m.getObservationRequests().stream()
                .forEach(req -> targetSites.add(req.getTarget()));

        this.observable = new HashSet<>();
        m.getObservationRequests().stream()
                .filter(obsReq -> m.getMultiObs().stream()
                        .noneMatch(mOb -> obsReq.equals(mOb.getRequest2())))
                .forEach(obsReq -> observable.add(obsReq));
    }

    abstract String getPDDLGoal();

    abstract String getPDDLInit();

    private String getPDDLObjects() {
        Mission m = getMission();

        StringBuilder s = new StringBuilder();

        // ObservationRequests (point)
        if (m.getObservationRequests().stream().anyMatch(req -> req.targetIsPoint())) {
            s.append("\t");
            m.getObservationRequests().stream()
                    .filter(req -> req.targetIsPoint())
                    .forEach((ObservationRequest req) -> {
                        s.append(req.pddl()).append(" ");
                    });
            s.append("- Point\n");
        }

        // ObservationRequests (LOC)
        if (m.getObservationRequests().stream().anyMatch(req -> !req.targetIsPoint())) {
            s.append("\t");
            m.getObservationRequests().stream()
                    .filter(req -> !req.targetIsPoint())
                    .forEach((ObservationRequest req) -> {
                        s.append(req.pddl()).append(" ");
                    });
            s.append("- Target\n");
        }
        // UAVs
        s.append("\t");
        m.getUAVsAvailability().stream()
                .map(uavAv -> uavAv.getUAV())
                .distinct()
                .forEach((UAV uav) -> {
                    s.append(uav.pddl()).append(" ");
                });
        s.append("- Uav\n");

        // Airports
        s.append("\t");
        m.getUAVsAvailability().stream()
                .map(uavAv -> uavAv.getAirport())
                .distinct()
                .forEach((Airport airport) -> {
                    s.append(airport.pddl()).append(" ");
                });
        s.append("- Airport\n");

        // Site2D
        s.append("\t");
        m.getUAVsAvailability().stream()
                .map(uavAv -> uavAv.getAirport())
                .distinct()
                .forEach((Airport airport) -> {
                    s.append(airport.pddlCenter()).append(" ")
                            .append(airport.pddlBeginLanding()).append(" ")
                            .append(airport.pddlEndTakeoff()).append(" ");

                });
        m.getObservationRequests().stream()
                .map(req -> req.getTarget())
                .forEach(target -> {
                    s.append(target.pddl()).append(" ");
                    if (!target.isPoint()) {
                        s.append(target.pddlLast()).append(" ");
                    }
                });
        s.append("- Site2D\n");

        // SensorSuites
        s.append("\t");
        m.getUAVsAvailability().stream()
                .map(uavAv -> uavAv.getSensorSuite())
                .distinct()
                .forEach((SensorSuite suite) -> {
                    s.append(suite.pddl()).append(" ");
                });
        s.append("- SensorSuite\n");

        return s.toString();
    }

    abstract String getPDDLMetric();

    public String toPDDL() {
        return "(define (problem MRD)\n"
                + "  (:domain SmatF2)\n"
                + "  (:objects\n" + getPDDLObjects() + "  )\n\n"
                + "  (:init\n" + getPDDLInit() + "  )\n\n"
                + "  (:goal\n" + getPDDLGoal() + "  )\n\n"
                + "  (:metric\n" + getPDDLMetric() + "\n)\n"
                + ")\n";
    }

    public Mission getMission() {
        return m;
    }

    public Set<ObservationRequest> getObservable() {
        return observable;
    }


    public Set<Target> getTargetSites() {
        return targetSites;
    }

    public Map<UAV, Integer> getNumTrgAssigned() {
        return numTrgAssigned;
    }

    public DecimalFormat getDf() {
        return df;
    }

    public boolean toMillis() {
        return toMillis;
    }

    public static int getDistanceWaitingFlight(boolean millisec) {
        return millisec? DISTANCE_WAITING_FLIGHT*1000 : DISTANCE_WAITING_FLIGHT;
    }

    public static int getTimeBetweenTakeOff(boolean millisec) {
        return millisec? TIME_BETWEEN_TAKE_OFF*1000 : TIME_BETWEEN_TAKE_OFF;
    }

    public static int getTimeLoiter(boolean millisec) {
        return millisec ? TIME_LOITER*1000 : TIME_LOITER;
    }

    public static int getTimeWaitOnGround(boolean millisec) {
        return millisec ? TIME_WAIT_ON_GROUND*1000 : TIME_WAIT_ON_GROUND;
    }

    public String getDomain() {
        return domain;
    }
}
