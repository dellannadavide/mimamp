package mimamp.controller.encoding;

import mimamp.controller.encoding.Encoder;
import mimamp.model.*;

import java.text.DecimalFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * Created by davide on 21/10/16.
 */
public class TemporalEncoder extends Encoder {

    public TemporalEncoder(Mission m, String domain, boolean millisec) {
        super(m, domain, millisec);
    }

    @Override
    String getPDDLGoal() {
        Mission m = getMission();
        boolean millis = toMillis();
        boolean isFreeDomain = Domains.isFreeDomain(getDomain());

        StringBuilder s = new StringBuilder();
        s.append("\t(and\n");

        // Covered Requests
        if(isFreeDomain) {
            m.getRequests().forEach((ObservationRequest req) -> {
                s.append(String.format(Locale.ENGLISH,
                        "\t  (covered %s)\n",
                        req.pddl()));
            });
        }
        else {
            m.getAssignments().keySet().stream()
                    .forEach((ObservationRequest req) -> {
                        UAV uav = m.getAssignments().get(req).getUAV();
                        s.append(String.format(Locale.ENGLISH,
                                "\t  (covered %s %s)\n",
                                uav.pddl(), req.pddl()));
                    });
        }


        // Landed UAVs
        m.getUAVsAvailability().stream()
                .distinct()
                .forEach((UAVAvailability uavAv) -> {
                    UAV uav = uavAv.getUAV();
                    Airport airport = uavAv.getAirport();
                    s.append(String.format(Locale.ENGLISH,
                            "\t  (landed %s %s)\n",
                            uav.pddl(), airport.pddl()));
                });

        /**
         * TODO: add timebounds
         */
        s.append("\t)\n");
        return s.toString();
    }

    @Override
    String getPDDLInit() {
        Mission m = getMission();
        Set<ObservationRequest> observable = getObservable();
        Set<Target> targetSites = getTargetSites();
        Map<UAV, Integer> numTrgAssigned = getNumTrgAssigned();
        DecimalFormat df = getDf();

        boolean millis = toMillis();
        boolean isTFDDomain = Domains.isTFDDomain(getDomain());
        boolean isFreeDomain = Domains.isFreeDomain(getDomain());

        StringBuilder s = new StringBuilder();

        // Global
        if(isFreeDomain)
            s.append("\t(= (NumberTrgCovered) 0)\n")
                    .append("\t(= (MinNumTrgt) "+Math.ceil(m.getObservationRequests().size()-m.getUAVsAvailability().size())+")\n")
                    .append("\n");

        // Requests
        m.getObservationRequests().stream()
                .forEach((ObservationRequest req) -> {
                    String pddlReq = req.pddl();
                    s.append(String.format(Locale.ENGLISH,
                            "\t(site_target %s %s)\n",
                            req.pddlSite(), req.pddl()))
                            .append(String.format(Locale.ENGLISH,
                                    "\t(= (Time-min-obs %s) %d)\n",
                                    pddlReq, req.getMinDuration(millis)))
                            .append(String.format(Locale.ENGLISH,
                                    "\t(unvisited_t %s)\n",
                                    pddlReq));
                    if (!isTFDDomain) {
                        s.append(String.format(Locale.ENGLISH,
                                "\t(at %d (osservabile %s))\n",
                                millis ? Duration.between(m.getEarliestTime(), req.getEarliestTime()).toMillis(): Duration.between(m.getEarliestTime(), req.getEarliestTime()).getSeconds(), pddlReq))
                            .append(String.format(Locale.ENGLISH,
                                "\t(at %d (not (osservabile %s)))\n",
                                millis ? Duration.between(m.getEarliestTime(), req.getLatestTime()).toMillis() : Duration.between(m.getEarliestTime(), req.getLatestTime()).getSeconds(), pddlReq));
                    }
                    else { /*TDF CASE: DOESN'T SUPPORT TIL*/
                        s.append(String.format(Locale.ENGLISH,
                                "\t(osservabile %s)\n",
                                pddlReq));
                    }

                    if(!isFreeDomain) {
                        UAVAvailability uavAv = m.getAssignments().get(req);
                        String pddlUav = uavAv.getUAV().pddl();
                        s.append(String.format(Locale.ENGLISH,
                                "\t(to_cover %s %s)\n",
                                pddlReq, pddlUav/*, uavAv.pddlSensorSuite()*/)) /*TODO: introdurre i sensori*/
                                .append(String.format(Locale.ENGLISH,
                                        "\t(uncovered %s %s)\n",
                                        pddlUav, pddlReq));
                    }
                    else {
                        s.append(String.format(Locale.ENGLISH,
                                "\t(uncovered %s)\n",
                                pddlReq));
                    }

                    if (!req.targetIsPoint()) {
                        s.append(String.format(Locale.ENGLISH,
                                "\t(site_target %s %s)\n",
                                req.pddlLastSite(), req.pddl()));
                    }
                });

        s.append("\n");

        // Observable requests
        observable.stream().forEach((ObservationRequest obsReq) -> {
            if(!isFreeDomain) {
                UAV uav = m.getAssignments().get(obsReq).getUAV();
                s.append(String.format(Locale.ENGLISH,
                        "\t(observable %s %s)\n", obsReq.pddl(), uav.pddl()));
            }
            else {
                s.append(String.format(Locale.ENGLISH,
                        "\t(observable %s)\n", obsReq.pddl()));
            }
        });

        s.append("\n");

        // Airports
        m.getUAVsAvailability().stream()
                .map(uavAv -> uavAv.getAirport())
                .distinct()
                .forEach((Airport airport) -> {
                            String pddl = airport.pddl();
                            String pddlBeginLanding = airport.pddlBeginLanding();
                            String pddlEndTakeoff = airport.pddlEndTakeoff();
                            String pddlCenter = airport.pddlCenter();

                            s.append(String.format(Locale.ENGLISH,
                                    "\t(site_for_begin_takeOff %s %s)\n",
                                    pddl, airport.pddlCenter()))
                                    .append(String.format(Locale.ENGLISH,
                                            "\t(site_for_end_takeOff %s %s)\n",
                                            pddl, airport.pddlEndTakeoff()))
                                    .append(String.format(Locale.ENGLISH,
                                            "\t(site_for_landing %s %s)\n",
                                            pddl, airport.pddlBeginLanding()))
                                    .append(String.format(Locale.ENGLISH,
                                            "\t(site_after_landing %s %s)\n",
                                            pddl, airport.pddlCenter()));
                    /*TODO: introdurre questo di seguito*/
                                    /*.append(String.format(Locale.ENGLISH,
                                            "\t(= (time-last-take-off %s) 0)\n",
                                            pddl));*/

                            // Distances between airports
                            m.getUAVsAvailability().stream()
                                    .map(uavAv -> uavAv.getAirport())
                                    .distinct()
                                    .forEach((Airport airport2) -> {
                                        m.getUAVsAvailability().stream()
                                                .distinct()
                                                .forEach((UAVAvailability uavAv) -> {
                                                    UAV uav = uavAv.getUAV();
                                                    double uav_speed = uav.getCruisingSpeed(millis);// in m/ms se millis, in m/s altrimenti
                                                    String pddlUav = uav.pddl();
                                                    s.append(String.format(Locale.ENGLISH,
                                                            "\t(= (time_required %s %s %s) %.1f)\n",
                                                            pddlCenter, airport2.pddlCenter(), pddlUav,
                                                            airport.getDistanceFromCenter(airport2.getPosition()) / uav_speed))
                                                            .append(String.format(Locale.ENGLISH,
                                                                    "\t(= (time_required %s %s %s) %.1f)\n",
                                                                    pddlCenter, airport2.pddlBeginLanding(), pddlUav,
                                                                    airport.getDistanceFromCenter(airport2.getBeginOfLanding()) / uav_speed))
                                                            .append(String.format(Locale.ENGLISH,
                                                                    "\t(= (time_required %s %s %s) %.1f)\n",
                                                                    pddlCenter, airport2.pddlEndTakeoff(), pddlUav,
                                                                    airport.getDistanceFromCenter(airport2.getEndOfTakeoff()) / uav_speed))
                                                            .append(String.format(Locale.ENGLISH,
                                                                    "\t(= (time_required %s %s %s) %.1f)\n",
                                                                    pddlBeginLanding, airport2.pddlCenter(), pddlUav,
                                                                    airport.getDistanceFromBeginOfLanding(airport2.getPosition()) / uav_speed))
                                                            .append(String.format(Locale.ENGLISH,
                                                                    "\t(= (time_required %s %s %s) %.1f)\n",
                                                                    pddlBeginLanding, airport2.pddlBeginLanding(), pddlUav,
                                                                    airport.getDistanceFromBeginOfLanding(airport2.getBeginOfLanding()) / uav_speed))
                                                            .append(String.format(Locale.ENGLISH,
                                                                    "\t(= (time_required %s %s %s) %.1f)\n",
                                                                    pddlBeginLanding, airport2.pddlEndTakeoff(), pddlUav,
                                                                    airport.getDistanceFromBeginOfLanding(airport2.getEndOfTakeoff()) / uav_speed))
                                                            .append(String.format(Locale.ENGLISH,
                                                                    "\t(= (time_required %s %s %s) %.1f)\n",
                                                                    pddlEndTakeoff, airport2.pddlCenter(), pddlUav,
                                                                    airport.getDistanceFromEndOfTakeoff(airport2.getPosition()) / uav_speed))
                                                            .append(String.format(Locale.ENGLISH,
                                                                    "\t(= (time_required %s %s %s) %.1f)\n",
                                                                    pddlEndTakeoff, airport2.pddlBeginLanding(), pddlUav,
                                                                    airport.getDistanceFromEndOfTakeoff(airport2.getBeginOfLanding()) / uav_speed))
                                                            .append(String.format(Locale.ENGLISH,
                                                                    "\t(= (time_required %s %s %s) %.1f)\n",
                                                                    pddlEndTakeoff, airport2.pddlEndTakeoff(), pddlUav,
                                                                    airport.getDistanceFromEndOfTakeoff(airport2.getEndOfTakeoff()) / uav_speed));
                                                });
                                    });
                            targetSites.stream().forEach(trg -> {
                                String targetPddl = trg.pddl();
                                Coordinates trgPosition = trg.getFirstPosition();
                                m.getUAVsAvailability().stream()
                                        .distinct()
                                        .forEach((UAVAvailability uavAv) -> {
                                            String targetPddl_inn = trg.pddl();
                                            Coordinates trgPosition_inn = trg.getFirstPosition();
                                            UAV uav = uavAv.getUAV();
                                            double uav_speed = uav.getCruisingSpeed(millis);// in m/ms se millis, in m/s altrimenti
                                            String pddlUav = uav.pddl();
                                            s.append(String.format(Locale.ENGLISH,
                                                    "\t(= (time_required %s %s %s) %.1f)\n",
                                                    pddlBeginLanding, targetPddl_inn, pddlUav,
                                                    airport.getDistanceFromBeginOfLanding(trgPosition_inn) / uav_speed))
                                                    .append(String.format(Locale.ENGLISH,
                                                            "\t(= (time_required %s %s %s) %.1f)\n",
                                                            targetPddl_inn, pddlBeginLanding, pddlUav,
                                                            airport.getDistanceFromBeginOfLanding(trgPosition_inn) / uav_speed))
                                                    .append(String.format(Locale.ENGLISH,
                                                            "\t(= (time_required %s %s %s) %.1f)\n",
                                                            pddlEndTakeoff, targetPddl_inn, pddlUav,
                                                            airport.getDistanceFromEndOfTakeoff(trgPosition_inn) / uav_speed))
                                                    .append(String.format(Locale.ENGLISH,
                                                            "\t(= (time_required %s %s %s) %.1f)\n",
                                                            targetPddl_inn, pddlEndTakeoff, pddlUav,
                                                            airport.getDistanceFromEndOfTakeoff(trgPosition_inn) / uav_speed))
                                                    .append(String.format(Locale.ENGLISH,
                                                            "\t(= (time_required %s %s %s) %.1f)\n",
                                                            pddlCenter, targetPddl_inn, pddlUav,
                                                            airport.getDistanceFromCenter(trgPosition_inn) / uav_speed))
                                                    .append(String.format(Locale.ENGLISH,
                                                            "\t(= (time_required %s %s %s) %.1f)\n",
                                                            targetPddl_inn, pddlCenter, pddlUav,
                                                            airport.getDistanceFromCenter(trgPosition_inn) / uav_speed));
                                        });

                                if (!trg.isPoint()) {
                                    targetPddl = trg.pddlLast();
                                    trgPosition = trg.getLastPosition();
                                    m.getUAVsAvailability().stream()
                                            .distinct()
                                            .forEach((UAVAvailability uavAv) -> {
                                                String targetPddl_inn = trg.pddlLast();
                                                Coordinates trgPosition_inn = trg.getLastPosition();
                                                UAV uav = uavAv.getUAV();
                                                double uav_speed = uav.getCruisingSpeed(millis);// in m/ms se millis, in m/s altrimenti
                                                String pddlUav = uav.pddl();
                                                s.append(String.format(Locale.ENGLISH,
                                                        "\t(= (time_required %s %s %s) %.1f)\n",
                                                        pddlBeginLanding, targetPddl_inn, pddlUav,
                                                        airport.getDistanceFromBeginOfLanding(trgPosition_inn) / uav_speed))
                                                        .append(String.format(Locale.ENGLISH,
                                                                "\t(= (time_required %s %s %s) %.1f)\n",
                                                                targetPddl_inn, pddlBeginLanding, pddlUav,
                                                                airport.getDistanceFromBeginOfLanding(trgPosition_inn) / uav_speed))
                                                        .append(String.format(Locale.ENGLISH,
                                                                "\t(= (time_required %s %s %s) %.1f)\n",
                                                                pddlEndTakeoff, targetPddl_inn, pddlUav,
                                                                airport.getDistanceFromEndOfTakeoff(trgPosition_inn) / uav_speed))
                                                        .append(String.format(Locale.ENGLISH,
                                                                "\t(= (time_required %s %s %s) %.1f)\n",
                                                                targetPddl_inn, pddlEndTakeoff, pddlUav,
                                                                airport.getDistanceFromEndOfTakeoff(trgPosition_inn) / uav_speed))
                                                        .append(String.format(Locale.ENGLISH,
                                                                "\t(= (time_required %s %s %s) %.1f)\n",
                                                                pddlCenter, targetPddl_inn, pddlUav,
                                                                airport.getDistanceFromCenter(trgPosition_inn) / uav_speed))
                                                        .append(String.format(Locale.ENGLISH,
                                                                "\t(= (time_required %s %s %s) %.1f)\n",
                                                                targetPddl_inn, pddlCenter, pddlUav,
                                                                airport.getDistanceFromCenter(trgPosition_inn) / uav_speed));
                                            });
                                }
                            });
                        }
                );

        s.append("\n");

        // UAVs
        m.getUAVsAvailability().stream()
                .distinct()
                .forEach((UAVAvailability uavAv) -> {
                            UAV uav = uavAv.getUAV();
                            Airport airport = uavAv.getAirport();
                            String pddlUav = uav.pddl();
                            String pddlArpt = airport.pddl();
                            s.append(String.format(Locale.ENGLISH,
                                    "\t(current_site %s %s)\n",
                                    pddlUav, airport.pddlCenter()))
                                    .append(String.format(Locale.ENGLISH,
                                            "\t(ready_to_takeoff %s %s)\n",
                                            pddlUav, pddlArpt))
                                    .append(String.format(Locale.ENGLISH,
                                            "\t(= (Time-take-off %s %s) %d)\n",
                                            pddlUav, pddlArpt, airport.getTakeoffDuration(millis)))
                                    .append(String.format(Locale.ENGLISH,
                                            "\t(= (Time-landing %s %s) %d)\n",
                                            pddlUav, pddlArpt, airport.getLandingDuration(millis)))
                                    .append(String.format(Locale.ENGLISH,
                                            "\t(at 0 (= (start_next %s) 0.01))\n",
                                            pddlUav));
                            if(!isFreeDomain) {
                                s.append(String.format("\t(= (NumberTrgAssigned %s) %d)\n",
                                        pddlUav, numTrgAssigned.get(uav)))
                                        .append(String.format("\t(= (NumberTrgCovered %s) 0)\n",
                                                pddlUav));
                            }
                        }
                );

        s.append("\n");

        // Distances between targets
        targetSites.stream().forEach(trg1 -> {
            targetSites.stream().forEach(trg2 -> {
                m.getUAVsAvailability().stream()
                        .distinct()
                        .forEach((UAVAvailability uavAv) -> {
                            UAV uav = uavAv.getUAV();
                            double uav_speed = uav.getCruisingSpeed(millis);// in m/ms se millis, in m/s altrimenti
                            String pddlUav = uav.pddl();
                            s.append(String.format(Locale.ENGLISH,
                                    "\t(= (time_required %s %s %s) %.1f)\n",
                                    trg1.pddl(), trg2.pddl(), pddlUav,
                                    trg1.getFirstPosition().distanceFrom(
                                            trg2.getFirstPosition()) / uav_speed));
                        });
                if (!trg2.isPoint()) {
                    m.getUAVsAvailability().stream()
                            .distinct()
                            .forEach((UAVAvailability uavAv) -> {
                                UAV uav = uavAv.getUAV();
                                double uav_speed = uav.getCruisingSpeed(millis);// in m/ms se millis, in m/s altrimenti
                                String pddlUav = uav.pddl();
                                s.append(String.format(Locale.ENGLISH,
                                        "\t(= (time_required %s %s %s) %.1f)\n",
                                        trg1.pddl(), trg2.pddlLast(), pddlUav,
                                        trg1.getFirstPosition().distanceFrom(
                                                trg2.getLastPosition()) / uav_speed));
                            });
                }
            });
        });
        targetSites.stream()
                .filter(trg -> !trg.isPoint())
                .forEach(trg1 -> {
                    targetSites.stream().forEach(trg2 -> {
                        m.getUAVsAvailability().stream()
                                .distinct()
                                .forEach((UAVAvailability uavAv) -> {
                                    UAV uav = uavAv.getUAV();
                                    double uav_speed = uav.getCruisingSpeed(millis);// in m/ms se millis, in m/s altrimenti
                                    String pddlUav = uav.pddl();
                                    s.append(String.format(Locale.ENGLISH,
                                            "\t(= (time_required %s %s %s) %.1f)\n",
                                            trg1.pddlLast(), trg2.pddl(), pddlUav,
                                            trg1.getLastPosition().distanceFrom(
                                                    trg2.getFirstPosition()) / uav_speed));
                                });
                        if (!trg2.isPoint()) {
                            m.getUAVsAvailability().stream()
                                    .distinct()
                                    .forEach((UAVAvailability uavAv) -> {
                                        UAV uav = uavAv.getUAV();
                                        double uav_speed = uav.getCruisingSpeed(millis);// in m/ms se millis, in m/s altrimenti
                                        String pddlUav = uav.pddl();
                                        s.append(String.format(Locale.ENGLISH,
                                                "\t(= (time_required %s %s %s) %.1f)\n",
                                                trg1.pddlLast(), trg2.pddlLast(), pddlUav,
                                                trg1.getLastPosition().distanceFrom(
                                                        trg2.getLastPosition()) / uav_speed));
                                    });
                        }
                    });
                });

        s.append("\n");

        // MultiObs
        m.getMultiObs()
                .stream().forEach((MultiObs mOb) -> {
                    s.append(String.format(Locale.ENGLISH,
                            "\t(%s %s %s)\n",
                            mOb.getConstraint(), mOb.pddlRequest1(), mOb.pddlRequest2()));
                }
        );

        return s.toString();
    }


    @Override
    String getPDDLMetric() {
        return "minimize (total-time)";
    }
}
