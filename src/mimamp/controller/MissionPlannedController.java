/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.controller;

import java.io.File;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import mimamp.model.UAV;
import mimamp.model.plan.Plan;
import mimamp.model.plan.SensorSuiteOnUAV;
import mimamp.model.plan.actions.DurativeSensorSuiteAction;
import mimamp.model.plan.actions.DurativeUAVAction;

/**
 *
 * @author marco
 */
public class MissionPlannedController {

    private Plan actualPlan;

    public MissionPlannedController(Plan plan) {
        this.actualPlan = plan;
    }

    public List<DurativeUAVAction> getActionsOf(UAV uav) {
        return actualPlan.getActionsOf(uav);
    }

    public List<DurativeSensorSuiteAction>
            getActionsOf(SensorSuiteOnUAV suite) {
        return actualPlan.getActionsOf(suite);
    }

    public Plan getActualPlan() {
        return actualPlan;
    }

    public boolean hasActualPlan() {
        return actualPlan != null;
    }

    public String getPlanName() {
        return actualPlan.getName();
    }

    public void saveKML(File kmlFile) {
        try {
            new KMLPlanEncoder(actualPlan).encode(kmlFile);
        }
        catch(TransformerException|ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
    }
}
