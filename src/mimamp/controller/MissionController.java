package mimamp.controller;

import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import mimamp.model.Database;
import mimamp.model.Mission;
import mimamp.model.MultiObs;
import mimamp.model.ObservationRequest;
import mimamp.model.UAV;
import mimamp.model.UAVAvailability;

public class MissionController {

    private final Mission mission;
    /*the list of the observation requests chosen by the user to be performed during the mission*/
    private final List<ObservationRequest> requests;
    /* the list of all possible uavs available in the date of the mission that could handle at least one request */
    private final List<UAV> uavs;

    public MissionController(List<ObservationRequest> requests) {
        LocalDate date = requests.get(0).getDate();
        this.mission = new Mission(date);
        this.requests = requests;
        try (Database db = new Database()) {
            uavs = db.getAllUAVs().stream()
                    .filter(uav -> requests.stream()
                            .anyMatch(req -> uav.canHandle(req)))
                    .collect(Collectors.toList());
        } catch (SQLException e) {
            throw new RuntimeException("Error getting UAVs: " + e.getMessage());
        }
    }

    public boolean canBeExecuted() {
        return mission.getRequestsNumber() == requests.size();
    }

    public long getMinAllowedDuration() {
        boolean millis = true;
        return requests.stream()
                .max((req1, req2) -> {
                    long minDuration1 = req1.getMinDuration(millis);
                    long minDuration2 = req2.getMinDuration(millis);
                    return (int) (minDuration1 - minDuration2);
                })
                .get()
                .getMinDuration(!millis);
    }
    
    public Mission getMission() {
        return mission;
    }

    public ObservationRequest getObservationRequest(int index) {
        return requests.get(index);
    }

    public int getRequestNumber() {
        return requests.size();
    }

    public List<ObservationRequest> getRequests() {
        return requests;
    }

    public UAV getUAV(int index) {
        return uavs.get(index);
    }

    public String getUAVName(int index) {
        return uavs.get(index).toString();
    }

    public int getUAVNumber() {
        return uavs.size();
    }

    public List<UAV> getUAVs() {
        return uavs;
    }

    public void setMissionParams(LocalTime earliestTime, LocalTime latestTime,
            Duration maxDuration, Set<UAVAvailability> uavs, Set<ObservationRequest> obsReqs, Map<ObservationRequest, UAVAvailability> assignments,
            Set<MultiObs> multiObs) {
        mission.setEarliestTime(earliestTime);
        mission.setLatestTime(latestTime);
        mission.setMaxDuration(maxDuration);
        mission.setUAVsAvailability(uavs);
        mission.setObservationRequests(obsReqs);
        mission.setAssignments(assignments);
        mission.setMultiObs(multiObs);
    }

    public ObservationRequest getInconsistentAssignment(Map<ObservationRequest, UAVAvailability> assignments) {
        return assignments.keySet().stream()
                .filter(obsReq -> {
                    UAV uav = assignments.get(obsReq).getUAV();
                    return !obsReq.targetIsPoint()
                    && obsReq.timeIncompatibleWith(uav);
                })
                .findAny()
                .orElse(null);
    }

    public ObservationRequest getInconsistentObsReqs(Set<ObservationRequest> observationRequests, Set<UAVAvailability> uavsAvailability) {
        return observationRequests.stream()
                .filter(obsReq -> {
                    /*find all  the observationrequests of LOC targets for which there is not any UAVS with a compatible time of observation*/
                    boolean incomp = true;
                    for (UAVAvailability ua: uavsAvailability) {
                        UAV uav = ua.getUAV();
                        if (obsReq.targetIsPoint() || !obsReq.timeIncompatibleWith(uav))
                            incomp = false;
                    }
                    return incomp;
                })
                .findAny()
                .orElse(null);
    }
}
