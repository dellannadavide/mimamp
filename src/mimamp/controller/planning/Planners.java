package mimamp.controller.planning;

import mimamp.model.Domains;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by davide on 22/10/16.
 */
public final class Planners {
    /*Numeric planners*/
    public static final String COLIN_NUMERIC = "Colin-Numeric";
    public static final String METRICFF = "Metric-FF";
    public static final String LPG = "LPG";
    public static final String POPF2_NUMERIC = "POPF2-Numeric";
    /*Temporal planners*/
    public static final String COLIN_TEMPORAL = "Colin-Temporal";
    public static final String TFD = "TFD";
    public static final String POPF2_TEMPORAL = "POPF2-Temporal";

    public static Collection<String> getNumericalPlanners() {
        return Arrays.asList(COLIN_NUMERIC, METRICFF, LPG, POPF2_NUMERIC);
    }

    public static Collection<String> getTemporalPlanners() {
        return Arrays.asList(COLIN_TEMPORAL, TFD, POPF2_TEMPORAL);
    }

    public static ProcessBuilder getPlannerProcessBuilder(String planner, int timeout, String domainPath, String problemPath, String addParam) {
        switch (planner) {
            case COLIN_NUMERIC:
            case COLIN_TEMPORAL:
                return new ProcessBuilder("timeout", timeout+"", "colin-clp", domainPath, problemPath);
            case LPG:
                return new ProcessBuilder("timeout", timeout+"", "lpg", "-o", domainPath, "-f", problemPath, "-n", "1", "-noout", "-same_objects");
            case METRICFF:
                return new ProcessBuilder("timeout", timeout+"", "ff", "-o", domainPath, "-f", problemPath, "-s", "3", "-w", "0");
            case POPF2_NUMERIC:
            case POPF2_TEMPORAL:
                return new ProcessBuilder("timeout", timeout+"", "optic-cplex", "-0", domainPath, problemPath);
            case TFD:
                return new ProcessBuilder("timeout", timeout+"", "tfd", domainPath, problemPath, addParam);
        }
        return null;
    }

}