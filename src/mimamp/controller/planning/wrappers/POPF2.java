package mimamp.controller.planning.wrappers;

import mimamp.controller.planning.Planner;
import mimamp.controller.planning.Planners;
import mimamp.model.Domains;
import mimamp.model.Mission;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by davide on 21/10/16.
 */
public abstract class POPF2 extends Planner {


    public POPF2(String plannerID, Mission mission, int timeout, String planning_model, String domain, boolean millis) {
        super(plannerID, mission, timeout, planning_model, domain, millis);
    }

    @Override
    public List<String> getOutput(File problem, String domain) {
        Timer timer = getTimer();
        int timeout = getTimeout();

        setChanged();
        notifyObservers("Calling POPF2");
        System.out.println("\nCalling POPF2\n"+domain);
        ProcessBuilder popf2Builder = Planners.getPlannerProcessBuilder(getPlannerId(), timeout, Domains.getDomainsFolder()+domain, problem.getPath(), "");
        Process popf2 = null;
        List<String> out;
        try {
            TimerTask task = new UpdateProgressTask();
            startTimer(task);
            popf2 = popf2Builder.start();
            out = getPOPF2Plan(popf2.getInputStream());
            popf2.waitFor(timeout, TimeUnit.SECONDS);
            endTimer(task);

            setChanged();
            notifyObservers("\nPOPF2 exited with status: " + popf2.exitValue());

            return out;
        } catch (IllegalThreadStateException itsEx) {
            if (popf2 != null) {
                popf2.destroyForcibly();
            }
            return null;
        } catch (IOException | InterruptedException ex) {
            throw new RuntimeException(ex);
        } finally {
            timer.cancel();
        }
    }

    public abstract List<String> getPOPF2Plan(InputStream inputStream);

}
