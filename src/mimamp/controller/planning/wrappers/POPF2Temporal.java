package mimamp.controller.planning.wrappers;

import mimamp.controller.planning.Planner;
import mimamp.model.Domains;
import mimamp.model.Mission;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by davide on 21/10/16.
 */
public class POPF2Temporal extends POPF2 {


    public POPF2Temporal(String plannerID, Mission mission, int timeout, String planning_model, String domain, boolean millis) {
        super(plannerID, mission, timeout, planning_model, domain, millis);
    }

    public List<String> getPOPF2Plan(InputStream inputStream) {
        Scanner output = new Scanner(inputStream);
        ArrayList<String> list = new ArrayList<>();
        boolean startWriting = false;
        boolean lastPlan = false;
        while (output.hasNextLine()) {
            String line = output.nextLine();
            setChanged();
            notifyObservers("\n"+line);
            if (startWriting) {
                list.add(line.toUpperCase());
            } else if (line.startsWith(";;;; Solution Found")) {
                /*TODO: dopo questo compare il costo*/
                lastPlan = true;
            }
            else if (lastPlan && line.startsWith("; Time")) {
                startWriting = true;
            }
        }
        return list;
    }


}
