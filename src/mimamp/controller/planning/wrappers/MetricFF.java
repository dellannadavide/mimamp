package mimamp.controller.planning.wrappers;

import mimamp.controller.planning.Planner;
import mimamp.controller.planning.Planners;
import mimamp.model.Domains;
import mimamp.model.Mission;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by davide on 21/10/16.
 */
public class MetricFF extends Planner {


    public MetricFF(String plannerID, Mission mission, int timeout, String planning_model, String domain, boolean millis) {
        super(plannerID, mission, timeout, planning_model, domain, millis);
    }

    @Override
    public List<String> getOutput(File problem, String domain) {
        Mission mission = getMission();
        Timer timer = getTimer();
        int timeout = getTimeout();

        if (mission.hasNotMultiObs()) {
            // Trying to find the best solution with A* (MetricFF)
            setChanged();
            notifyObservers("\nCalling MetricFF");
            System.out.println("Calling MetricFF");
            //System.out.println("ff -o "+Domains.getDomainsFolder()+domain+" -f "+problem.getPath()+" -s 3 -w 0");
            ProcessBuilder metricffBuilder = Planners.getPlannerProcessBuilder(getPlannerId(), timeout, Domains.getDomainsFolder()+domain, problem.getPath(), "");
            Process metricff = null;
            List<String> out;
            try {
                TimerTask task = new UpdateProgressTask();
                startTimer(task);
                metricff = metricffBuilder.start();
                out = getMetricFFPlan(metricff.getInputStream());
                metricff.waitFor(timeout, TimeUnit.SECONDS);
                endTimer(task);

                setChanged();
                notifyObservers("\nMetricFF exited with status: " + metricff.exitValue());

                if (metricff.exitValue() == 0) {
                    timer.cancel();
                    if(!out.isEmpty())
                        return out;
                    else
                        timer = new Timer();
                }
            } catch (IllegalThreadStateException itsEx) {
                if (metricff != null) {
                    metricff.destroyForcibly();
                }
            } catch (IOException | InterruptedException ex) {
                setChanged();
                notifyObservers("\nMetricFF execution caused an exception: " + ex);
            }
        }
        else
            notifyObservers("\nProblem contains multiobs");

        return null;
    }

    private List<String> getMetricFFPlan(InputStream inputStream) {
        Scanner output = new Scanner(inputStream);
        ArrayList<String> list = new ArrayList<>();
        boolean startWriting = false;
        while (output.hasNextLine()) {
            String line = output.nextLine();
            setChanged();
            notifyObservers("\n"+line);
            if (startWriting) {
                if (line.startsWith("plan cost:")) {
                    return list;
                } else {
                    list.add(line.substring(11).toUpperCase());
                }
            } else if (line.startsWith("ff: found legal plan as follows")) {
                startWriting = true;
            }
        }
        return list;
    }


}
