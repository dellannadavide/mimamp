package mimamp.controller.planning.wrappers;

import mimamp.controller.planning.Planner;
import mimamp.controller.planning.Planners;
import mimamp.model.Domains;
import mimamp.model.Mission;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by davide on 21/10/16.
 */
public class LPG extends Planner {


    public LPG(String plannerID, Mission mission, int timeout, String planning_model, String domain, boolean millis) {
        super(plannerID, mission, timeout, planning_model, domain, millis);
    }

    @Override
    public List<String> getOutput(File problem, String domain) {
        Timer timer = getTimer();
        int timeout = getTimeout();

        setChanged();
        notifyObservers("Calling LPG");
        System.out.println("\nCalling LPG\n"+domain);
        ProcessBuilder lpgBuilder = Planners.getPlannerProcessBuilder(getPlannerId(), timeout, Domains.getDomainsFolder()+domain, problem.getPath(), "");
        Process lpg = null;
        List<String> out;
        try {
            TimerTask task = new UpdateProgressTask();
            startTimer(task);
            lpg = lpgBuilder.start();
            out = getLPGPlan(lpg.getInputStream());
            lpg.waitFor(timeout, TimeUnit.SECONDS);
            endTimer(task);

            setChanged();
            notifyObservers("\nLPG exited with status: " + lpg.exitValue());

            return out;
        } catch (IllegalThreadStateException itsEx) {
            if (lpg != null) {
                lpg.destroyForcibly();
            }
            return null;
        } catch (IOException | InterruptedException ex) {
            throw new RuntimeException(ex);
        } finally {
            timer.cancel();
        }
    }

    private List<String> getLPGPlan(InputStream inputStream) {
        Scanner output = new Scanner(inputStream);
        ArrayList<String> list = new ArrayList<>();
        boolean startWriting = false;
        boolean lastPlan = false;
        while (output.hasNextLine()) {
            String line = output.nextLine();
            setChanged();
            notifyObservers("\n"+line);
            if (startWriting) {
                if(line.length()>0) {
                    int lpar = 1 + line.indexOf('(');
                    int rpar = line.indexOf(')');
                    list.add(line.substring(lpar, rpar).toUpperCase());
                }
                else startWriting=false;
            } else if (line.startsWith("Last plan computed:")) {
                lastPlan = true;
            }
            else if (lastPlan && line.startsWith("   Time:")) {
                startWriting = true;
            }
        }
        return list;
    }


}
