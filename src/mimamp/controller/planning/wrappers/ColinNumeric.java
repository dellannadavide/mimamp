package mimamp.controller.planning.wrappers;

import mimamp.model.Domains;
import mimamp.model.Mission;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by davide on 21/10/16.
 */
public class ColinNumeric extends Colin {

    public ColinNumeric(String plannerID, Mission mission, int timeout, String planning_model, String domain, boolean millis) {
        super(plannerID, mission, timeout, planning_model, domain, millis);
    }

    public List<String> getColinPlan(InputStream inputStream) {
        Scanner output = new Scanner(inputStream);
        ArrayList<String> list = new ArrayList<>();
        boolean startWriting = false;
        while (output.hasNextLine()) {
            String line = output.nextLine();
            setChanged();
            notifyObservers("\n"+line);
            /*TODO: ottimizzazione (da capire se va fatta qua o più ad alto livello)*/
            //recupero il costo del piano appena ottenuto (se esiste)
            /*if(line.toUpperCase().contains("COST")) {
                setLast_plan_cost(Integer.parseInt((line.replace("; Cost: ", "")).replace(".000", "")));
                System.out.println("Current plan cost: "+getLast_plan_cost());
            }*/
            if (startWriting) {
                int lpar = 1 + line.indexOf('(');
                int rpar = line.indexOf(')');
                list.add(line.substring(lpar, rpar).toUpperCase());
            } else if (line.startsWith("; Time")) {
                startWriting = true;
            }
        }
        return list;
    }


}
