package mimamp.controller.planning.wrappers;

import mimamp.controller.planning.Planner;
import mimamp.controller.planning.Planners;
import mimamp.model.Domains;
import mimamp.model.Mission;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by davide on 31/10/16.
 */
public abstract class Colin extends Planner {

    private int last_plan_cost;

    public Colin(String plannerID, Mission mission, int timeout, String planning_model, String domain, boolean millis) {
        super(plannerID, mission, timeout, planning_model, domain, millis);
    }

    @Override
    public List<String> getOutput(File problem, String domain) {
        Timer timer = getTimer();
        int timeout = getTimeout();
        System.out.println(timeout);

        setChanged();
        notifyObservers("Calling COLIN");
        System.out.println("\nCalling COLIN\n"+domain);
        ProcessBuilder colinBuilder = Planners.getPlannerProcessBuilder(getPlannerId(), timeout, Domains.getDomainsFolder()+domain, problem.getPath(), "");
        Process colin = null;
        List<String> out;
        try {
            TimerTask task = new UpdateProgressTask();
            startTimer(task);
            colin = colinBuilder.start();
            out = getColinPlan(colin.getInputStream());
            colin.waitFor(timeout, TimeUnit.SECONDS);
            endTimer(task);

            setChanged();
            notifyObservers("\n\nColin exited with status: " + colin.exitValue());

            return out;
        } catch (IllegalThreadStateException itsEx) {
            if (colin != null) {
                colin.destroyForcibly();
            }
            return null;
        } catch (IOException | InterruptedException ex) {
            throw new RuntimeException(ex);
        } finally {
            timer.cancel();
        }
    }

    /**
     * This method will be implemented by the temporal or numeric definition of colin
     * @param inputStream lo stream in input fornito dal planner colin
     * @return
     */
    public abstract List<String> getColinPlan(InputStream inputStream);


    public int getLast_plan_cost() {
        return last_plan_cost;
    }

    public void setLast_plan_cost(int last_plan_cost) {
        this.last_plan_cost = last_plan_cost;
    }

}
