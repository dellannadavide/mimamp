package mimamp.controller.planning.wrappers;

import mimamp.model.Mission;

import java.io.InputStream;
import java.util.*;

/**
 * Created by davide on 21/10/16.
 */
public class ColinTemporal extends Colin {

    public ColinTemporal(String plannerID, Mission mission, int timeout, String planning_model, String domain, boolean millis) {
        super(plannerID, mission, timeout, planning_model, domain, millis);
    }

    public List<String> getColinPlan(InputStream inputStream) {
        Scanner output = new Scanner(inputStream);
        ArrayList<String> list = new ArrayList<>();
        boolean startWriting = false;
        while (output.hasNextLine()) {
            String line = output.nextLine();
            setChanged();
            notifyObservers("\n"+line);
            if (startWriting) {
                list.add(line.toUpperCase());
            } else if (line.startsWith("; Time")) {
                startWriting = true;
            }
        }
        return list;
    }


}
