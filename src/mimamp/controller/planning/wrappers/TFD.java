package mimamp.controller.planning.wrappers;

import mimamp.controller.planning.Planner;
import mimamp.controller.planning.Planners;
import mimamp.model.Domains;
import mimamp.model.Mission;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by davide on 21/10/16.
 * Note about TFD:
 * 1. It doesn't support either timed-initial-literals -> NO TEMPORAL WINDOWS OF OBSERVATION OF TARGETS
 * 2. It doesn't support continuous numeric change -> NO WARRANTY OF CONTIGUITY BETWEEN ACTIONS (but usually they are)
 * TODO: mostrare un alert con quanto scritto qua sopra, quando si sceglie tfd
 */
public class TFD extends Planner {

    public TFD(String plannerID, Mission mission, int timeout, String planning_model, String domain, boolean millis) {
        super(plannerID, mission, timeout, planning_model, domain, millis);
    }

    @Override
    public List<String> getOutput(File problem, String domain) {
        Timer timer = getTimer();
        int timeout = getTimeout();

        setChanged();
        notifyObservers("Calling TFD");
        System.out.println("\nCalling TFD\n"+domain);
        ProcessBuilder tfdBuilder = Planners.getPlannerProcessBuilder(getPlannerId(), timeout, Domains.getDomainsFolderAbsolutePath()+"/"+domain, problem.getAbsolutePath(), "output_planners/sol_tfd_"+problem.getName().replace(".pddl",""));
        Process tfd = null;
        List<String> out;
        try {
            TimerTask task = new UpdateProgressTask();
            startTimer(task);
            tfd = tfdBuilder.start();
            out = getTFDPlan(tfd.getInputStream());
            tfd.waitFor(timeout, TimeUnit.SECONDS);
            endTimer(task);

            setChanged();
            notifyObservers("\nTFD exited with status: " + tfd.exitValue());

            return out;
        } catch (IllegalThreadStateException itsEx) {
            if (tfd != null) {
                tfd.destroyForcibly();
            }
            return null;
        } catch (IOException | InterruptedException ex) {
            throw new RuntimeException(ex);
        } finally {
            timer.cancel();
        }
    }

    private List<String> getTFDPlan(InputStream inputStream) {
        Scanner output = new Scanner(inputStream);
        ArrayList<String> list = new ArrayList<>();
        boolean startWriting = false;
        while (output.hasNextLine()) {
            String line = output.nextLine();
            setChanged();
            notifyObservers("\n"+line);
            if (startWriting) {
                if (line.startsWith("Solution with original makespan")) {
                    startWriting = false;
                } else list.add(line.toUpperCase());
            }
            else if (line.startsWith("Rescheduled Plan:")) {
                startWriting = true;
                list = new ArrayList<>(); /*dovrebbe già prendere il migliore in questo modo*/
            }
        }
        /*TODO: verificare se tfd ha generato altri piani nei file ed eventualmente usare il migliore*/
        return list;
    }


}
