package mimamp.controller.planning;

import mimamp.controller.planning.wrappers.*;
import mimamp.model.Mission;
import mimamp.model.Domains;

/**
 * Created by davide on 21/10/16.
 */
public class PlannerFactory {

    public Planner getPlanner(String planner, Mission mission, int timeout, String planning_model, String domain, boolean millis) {
        if (planner == null) {
            return null;
        }
        switch(planner) {
            /*Numerical planners*/
            case Planners.COLIN_NUMERIC:
                return new ColinNumeric(planner, mission, timeout, planning_model, Domains.getDomain(domain), millis);
            case Planners.METRICFF:
                return new MetricFF(planner, mission, timeout, planning_model, Domains.getDomain(domain), millis);
            case Planners.LPG:
                return new LPG(planner, mission, timeout, planning_model, Domains.getDomain(domain), millis);
            case Planners.POPF2_NUMERIC:
                return new POPF2Numeric(planner, mission, timeout, planning_model, Domains.getDomain(domain), millis);
            /*Temporal planners*/
            case Planners.COLIN_TEMPORAL:
                return new ColinTemporal(planner, mission, timeout, planning_model, Domains.getDomain(domain), millis);
            case Planners.TFD:
                return new TFD(planner, mission, timeout, planning_model, Domains.getDomain(domain), millis);
            case Planners.POPF2_TEMPORAL:
                return new POPF2Temporal(planner, mission, timeout, planning_model, Domains.getDomain(domain), millis);
        }
        return null;
    }
}
