package mimamp.controller.planning;

import mimamp.controller.decoding.Decoder;
import mimamp.controller.decoding.DecoderFactory;
import mimamp.controller.encoding.Encoder;
import mimamp.controller.encoding.EncoderFactory;
import mimamp.model.Database;
import mimamp.model.Mission;
import mimamp.model.plan.Plan;
import mimamp.model.plan.actions.TransferToTarget;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

/**
 *
 * @author marco
 */
public abstract class Planner extends Observable {

    private static final String  PROBLEMS_FOLDER = "problems/";

    private final Mission mission;
    private final Plan plan;
    private Timer timer;
    private final int timeout;
    private String planning_model;

    private TransferToTarget lastTransferToTarget;

    private final DecoderFactory decoderFactory;
    private final EncoderFactory encoderFactory;
    private Decoder decoder;
    private Encoder encoder;

    private String domain;

    private String plannerId;


    public Planner(String plannerID, Mission mission, int timeout, String planning_model, String domain, boolean millis) {
        this.plannerId =  plannerID;
        this.mission = mission;
        this.plan = new Plan(mission, planning_model);
        this.timer = new Timer();
        this.timeout = timeout;
        this.planning_model = planning_model;
        lastTransferToTarget = null;
        decoderFactory = new DecoderFactory();
        encoderFactory = new EncoderFactory();
        decoder = decoderFactory.getDecoder(planning_model, mission, plan, domain, millis);
        encoder = encoderFactory.getEncoder(planning_model, mission, domain, millis);
        this.domain = domain;
    }


    protected void endTimer(TimerTask task) {
        task.cancel();
    }

    public abstract List<String> getOutput(File problem, String domain);

    public Plan plan() {

        String pddl = encoder.toPDDL();

        File problem = new File(PROBLEMS_FOLDER+"problem_"+(new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date()))+".pddl");
        try (PrintWriter writer = new PrintWriter(problem)) {
            writer.write(pddl);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        long startTime = System.currentTimeMillis();
        List<String> output = getOutput(problem, domain);
        long stopTime = System.currentTimeMillis();
        if (output != null && output.size() > 0) {
            setChanged();
            System.out.println("Plan found in about: " + (stopTime - startTime)/1000+" sec.");
            decoder.decode(output);
            decoder.getPlan().printPlan();
            return decoder.getPlan();
        }
        else {
            return null;
        }


    }

    private void savePlanOnDatabase() {
        try (Database db = new Database()) {
            db.addMissionPlan(mission, plan);
        } catch (SQLException ex) {
            System.out.println("Error saving plan on database: " + ex.getMessage());
        }
    }

    protected void startTimer(TimerTask task) {
        timer.schedule(task, 0, 250);
    }

    protected class UpdateProgressTask extends TimerTask {

        private double i;

        public UpdateProgressTask() {
            i = 0;
        }

        @Override
        public void run() {
            setChanged();
            notifyObservers(i / timeout);
            i += 0.25;
        }
    }

    public Mission getMission() {
        return mission;
    }

    public Timer getTimer() {
        return timer;
    }

    public int getTimeout() {
        return timeout;
    }

    public String getDomain() {
        return domain;
    }

    public String getPlannerId() {
        return plannerId;
    }
}
