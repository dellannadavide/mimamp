/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.controller;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Random;
import mimamp.model.SensorSuite;

/**
 *
 * @author marco
 */
public class UAVAvailabilityQueryCreator {
    public static void main(String[] args) {
        Random rnd = new Random();
        String[] AIRPORTS = {"TRN", "CUF"};
        SensorSuite[] SUITES = SensorSuite.values();
        String pattern = "INSERT INTO UAV_AVAILABILITY(UAV, SENSOR_SUITE, AIRPORT, BEGIN_DATE, ENDING_DATE) "
                + "VALUES(%d, '%s', '%s', '%s', '%s');";
        for(LocalDate date = LocalDate.now(); date.getYear() == LocalDate.now().getYear(); date = date.plusDays(1)) {
            if(date.getDayOfMonth() % 2 == 0) {
                System.out.println(String.format(pattern, 4, SUITES[rnd.nextInt(SUITES.length)], AIRPORTS[rnd.nextInt(2)], Date.valueOf(date), Date.valueOf(date.plusDays(1))));
                System.out.println(String.format(pattern, 5, SUITES[rnd.nextInt(SUITES.length)], AIRPORTS[rnd.nextInt(2)], Date.valueOf(date), Date.valueOf(date.plusDays(1))));
                System.out.println(String.format(pattern, 7, SUITES[rnd.nextInt(SUITES.length)], AIRPORTS[rnd.nextInt(2)], Date.valueOf(date), Date.valueOf(date.plusDays(1))));
            }
            else {
                System.out.println(String.format(pattern, 6, SUITES[rnd.nextInt(SUITES.length)], AIRPORTS[rnd.nextInt(2)], Date.valueOf(date), Date.valueOf(date.plusDays(1))));
                System.out.println(String.format(pattern, 8, SUITES[rnd.nextInt(SUITES.length)], AIRPORTS[rnd.nextInt(2)], Date.valueOf(date), Date.valueOf(date.plusDays(1))));
            }
        }
    }
}
