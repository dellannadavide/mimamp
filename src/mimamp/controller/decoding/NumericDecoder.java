package mimamp.controller.decoding;

import mimamp.controller.encoding.NumericEncoder;
import mimamp.model.*;
import mimamp.model.plan.Plan;
import mimamp.model.plan.SensorSuiteOnUAV;
import mimamp.model.plan.actions.*;

import java.util.List;

/**
 * Created by davide on 21/10/16.
 * The class is the decoder for numeric planners.
 * Plans of numeric planners doesn't contain temporal information.
 */
public class NumericDecoder extends Decoder {

    public NumericDecoder(Mission mission, Plan plan, String domain, boolean millis) {
        super(mission, plan, domain, millis);
    }

    /***
     *
     * @param line the action reported by the planner, in the form <action_name> <parameter>*
     */
    @Override
    void decodeAction(String line) {
        String[] actionParams = line.split(" ");

        Airport airport;
        Coordinates prevlocation;
        Coordinates nextlocation;
        ObservationRequest obsReq;

        DurativeUAVAction uavAction;
        UAV uav;

        DurativeSensorSuiteAction suiteAction;
        SensorSuiteOnUAV suite;

        Mission mission = getMission();
        Plan plan = getPlan();
        TransferToTarget lastTransferToTarget = getLastTransferToTarget();

        switch (actionParams[0]) {
            case "WAIT_ON_GROUND":
                uav = mission.decodeUav(actionParams[1]);
                uavAction = new WaitOnGround(plan.getLastTimeFor(uav));
                plan.addUAVAction(uav, uavAction);

                suite = mission.getSensorSuiteOn(uav);
                suiteAction = new Idle(plan.getLastTimeFor(suite),
                        NumericEncoder.getTimeWaitOnGround(true));
                plan.addSensorSuiteAction(suite, suiteAction);
                break;
            case "TAKE_OFF_PREV":
            case "TAKE_OFF":
                airport = mission.decodeAirport(actionParams[4]);

                uav = mission.decodeUav(actionParams[1]);
                uavAction = new TakeOff(plan.getLastTimeFor(uav),
                        airport, actionParams[0].endsWith("PREV"));
                plan.addUAVAction(uav, uavAction);

                suite = mission.getSensorSuiteOn(uav);
                suiteAction = new Transfer(plan.getLastTimeFor(suite),
                        uavAction.getDuration(), airport.getEndOfTakeoff());
                plan.addSensorSuiteAction(suite, suiteAction);
                break;
            case "LANDING":
            case "LANDING_SUCC":
                airport = mission.decodeAirport(actionParams[4]);

                uav = mission.decodeUav(actionParams[1]);
                uavAction = new Landing(plan.getLastTimeFor(uav),
                        airport, actionParams[0].endsWith("SUCC"));
                plan.addUAVAction(uav, uavAction);

                suite = mission.getSensorSuiteOn(uav);
                suiteAction = new Transfer(plan.getLastTimeFor(suite),
                        uavAction.getDuration(), airport.getPosition());
                plan.addSensorSuiteAction(suite, suiteAction);
                break;
            case "TRANSFER_TO_TRG":
                prevlocation = mission.decodeLocation(actionParams[2]);
                nextlocation = mission.decodeLocation(actionParams[3]);

                uav = mission.decodeUav(actionParams[1]);
                lastTransferToTarget = new TransferToTarget(plan.getLastTimeFor(uav),
                        mission.decodeTarget(actionParams[4]), prevlocation, nextlocation,
                        uav.getCruisingSpeed(true));
                plan.addUAVAction(uav, lastTransferToTarget);

                suite = mission.getSensorSuiteOn(uav);
                suiteAction = new Transfer(plan.getLastTimeFor(suite),
                        lastTransferToTarget.getDuration(), nextlocation);
                plan.addSensorSuiteAction(suite, suiteAction);
                break;
            case "TRANSFER_TO_LAND":
                airport = mission.decodeAirport(actionParams[4]);

                uav = mission.decodeUav(actionParams[1]);
                uavAction = new TransferToLand(plan.getLastTimeFor(uav),
                        airport, mission.decodeLocation(actionParams[2]),
                        uav.getCruisingSpeed(true));
                plan.addUAVAction(uav, uavAction);

                suite = mission.getSensorSuiteOn(uav);
                suiteAction = new Transfer(plan.getLastTimeFor(suite),
                        uavAction.getDuration(), airport.getBeginOfLanding());
                plan.addSensorSuiteAction(suite, suiteAction);
                break;
            case "MONITOR_POINT_TARGET":
                obsReq = mission.decodeObservationRequest(actionParams[3]);

                uav = mission.decodeUav(actionParams[1]);
                uavAction = new Loiter(plan.getLastTimeFor(uav), obsReq.getMinDurationObj());
                plan.addUAVAction(uav, uavAction);

                suite = mission.getSensorSuiteOn(uav);
                suiteAction = new Monitor(plan.getLastTimeFor(suite),
                        obsReq);
                plan.addSensorSuiteAction(suite, suiteAction);
                break;
            case "MONITOR_TARGET":
                obsReq = mission.decodeObservationRequest(actionParams[4]);

                uav = mission.decodeUav(actionParams[1]);
                uavAction = new FlyThroughTarget(plan.getLastTimeFor(uav),
                        obsReq, mission.decodeLocation(actionParams[2]));
                plan.addUAVAction(uav, uavAction);
                /*if (lastTransferToTarget != null) {
                    lastTransferToTarget.setFromFirstPoint(
                            ((FlyThroughTarget) uavAction).isForward());
                }*/

                suite = mission.getSensorSuiteOn(uav);
                suiteAction = new Monitor(plan.getLastTimeFor(suite), obsReq);
                plan.addSensorSuiteAction(suite, suiteAction);
                break;
            case "LOITER_NO":
            case "LOITER":
                uav = mission.decodeUav(actionParams[1]);
                uavAction = new Loiter(plan.getLastTimeFor(uav),
                        actionParams[0].endsWith("NO"));
                plan.addUAVAction(uav, uavAction);

                suite = mission.getSensorSuiteOn(uav);
                suiteAction = new Idle(plan.getLastTimeFor(suite),
                        uavAction.getDuration());
                plan.addSensorSuiteAction(suite, suiteAction);
                break;
            default:
                // System.out.println("Ignoring line: '" + line + "'");
        }
    }

}
