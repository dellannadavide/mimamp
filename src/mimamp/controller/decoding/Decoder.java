package mimamp.controller.decoding;

import mimamp.model.Mission;
import mimamp.model.plan.Plan;
import mimamp.model.plan.actions.TransferToTarget;

import java.util.List;

/**
 *
 * @author davide
 */
public abstract class Decoder {

    private final Mission mission;
    private final Plan plan;
    private final boolean toMillis;
    private final String domain;

    private TransferToTarget lastTransferToTarget;



    public Decoder(Mission mission, Plan plan, String domain, boolean millis) {
        this.mission = mission;
        this.plan = plan;
        this.lastTransferToTarget = null;
        this.toMillis = millis;
        this.domain = domain;
    }

    public Mission getMission() {
        return mission;
    }

    public Plan getPlan() {
        return plan;
    }

    public TransferToTarget getLastTransferToTarget() {
        return lastTransferToTarget;
    }

    public void decode(List<String> planneroutput) {
        planneroutput.stream().forEach(action -> decodeAction(action));
    }

    abstract void decodeAction(String line);

    public boolean toMillis() {
        return toMillis;
    }

    public String getDomain() {
        return domain;
    }
}
