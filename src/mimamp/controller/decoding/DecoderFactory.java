package mimamp.controller.decoding;

import mimamp.model.Mission;
import mimamp.model.plan.Plan;

/**
 * Created by davide on 21/10/16.
 */
public class DecoderFactory {

    public Decoder getDecoder(String decoderType, Mission mission, Plan plan, String domain, boolean millis) {
        if (decoderType == null) {
            return null;
        }
        if (decoderType.equalsIgnoreCase("NUMERIC")) {
            return new NumericDecoder(mission, plan, domain, millis);

        } else if (decoderType.equalsIgnoreCase("TEMPORAL")) {
            return new TemporalDecoder(mission, plan, domain, millis);
        }

        return null;
    }
}
