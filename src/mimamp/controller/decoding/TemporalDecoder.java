package mimamp.controller.decoding;

import mimamp.controller.encoding.NumericEncoder;
import mimamp.model.*;
import mimamp.model.plan.Plan;
import mimamp.model.plan.SensorSuiteOnUAV;
import mimamp.model.plan.actions.*;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;

/**
 * Created by davide on 21/10/16.
 * The class is the decoder for temporal planners.
 * Plans of temporal planners contain also temporal information.
 */
public class TemporalDecoder extends Decoder {

    public TemporalDecoder(Mission mission, Plan plan, String domain, boolean millis) {
        super(mission, plan, domain, millis);
    }

    /**
     *
     * @param line the action reported by the planner, in the form <action_starting_time>: (<action_name> <parameter>*) [action_duration]
     */
    @Override
    void decodeAction(String line) {
        String clean_line = line.replace(":", "").replace("(", "").replace(")", "").replace("[", "").replace("]", "").replace("  ", " ");
        System.out.println(clean_line);
        String[] actionParams = clean_line.split(" ");

        Airport airport;
        Coordinates prevlocation;
        Coordinates nextlocation;
        ObservationRequest obsReq;

        DurativeUAVAction uavAction;
        UAV uav;

        DurativeSensorSuiteAction suiteAction;
        SensorSuiteOnUAV suite;

        Mission mission = getMission();
        Plan plan = getPlan();
        TransferToTarget lastTransferToTarget = getLastTransferToTarget();

        /**
         * le seguenti durate saranno in millisecondi dopo il calcolo
         */
        long actionStart;
        long actionDuration;
        actionStart = (long) (Double.parseDouble(actionParams[0])*(toMillis()?1:1000));
        actionDuration = (long) (Double.parseDouble(actionParams[actionParams.length-1])*(toMillis()?1:1000));

        LocalDateTime actionStartTime =  mission.getEarliestTime().plus(actionStart, ChronoUnit.MILLIS);

        switch (actionParams[1]) {
            case "TAKE_OFF":
                airport = mission.decodeAirport(actionParams[5]);

                uav = mission.decodeUav(actionParams[2]);
                uavAction = new TakeOff(plan.getLastTimeFor(uav, actionStartTime),
                        airport, actionParams[1].endsWith("PREV"), actionDuration); /*TODO: qui bisogna fixare la storia del succ*/
                plan.addUAVAction(uav, uavAction);

                suite = mission.getSensorSuiteOn(uav);
                suiteAction = new Transfer(plan.getLastTimeFor(suite),
                        uavAction.getDuration(), airport.getEndOfTakeoff());
                plan.addSensorSuiteAction(suite, suiteAction);
                break;
            case "LANDING":
                airport = mission.decodeAirport(actionParams[5]);

                uav = mission.decodeUav(actionParams[2]);
                uavAction = new Landing(plan.getLastTimeFor(uav, actionStartTime),
                        airport, actionParams[0].endsWith("SUCC"), actionDuration);
                plan.addUAVAction(uav, uavAction);

                suite = mission.getSensorSuiteOn(uav);
                suiteAction = new Transfer(plan.getLastTimeFor(suite),
                        uavAction.getDuration(), airport.getPosition());
                plan.addSensorSuiteAction(suite, suiteAction);
                break;
            case "TRANSFER_TO_TRG":
                prevlocation = mission.decodeLocation(actionParams[3]);
                nextlocation = mission.decodeLocation(actionParams[4]);


                uav = mission.decodeUav(actionParams[2]);
                lastTransferToTarget = new TransferToTarget(plan.getLastTimeFor(uav, actionStartTime),
                        mission.decodeTarget(actionParams[5]), prevlocation, nextlocation,
                        uav.getCruisingSpeed(true), actionDuration);
                plan.addUAVAction(uav, lastTransferToTarget);

                suite = mission.getSensorSuiteOn(uav);
                suiteAction = new Transfer(plan.getLastTimeFor(suite),
                        lastTransferToTarget.getDuration(), nextlocation);
                plan.addSensorSuiteAction(suite, suiteAction);
                break;
            case "TRANSFER_TO_LAND":
                airport = mission.decodeAirport(actionParams[5]);

                uav = mission.decodeUav(actionParams[2]);
                uavAction = new TransferToLand(plan.getLastTimeFor(uav, actionStartTime),
                        airport, mission.decodeLocation(actionParams[3]),
                        uav.getCruisingSpeed(true), actionDuration);
                plan.addUAVAction(uav, uavAction);

                suite = mission.getSensorSuiteOn(uav);
                suiteAction = new Transfer(plan.getLastTimeFor(suite),
                        uavAction.getDuration(), airport.getBeginOfLanding());
                plan.addSensorSuiteAction(suite, suiteAction);
                break;
            case "STAY_ON_TRG": /*a differenza del modello numerico questa azione ora riguarda solo l'uav*/
                obsReq = mission.decodeObservationRequest(actionParams[4]);
                uav = mission.decodeUav(actionParams[2]);

                /** TODO:  bisogna ripristinare il predicato not_same nel dominio e nel problema
                 *
                 *
                 */
                if(obsReq.targetIsPoint()) {
                    uavAction = new Loiter(plan.getLastTimeFor(uav, actionStartTime), actionDuration);
                }
                else {
                    uavAction = new FlyThroughTarget(plan.getLastTimeFor(uav, actionStartTime),
                            obsReq, mission.decodeLocation(actionParams[3]), actionDuration);
                    /*if (lastTransferToTarget != null) {
                        lastTransferToTarget.setFromFirstPoint(
                                ((FlyThroughTarget) uavAction).isForward());
                    }*/
                }
                plan.addUAVAction(uav, uavAction);
                break;
            case "MONITOR": /*a differenza del modello numerico questa azione ora riguarda solo la suite*/
                obsReq = mission.decodeObservationRequest(actionParams[3]);
                uav = mission.decodeUav(actionParams[2]);

                suite = mission.getSensorSuiteOn(uav);
                suiteAction = new Monitor(plan.getLastTimeFor(suite), obsReq, actionDuration);
                plan.addSensorSuiteAction(suite, suiteAction);
                break;
            /*TODO: introdurre le loiter*/
            /*case "LOITER_NO":
            case "LOITER":
                uav = mission.decodeUav(actionParams[1]);
                uavAction = new Loiter(plan.getLastTimeFor(uav, actionStartTime),
                        actionParams[0].endsWith("NO"));
                plan.addUAVAction(uav, uavAction);

                suite = mission.getSensorSuiteOn(uav);
                suiteAction = new Idle(plan.getLastTimeFor(suite),
                        uavAction.getDuration());
                plan.addSensorSuiteAction(suite, suiteAction);
                break;*/
            default:
                // System.out.println("Ignoring line: '" + line + "'");
        }
    }
}
