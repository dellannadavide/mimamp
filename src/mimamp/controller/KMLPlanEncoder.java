/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.controller;

import java.io.File;
import java.time.format.DateTimeFormatter;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import mimamp.model.Coordinates;
import mimamp.model.UAV;
import mimamp.model.plan.Plan;
import mimamp.model.plan.SensorSuiteOnUAV;
import mimamp.model.plan.SensorSuiteState;
import mimamp.model.plan.actions.DurativeSensorSuiteAction;
import mimamp.model.plan.actions.Monitor;
import mimamp.view.KMLIcons;

/**
 *
 * @author marco
 */
public class KMLPlanEncoder {

    private final Plan plan;
    private final DateTimeFormatter formatter;

    public KMLPlanEncoder(Plan plan) {
        this.plan = plan;
        formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
    }

    public void encode(File file) throws TransformerException, ParserConfigurationException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        // root element
        Document doc = docBuilder.newDocument();
        Element root = doc.createElement("kml");
        root.setAttribute("xmlns", "http://www.opengis.net/kml/2.2");
        root.setAttribute("xmlns:gx", "http://www.google.com/kml/ext/2.2");
        root.setAttribute("xmlns:kml", "http://www.opengis.net/kml/2.2");
        root.setAttribute("xmlns:atom", "http://www.w3.org/2005/Atom");
        doc.appendChild(root);

        // root folder
        Element mainFolder = doc.createElement("Folder");
        root.appendChild(mainFolder);

        // Name, open, (lookat)
        mainFolder.appendChild(textElement(doc, "name", plan.getName()));
        mainFolder.appendChild(textElement(doc, "open", "1"));

        // Folders
        plan.getSuites().forEach(suiteOnUav -> {
            Element uavFolder = doc.createElement("Folder");
            uavFolder.appendChild(textElement(doc, "name", suiteOnUav.getUAVDescription()));
            uavFolder.appendChild(createUAVFolder(doc, suiteOnUav.getUAV()));
            uavFolder.appendChild(createSensorsFolder(doc, suiteOnUav));
            mainFolder.appendChild(uavFolder);
        });

        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(file);
        transformer.transform(source, result);
    }

    private Element createUAVFolder(Document doc, UAV uav) {
        Element folder = doc.createElement("Folder");
        folder.appendChild(textElement(doc, "name", uav.toString()));
        folder.appendChild(textElement(doc, "open", "1"));

        Element document = doc.createElement("Document");
        document.appendChild(textElement(doc, "name", "Route.kml"));
        // Style tag
        Element style = doc.createElement("Style");
        style.setAttribute("id", "stile_rotta0022");
        Element iconStyle = doc.createElement("IconStyle");
        iconStyle.appendChild(textElement(doc, "scale", "0"));
        style.appendChild(iconStyle);
        Element lineStyle = doc.createElement("LineStyle");
        lineStyle.appendChild(textElement(doc, "color", "88cccccc"));
        lineStyle.appendChild(textElement(doc, "width", "8"));
        style.appendChild(lineStyle);
        document.appendChild(style);

        // Placemark tag
        Element placemark = doc.createElement("Placemark");
        placemark.appendChild(textElement(doc, "name",
                "Takeoff at " + formatter.format(plan.getStartTimeOf(uav))
                + ", landing at " + formatter.format(plan.getLastTimeFor(uav))));
        placemark.appendChild(textElement(doc, "styleUrl", "#stile_rotta0022"));
        Element lineString = doc.createElement("LineString");
        lineString.appendChild(textElement(doc, "tessellate", "1"));
        lineString.appendChild(textElement(doc, "altitudeMode", "relativeToGround"));
        lineString.appendChild(textElement(doc, "coordinates", plan.getKMLPathOf(uav)));
        placemark.appendChild(lineString);
        document.appendChild(placemark);

        folder.appendChild(document);
        return folder;
    }

    private Element createSensorsFolder(Document doc, SensorSuiteOnUAV suite) {
        Element folder = doc.createElement("Folder");
        folder.appendChild(textElement(doc, "name", suite.toString()));
        folder.appendChild(textElement(doc, "visibility", "0"));
        folder.appendChild(textElement(doc, "open", "1"));

        SensorSuiteState state = new SensorSuiteState(suite,
                plan.getAirportOf(suite.getUAV()), plan.getLastTimeFor(suite));
        Coordinates prevPosition = state.getPosition();
        for (DurativeSensorSuiteAction action : plan.getActionsOf(suite)) {
            action.execute(state);
            if (action instanceof Monitor) {
                Monitor m = (Monitor) action;
                Element document = doc.createElement("Document");
                document.appendChild(textElement(doc, "name", m.getObservationRequest().getTargetName()));
                document.appendChild(textElement(doc, "visibility", "0"));

                Element style = doc.createElement("Style");
                style.setAttribute("id", "iconStyle");
                Element iconStyle = doc.createElement("IconStyle");
                iconStyle.appendChild(textElement(doc, "scale", "2"));
                Element icon = doc.createElement("Icon");
                icon.appendChild(textElement(doc, "href", KMLIcons.ICON));
                iconStyle.appendChild(icon);
                style.appendChild(iconStyle);
                document.appendChild(style);

                style = doc.createElement("Style");
                style.setAttribute("id", "locStyle");
                Element lineStyle = doc.createElement("LineStyle");
                lineStyle.appendChild(textElement(doc, "color", m.getObservationRequest().getKMLSensorColor()));
                lineStyle.appendChild(textElement(doc, "width", "12"));
                style.appendChild(lineStyle);
                document.appendChild(style);

                Element placemark = doc.createElement("Placemark");
                placemark.appendChild(textElement(doc, "visibility", "0"));

                if (m.getObservationRequest().targetIsPoint()) {
                    placemark.appendChild(textElement(doc, "styleUrl", "#iconStyle"));
                    placemark.appendChild(textElement(doc, "name", action.toString().split("\n")[0]));
                    Element point = doc.createElement("Point");
                    point.appendChild(textElement(doc, "altitudeMode", "relativeToGround"));
                    Coordinates c = m.getObservationRequest().getTargetPosition();
                    String coord = c.getLongitude() + "," + c.getLatitude() + ","
                            + 4 * suite.getUAV().getCruisingAltitude();
                    point.appendChild(textElement(doc, "coordinates", coord));
                    placemark.appendChild(point);
                    document.appendChild(placemark);
                } else {
                    placemark.appendChild(textElement(doc, "styleUrl", "#locStyle"));
                    Element lineString = doc.createElement("LineString");
                    lineString.appendChild(textElement(doc, "tessellate", "1"));
                    lineString.appendChild(textElement(doc, "altitudeMode", "relativeToGround"));
                    List<Coordinates> path = m.getObservationRequest().getTarget().getPositions();
                    lineString.appendChild(textElement(doc, "coordinates", getPath(path, suite.getUAV())));
                    placemark.appendChild(lineString);
                    document.appendChild(placemark);

                    Coordinates first, last;
                    if (plan.isObservedForward(m.getObservationRequest())) {
                        first = m.getObservationRequest().getTargetFirstPosition();
                        last = m.getObservationRequest().getTargetLastPosition();
                    } else {
                        first = m.getObservationRequest().getTargetLastPosition();
                        last = m.getObservationRequest().getTargetFirstPosition();
                    }

                    // start
                    placemark = doc.createElement("Placemark");
                    placemark.appendChild(textElement(doc, "visibility", "0"));
                    placemark.appendChild(textElement(doc, "styleUrl", "#iconStyle"));
                    placemark.appendChild(textElement(doc, "name", m.getBeginOfActionStr()));
                    Element point = doc.createElement("Point");
                    point.appendChild(textElement(doc, "altitudeMode", "relativeToGround"));
                    String coord = first.getLongitude() + "," + first.getLatitude() + ","
                            + 4 * suite.getUAV().getCruisingAltitude();
                    point.appendChild(textElement(doc, "coordinates", coord));
                    placemark.appendChild(point);
                    document.appendChild(placemark);

                    // end
                    placemark = doc.createElement("Placemark");
                    placemark.appendChild(textElement(doc, "visibility", "0"));
                    placemark.appendChild(textElement(doc, "styleUrl", "#iconStyle"));
                    placemark.appendChild(textElement(doc, "name", m.getEndOfActionStr()));
                    point = doc.createElement("Point");
                    point.appendChild(textElement(doc, "altitudeMode", "relativeToGround"));
                    coord = last.getLongitude() + "," + last.getLatitude() + ","
                            + 4 * suite.getUAV().getCruisingAltitude();
                    point.appendChild(textElement(doc, "coordinates", coord));
                    placemark.appendChild(point);
                    document.appendChild(placemark);
                }
                folder.appendChild(document);
            }
            prevPosition = state.getPosition();
        }

        return folder;
    }

    private Element textElement(Document doc, String tag, String text) {
        Element element = doc.createElement(tag);
        element.setTextContent(text);
        return element;
    }

    private String getPath(List<Coordinates> path, UAV uav) {
        StringBuilder builder = new StringBuilder();
        path.stream().forEach((c) -> {
            builder.append(' ')
                    .append(c.getLongitude()).append(',')
                    .append(c.getLatitude()).append(',')
                    .append(4 * uav.getCruisingAltitude());
        });
        builder.deleteCharAt(0);
        return builder.toString();
    }
}
