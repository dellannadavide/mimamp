package mimamp.view;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.Window;
import mimamp.model.plan.Plan;

import java.awt.*;

/**
 * Created by davide on 23/11/16.
 */
public class MainFrame extends Application {
    private Stage stage;
    private MainBorderPaneFactory mainBorderPaneFactory;
    private MainMenu mainMenu;
    private Label introTips;

    @Override
    public void start(Stage welcomeStage) {
        stage = welcomeStage;
        mainBorderPaneFactory = new MainBorderPaneFactory();
        mainMenu = new MainMenu(this);

        introTips = new Label("");
        introTips.setStyle("-fx-text-alignment: center; -fx-font-weight: 100; -fx-font-size: 10pt;");
        introTips.setTextFill(Color.LIGHTGRAY);


        Button btnTargetInsertion = new Button("Data managing");
        btnTargetInsertion.setMaxWidth(Double.MAX_VALUE);
        btnTargetInsertion.setPadding(new Insets(20));
        btnTargetInsertion.setOnAction((ActionEvent event) -> {
            try {
                replaceSceneContent(Views.MAIN_TARGET_INSERTION, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        btnTargetInsertion.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> introTips.setText("Insert or edit targets and/or observation requests."));
        btnTargetInsertion.addEventHandler(MouseEvent.MOUSE_EXITED, e -> introTips.setText(""));

        Button btnMissionDef = new Button("Mission definition");
        btnMissionDef.setMaxWidth(Double.MAX_VALUE);
        btnMissionDef.setPadding(new Insets(20));
        btnMissionDef.setOnAction((ActionEvent event) -> {
            try {
                replaceSceneContent(Views.MAIN_MISSION_DEFINITION, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        btnMissionDef.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> introTips.setText("Define a new mission and find a plan for it."));
        btnMissionDef.addEventHandler(MouseEvent.MOUSE_EXITED, e -> introTips.setText(""));

        Button btnPlans = new Button("Planned missions");
        btnPlans.setMaxWidth(Double.MAX_VALUE);
        btnPlans.setPadding(new Insets(20));
        btnPlans.setOnAction((ActionEvent event) -> {
            try {
                replaceSceneContent(Views.MAIN_PLANNED_MISSIONS, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        btnPlans.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> introTips.setText("Examine already planned missions."));
        btnPlans.addEventHandler(MouseEvent.MOUSE_EXITED, e -> introTips.setText(""));

        Label introTitle = new Label("MIMAMP\n A Mixed-Initiative Multi Agent Mission Planner\n\n");
        //introTitle.setFont(Font.font("Sans", FontWeight.EXTRA_LIGHT, 21));
        introTitle.setStyle("-fx-text-alignment: center; -fx-font-weight: 100; -fx-font-size: 15pt;");
        introTitle.setTextFill(Color.LIGHTGRAY);
        //introTitle.setMaxWidth(500);
        Label credits = new Label("D. Dell'Anna, M. Roberti\nUniversity of Turin, Department of Computer Science, 2016\n");
        //introTitle.setFont(Font.font("Sans", FontWeight.EXTRA_LIGHT, 21));
        credits.setStyle("-fx-text-alignment: center; -fx-font-size: 10pt;");
        credits.setTextFill(Color.LIGHTGREY);
        Hyperlink link = new Hyperlink();
        link.setText("MIMAMP official webpage");
        link.setStyle("-fx-text-alignment: center; -fx-font-size: 10pt; -fx-underline: true;");
        link.setPadding(new Insets(0, 0, 0, 115));
        link.setTextFill(Color.LIGHTGREY);
        link.setOnAction(e -> getHostServices().showDocument("http://www.davidedellanna.com/resources/mimamp"));

        HBox introTitlePane = new HBox(introTitle);
        //introTitlePane.setMaxWidth(500);
        //introTitlePane.setMaxHeight(100);
//        introTitlePane.setSpacing(300);
        introTitlePane.setAlignment(Pos.CENTER);
        introTitlePane.setMargin(introTitle, new Insets(0, 0, -150, 0));

        GridPane creditsPane = new GridPane();
        creditsPane.setAlignment(Pos.CENTER);
        //creditsPane.setMargin(credits, new Insets(100, 0, -150, 0));
        creditsPane.add(credits, 0, 0);
        creditsPane.add(link, 0, 1);
        creditsPane.setPadding(new Insets(100, 0, -150, 0));

        BorderPane mainMenuPane =  new BorderPane();
        mainMenuPane.setMaxWidth(500);
        mainMenuPane.setMaxHeight(20);
        GridPane gp = new GridPane();
        gp.add(btnTargetInsertion, 0, 0);
        gp.add(btnMissionDef, 1, 0);
        gp.add(btnPlans, 2, 0);
        gp.setHgap(20);
        mainMenuPane.setTop(gp);

        HBox introTipsPane = new HBox(introTips);
        introTipsPane.setAlignment(Pos.CENTER);
        introTipsPane.setMargin(introTips, new Insets(20, 0, 0, 0));
        mainMenuPane.setCenter(introTipsPane);

        BorderPane mainFramePanel = new BorderPane();
        mainFramePanel.setTop(introTitlePane);
        mainFramePanel.setCenter(mainMenuPane);
        mainMenuPane.setBottom(creditsPane);

        BorderPane root = new BorderPane();
        root.setTop(mainMenu);
        root.setCenter(mainFramePanel);
        root.setStyle("-fx-background-color: #2b2b2b;");
        //Scene scene = new Scene(root, 350, 200);

        /*stage.setTitle("JavaFX Menu (o7planning.org)");
        stage.setScene(scene);
        stage.show();*/

        /*HBox root = new HBox(mainMenuPanel);
        root.setStyle("-fx-background-color: #2b2b2b;");
        root.setAlignment(Pos.CENTER);
*/
        Scene scene = new Scene(root);

        welcomeStage.setTitle("MIMAMP");
        welcomeStage.setScene(scene);
        welcomeStage.setWidth(620);
        welcomeStage.setHeight(450);
        welcomeStage.setResizable(false);
        welcomeStage.show();
        introTips.requestFocus();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public void replaceSceneContent(String view, Plan plan) throws Exception {
        Pane page = mainBorderPaneFactory.getMainBorderPane(view, this, plan);

        BorderPane root = new BorderPane();
        root.setTop(mainMenu);
        page.setPrefSize(stage.getWidth(), stage.getScene().getHeight()-mainMenu.getHeight());
        root.setCenter(page);

        stage.getScene().setRoot(root);
        stage.sizeToScene();
        centreWindow(stage);
        stage.setResizable(true);
        stage.setTitle("MIMAMP - "+view);
        mainMenu.updateMenuEnabling(view);
    }

    public void centreWindow(Window frame) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
        frame.setX(x);
        frame.setY(y);
    }

    public Stage getStage() {
        return stage;
    }
}