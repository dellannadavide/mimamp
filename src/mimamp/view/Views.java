package mimamp.view;

/**
 * Created by davide on 24/11/16.
 */
public final class Views {
    public static final String WELCOME = "MIMAMP";
    public static final String MAIN_TARGET_INSERTION = "Data Managing";
    public static final String MAIN_MISSION_DEFINITION = "Mission Definition";
    public static final String MAIN_PLANNED_MISSIONS = "Planned Mission";

}
