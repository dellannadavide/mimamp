package mimamp.view;

import java.awt.*;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;
import mimamp.controller.MissionController;
import mimamp.controller.planning.Planners;
import mimamp.controller.planning.PlannerFactory;
import mimamp.model.*;

/**
 *
 * @author marco
 */
public class MissionUI extends Stage {

    private final MissionController ctrl;

    private final AssignmentsPane assignmentsPane;
    private final TextField earliestTimePicker;
    private final TextField latestTimePicker;
    private final Spinner maxDurationSpinner;
    private final MultiObsPane multiObsPane;
    private final Spinner timeoutSpinner;
    private final ComboBox planners;
    private final ToggleGroup planning_model_group;
    private final PlannerFactory plannerFactory;
    private final ToggleGroup timeUnitGroup;
    private final ToggleGroup freeAssignmentGroup;
    private final ComboBox domains;

    private final MissionDefinitionUI mainPane;


    public MissionUI(MissionController ctrl, MissionDefinitionUI mainPane) {
        this.ctrl = ctrl;
        this.mainPane = mainPane;
        plannerFactory = new PlannerFactory();

        setTitle("Mission definition");

        earliestTimePicker = new TextField("00:00:00");
        latestTimePicker = new TextField("23:59:59");
        maxDurationSpinner = new Spinner(
                ctrl.getMinAllowedDuration(), TimeUnit.DAYS.toSeconds(1),
                TimeUnit.DAYS.toSeconds(1));
        maxDurationSpinner.setEditable(true);
        GridPane chronoPane = new GridPane();
        chronoPane.setMaxWidth(Double.MAX_VALUE);
        chronoPane.setHgap(10);
        chronoPane.setVgap(10);
        chronoPane.addRow(0, new Label("Earliest time"), earliestTimePicker);
        chronoPane.addRow(1, new Label("Latest time"), latestTimePicker);
        chronoPane.addRow(2, new Label("Maximum mission duration (secs)"), maxDurationSpinner);

        GridPane planningPane = new GridPane();
        planningPane.setMaxWidth(Double.MAX_VALUE);
        planningPane.setHgap(10);
        planningPane.setVgap(10);
        
        planning_model_group = new ToggleGroup();
        ToggleButton rb1 = new RadioButton("Numeric");
        rb1.setUserData(PlanningModels.NUMERIC);
        rb1.setToggleGroup(planning_model_group);
        rb1.setSelected(true);
        ToggleButton rb2 = new RadioButton("Temporal");
        rb2.setUserData(PlanningModels.TEMPORAL);
        rb2.setToggleGroup(planning_model_group);
        HBox rbContainer = new HBox(rb1,rb2);
        planningPane.addRow(0, new Label("Select planning model"), rbContainer);

        
        ObservableList numeric_planners = FXCollections.observableArrayList(Planners.getNumericalPlanners());
        
        ObservableList temporal_planners = FXCollections.observableArrayList(Planners.getTemporalPlanners());
        
        planners = new ComboBox(numeric_planners);
        planningPane.addRow(1, new Label("Select planner"), planners);

        domains = new ComboBox();
        planningPane.addRow(2, new Label("Select domain"), domains);

        timeUnitGroup = new ToggleGroup();
        ToggleButton sec = new RadioButton("Seconds");
        sec.setUserData("seconds");
        sec.setToggleGroup(timeUnitGroup);
        ToggleButton msec = new RadioButton("Milliseconds");
        msec.setUserData("millisec");
        msec.setToggleGroup(timeUnitGroup);
        HBox tuContainer = new HBox(sec,msec);
        planningPane.addRow(3, new Label("Select time unit"), tuContainer);

        freeAssignmentGroup = new ToggleGroup();
        ToggleButton assigned = new RadioButton("User defined");
        assigned.setUserData("false");
        assigned.setToggleGroup(freeAssignmentGroup);
        assigned.setSelected(true);
        ToggleButton free = new RadioButton("Automatic");
        free.setUserData("true");
        free.setToggleGroup(freeAssignmentGroup);
        HBox freeAssContainer = new HBox(assigned,free);
        planningPane.addRow(4, new Label("Select assignment type"), freeAssContainer);

        timeoutSpinner = new Spinner(10, Integer.MAX_VALUE, 60);
        timeoutSpinner.setEditable(true);
        planningPane.addRow(5, new Label("Planning time (secs)"), timeoutSpinner);

        domains.valueProperty().addListener(new ChangeListener<String>() {
            @Override public void changed(ObservableValue ov, String t, String t1) {
                if(domains.getValue()!=null) {
                    sec.setDisable(!Domains.getSecondsAvailable(domains.getValue().toString()));
                    msec.setDisable(!Domains.getMilliSecondsAvailable(domains.getValue().toString()));
                    if(!sec.isDisable())
                        sec.setSelected(true);
                    else
                    if(!msec.isDisable())
                        msec.setSelected(true);

                }
                else {
                    sec.setDisable(true);
                    msec.setDisable(true);
                }
            }
        });

        planners.valueProperty().addListener(new ChangeListener<String>() {
            @Override public void changed(ObservableValue ov, String t, String t1) {
                if(planners.getValue()!=null) {
                    if(freeAssignmentGroup.getSelectedToggle().getUserData().toString().equals("false"))
                        domains.setItems(FXCollections.observableArrayList(Domains.getDomains(planning_model_group.getSelectedToggle().getUserData().toString(), planners.getValue().toString())));
                    else
                        domains.setItems(FXCollections.observableArrayList(Domains.getDomainsFree(planning_model_group.getSelectedToggle().getUserData().toString(), planners.getValue().toString())));

                    if (domains.getItems().size() > 0)
                        domains.setValue(domains.getItems().get(0));
                }
            }
        });


        planning_model_group.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
            public void changed(ObservableValue<? extends Toggle> ov,
                                Toggle old_toggle, Toggle new_toggle) {
                if (planning_model_group.getSelectedToggle() != null)
                    if(planning_model_group.getSelectedToggle().getUserData().toString().equals(PlanningModels.NUMERIC))
                        planners.setItems(numeric_planners);
                    else planners.setItems(temporal_planners);
                if(planners.getItems().size()>0)
                    planners.setValue(planners.getItems().get(0));

                if(freeAssignmentGroup.getSelectedToggle().getUserData().toString().equals("false"))
                    domains.setItems(FXCollections.observableArrayList(Domains.getDomains(planning_model_group.getSelectedToggle().getUserData().toString(), planners.getValue().toString())));
                else
                    domains.setItems(FXCollections.observableArrayList(Domains.getDomainsFree(planning_model_group.getSelectedToggle().getUserData().toString(), planners.getValue().toString())));
                if(domains.getItems().size()>0)
                    domains.setValue(domains.getItems().get(0));
            }
        });

        planners.setValue(numeric_planners.get(0));


        assignmentsPane = new AssignmentsPane(
                ctrl.getRequests(), ctrl.getUAVs());
        assignmentsPane.setMaxWidth(Double.MAX_VALUE);

        freeAssignmentGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            public void changed(ObservableValue<? extends Toggle> ov,
                                Toggle old_toggle, Toggle new_toggle) {
                if (freeAssignmentGroup.getSelectedToggle() != null) {
                    if(freeAssignmentGroup.getSelectedToggle().getUserData().toString().equals("false")) {
                        domains.setItems(FXCollections.observableArrayList(Domains.getDomains(planning_model_group.getSelectedToggle().getUserData().toString(), planners.getValue().toString())));
                        assignmentsPane.setType(AssignmentsPane.USER);
                    }
                    else {
                        domains.setItems(FXCollections.observableArrayList(Domains.getDomainsFree(planning_model_group.getSelectedToggle().getUserData().toString(), planners.getValue().toString())));
                        assignmentsPane.setType(AssignmentsPane.AUTOMATIC);
                    }
                    if(domains.getItems().size()>0)
                        domains.setValue(domains.getItems().get(0));
                }
            }
        });

        assignmentsPane.setType(AssignmentsPane.USER);

        multiObsPane = new MultiObsPane(ctrl.getRequests());
        multiObsPane.setMaxWidth(Double.MAX_VALUE);

        Button confirmButton = new Button("Confirm!");
        confirmButton.setMaxWidth(Double.MAX_VALUE);
        confirmButton.setOnAction(click -> confirm());

        chronoPane.setPadding(new Insets(20));
        planningPane.setPadding(new Insets(20));
        assignmentsPane.setPadding(new Insets(20));
        multiObsPane.setPadding(new Insets(20));

        TabPane tabPane = new TabPane();
        Tab chronoTab = new Tab("Chronological settings", chronoPane);
        Tab planningTab = new Tab("Planning settings", planningPane);
        Tab assignmentsTab = new Tab("Assignments", assignmentsPane);
        Tab multiobsTab = new Tab("MultiObs", multiObsPane);
        tabPane.getTabs().setAll(chronoTab, planningTab, assignmentsTab, multiobsTab);


        BorderPane root = new BorderPane();
        root.setCenter(tabPane);
        root.setBottom(confirmButton);
        root.setMaxWidth(Double.MAX_VALUE);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(root);
        scrollPane.setFitToWidth(true);
        scrollPane.setFitToHeight(true);
        setScene(new Scene(scrollPane));
        sizeToScene();

        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - (mainPane.getWidth()/2)) / 2);
        int y = (int) ((dimension.getHeight() - (mainPane.getHeight()/2)) / 2);
        this.setX(x);
        this.setY(y);
        setResizable(true);
    }

    private void confirm() {
        // Check time bounds
        String earliestTimeStr = earliestTimePicker.getText();
        String latestTimeStr = latestTimePicker.getText();
        if (!(earliestTimeStr.matches("\\d\\d:\\d\\d:\\d\\d")
                && latestTimeStr.matches("\\d\\d:\\d\\d:\\d\\d"))) {
            Alert alert = new Alert(Alert.AlertType.WARNING,
                    "Insert valid time strings! Format is HH:MM:SS.");
            alert.setResizable(true);
            alert.show();
            return;
        }
        if (earliestTimeStr.compareTo(latestTimeStr) >= 0) {
            Alert alert = new Alert(Alert.AlertType.WARNING,
                    "Earliest time must precede the latest time!");
            alert.setResizable(true);
            alert.show();
            return;
        }
        LocalTime earliestTime = LocalTime.parse(earliestTimeStr);
        LocalTime latestTime = LocalTime.parse(latestTimeStr);
        Duration timeDelta = Duration.between(earliestTime, latestTime);
        Duration maxDuration = Duration.ofSeconds(
                ((Double) maxDurationSpinner.getValue()).longValue());
        if (maxDuration.compareTo(timeDelta) > 0) {
            maxDuration = timeDelta;
        }

        String assignmentsTestResponse = assignmentsPane.notComplete();
        if (!assignmentsTestResponse.equals("")) {
            Alert alert = new Alert(Alert.AlertType.WARNING, assignmentsTestResponse);
            alert.setResizable(true);
            alert.show();
            return;
        }

        ObservationRequest req;
        if(assignmentsPane.getType().equals(AssignmentsPane.USER)) {
            while ((req = ctrl.getInconsistentAssignment(assignmentsPane.getAssignments())) != null) {
                // Let the user edit the inconsistend value
                UAV uav = assignmentsPane.getAssignments().get(req).getUAV();
                long bestTimeSec = req.bestTimeFor(uav);
                int min = (int) Math.ceil(0.85 * bestTimeSec);
                int max = (int) Math.floor(1.15 * bestTimeSec);
                Dialog d = new SpinnerDialog("Inconsistent time value",
                        req.getTarget() + " can't be observed in " + req.getMinDuration(false) + " seconds!",
                        "New value (between " + min + " and " + max + " seconds): ",
                        min, max, (int) bestTimeSec);
                Optional<Integer> input = d.showAndWait();
                if (input.isPresent()) {
                    req.setMinDurationSec(input.get());
                } else {
                    return;
                }
            }
        }
        else {
            /*TODO: CASO NO ASSEGNAMENTI, PER ORA SI FA IL CALCOLO SULL'UAV PIÙ LENTO NELLA MISSIONE,
            * CALCOLARLI TUTTI E SCRIVERLI NEL DOMINIO, COSÌ POI SCEGLIE IL SISTEMA
            */
            UAV slowest_uav = null;
            for (UAVAvailability ua: assignmentsPane.getUAVSAvailability()) {
                UAV uav = ua.getUAV();
                if (slowest_uav == null || slowest_uav.getCruisingSpeed(true) > uav.getCruisingSpeed(true))
                    slowest_uav = uav;
            }
            while ((req = ctrl.getInconsistentObsReqs(assignmentsPane.getObservationRequests(), assignmentsPane.getUAVSAvailability())) != null) {
                long bestTimeSec = req.bestTimeFor(slowest_uav);
                int min = (int) Math.ceil(0.85 * bestTimeSec);
                int max = (int) Math.floor(1.15 * bestTimeSec);
                Dialog d = new SpinnerDialog("Inconsistent time value",
                        req.getTarget() + " can't be observed in " + req.getMinDuration(false) + " seconds!",
                        "New value (between " + min + " and " + max + " seconds): ",
                        min, max, (int) bestTimeSec);
                Optional<Integer> input = d.showAndWait();
                if (input.isPresent()) {
                    req.setMinDurationSec(input.get());
                } else {
                    return;
                }
            }
        }

        if (multiObsPane.notComplete()) {
            Alert alert = new Alert(Alert.AlertType.WARNING,
                    "MultiObs definition is not complete!");
            alert.setResizable(true);
            alert.show();
            return;
        }
        
        int timeout = (int) timeoutSpinner.getValue();
        System.out.println(timeout);
        String planning_model = planning_model_group.getSelectedToggle().getUserData().toString();
        String planner = planners.getValue().toString();
        String domain = domains.getValue().toString();

        ctrl.setMissionParams(earliestTime, latestTime, maxDuration, assignmentsPane.getUAVSAvailability(), assignmentsPane.getObservationRequests(), assignmentsPane.getAssignments(), multiObsPane.getMultiObs());

        boolean millis = timeUnitGroup.getSelectedToggle().getUserData().toString().equalsIgnoreCase("MILLISEC");

        PlanningProgressUI newPlanningProgressUI = new PlanningProgressUI(plannerFactory.getPlanner(planner, ctrl.getMission(), timeout, planning_model, domain, millis), mainPane);
        newPlanningProgressUI.initOwner(mainPane.getStage());
        newPlanningProgressUI.initModality(Modality.WINDOW_MODAL);
        newPlanningProgressUI.show();
        close();

    }
}
