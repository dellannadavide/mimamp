package mimamp.view;

import javafx.scene.Cursor;
import javafx.scene.control.*;

import java.util.Optional;

/**
 * Created by davide on 24/11/16.
 */
public class MainMenu extends MenuBar {
    Menu dataMenu;
    Menu missionMenu;
    Menu plannedMenu;

    MainFrame mainFrame;

    public MainMenu(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
        // Create menus
        //Menu fileMenu = new Menu("File");
        Label dataMenuLabel = new Label("Data Managing");
        dataMenu = new Menu();
        dataMenuLabel.setId(Views.MAIN_TARGET_INSERTION);
        dataMenu.setGraphic(dataMenuLabel);
        dataMenu.getGraphic().setCursor(Cursor.CLOSED_HAND);

        Label missionMenuLabel = new Label("Mission Definition");
        missionMenu = new Menu();
        missionMenuLabel.setId(Views.MAIN_MISSION_DEFINITION);
        missionMenu.setGraphic(missionMenuLabel);
        missionMenu.getGraphic().setCursor(Cursor.CLOSED_HAND);

        Label plannedMenuLabel = new Label("Planned Missions");
        plannedMenu = new Menu();
        plannedMenuLabel.setId(Views.MAIN_PLANNED_MISSIONS);
        plannedMenu.setGraphic(plannedMenuLabel);
        plannedMenu.getGraphic().setCursor(Cursor.CLOSED_HAND);


        // Create MenuItems
        /*MenuItem newItem = new MenuItem("New");
        MenuItem openFileItem = new MenuItem("Open File");
/*        MenuItem exitItem = new MenuItem("Exit");*//*

        MenuItem copyItem = new MenuItem("Copy");
        MenuItem pasteItem = new MenuItem("Paste");*/

        // Add menuItems to the Menus
        //fileMenu.getItems().addAll(newItem, openFileItem, exitItem);
//        editMenu.getItems().addAll(copyItem, pasteItem);

        // Add Menus to the MenuBar
        getMenus().addAll(dataMenu, missionMenu, plannedMenu);

        getMenus().stream().forEach(menu -> menu.getGraphic().setOnMouseClicked(event -> menuClick(menu.getGraphic().getId())));

        updateMenuEnabling(Views.WELCOME);
    }

    public void updateMenuEnabling(String view) {
        switch(view) {
            case Views.WELCOME:
                for (Menu m: getMenus())
                    m.setDisable(true);
                break;
            default:
                for (Menu m: getMenus())
                    m.setDisable(m.getGraphic().getId().equals(view));
                break;
        }
    }
    private void menuClick(String newPage) {
        Alert alert = new Alert(Alert.AlertType.WARNING,
                "Leaving this page you will lose all the\nunsaved changes, are you sure?",
                ButtonType.YES, ButtonType.NO);
        alert.setResizable(false);
        Optional<ButtonType> resp = alert.showAndWait();
        if (resp.get().equals(ButtonType.YES)) {
            try {
                mainFrame.replaceSceneContent(newPage, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
