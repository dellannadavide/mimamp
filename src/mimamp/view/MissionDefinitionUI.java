/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.view;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.javascript.JavascriptObject;
import com.lynden.gmapsfx.javascript.event.UIEventType;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MVCArray;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.MapShape;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;
import com.lynden.gmapsfx.shapes.Polygon;
import com.lynden.gmapsfx.shapes.PolygonOptions;
import com.lynden.gmapsfx.shapes.Polyline;
import com.lynden.gmapsfx.shapes.PolylineOptions;
import java.time.LocalDate;

import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.effect.BlendMode;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import netscape.javascript.JSObject;
import mimamp.controller.MissionDefiner;
import mimamp.controller.MissionController;
import mimamp.model.ObservationRequest;

/**
 *
 * @author marco
 */
class MissionDefinitionUI extends MainBorderPane {
    
    static final int MODE_DEFAULT = 0;
    static final int MODE_AREA_NW = 1;
    static final int MODE_AREA_SE = 2;

    static final String SET_AREA_START = "Set area of interest";
    static final String SET_AREA_CANCEL = "Cancel area setting";

    static final String TIP_DEFAULT = "Map loaded.";

    private final MissionDefiner ctrl;
    private GoogleMap map;
    private int mode;
    
    private Label availabilityLabel;
    private Polygon areaOfInterest;
    private MVCArray array;
    private final Button startPlanningButton;
    private final DatePicker datePicker;
    private final Label infoLabel;
    private final GoogleMapView mapView;
    private Marker nwMarker;
    private LatLong nwPosition;
    private final VBox rightPane;
    private final Button setAreaButton;
    private final ListView<ObservationRequest> requestListView;

    public MissionDefinitionUI(MissionDefiner ctrl, MainFrame mainScene) {
        super(mainScene);
        this.ctrl = ctrl;
        mode = MODE_DEFAULT;
        
        //setTitle("Mission definition");
        
        mapView = new GoogleMapView();
        mapView.addMapInializedListener(() -> {
            onMapInitialized();
        });

        datePicker = new DatePicker();
        
        datePicker.setMaxWidth(Double.MAX_VALUE);
        datePicker.setDisable(true);
        datePicker.setEditable(false);
        //datePicker.setDayCellFactory(picker -> new BoundedDateCell());
        datePicker.setOnAction(event -> onDateSet());
        
        availabilityLabel = new Label("<Pick a date>");
        availabilityLabel.setMinHeight(80);
        
        setAreaButton = new Button(SET_AREA_START);
        setAreaButton.setMaxWidth(Double.MAX_VALUE);
        setAreaButton.setDisable(true);
        setAreaButton.setOnAction((ActionEvent event) -> {
            startSetArea();
        });
        
        requestListView = new ListView<>();
        requestListView.setMaxWidth(Double.MAX_VALUE);
        requestListView.setDisable(true);
        requestListView.setCellFactory(CheckBoxListCell.forListView(req -> {
            ObservableValue<Boolean> val
                    = new SimpleBooleanProperty(ctrl.isSelected(req));
            val.addListener((observable, oldValue, newValue)
                    -> setObsRequestSelected(req, newValue));
            return val;
        }));
        requestListView.setOnMouseClicked(
                event -> updateInfoLabel(
                        requestListView.getSelectionModel().getSelectedItem()));
        
        startPlanningButton = new Button("Start planning");
        startPlanningButton.setMaxWidth(Double.MAX_VALUE);
        startPlanningButton.setDisable(true);
        startPlanningButton.setOnAction(action -> startPlanning());
        
        infoLabel = new Label("<Click on an item to see its details>");
        infoLabel.setPadding(new Insets(20));
        infoLabel.setMinHeight(120);
        
        rightPane = new VBox(new Label("Date of interest"), datePicker, availabilityLabel,
                setAreaButton, requestListView, startPlanningButton, infoLabel);
        rightPane.setPadding(new Insets(20));
        VBox.setMargin(setAreaButton, new Insets(10, 0, 10, 0));
        /*Pane mainPane = new BorderPane(mapView, null, rightPane, tips, null);
        setScene(new Scene(mainPane));
        setMaximized(true);*/
        setCenter(mapView);
        setRight(rightPane);

    }
    
    private void addRequestShape(ObservationRequest request) {
        JavascriptObject shape;
        if (request.targetIsPoint()) {
            shape = new Marker(
                    new MarkerOptions()
                    //.icon(Icons.TARGET)
                    .position(request.getTargetLatLongPosition())
                    .title(request.getTargetName()));
            map.addMarker((Marker) shape);
        } else {
            PolylineOptions opt = new PolylineOptions()
                    .path(new MVCArray(request.getPathArray()))
                    .strokeColor("BLUE")
                    .strokeWeight(3)
                    .clickable(true);
            
            shape = new Polyline(opt);
            map.addMapShape((MapShape) shape);
        }
        request.setTargetShape(shape);
    }
    
    private void cancelSetArea() {
        if (areaOfInterest != null) {
            map.removeMapShape(areaOfInterest);
        }
        mode = MODE_DEFAULT;
        getTipsLabel().setText(TIP_DEFAULT);
        setAreaButton.setText(SET_AREA_START);
        setAreaButton.setOnAction(event -> startSetArea());
    }
    
    private void onDateSet() {
        LocalDate date = datePicker.getValue();
        if (date != null) {
            ctrl.setDate(datePicker.getValue());
            availabilityLabel.setText(ctrl.getAvailabilitiesString());
            setAreaButton.setDisable(false);
        }
    }
    
    private void onMapClick(JSObject mouseEvent) {
        LatLong clickPosition = new LatLong((JSObject) mouseEvent.getMember("latLng"));
        switch (mode) {
            case MODE_AREA_NW:
                nwPosition = clickPosition;
                nwMarker = new Marker(new MarkerOptions()
                        .position(nwPosition));
                        //.icon(Icons.TEMP));
                map.addMarker(nwMarker);
                getTipsLabel().setText(TIP_SET_AREA_SE);
                mode = MODE_AREA_SE;
                break;
            case MODE_AREA_SE:
                Platform.runLater(() -> {
                    array.setAt(0, nwPosition);
                    array.setAt(1, new LatLong(nwPosition.getLatitude(),
                            clickPosition.getLongitude()));
                    array.setAt(2, clickPosition);
                    array.setAt(3, new LatLong(clickPosition.getLatitude(),
                            nwPosition.getLongitude()));
                    
                    PolygonOptions options = new PolygonOptions()
                            .paths(array)
                            .strokeColor("BLUE")
                            .strokeWeight(1)
                            .fillColor("TRANSPARENT")
                            .clickable(false);
                    areaOfInterest = new Polygon(options);
                    
                    ctrl.getFilteredRequests().stream()
                            .forEach(req -> {
                                JavascriptObject shape = req.getTargetShape();
                                if (shape != null) {
                                    if (shape instanceof Marker) {
                                        map.removeMarker((Marker) shape);
                                    } else {
                                        map.removeMapShape((MapShape) shape);
                                    }
                                }
                            });
                    
                    ctrl.setArea(nwPosition, clickPosition);
                    map.removeMarker(nwMarker);
                    map.addMapShape(areaOfInterest);
                    ctrl.getFilteredRequests().stream()
                            .forEach(req -> addRequestShape(req));
                    ctrl.getAirports().forEach(airport -> {
                        MarkerOptions airptOptions = new MarkerOptions()
                                .position(airport.getPosition().toLatLong())
                                //.icon(Icons.AIRPORT)
                                .title(airport.toString());
                        Marker m = new Marker(airptOptions);
                        map.addMarker(m);
                    });
                    
                    nwMarker = null;
                    setAreaButton.setText(SET_AREA_START);
                    setAreaButton.setOnAction(event -> startSetArea());
                    requestListView.setItems(ctrl.getFilteredRequests());
                    requestListView.setDisable(false);
                    getTipsLabel().setText(TIP_DEFAULT);
                    mode = MODE_DEFAULT;
                });
        }
    }
    
    private void onMapInitialized() {
        LatLong center = new LatLong(45.0734888, 7.6756065);
        MapOptions mapOptions = new MapOptions()
                .center(center)
                .mapTypeControl(false)
                .streetViewControl(false)
                .zoom(8)
                .zoomControl(false);
        map = mapView.createMap(mapOptions);
        array = new MVCArray(new LatLong[]{});
        
        map.addUIEventHandler(UIEventType.click,
                mouseEvent -> onMapClick(mouseEvent));
        
        datePicker.setDisable(false);
        getTipsLabel().setText(TIP_DEFAULT);
    }
    
    private void setObsRequestSelected(ObservationRequest req, boolean selected) {
        if (selected) {
            ctrl.selectObservationRequest(req);
        } else {
            ctrl.unselectObservationRequest(req);
        }
        
        if (ctrl.getSelectedRequestsCount() == 0) {
            startPlanningButton.setDisable(true);
        } else if (ctrl.checkCompatibilityOfSelectedRequests()) {
            startPlanningButton.setDisable(false);
        } else {
            startPlanningButton.setDisable(true);
            Alert alert = new Alert(Alert.AlertType.WARNING,
                    "The actual UAV availability can't satisfy the selected requests. "
                    + "Unselect one or more of them");
            alert.setResizable(true);
            alert.show();
        }
    }
    
    private void startPlanning() {
        //close();
        MissionUI newMission = new MissionUI(new MissionController(ctrl.getSelectedRequests()), this);
        newMission.initOwner(getStage());
        newMission.initModality(Modality.WINDOW_MODAL);
        newMission.show();

    }
    
    private void startSetArea() {
        mode = MODE_AREA_NW;
        getTipsLabel().setText(TIP_SET_AREA_NW);
        setAreaButton.setText(SET_AREA_CANCEL);
        setAreaButton.setOnAction(event -> cancelSetArea());
    }
    
    private void updateInfoLabel(ObservationRequest req) {
        if (req != null) {
            infoLabel.setText(req.getDetailString());
        }
    }

}
