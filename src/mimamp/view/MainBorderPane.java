package mimamp.view;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

/**
 * Created by davide on 24/11/16.
 */
public abstract class MainBorderPane extends BorderPane{
    private MainFrame mainScene;

    private final Label tips;

    static final String TIP_WAIT_MAP = "Wait a moment while loading map...";
    static final String TIP_SET_AREA_NW = "Click on the map to set the upper left area corner...";
    static final String TIP_SET_AREA_SE = "Click on the map to set the bottom right area corner...";
    static final String TIP_NEW_MARKER = "Click on the map to create the target...";
    static final String TIP_NEW_POLYLINE = "Click on the map to create the target's path...";
    static final String TIP_NEW_REQUEST = "Click on a target on the map...";


    public MainBorderPane(MainFrame mainScene){
        this.mainScene = mainScene;

        tips = new Label(TIP_WAIT_MAP);
        tips.setMaxWidth(Double.MAX_VALUE);
        tips.setPadding(new Insets(2));
        tips.setTextAlignment(TextAlignment.CENTER);

        setBottom(tips);

        setMinWidth(900);
        setMinHeight(600);
    }

    public MainFrame getMainScene() {
        return mainScene;
    }

    public Stage getStage() {
        return mainScene.getStage();
    }

    public Label getTipsLabel() {
        return tips;
    }
}
