/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.view;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.javascript.JavascriptObject;
import com.lynden.gmapsfx.javascript.event.UIEventType;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MVCArray;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.MapShape;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;
import com.lynden.gmapsfx.shapes.Polyline;
import com.lynden.gmapsfx.shapes.PolylineOptions;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import netscape.javascript.JSObject;
import mimamp.controller.TargetInsertionController;
import mimamp.model.Coordinates;
import mimamp.model.ObservationRequest;
import mimamp.model.Target;

/**
 *
 * @author marco
 */
class TargetInsertionUI extends MainBorderPane {
    
    static final int MODE_NULL = 0;
    static final int MODE_NEW_TARGET_POINT = 1;
    static final int MODE_SELECT_TARGET = 2;
    static final int MODE_EDITING_REQUEST = 3;
    static final int MODE_NEW_TARGET_LINE = 4;
    
    static final String REQUEST_START_CREATE = "Create a new observation request";
    static final String REQUEST_CANCEL_CREATE = "Cancel request creation";
    static final String REQUEST_CANCEL_EDIT = "Cancel request edit";
    
    static final String TARGET_POINT_START_CREATE = "Create a new target point";
    static final String TARGET_LINE_START_CREATE = "Create a new linear target";
    static final String TARGET_END_CREATE = "End target creation";
    static final String TARGET_CANCEL_CREATE = "Cancel target creation";

    static final String TIP_DEFAULT = "Select a target or a request to interact with it.";



    private final TargetInsertionController ctrl;
    private GoogleMap map;
    private int mode;
    private final Map<JavascriptObject, Target> targetMap;
    
    private final Button deleteButton;
    private final Button editButton;
    private final EditRequestView editRequestView;
    private final GoogleMapView mapView;
    private final Button newRequestButton;
    private final Button newTargetPointButton;
    private final Button newTargetLineButton;
    private final Label infoLabel;
    private ListView<ObservationRequest> requestList;
    private final Tab requestsTab;
    private final Pane rightPane;
    private ListView<Target> targetList;
    private final Tab targetsTab;

    public TargetInsertionUI(TargetInsertionController ctrl, MainFrame mainScene) {
        super(mainScene);
        this.ctrl = ctrl;

        this.mode = MODE_NULL;
        this.targetMap = new HashMap<>();
        
        //setTitle("Targets insertion");
        
        mapView = new GoogleMapView();
        mapView.addMapInializedListener(() -> onMapInitialized());
        
        newTargetPointButton = new Button(TARGET_POINT_START_CREATE);
        newTargetPointButton.setMaxWidth(Double.MAX_VALUE);
        newTargetPointButton.setDisable(true);
        newTargetPointButton.setOnAction(event -> startNewTargetPoint());
        
        newTargetLineButton = new Button(TARGET_LINE_START_CREATE);
        newTargetLineButton.setMaxWidth(Double.MAX_VALUE);
        newTargetLineButton.setDisable(true);
        newTargetLineButton.setOnAction(event -> startNewTargetLine());
        
        targetList = new ListView<>(ctrl.getTargets());
        targetList.setDisable(true);
        targetList.setOnMouseClicked(
                event -> onTargetListItemClick(
                        targetList.getSelectionModel().getSelectedItem()));
        
        targetsTab = new Tab("Targets",
                new VBox(newTargetPointButton, newTargetLineButton, targetList));
        targetsTab.setClosable(false);
        
        newRequestButton = new Button(REQUEST_START_CREATE);
        newRequestButton.setMaxWidth(Double.MAX_VALUE);
        newRequestButton.setDisable(true);
        newRequestButton.setOnAction(event -> startNewRequest());
        
        requestList = new ListView<>(ctrl.getObservationRequests());
        requestList.setDisable(true);
        requestList.setOnMouseClicked(
                event -> onRequestListItemClick(
                        requestList.getSelectionModel().getSelectedIndex()));
        
        requestsTab = new Tab(
                "Observation requests", new VBox(newRequestButton, requestList));
        requestsTab.setClosable(false);
        
        TabPane tabPane = new TabPane(targetsTab, requestsTab);
        tabPane.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> onTabChange(newValue));
        
        infoLabel = new Label("<Click on an item to see its details>");
        infoLabel.setPadding(new Insets(20));
        infoLabel.setMinHeight(150);
        
        editButton = new Button("Edit");
        editButton.setMaxWidth(Double.MAX_VALUE);
        editButton.setDisable(true);
        editButton.setOnAction(event -> onEditButtonClick());
        
        deleteButton = new Button("Delete");
        deleteButton.setMaxWidth(Double.MAX_VALUE);
        deleteButton.setDisable(true);
        deleteButton.setOnAction(event -> onDeleteButtonClick());
        
        editRequestView = new EditRequestView(ctrl, this);
        
        HBox buttonContainer = new HBox(10, editButton, deleteButton);
        
        rightPane = new VBox(tabPane, infoLabel, buttonContainer);
        rightPane.setPadding(new Insets(20));
/*        Pane mainPane = new BorderPane(mapView, null, rightPane, tipsLabel, null);
        setScene(new Scene(mainPane));
        setMaximized(true);*/
        setCenter(mapView);
        setRight(rightPane);
    }
    
    private void cancelNewTargetLine() {
        requestsTab.setDisable(false);
        newTargetLineButton.setText(TARGET_LINE_START_CREATE);
        newTargetLineButton.setOnAction(event -> startNewTargetLine());
        newTargetPointButton.setText(TARGET_POINT_START_CREATE);
        newTargetPointButton.setOnAction(event -> startNewTargetPoint());
        // CANCELLARE DALLA MAPPA!

        getTipsLabel().setText(TIP_DEFAULT);
        mode = MODE_NULL;
    }
    
    private void cancelNewTargetPoint() {
        requestsTab.setDisable(false);
        newTargetLineButton.setDisable(false);
        newTargetPointButton.setText(TARGET_POINT_START_CREATE);
        newTargetPointButton.setOnAction(event -> startNewTargetPoint());
        getTipsLabel().setText(TIP_DEFAULT);
        mode = MODE_NULL;
    }
    
    void endEditRequest(ObservationRequest req) {
        getTipsLabel().setText(TIP_DEFAULT);
        targetsTab.setDisable(false);
        newRequestButton.setText(REQUEST_START_CREATE);
        newRequestButton.setOnAction(event -> startNewRequest());
        rightPane.getChildren().set(1, infoLabel);
        rightPane.getChildren().get(2).setVisible(true);
        requestList.refresh();
        if (req != null) {
            requestList.getSelectionModel().select(req);
            updateRequestInfo(req);
        }
        mode = MODE_NULL;
    }
    
    private void endNewTargetLine() {
        Target target = ctrl.getTmpTarget();
        
        if (setTargetName(target)) {
            map.addUIEventHandler(target.getShape(), UIEventType.click,
                    mouseClick -> onShapeClick(target.getShape()));
            targetList.getSelectionModel().select(target);
        }
        
        requestsTab.setDisable(false);
        newTargetLineButton.setText(TARGET_LINE_START_CREATE);
        newTargetLineButton.setOnAction(event -> startNewTargetLine());
        newTargetPointButton.setText(TARGET_POINT_START_CREATE);
        newTargetPointButton.setOnAction(event -> startNewTargetPoint());
        getTipsLabel().setText(TIP_DEFAULT);
        mode = MODE_NULL;
    }
    
    private void onDeleteButtonClick() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                "Do you really want to delete this item and all related ones?");
        alert.setResizable(true);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            if (targetsTab.isSelected()) {
                Target target = targetList.getSelectionModel().getSelectedItem();
                if (target.isPoint()) {
                    map.removeMarker((Marker) target.getShape());
                } else {
                    map.removeMapShape((MapShape) target.getShape());
                }
                ctrl.deleteTarget(target);
                onTabChange(targetsTab);
            } else if (requestsTab.isSelected()) {
                ctrl.deleteObservationRequest(requestList.getSelectionModel().getSelectedItem());
                onTabChange(requestsTab);
            }
        }
    }
    
    private void onEditButtonClick() {
        if (targetsTab.isSelected()) {
            Target target = targetList.getSelectionModel().getSelectedItem();
            
            TextInputDialog d = new TextInputDialog(target.getName());
            d.setTitle("Target's name");
            d.setContentText("Name: ");
            Optional<String> result = d.showAndWait();
            if (result.isPresent() && result.get().length() > 0) {
                target.setName(result.get());
                ctrl.saveTarget(target);
                targetList.refresh();
            }
            
        } else if (requestsTab.isSelected()) {
            startEditRequest(requestList.getSelectionModel().getSelectedItem());
        }
    }

    /**
     * Puts a marker on the map and lets the user edit the new Observation
     * Request.
     */
    private void onMapClick(JSObject mouseEvent) {
        LatLong latLng;
        switch (mode) {
            case MODE_NEW_TARGET_POINT:
                latLng = new LatLong((JSObject) mouseEvent.getMember("latLng"));
                Target newTarget = new Target(new Coordinates(latLng));
                
                if (setTargetName(newTarget)) {
                    Marker marker = new Marker(new MarkerOptions()
                            //.icon(Icons.TARGET)
                            .position(latLng)
                            .title(newTarget.getName()));
                    map.addMarker(marker);
                    map.addUIEventHandler(marker, UIEventType.click,
                            mouseClick -> onShapeClick(marker));
                    newTarget.setShape(marker);
                    targetMap.put(marker, newTarget);
                    targetList.getSelectionModel().select(newTarget);
                }
                
                requestsTab.setDisable(false);
                newTargetPointButton.setText(TARGET_POINT_START_CREATE);
                newTargetPointButton.setOnAction(event -> startNewTargetPoint());
                newTargetLineButton.setDisable(false);
                getTipsLabel().setText(TIP_DEFAULT);
                mode = MODE_NULL;
                break;
            case MODE_NEW_TARGET_LINE:
                latLng = new LatLong((JSObject) mouseEvent.getMember("latLng"));
                Target t = ctrl.getTmpTarget(new Coordinates(latLng));
                if (t.isPoint()) {
                    Marker marker = new Marker(new MarkerOptions()
                            .position(latLng));
                            //.icon(Icons.TEMP));
                    map.addMarker(marker);
                    t.setShape(marker);
                    targetMap.put(marker, t);
                } else {
                    if (t.getShape() instanceof Marker) {
                        map.removeMarker((Marker) t.getShape());
                    } else {
                        map.removeMapShape((MapShape) t.getShape());
                    }
                    PolylineOptions opt = new PolylineOptions()
                            .path(new MVCArray(t.getPathArray()))
                            .strokeColor("BLUE")
                            .strokeWeight(3)
                            .clickable(true);
                    Polyline line = new Polyline(opt);
                    map.addMapShape(line);
                    t.setShape(line);
                    targetMap.put(line, t);
                }
                break;
        }
    }

    /**
     * Centers the map on Turin and adds all ObservationRequests' positions.
     */
    private void onMapInitialized() {
        LatLong center = new LatLong(45.0734888, 7.6756065);
        MapOptions mapOptions = new MapOptions()
                .center(center)
                .mapTypeControl(false)
                .streetViewControl(false)
                .zoom(8)
                .zoomControl(false);
        map = mapView.createMap(mapOptions);
        ctrl.getTargets().forEach(target -> {
            JavascriptObject shape;
            if (target.isPoint()) {
                MarkerOptions options = new MarkerOptions()
                        .position(target.getFirstPosition().toLatLong())
                        //.icon(Icons.TARGET)
                        .title(target.getName());
                shape = new Marker(options);
                map.addMarker((Marker) shape);
            } else {
                PolylineOptions opt = new PolylineOptions()
                        .path(new MVCArray(target.getPathArray()))
                        .strokeColor("BLUE")
                        .strokeWeight(3)
                        .clickable(true);
                shape = new Polyline(opt);
                map.addMapShape((MapShape) shape);
            }
            map.addUIEventHandler(shape, UIEventType.click,
                    mouseClick -> onShapeClick(shape));
            target.setShape(shape);
            targetMap.put(shape, target);
        });
        map.addUIEventHandler(UIEventType.click,
                mouseClick -> onMapClick(mouseClick));
        
        targetList.setDisable(false);
        requestList.setDisable(false);
        newTargetPointButton.setDisable(false);
        newTargetLineButton.setDisable(false);
        newRequestButton.setDisable(false);
        getTipsLabel().setText(TIP_DEFAULT);
    }
    
    private void onShapeClick(JavascriptObject jso) {
        switch (mode) {
            case MODE_SELECT_TARGET:
                startEditRequest(new ObservationRequest(targetMap.get(jso)));
                break;
            case MODE_NULL:
                if (targetsTab.isSelected()) {
                    targetList.getSelectionModel().select(targetMap.get(jso));
                }
                break;
        }
    }
    
    private void onRequestListItemClick(int index) {
        if (mode == MODE_NULL && index >= 0) {
            editButton.setDisable(false);
            deleteButton.setDisable(false);
            updateRequestInfo(ctrl.getObservationRequest(index));
        }
    }
    
    private void onTabChange(Tab selectedTab) {
        boolean active
                = (selectedTab == targetsTab && !targetList.getSelectionModel().isEmpty())
                || (selectedTab == requestsTab && !requestList.getSelectionModel().isEmpty());
        editButton.setDisable(!active);
        deleteButton.setDisable(!active);
    }
    
    private void onTargetListItemClick(Target target) {
        if (mode == MODE_NULL && target != null) {
            editButton.setDisable(false);
            deleteButton.setDisable(false);
            updateTargetInfo(target);
        }
    }
    
    private boolean setTargetName(Target target) {
        Dialog d = new TextInputDialog();
        d.setTitle("New target's name");
        d.setContentText("Name: ");
        Optional<String> result = d.showAndWait();
        if (result.isPresent()) {
            String name = result.get();
            if (name.length() == 0) {
                name = "noName";
            }
            target.setName(name);
            ctrl.saveTarget(target);
            return true;
        }
        return false;
    }
    
    private void startEditRequest(ObservationRequest req) {
        newRequestButton.setText(REQUEST_CANCEL_EDIT);
        newRequestButton.setOnAction(event -> endEditRequest(req));
        getTipsLabel().setText(TIP_DEFAULT);
        editRequestView.setObservationRequest(req);
        rightPane.getChildren().set(1, editRequestView);
        rightPane.getChildren().get(2).setVisible(false);
        mode = MODE_EDITING_REQUEST;
    }
    
    private void startNewRequest() {
        targetsTab.setDisable(true);
        newRequestButton.setText(REQUEST_CANCEL_CREATE);
        newRequestButton.setOnAction(event -> endEditRequest(null));
        getTipsLabel().setText(TIP_NEW_REQUEST);
        mode = MODE_SELECT_TARGET;
    }
    
    private void startNewTargetLine() {
        requestsTab.setDisable(true);
        newTargetPointButton.setText(TARGET_END_CREATE);
        newTargetPointButton.setOnAction(event -> endNewTargetLine());
        newTargetLineButton.setText(TARGET_CANCEL_CREATE);
        newTargetLineButton.setOnAction(event -> cancelNewTargetLine());
        getTipsLabel().setText(TIP_NEW_POLYLINE);
        mode = MODE_NEW_TARGET_LINE;
    }
    
    private void startNewTargetPoint() {
        requestsTab.setDisable(true);
        newTargetPointButton.setText(TARGET_CANCEL_CREATE);
        newTargetPointButton.setOnAction(event -> cancelNewTargetPoint());
        newTargetLineButton.setDisable(true);
        getTipsLabel().setText(TIP_NEW_MARKER);
        mode = MODE_NEW_TARGET_POINT;
    }
    
    private void updateRequestInfo(ObservationRequest req) {
        infoLabel.setText(req.getDetailString());
    }
    
    private void updateTargetInfo(Target target) {
        infoLabel.setText(target.getDetailString());
    }

}
