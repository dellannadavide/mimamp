/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.view;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import mimamp.model.MultiObs;
import mimamp.model.ObservationRequest;

/**
 *
 * @author marco
 */
class MultiObsPane extends VBox {

    private final List<ObservationRequest> requests;

    public MultiObsPane(List<ObservationRequest> requests) {
        this.requests = requests;

        Button addMultiObsButton = new Button("Add");
        addMultiObsButton.setOnAction(click -> addMultiObs());

        Label label = new Label("Constraints on request pairs (MultiObs)");
        label.setFont(Font.font(null, FontWeight.BOLD, 16));

        getChildren().addAll(label, addMultiObsButton);
    }

    private void addMultiObs() {
        getChildren().add(getChildren().size() - 1, new Row());
        getScene().getWindow().sizeToScene();
    }

    Set<MultiObs> getMultiObs() {
        Set<MultiObs> multiObs = new HashSet<>();
        getChildren().stream()
                .filter(node -> node instanceof Row)
                .map(node -> (Row) node)
                .forEach(row -> multiObs.add(row.getMultiObs()));
        return multiObs;
    }

    boolean notComplete() {
        return getChildren().stream()
                .filter(node -> node instanceof Row)
                .map(node -> (Row) node)
                .anyMatch(row -> row.notComplete());
    }

    private class Row extends HBox {

        private final ChoiceBox<ObservationRequest> req1Choice;
        private final ChoiceBox<ObservationRequest> req2Choice;
        private final ChoiceBox<MultiObs.Constraint> constraintChoice;

        public Row() {
            req1Choice = new ChoiceBox<>(
                    FXCollections.observableList(requests));
            req1Choice.setOnAction(action -> filterChoice(req1Choice.getValue()));
            req1Choice.setDisable(true);

            constraintChoice = new ChoiceBox<>(
                    FXCollections.observableArrayList(MultiObs.Constraint.values()));
            constraintChoice.setOnAction(action -> enableRequestChoice());

            req2Choice = new ChoiceBox<>();
            req2Choice.setOnAction(action -> getScene().getWindow().sizeToScene());
            req2Choice.setDisable(true);

            Button deleteButton = new Button("Delete");
            deleteButton.setOnAction(click -> removeThis());

            setSpacing(10);
            getChildren().addAll(req1Choice, constraintChoice,
                    req2Choice, deleteButton);
        }

        private void enableRequestChoice() {
            req1Choice.getSelectionModel().clearSelection();
            req1Choice.setDisable(constraintChoice.getValue() == null);
            req2Choice.setItems(FXCollections.observableList(requests));
            req2Choice.getSelectionModel().clearSelection();
            req2Choice.setDisable(true);
        }

        private void filterChoice(ObservationRequest req1) {
            req2Choice.setDisable(req1Choice.getValue() == null);
            req2Choice.getSelectionModel().clearSelection();
            if (!req2Choice.isDisable()) {
                req2Choice.setItems(FXCollections.observableList(
                        requests.stream()
                        .filter(req2 -> {
                            switch (constraintChoice.getValue()) {
                                case BEFORE:
                                    return !req1.equals(req2)
                                    && req1.getEarliestTime().isBefore(
                                            req2.getLatestTime());
                                case OVERLAP:
                                    return !req1.equals(req2)
                                    && req1.getLatestTime().isAfter(
                                            req2.getEarliestTime());
                                default:
                                    return false;
                            }
                        })
                        .collect(Collectors.toList())
                ));
            }
            getScene().getWindow().sizeToScene();
        }

        private MultiObs getMultiObs() {
            return new MultiObs(req1Choice.getValue(), req2Choice.getValue(),
                    constraintChoice.getValue());
        }

        private boolean notComplete() {
            return req1Choice.getValue() == null
                    || constraintChoice.getValue() == null
                    || req2Choice.getValue() == null;
        }

        private void removeThis() {
            MultiObsPane.this.getChildren().remove(this);
            MultiObsPane.this.getScene().getWindow().sizeToScene();
        }
    }
}
