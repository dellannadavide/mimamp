/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.view;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.javascript.JavascriptObject;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MVCArray;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.MapShape;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;
import com.lynden.gmapsfx.shapes.Circle;
import com.lynden.gmapsfx.shapes.CircleOptions;
import com.lynden.gmapsfx.shapes.Polyline;
import com.lynden.gmapsfx.shapes.PolylineOptions;
import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.CheckBoxTreeCell;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import mimamp.controller.MissionPlannedController;
import mimamp.model.ClusteredRequest;
import mimamp.model.Mission;
import mimamp.model.ObservationRequest;
import mimamp.model.UAV;
import mimamp.model.plan.Plan;
import mimamp.model.plan.SensorSuiteOnUAV;
import mimamp.model.plan.SensorSuiteState;
import mimamp.model.plan.UAVState;
import mimamp.model.plan.actions.DurativeAction;
import mimamp.model.plan.actions.FlyThroughTarget;
import mimamp.model.plan.actions.Landing;
import mimamp.model.plan.actions.Loiter;
import mimamp.model.plan.actions.Monitor;
import mimamp.model.plan.actions.TakeOff;
import mimamp.model.plan.actions.TransferToLand;
import mimamp.model.plan.actions.TransferToTarget;

/**
 *
 * @author marco
 */
public class PlannedMissionsUI extends MainBorderPane{

    private final MissionPlannedController ctrl;
    private GoogleMap map;
    private DurativeAction lastAction;

    private final GoogleMapView mapView;
    private final TreeView planTree;
    private final ListView<String/*Plan*/> planListView;
    private final Pane rightPane;
    private final Set<JavascriptObject> shapesOnMap;

    static final String TIP_DEFAULT = "Map loaded.";

    PlannedMissionsUI(MissionPlannedController ctrl, MainFrame mainScene) {
        super(mainScene);
        this.ctrl = ctrl;

        mapView = new GoogleMapView();
        mapView.addMapInializedListener(() -> {
            onMapInitialized();
        });
        shapesOnMap = new HashSet<>();

        planListView = new ListView<>(FXCollections.observableList(
                Arrays.asList(new String[]{
                    })));
        planListView.setMaxWidth(Double.MAX_VALUE);

        Button openPlanBtn = new Button("Open");
        openPlanBtn.setMaxWidth(Double.MAX_VALUE);

        Button deletePlanBtn = new Button("Delete");
        deletePlanBtn.setMaxWidth(Double.MAX_VALUE);

        HBox actionButtons = new HBox(openPlanBtn, deletePlanBtn);
        actionButtons.setMaxWidth(Double.MAX_VALUE);

        planTree = new TreeView();
        planTree.setMaxWidth(Double.MAX_VALUE);
        planTree.setCellFactory(CheckBoxTreeCell.<String>forTreeView());

        Button saveKMLBtn = new Button("Save plan as KML file");
        saveKMLBtn.setMaxWidth(Double.MAX_VALUE);
        saveKMLBtn.setOnAction(action -> saveKML());

        rightPane = new VBox(
                new Label("Missions successfully planned"), planListView, actionButtons,
                new Label("Show/hide plan components"), planTree, saveKMLBtn);
        rightPane.setPadding(new Insets(20));
        rightPane.setMinWidth(400);
        VBox.setMargin(actionButtons, new Insets(0, 0, 10, 0));
        setCenter(mapView);
        setRight(rightPane);
    }

    private void clearMap() {
        shapesOnMap.forEach(jso -> {
            if (jso instanceof Marker) {
                map.removeMarker((Marker) jso);
            } else {
                map.removeMapShape((MapShape) jso);
            }
        });
        shapesOnMap.clear();
    }

    private void drawActualPlanOnMap() {
        clearMap();

        CheckBoxTreeItem root = new CheckBoxTreeItem(ctrl.getPlanName(), null, true);
        ctrl.getActualPlan().getAirports().forEach(airport -> {
            MarkerOptions options = new MarkerOptions()
                    .position(airport.getPosition().toLatLong())
                    //.icon(Icons.AIRPORT)
                    .title(airport.toString());
            Marker m = new Marker(options);
            map.addMarker(m);
            shapesOnMap.add(m);
        });
        ctrl.getActualPlan().getSuites().forEach(suite -> {
            CheckBoxTreeItem suiteOnUavItem = new CheckBoxTreeItem(suite.getUAVDescription(), null, true);
            suiteOnUavItem.getChildren().add(drawUavPlan(suite.getUAV()));
            suiteOnUavItem.getChildren().add(drawSuitePlan(suite));
            root.getChildren().add(suiteOnUavItem);
        });

        planTree.setRoot(root);
        expandTreeRec(root);
    }

    private CheckBoxTreeItem drawUavPlan(UAV uav) {
        CheckBoxTreeItem treeItem = new CheckBoxTreeItem(uav, null, true);
        Plan plan = ctrl.getActualPlan();
        UAVState state = new UAVState(uav,
                plan.getAirportOf(uav), plan.getStartTimeOf(uav));
        lastAction = null;
        ctrl.getActionsOf(uav).forEach(action -> {
            LatLong previousPosition = state.getPosition().toLatLong();
            action.execute(state);
            if (action instanceof Loiter) {
                if (!(lastAction instanceof Loiter)) {
                    CircleOptions options = new CircleOptions()
                            .center(previousPosition)
                            .radius(Mission.CLUSTER_RADIUS)
                            .strokeColor("GREY")
                            .strokeWeight(3)
                            .fillColor("TRANSPARENT")
                            .clickable(false);
                    Circle circle = new Circle(options);
                    map.addMapShape(circle);
                    shapesOnMap.add(circle);
                    CheckBoxTreeItem actionItem = new CheckBoxTreeItem(action, null, true);
                    actionItem.addEventHandler(CheckBoxTreeItem.<String>checkBoxSelectionChangedEvent(),
                            event -> setShapeVisible(circle, actionItem.isSelected()));
                    treeItem.getChildren().add(actionItem);
                }
            } else if (action instanceof FlyThroughTarget) {
                PolylineOptions pathOptions = new PolylineOptions()
                        .path(new MVCArray(((FlyThroughTarget) action).getPathArray()))
                        .strokeColor("GREY")
                        .strokeWeight(3)
                        .clickable(false);
                Polyline poly = new Polyline(pathOptions);
                map.addMapShape(poly);
                shapesOnMap.add(poly);
                CheckBoxTreeItem actionItem = new CheckBoxTreeItem(action, null, true);
                actionItem.addEventHandler(CheckBoxTreeItem.<String>checkBoxSelectionChangedEvent(),
                        event -> setShapeVisible(poly, actionItem.isSelected()));
                treeItem.getChildren().add(actionItem);
            } else if (action instanceof TransferToLand
                    || action instanceof TransferToTarget) {
                LatLong[] path = new LatLong[]{
                    previousPosition,
                    state.getPosition().toLatLong()
                };
                PolylineOptions pathOptions = new PolylineOptions()
                        .path(new MVCArray(path))
                        .strokeColor("GREY")
                        .strokeWeight(3)
                        .clickable(false);
                Polyline poly = new Polyline(pathOptions);
                map.addMapShape(poly);
                shapesOnMap.add(poly);
                CheckBoxTreeItem actionItem = new CheckBoxTreeItem(action, null, true);
                actionItem.addEventHandler(CheckBoxTreeItem.<String>checkBoxSelectionChangedEvent(),
                        event -> setShapeVisible(poly, actionItem.isSelected()));
                treeItem.getChildren().add(actionItem);
            } else if (action instanceof TakeOff
                    || action instanceof Landing) {
                Object[] array = new Object[]{
                    previousPosition,
                    state.getPosition().toLatLong()
                };
                PolylineOptions options = new PolylineOptions()
                        .path(new MVCArray(array))
                        .strokeColor("BLACK")
                        .strokeWeight(3)
                        .clickable(false);
                Polyline poly = new Polyline(options);
                map.addMapShape(poly);
                shapesOnMap.add(poly);
                CheckBoxTreeItem actionItem = new CheckBoxTreeItem(action, null, true);
                actionItem.addEventHandler(CheckBoxTreeItem.<String>checkBoxSelectionChangedEvent(),
                        event -> setShapeVisible(poly, actionItem.isSelected()));
                treeItem.getChildren().add(actionItem);
            }

            lastAction = action;
        });

        return treeItem;
    }

    private CheckBoxTreeItem drawSuitePlan(SensorSuiteOnUAV suite) {
        CheckBoxTreeItem treeItem = new CheckBoxTreeItem(suite, null, true);
        Plan plan = ctrl.getActualPlan();
        SensorSuiteState state = new SensorSuiteState(suite,
                plan.getAirportOf(suite.getUAV()), plan.getStartTimeOf(suite.getUAV()));
        lastAction = null;

        ctrl.getActionsOf(suite).forEach(action -> {
            action.execute(state);
            if (action instanceof Monitor) {
                ObservationRequest request = ((Monitor) action).getObservationRequest();
                if (request instanceof ClusteredRequest) {
                    Set<JavascriptObject> clusterShapes = new HashSet<>();
                    ((ClusteredRequest) request).getRepresentedRequests()
                            .forEach(req -> {
                                MarkerOptions options = new MarkerOptions()
                                .position(req.getTargetLatLongPosition())
                                //.icon(Icons.TARGET)
                                .title(action.toString());
                                Marker m = new Marker(options);
                                map.addMarker(m);
                                shapesOnMap.add(m);
                                clusterShapes.add(m);
                            });
                    CircleOptions options = new CircleOptions()
                            .center(request.getTargetLatLongPosition())
                            .radius(Mission.CLUSTER_RADIUS)
                            .strokeWeight(2)
                            .strokeColor(request.getSensorColor())
                            .fillColor(request.getSensorColor())
                            .fillOpacity(0.5)
                            .clickable(false);
                    Circle circle = new Circle(options);
                    map.addMapShape(circle);
                    shapesOnMap.add(circle);
                    clusterShapes.add(circle);
                    CheckBoxTreeItem actionItem = new CheckBoxTreeItem(action, null, true);
                    actionItem.addEventHandler(CheckBoxTreeItem.<String>checkBoxSelectionChangedEvent(),
                            event -> clusterShapes.forEach(shape -> setShapeVisible(shape, actionItem.isSelected())));
                    treeItem.getChildren().add(actionItem);

                } else if (request.targetIsPoint()) {
                    MarkerOptions options = new MarkerOptions()
                            .position(request.getTargetLatLongPosition())
                            //.icon(Icons.TARGET)
                            .title(action.toString());
                    Marker marker = new Marker(options);
                    map.addMarker(marker);
                    shapesOnMap.add(marker);
                    CheckBoxTreeItem actionItem = new CheckBoxTreeItem(action, null, true);
                    actionItem.addEventHandler(CheckBoxTreeItem.<String>checkBoxSelectionChangedEvent(),
                            event -> setShapeVisible(marker, actionItem.isSelected()));
                    treeItem.getChildren().add(actionItem);
                } else {
                    PolylineOptions options = new PolylineOptions()
                            .path(new MVCArray(request.getPathArray()))
                            .strokeColor(request.getSensorColor())
                            .strokeWeight(5)
                            .clickable(false);
                    Polyline poly = new Polyline(options);
                    map.addMapShape(poly);
                    shapesOnMap.add(poly);
                    CheckBoxTreeItem actionItem = new CheckBoxTreeItem(action, null, true);
                    actionItem.addEventHandler(CheckBoxTreeItem.<String>checkBoxSelectionChangedEvent(),
                            event -> setShapeVisible(poly, actionItem.isSelected()));
                    treeItem.getChildren().add(actionItem);
                }
                // (colore in base al sensore?)
            }
            lastAction = action;
        });

        return treeItem;
    }

    private void expandTreeRec(TreeItem item) {
        item.setExpanded(true);
        item.getChildren().stream()
                .forEach(child -> expandTreeRec((TreeItem) child));
    }

    private void onMapInitialized() {
        System.out.println("disegno il piano");
        LatLong center = new LatLong(45.0734888, 7.6756065);
        MapOptions mapOptions = new MapOptions()
                .center(center)
                .mapTypeControl(false)
                .streetViewControl(false)
                .zoom(8)
                .zoomControl(false);
        map = mapView.createMap(mapOptions);

        getTipsLabel().setText(TIP_DEFAULT);

        if (ctrl.hasActualPlan()) {
            drawActualPlanOnMap();
        }
    }

    private void saveKML() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save mission plan as KML");
        File kmlFile = fileChooser.showSaveDialog(new Stage());
        ctrl.saveKML(kmlFile);
    }

    private void setShapeVisible(JavascriptObject shape, boolean visible) {
        if (shape instanceof Marker) {
            ((Marker) shape).setVisible(visible);
        } else if (shape instanceof MapShape) {
            ((MapShape) shape).setVisible(visible);
        }
    }

}
