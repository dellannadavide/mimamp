package mimamp.view;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import mimamp.model.ObservationRequest;
import mimamp.model.UAV;
import mimamp.model.UAVAvailability;

/**
 *
 * @author marco
 */
public class AssignmentsPane extends GridPane {
    public final static String USER = "user";
    public final static String AUTOMATIC = "automatic";

    private final UAVAvailability[] availabilities_uavs;
    private final UAVAvailability[][] availabilities_req_uavs;
    private final ObservationRequest[] requests;
    private final UAV[] uavs;
    private final GridPane userDefinedPane;
    private final GridPane automaticPane;

    private String type;

    public AssignmentsPane(List<ObservationRequest> requestList, List<UAV> uavList) {
        userDefinedPane = new GridPane();
        userDefinedPane.setMaxWidth(Double.MAX_VALUE);
        userDefinedPane.setHgap(10);
        userDefinedPane.setVgap(10);

        automaticPane = new GridPane();
        automaticPane.setMaxWidth(Double.MAX_VALUE);
        automaticPane.setHgap(10);
        automaticPane.setVgap(10);

        availabilities_uavs = new UAVAvailability[uavList.size()];
        availabilities_req_uavs = new UAVAvailability[requestList.size()][uavList.size()];
        requests = requestList.toArray(new ObservationRequest[requestList.size()]);
        uavs = uavList.toArray(new UAV[uavList.size()]);

        int last_row_index = 0;

        Label label = new Label("User defined: assign an UAV to each request:");
        label.setFont(Font.font(null, FontWeight.BOLD, 12));
        userDefinedPane.add(label, 0, last_row_index, 1 + uavs.length, 1);
        last_row_index++;

        for (int col = 0; col < uavs.length; col++) {
            userDefinedPane.add(new Label(uavs[col].toString()), 1 + col, last_row_index);
        }
        last_row_index++;

        for (int row = 0; row < requests.length; row++) {
            ObservationRequest req = requests[row];
            userDefinedPane.add(new Label(req.toString()), 0, last_row_index + row);
            ToggleGroup group = new ToggleGroup();
            for (int col = 0; col < uavs.length; col++) {
                UAV uav = uavs[col];
                UAVAvailability uavAvailability = uav.getAvailabilityFor(req);
                if(uavAvailability!=null) /*IMPORTANT: THIS ASSUMES THAT THERE IS ONLY ONE POSSIBLE UAV AVAILABILITY PER MISSION*/
                    availabilities_uavs[col] = uavAvailability;
                availabilities_req_uavs[row][col] = uavAvailability;

                RadioButton radio = new RadioButton();
                radio.setAlignment(Pos.CENTER);
                radio.setToggleGroup(group);
                radio.setDisable(uavAvailability == null);
                userDefinedPane.add(radio, 1 + col, last_row_index + row);
            }
        }

        last_row_index = 0;
        Label label2 = new Label("Automatic: select the UAVs to use in the mission:");
        label2.setFont(Font.font(null, FontWeight.BOLD, 12));
        automaticPane.add(label2, 0, last_row_index, 1 + uavs.length, 1);
        last_row_index++;

        for (int col = 0; col < uavs.length; col++) {
            automaticPane.add(new Label(uavs[col].toString()), col, last_row_index);
        }
        last_row_index++;
        CheckBox[] check_group = new CheckBox[uavs.length];
        for (int col = 0; col < uavs.length; col++) {
            CheckBox check = new CheckBox();
            check.setAlignment(Pos.CENTER);
            check_group[col] = check;
            automaticPane.add(check, col, last_row_index);
        }

        add(userDefinedPane, 0, 1);
        add(automaticPane, 0, 2);

        setMaxWidth(Double.MAX_VALUE);
        setHgap(10);
        setVgap(15);
    }



    public Map<ObservationRequest, UAVAvailability> getAssignments() {
        if(type.equalsIgnoreCase(USER)) {
            Map<ObservationRequest, UAVAvailability> map = new HashMap<>();
            userDefinedPane.getChildren().stream()
                    .filter(node -> node instanceof RadioButton)
                    .map(node -> (RadioButton) node)
                    .filter(radio -> radio.isSelected())
                    .forEach(radio -> {
                        int row = getRowIndex(radio) - 2;
                        int column = getColumnIndex(radio) - 1;
                        ObservationRequest req = requests[row];
                        UAVAvailability uavAv = availabilities_req_uavs[row][column];
                        map.put(req, uavAv);
                    });
            return map;
        }
        /*else the type of mission is without assignment, so  return a null set of assignment*/
        else return null;
    }

    public Set<UAVAvailability> getUAVSAvailability() {
        Set<UAVAvailability> availabilities = new HashSet<>();
        if (type.equalsIgnoreCase(USER)) {
            userDefinedPane.getChildren().stream()
                    .filter(node -> node instanceof RadioButton)
                    .map(node -> (RadioButton) node)
                    .filter(RadioButton::isSelected)
                    .forEach(radio -> {
                        int row = getRowIndex(radio) - 2;
                        int column = getColumnIndex(radio) - 1;
                        UAVAvailability uavAv = availabilities_req_uavs[row][column];
                        availabilities.add(uavAv);
                    });
        }
        else {
            automaticPane.getChildren().stream()
                    .filter(node -> node instanceof CheckBox)
                    .map(node -> (CheckBox) node)
                    .filter(CheckBox::isSelected)
                    .forEach(check -> {
                        int column = getColumnIndex(check);
                        UAVAvailability uavAv = availabilities_uavs[column];
                        availabilities.add(uavAv);
                    });
        }
        return availabilities;
    }

    public Set<ObservationRequest> getObservationRequests() {
        Set<ObservationRequest> observationRequests = new HashSet<>();
        for (ObservationRequest o: requests) {
            observationRequests.add(o);
        }
        return observationRequests;
    }

    public String notComplete() {
        if(type.equals(USER)) {
            if (userDefinedPane.getChildren().stream()
                    .filter(node -> node instanceof RadioButton)
                    .map(node -> (RadioButton) node)
                    .filter(radio -> radio.isSelected())
                    .count() < requests.length)
                return "Assign an UAV to each observation request!";
            else return "";
        }
        else {
            Set<CheckBox> selected_checkboxes =  automaticPane.getChildren().stream()
                    .filter(node -> node instanceof CheckBox)
                    .map(node -> (CheckBox) node)
                    .filter(check -> check.isSelected())
                    .collect(Collectors.toSet());
            long num_uav_selected = selected_checkboxes.size();
            if(num_uav_selected<1 || num_uav_selected>requests.length)
                return "Select a fleet of at least 1 and at most "+requests.length+" UAVs";

            String observabilities="";
            for (ObservationRequest req: requests) {
                boolean is_observable = false;
                for(CheckBox check: selected_checkboxes) {
                    int column = getColumnIndex(check);
                    UAVAvailability uavAv = availabilities_uavs[column];
                    if(uavAv!=null && uavAv.getUAV()!=null) {
                        if (uavAv.getUAV().getAvailabilityFor(req) != null) {
                            is_observable = true;
                            break;
                        }
                    }
                    else System.out.println("uno dei due e null: "+ ((uavAv==null) ? "uavAv" : uavAv.getUAV()==null));
                }
                if(!is_observable)
                    observabilities=observabilities+"\n\t"+req.toString();
            }
            if(!observabilities.equals(""))
                return "The observation requests:"+observabilities+"\nare not observable by any selected UAV.\nSelect a fleet of UAVs able to cover all the observation requests.";
        }
        return "";
    }

    public void setType(String type) {
        switch(type) {
            case USER:
                automaticPane.setDisable(true);
                userDefinedPane.setDisable(false);
                break;
            case AUTOMATIC:
                automaticPane.setDisable(false);
                userDefinedPane.setDisable(true);
                break;
        }
        this.type = type;
    }

    public String getType() {
        return type;
    }

}
