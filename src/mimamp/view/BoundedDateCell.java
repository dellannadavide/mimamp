package mimamp.view;

import java.time.LocalDate;
import javafx.scene.control.DateCell;

/**
 *
 * @author marco
 */
public class BoundedDateCell extends DateCell {

    @Override
    public void updateItem(LocalDate item, boolean empty) {
        super.updateItem(item, empty);
        if (item.isBefore(LocalDate.now())) {
            setDisable(true);
        } 
   }

}
