package mimamp.view;

import mimamp.controller.MissionDefiner;
import mimamp.controller.MissionPlannedController;
import mimamp.controller.TargetInsertionController;
import mimamp.model.plan.Plan;

/**
 * Created by davide on 24/11/16.
 */
public class MainBorderPaneFactory {

    public MainBorderPane getMainBorderPane(String pane, MainFrame mainFrame, Plan plan) {
        if (pane == null) {
            return null;
        }
        switch(pane) {
            case Views.MAIN_TARGET_INSERTION:
                return new TargetInsertionUI(new TargetInsertionController(), mainFrame);
            case Views.MAIN_MISSION_DEFINITION:
                return new MissionDefinitionUI(new MissionDefiner(), mainFrame);
            case Views.MAIN_PLANNED_MISSIONS:
                return new PlannedMissionsUI(new MissionPlannedController(plan), mainFrame);
        }
        return null;
    }
}
