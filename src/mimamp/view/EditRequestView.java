package mimamp.view;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import mimamp.controller.TargetInsertionController;
import mimamp.model.ObservationRequest;
import mimamp.model.Sensor;

/**
 *
 * @author marco
 */
public class EditRequestView extends GridPane {

    private final TargetInsertionController ctrl;
    private ObservationRequest request;

    private final TextField clientNameField;
    private final Label targetLabel;
    private final VBox sensorsBox;
    private final DatePicker datePicker;
    private final TextField earliestTimePicker;
    private final TextField latestTimePicker;
    private final Spinner minDurationSpinner;
    private final Button saveButton;

    private final TargetInsertionUI mainPane;

    public EditRequestView(TargetInsertionController ctrl, TargetInsertionUI mainPane) {
        this.ctrl = ctrl;
        this.mainPane = mainPane;

        setPadding(new Insets(20, 0, 20, 0));
        setHgap(5);
        setVgap(5);

        clientNameField = new TextField();
        targetLabel = new Label("");
        sensorsBox = new VBox();
        for (Sensor.SensorType sensor : Sensor.SensorType.values()) {
            CheckBox cb = new CheckBox(sensor.toString());
            cb.setOnAction(event -> filterSensorsCheckBoxes());
            sensorsBox.getChildren().add(cb);
        }
        datePicker = new DatePicker();
        earliestTimePicker = new TextField("__:__:__");
        latestTimePicker = new TextField("__:__:__");
        minDurationSpinner = new Spinner(1, TimeUnit.DAYS.toSeconds(1), 1);
        minDurationSpinner.setEditable(true);
        minDurationSpinner.getEditor().setOnKeyTyped((KeyEvent event) -> {
            if (!Character.isDigit(event.getCharacter().charAt(0))) {
                event.consume();
            }
        });
        saveButton = new Button("Save request");
        saveButton.setMaxWidth(Double.MAX_VALUE);
        saveButton.setOnAction((ActionEvent event) -> {
            checkInputAndSave();
        });

        addRow(0, new Label("Client name"), clientNameField);
        addRow(1, new Label("Target "), targetLabel);
        addRow(2, new Label("Using sensors"), sensorsBox);
        addRow(3, new Label("Observation date"), datePicker);
        addRow(4, new Label("Earliest time"), earliestTimePicker);
        addRow(5, new Label("Latest time"), latestTimePicker);
        addRow(6, new Label("Minimum duration (sec)"), minDurationSpinner);
        add(saveButton, 0, 7, 2, 1);
    }

    private void checkInputAndSave() {
        // Check client name
        if (clientNameField.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Insert a client name!");
            alert.setResizable(true);
            alert.show();
            return;
        }

        // Check sensors
        int numSensors = (int) sensorsBox.getChildren().stream()
                .map(n -> (CheckBox) n)
                .filter(cb -> cb.isSelected())
                .count();
        if (numSensors == 0) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Select at least one sensor!");
            alert.setResizable(true);
            alert.show();
            return;
        }

        // Check time
        String earliestTimeStr = earliestTimePicker.getText();
        String latestTimeStr = latestTimePicker.getText();
        if (!(earliestTimeStr.matches("\\d\\d:\\d\\d:\\d\\d")
                && latestTimeStr.matches("\\d\\d:\\d\\d:\\d\\d"))) {
            Alert alert = new Alert(Alert.AlertType.WARNING,
                    "Insert valid time strings! Format is HH:MM:SS.");
            alert.setResizable(true);
            alert.show();
            return;
        }
        if (earliestTimeStr.compareTo(latestTimeStr) >= 0) {
            Alert alert = new Alert(Alert.AlertType.WARNING,
                    "Earliest time must precede the latest time!");
            alert.setResizable(true);
            alert.show();
            return;
        }

        // Alert about data fusion
        if (numSensors > 1) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                    "You selected more than a sensor: "
                    + "this will be interpreted as a data fusion request.");
            alert.setResizable(true);
            Optional<ButtonType> result = alert.showAndWait();
            if (!result.isPresent() || result.get().getButtonData().isCancelButton()) {
                return;
            }
        }

        // Save values on main memory
        request.setClient(clientNameField.getText());
        sensorsBox.getChildren().stream()
                .map((n) -> (CheckBox) n)
                .forEach((cb) -> {
                    if (cb.isSelected()) {
                        request.addSensor(cb.getText());
                    } else {
                        request.removeSensor(cb.getText());
                    }
                });
        LocalDate date = datePicker.getValue();
        LocalDateTime earliestTime = LocalDateTime.of(date,
                LocalTime.parse(earliestTimeStr, DateTimeFormatter.ofPattern("HH:mm:ss")));
        LocalDateTime latestTime = LocalDateTime.of(date,
                LocalTime.parse(latestTimeStr, DateTimeFormatter.ofPattern("HH:mm:ss")));
        request.setEarliestTime(earliestTime);
        request.setLatestTime(latestTime);
        minDurationSpinner.getValueFactory().increment(0);
        long minDuration = Long.parseLong(minDurationSpinner.getEditor().getText());
        request.setMinDurationSec(minDuration);

        // Save values on DB
        ctrl.saveObservationRequest(request);
        mainPane.endEditRequest(request);
    }

    private void filterSensorsCheckBoxes() {
        Set<Sensor.SensorType> selection = new HashSet<>();
        sensorsBox.getChildren().stream()
                .map(node -> (CheckBox) node)
                .filter(cb -> cb.isSelected())
                .forEach(cb -> selection.add(
                                Sensor.SensorType.valueOf(cb.getText())));
        sensorsBox.getChildren().stream()
                .map(node -> (CheckBox) node)
                .filter(cb -> !cb.isSelected())
                .forEach(cb -> {
                    Sensor.SensorType type = Sensor.SensorType.valueOf(cb.getText());
                    cb.setDisable(type.dataFusionIncompatible(selection));
                });
    }

    public void setObservationRequest(ObservationRequest request) {
        this.request = request;

        clientNameField.setText(request.getClient());
        targetLabel.setText(request.getTargetName());
        for (Node n : sensorsBox.getChildren()) {
            CheckBox cb = (CheckBox) n;
            cb.setDisable(false);
            cb.setSelected(false);
            for (Sensor sensor : request.getSensors()) {
                if (cb.getText().equalsIgnoreCase(sensor.toString())) {
                    cb.setSelected(true);
                    break;
                }
            }
        }
        datePicker.setValue(request.getDate());
        earliestTimePicker.setText(request.getEarliestTimeStr());
        latestTimePicker.setText(request.getLatestTimeStr());
        boolean millisec = false;
        minDurationSpinner.getValueFactory().setValue((double) request.getMinDuration(millisec));
    }
}
