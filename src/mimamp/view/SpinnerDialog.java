package mimamp.view;

//import org.controlsfx.DialogResources;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * A dialog that shows a spinner input control to the user.
 */
public class SpinnerDialog extends Dialog<Integer> {

    private final GridPane grid;
    private final Label label;
    private final Spinner spinner;
    private final int defaultValue;

    public SpinnerDialog(String title, String header, String content,
            int min, int max, int defaultValue) {
        final DialogPane dialogPane = getDialogPane();
        dialogPane.setContentText(content);

        // -- textfield
        this.spinner = new Spinner(min, max, defaultValue);
        this.spinner.setMaxWidth(Double.MAX_VALUE);
        GridPane.setHgrow(spinner, Priority.ALWAYS);
        GridPane.setFillWidth(spinner, true);

        // -- label
        label = new Label(dialogPane.getContentText());
        label.setMaxWidth(Double.MAX_VALUE);
        label.setMaxHeight(Double.MAX_VALUE);
        label.getStyleClass().add("content");
        label.setWrapText(true);
        label.setPrefWidth(360);
        label.textProperty().bind(dialogPane.contentTextProperty());

        this.defaultValue = defaultValue;

        this.grid = new GridPane();
        this.grid.setHgap(10);
        this.grid.setMaxWidth(Double.MAX_VALUE);
        this.grid.setAlignment(Pos.CENTER_LEFT);

        dialogPane.contentTextProperty().addListener(o -> updateGrid());

        setTitle(title);
        dialogPane.setHeaderText(header);
        dialogPane.getStyleClass().add("spinner-dialog");
        dialogPane.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        updateGrid();

        setResultConverter((dialogButton) -> dialogButton == ButtonType.OK ? (Integer) spinner.getValue() : null);
    }

    /**
     * @return the Spinner used within this dialog.
     */
    public final Spinner getEditor() {
        return spinner;
    }

    /**
     * @return the default value that was specified in the constructor.
     */
    public final int getDefaultValue() {
        return defaultValue;
    }

    private void updateGrid() {
        grid.getChildren().clear();

        grid.add(label, 0, 0);
        grid.add(spinner, 1, 0);
        getDialogPane().setContent(grid);

        Platform.runLater(() -> spinner.requestFocus());
    }
}
