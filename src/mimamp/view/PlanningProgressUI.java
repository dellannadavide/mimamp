/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.view;

import java.util.Observable;
import java.util.Observer;
import java.util.Optional;

import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import mimamp.controller.MissionPlannedController;
import mimamp.controller.planning.Planner;
import mimamp.model.plan.Plan;

/**
 *
 * @author marco
 */
class PlanningProgressUI extends Stage implements Observer {

    private final Planner ctrl;

    private final TextArea logArea;
    private final ProgressBar progressBar;

    private final MissionDefinitionUI mainPane;

    public PlanningProgressUI(Planner ctrl, MissionDefinitionUI mainPane) {
        this.ctrl = ctrl;
        this.mainPane = mainPane;

        setTitle("Planning...");

        logArea = new TextArea("Preparing planning process...");
        logArea.setEditable(false);

        progressBar = new ProgressBar();
        progressBar.setMaxWidth(Double.MAX_VALUE);

        BorderPane pane = new BorderPane();
        pane.setCenter(logArea);
        pane.setBottom(progressBar);
        setScene(new Scene(pane));

        startPlanning();
    }

    private void startPlanning() {
        ctrl.addObserver(this);
        new Thread(() -> {
            Plan plan = ctrl.plan();
            Platform.runLater(() -> {
                if (plan == null) {
                    Alert alert = new Alert(Alert.AlertType.WARNING,
                            "Couldn't find a mission plan!\nDo you want to see old saved ones?",
                            ButtonType.YES, ButtonType.NO);
                    alert.setResizable(true);
                    Optional<ButtonType> resp = alert.showAndWait();
                    if (resp.get().equals(ButtonType.YES)) {
                        try {
                            mainPane.getMainScene().replaceSceneContent(Views.MAIN_PLANNED_MISSIONS, null);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (resp.get().equals(ButtonType.NO)) {
                        //System.exit(0);
                    }
                } else {
                    try {
                        close();
                        mainPane.getMainScene().replaceSceneContent(Views.MAIN_PLANNED_MISSIONS, plan);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //close();
                System.gc();
            });
        }).start();
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Double) {
            double progress = (double) arg;
            Platform.runLater(() -> progressBar.setProgress(progress));
        } else if (arg instanceof String) {
            Platform.runLater(() -> logArea.appendText(arg.toString()));
        }
    }
}
