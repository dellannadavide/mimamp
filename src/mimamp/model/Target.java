/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.model;

import com.lynden.gmapsfx.javascript.JavascriptObject;
import com.lynden.gmapsfx.javascript.object.LatLong;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author marco
 */
public class Target implements PDDLObject {

    /**
     * An unique identificator.
     */
    long id;
    /**
     * The human readable name.
     */
    String name;
    /**
     * The Target's vertices.
     */
    private List<Coordinates> positions;
    /**
     * The Target path's length, in meters.
     */
    private double length;
    /**
     * The Target shape on the map.
     */
    private JavascriptObject shape;

    public Target(long id, String name) {
        this.id = id;
        this.name = name;
        positions = new ArrayList<>();
        this.length = 0;
    }

    public Target(long id, String name, double latitude, double longitude) {
        this(id, name);
        positions.add(new Coordinates(latitude, longitude));
    }

    public Target(String name, Coordinates coordinates) {
        this(-1, name);
        positions.add(coordinates);
    }
    
    public Target(Coordinates coordinates) {
        this("tempTarget", coordinates);
    }
    
    public void addPosition(Coordinates newPosition) {
        positions.add(newPosition);
    }

    public void calculateLength() {
        length = 0;
        Coordinates previous = null;
        for (Coordinates p : positions) {
            if (previous != null) {
                length += previous.distanceFrom(p);
            }
            previous = p;
        }
    }

    public String getDetailString() {
        if (isPoint()) {
            return "Target nr. " + id + ": " + name + "\n"
                    + "At coordinates: " + getFirstPosition();
        } else {
            return "Target nr. " + id + ": " + name + "\n"
                    + "Path from " + getFirstPosition() + "\nto " + getLastPosition();
        }
    }

    public Coordinates getLastPosition() {
        return positions.get(positions.size() - 1);
    }

    public Coordinates getFirstPosition() {
        return positions.get(0);
    }

    public long getId() {
        return id;
    }

    public double getLength() {
        return length;
    }

    double getMostFarPositionFrom(Coordinates position) {
        return positions.stream()
                .mapToDouble(p -> p.distanceFrom(position))
                .max()
                .getAsDouble();
    }

    double getLessFarBoundary(Coordinates position) {
        return Math.min(getFirstPosition().distanceFrom(position),
                getLastPosition().distanceFrom(position));
    }

    public String getName() {
        return name;
    }

    public LatLong[] getPathArray() {
        List<LatLong> list = positions.stream()
                .map(coord -> coord.toLatLong())
                .collect(Collectors.toList());
        return list.toArray(new LatLong[list.size()]);
    }

    Coordinates getPosition() {
        if (!isPoint()) {
            throw new IllegalStateException("Target line can't have a 'position'!");
        }
        return getFirstPosition();
    }

    Coordinates getPositionAt(int index) {
        return positions.get(index);
    }

    public List<Coordinates> getPositions() {
        return positions;
    }

    int getPositionsSize() {
        return positions.size();
    }

    public JavascriptObject getShape() {
        return shape;
    }

    void insertPosition(int index, Coordinates position) {
        positions.add(index, position);
    }

    boolean isFirstPosition(Coordinates startPosition) {
        return positions.get(0).equals(startPosition);
    }

    public boolean isPoint() {
        return positions.size() == 1;
    }

    @Override
    public String pddl() {
        return "target" + id;
    }
    
    public String pddlLast() {
        return "targetEnd" + id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setShape(JavascriptObject shape) {
        this.shape = shape;
    }

    @Override
    public String toString() {
        return name;
    }
}
