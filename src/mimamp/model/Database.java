package mimamp.model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import mimamp.model.plan.Plan;
import mimamp.model.plan.SensorSuiteOnUAV;
import mimamp.model.plan.actions.DurativeSensorSuiteAction;
import mimamp.model.plan.actions.DurativeUAVAction;
import mimamp.model.plan.actions.FlyThroughTarget;
import mimamp.model.plan.actions.Idle;
import mimamp.model.plan.actions.Landing;
import mimamp.model.plan.actions.Loiter;
import mimamp.model.plan.actions.Monitor;
import mimamp.model.plan.actions.TakeOff;
import mimamp.model.plan.actions.Transfer;
import mimamp.model.plan.actions.TransferToLand;
import mimamp.model.plan.actions.TransferToTarget;
import mimamp.model.plan.actions.WaitOnGround;

/**
 * The Information Expert class that performs all database-related operations.
 */
public class Database implements AutoCloseable {

    /** the JDBC connection used to communicate with the database */
    private final Connection dbConnection;

    /**
     * The only constructor has no parameters.
     * @throws SQLException 
     */
    public Database() throws SQLException {
        dbConnection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/mimamp",
                "postgres", "postgres");
        dbConnection.setAutoCommit(false);
    }

    private void addAvailabilitiesToUAV(UAV uav, Set<Airport> airports)
            throws SQLException {
        PreparedStatement st = dbConnection.prepareStatement(
                "SELECT AIRPORT.ID ARPID, NAME, POSITION_LAT, POSITION_LNG, "
                + "ALTITUDE, END_TAKEOFF_LAT, END_TAKEOFF_LNG, "
                + "BEGIN_LANDING_LAT, BEGIN_LANDING_LNG, TAKEOFF_DURATION, "
                + "LANDING_DURATION, UAV_AVAILABILITY.ID AVAID, SENSOR_SUITE, "
                + "BEGIN_DATE, ENDING_DATE "
                + "FROM UAV_AVAILABILITY JOIN AIRPORT ON AIRPORT=AIRPORT.ID "
                + "WHERE UAV=?");
        st.setLong(1, uav.getId());
        ResultSet rs = st.executeQuery();

        while (rs.next()) {
            String airportId = rs.getString("ARPID");
            Airport airport = airports.stream()
                    .filter(a -> a.getId().equals(airportId))
                    .findFirst()
                    .orElse(new Airport(airportId, rs.getString("NAME"),
                                    rs.getDouble("POSITION_LAT"), rs.getDouble("POSITION_LNG"),
                                    rs.getDouble("ALTITUDE"),
                                    rs.getDouble("END_TAKEOFF_LAT"), rs.getDouble("END_TAKEOFF_LNG"),
                                    rs.getDouble("BEGIN_LANDING_LAT"), rs.getDouble("BEGIN_LANDING_LNG"),
                                    rs.getLong("TAKEOFF_DURATION"),
                                    rs.getLong("LANDING_DURATION")));
            airports.add(airport);

            uav.addAvailability(new UAVAvailability(rs.getLong("AVAID"), uav,
                    rs.getString("SENSOR_SUITE"), airport,
                    rs.getDate("BEGIN_DATE").toLocalDate(),
                    rs.getDate("ENDING_DATE").toLocalDate()));
        }
    }

    private void addCluster(ClusteredRequest cluster) throws SQLException {
        addRequest(cluster);
        for (ObservationRequest req : cluster.getRepresentedRequests()) {
            PreparedStatement st = dbConnection.prepareStatement(
                    "INSERT INTO REQUEST_CLUSTER(CLUSTER, OBSERVATION_REQUEST) "
                    + "VALUES (?, ?)");
            st.setLong(1, cluster.getId());
            st.setLong(2, req.getId());
            int result = st.executeUpdate();
            st.close();
            if (result <= 0) {
                throw new SQLException("Database insertion failed!");
            }
        }
    }

    private void addMissionAssignment(Mission mission,
            ObservationRequest obsReq, UAVAvailability uavAv) throws SQLException {
        PreparedStatement st = dbConnection.prepareStatement(
                "INSERT INTO MISSION_ASSIGNMENTS("
                + "MISSION, OBSERVATION_REQUEST, UAV_AVAILABILITY) "
                + "VALUES (?, ?, ?)");
        st.setLong(1, mission.getId());
        st.setLong(2, obsReq.getId());
        st.setLong(3, uavAv.getId());
        int result = st.executeUpdate();
        st.close();
        if (result <= 0) {
            throw new SQLException("Database insertion failed!");
        }
    }

    private void addMissionMultiObs(Mission mission, MultiObs multiobs)
            throws SQLException {
        PreparedStatement st = dbConnection.prepareStatement(
                "INSERT INTO MULTI_OBS(MISSION, REQUEST1, TYPE, REQUEST2) "
                + "VALUES (?, ?, ?, ?)");
        st.setLong(1, mission.getId());
        st.setLong(2, multiobs.getRequest1().getId());
        st.setString(3, multiobs.getConstraint());
        st.setLong(4, multiobs.getRequest2().getId());

        int result = st.executeUpdate();
        st.close();
        if (result <= 0) {
            throw new SQLException("Database insertion failed!");
        }
    }

    public void addMissionPlan(Mission mission, Plan plan) throws SQLException {
        PreparedStatement st = dbConnection.prepareStatement(
                "INSERT INTO MISSION(EARLIEST_TIME, LATEST_TIME, MAX_DURATION) "
                + "VALUES (?, ?, ?)");
        st.setTimestamp(1, Timestamp.valueOf(mission.getEarliestTime()));
        st.setTimestamp(2, Timestamp.valueOf(mission.getLatestTime()));
        st.setLong(3, mission.getMaxDuration().toMillis());
        int result = st.executeUpdate();
        st.close();
        if (result <= 0) {
            throw new SQLException("Database insertion failed!");
        }

        // Gets the new Mission's ID
        st = dbConnection.prepareStatement("SELECT MAX(ID) FROM MISSION");
        ResultSet rs = st.executeQuery();
        rs.next();
        mission.setId(rs.getLong(1));
        rs.close();
        st.close();

        // Adds ObservationRequest and clusters to the mission
        for (ObservationRequest obsReq : mission.getRequests()) {
            try {
                if (obsReq instanceof ClusteredRequest) {
                    addCluster((ClusteredRequest) obsReq);
                }
                addMissionAssignment(mission, obsReq, mission.getAssignment(obsReq));
            } catch (SQLException e) {
                dbConnection.rollback();
                throw e;
            }
        }

        // Adds mission's MultiObs
        for (MultiObs multiobs : mission.getMultiObs()) {
            try {
                addMissionMultiObs(mission, multiobs);
            } catch (SQLException e) {
                dbConnection.rollback();
                throw e;
            }
        }

        // Add Plan
        st = dbConnection.prepareStatement(
                "INSERT INTO PLAN(NAME, MISSION) "
                + "VALUES (?, ?)");
        st.setString(1, plan.getName());
        st.setLong(2, mission.getId());
        result = st.executeUpdate();
        st.close();
        if (result <= 0) {
            dbConnection.rollback();
            throw new SQLException("Database insertion failed!");
        }
        // Gets the new Plan's ID
        st = dbConnection.prepareStatement("SELECT MAX(ID) FROM PLAN");
        rs = st.executeQuery();
        rs.next();
        plan.setId(rs.getLong(1));
        rs.close();
        st.close();

        // Initial state
        for (UAVAvailability uavAv : plan.getInitialStates()) {
            st = dbConnection.prepareStatement(
                    "INSERT INTO PLAN_INITIAL_STATE(PLAN, UAV_AVAILABILITY) "
                    + "VALUES (?, ?)");
            st.setLong(1, plan.getId());
            st.setLong(2, uavAv.getId());
            result = st.executeUpdate();
            st.close();
            if (result <= 0) {
                dbConnection.rollback();
                throw new SQLException("Database insertion failed!");
            }
        }

        // Start times
        for (UAV uav : plan.getUAVs()) {
            st = dbConnection.prepareStatement(
                    "INSERT INTO PLAN_START_TIME(PLAN, UAV, START_TIME) "
                    + "VALUES (?, ?, ?)");
            st.setLong(1, plan.getId());
            st.setLong(2, uav.getId());
            st.setTimestamp(3, Timestamp.valueOf(plan.getStartTimeOf(uav)));
            result = st.executeUpdate();
            st.close();
            if (result <= 0) {
                dbConnection.rollback();
                throw new SQLException("Database insertion failed!");
            }
        }

        try {
            addPlanActions(plan);
        } catch (SQLException e) {
            dbConnection.rollback();
            throw new SQLException("Database plan actions insertion failed!");
        }

        dbConnection.commit();
    }

    private void addPlanActions(Plan plan) throws SQLException {
        PreparedStatement st;
        int result;

        for (UAV uav : plan.getUAVs()) {
            int index = 0;
            for (DurativeUAVAction action : plan.getActionsOf(uav)) {
                if (action instanceof FlyThroughTarget) {
                    FlyThroughTarget a = (FlyThroughTarget) action;
                    st = dbConnection.prepareStatement(
                            "INSERT INTO UAV_ACTION(TYPE, BEGIN_TIME, "
                            + "OBSERVATION_REQUEST, BOOLEAN_VALUE) "
                            + "VALUES (?, ?, ?, ?)");
                    st.setLong(3, a.getRequestId());
                    st.setBoolean(4, a.isForward());
                } else if (action instanceof Landing) {
                    Landing a = (Landing) action;
                    st = dbConnection.prepareStatement(
                            "INSERT INTO UAV_ACTION(TYPE, BEGIN_TIME, "
                            + "BOOLEAN_VALUE, AIRPORT) "
                            + "VALUES (?, ?, ?, ?)");
                    st.setBoolean(3, a.isPrev());
                    st.setString(4, a.getAirportId());
                } else if (action instanceof Loiter) {
                    Loiter a = (Loiter) action;
                    st = dbConnection.prepareStatement(
                            "INSERT INTO UAV_ACTION(TYPE, BEGIN_TIME, "
                            + "DURATION, BOOLEAN_VALUE) "
                            + "VALUES (?, ?, ?, ?)");
                    st.setLong(3, a.getDuration().toMillis());
                    st.setBoolean(4, a.isNo());
                } else if (action instanceof TakeOff) {
                    TakeOff a = (TakeOff) action;
                    st = dbConnection.prepareStatement(
                            "INSERT INTO UAV_ACTION(TYPE, BEGIN_TIME, "
                            + "BOOLEAN_VALUE, AIRPORT) "
                            + "VALUES (?, ?, ?, ?)");
                    st.setBoolean(3, a.isSucc());
                    st.setString(4, a.getAirportId());
                } else if (action instanceof TransferToLand) {
                    TransferToLand a = (TransferToLand) action;
                    st = dbConnection.prepareStatement(
                            "INSERT INTO UAV_ACTION(TYPE, BEGIN_TIME, "
                            + "COORDINATES_LAT, COORDINATES_LNG, SPEED, AIRPORT) "
                            + "VALUES (?, ?, ?, ?, ?, ?)");
                    st.setDouble(3, a.getLatitude());
                    st.setDouble(4, a.getLongitude());
                    st.setDouble(5, a.getSpeed());
                    st.setString(6, a.getAirportId());
                } else if (action instanceof TransferToTarget) {
                    TransferToTarget a = (TransferToTarget) action;
                    st = dbConnection.prepareStatement(
                            "INSERT INTO UAV_ACTION(TYPE, BEGIN_TIME, "
                            + "COORDINATES_LAT, COORDINATES_LNG, SPEED, "
                            + "TARGET, BOOLEAN_VALUE) "
                            + "VALUES (?, ?, ?, ?, ?, ?, ?)");
                    st.setDouble(3, a.getLatitude());
                    st.setDouble(4, a.getLongitude());
                    st.setDouble(5, a.getSpeed());
                    st.setLong(6, a.getTargetId());
                    st.setBoolean(7, a.isFromFirstPoint());
                } else if (action instanceof WaitOnGround) {
                    st = dbConnection.prepareStatement(
                            "INSERT INTO UAV_ACTION(TYPE, BEGIN_TIME) "
                            + "VALUES (?, ?)");
                } else {
                    throw new IllegalStateException();
                }
                st.setString(1, action.getClass().getSimpleName());
                st.setTimestamp(2, Timestamp.valueOf(action.getBeginOfAction()));
                result = st.executeUpdate();
                st.close();
                if (result <= 0) {
                    dbConnection.rollback();
                    throw new SQLException("Database insertion failed!");
                }

                // Gets the new Action's ID
                st = dbConnection.prepareStatement("SELECT MAX(ID) FROM UAV_ACTION");
                ResultSet rs = st.executeQuery();
                rs.next();
                long actId = rs.getLong(1);
                rs.close();
                st.close();

                // Insert action
                st = dbConnection.prepareStatement(
                        "INSERT INTO PLAN_UAV_ACTION(PLAN, UAV, UAV_ACTION, INDEX) "
                        + "VALUES (?, ?, ?, ?)");
                st.setLong(1, plan.getId());
                st.setLong(2, uav.getId());
                st.setLong(3, actId);
                st.setInt(4, index++);
                result = st.executeUpdate();
                st.close();
                if (result <= 0) {
                    dbConnection.rollback();
                    throw new SQLException("Database insertion failed!");
                }
            }
        }

        for (SensorSuiteOnUAV suiteOnUav : plan.getSuites()) {
            int index = 0;
            for (DurativeSensorSuiteAction action : plan.getActionsOf(suiteOnUav)) {
                if (action instanceof Monitor) {
                    Monitor a = (Monitor) action;
                    st = dbConnection.prepareStatement(
                            "INSERT INTO SENSOR_SUITE_ACTION(TYPE, BEGIN_TIME, "
                            + "DURATION, OBSERVATION_REQUEST) "
                            + "VALUES (?, ?, ?, ?)");
                    st.setLong(3, a.getDuration().toMillis());
                    st.setLong(4, a.getObservationRequestId());
                } else if (action instanceof Idle) {
                    Idle a = (Idle) action;
                    st = dbConnection.prepareStatement(
                            "INSERT INTO SENSOR_SUITE_ACTION"
                            + "(TYPE, BEGIN_TIME, DURATION) VALUES (?, ?, ?)");
                    st.setLong(3, a.getDuration().toMillis());
                } else if (action instanceof Transfer) {
                    Transfer a = (Transfer) action;
                    st = dbConnection.prepareStatement(
                            "INSERT INTO SENSOR_SUITE_ACTION(TYPE, BEGIN_TIME, "
                            + "DURATION, COORDINATES_LAT, COORDINATES_LNG) "
                            + "VALUES (?, ?, ?, ?, ?)");
                    st.setLong(3, a.getDuration().toMillis());
                    st.setDouble(4, a.getLatitude());
                    st.setDouble(5, a.getLongitude());
                } else {
                    throw new IllegalStateException();
                }
                st.setString(1, action.getClass().getSimpleName());
                st.setTimestamp(2, Timestamp.valueOf(action.getBeginOfAction()));
                result = st.executeUpdate();
                st.close();
                if (result <= 0) {
                    dbConnection.rollback();
                    throw new SQLException("Database insertion failed!");
                }

                // Gets the new Action's ID
                st = dbConnection.prepareStatement("SELECT MAX(ID) FROM SENSOR_SUITE_ACTION");
                ResultSet rs = st.executeQuery();
                rs.next();
                long actId = rs.getLong(1);
                rs.close();
                st.close();

                // Insert action
                st = dbConnection.prepareStatement(
                        "INSERT INTO PLAN_SUITE_ACTION(PLAN, UAV_AVAILABILITY, "
                        + "SENSOR_SUITE_ACTION, INDEX) "
                        + "VALUES (?, ?, ?, ?)");
                st.setLong(1, plan.getId());
                st.setLong(2, plan.getAvailabilityIdOf(suiteOnUav.getUAV()));
                st.setLong(3, actId);
                st.setInt(4, index++);
                result = st.executeUpdate();
                st.close();
                if (result <= 0) {
                    dbConnection.rollback();
                    throw new SQLException("Database insertion failed!");
                }
            }
        }
    }

    private void addPositionstoTarget(Target target) throws SQLException {
        PreparedStatement st = dbConnection.prepareStatement(
                "SELECT INDEX, POSITION_LAT, POSITION_LNG FROM TARGET_POINT "
                + "WHERE TARGET=?");
        st.setLong(1, target.getId());
        ResultSet rs = st.executeQuery();
        while (rs.next()) {
            target.insertPosition(rs.getInt("INDEX"), new Coordinates(
                    rs.getDouble("POSITION_LAT"), rs.getDouble("POSITION_LNG")));
        }
        target.calculateLength();
    }

    public void addRequest(ObservationRequest req) throws SQLException {
        // Adds the request tuple and checks the result
        PreparedStatement st = dbConnection.prepareStatement(
                "INSERT INTO OBSERVATION_REQUEST(CLIENT, TARGET, "
                + "EARLIEST_TIME, LATEST_TIME, MIN_DURATION, NEED_TO_PLAN) "
                + "VALUES(?, ?, ?, ?, ?, ?)");
        st.setString(1, req.getClient());
        st.setLong(2, req.getTargetId());
        st.setTimestamp(3, Timestamp.valueOf(req.getEarliestTime()));
        st.setTimestamp(4, Timestamp.valueOf(req.getLatestTime()));
        st.setLong(5, req.getMinDuration(true));
        st.setBoolean(6, req.getNeedToPlan());
        int result = st.executeUpdate();
        st.close();
        if (result <= 0) {
            throw new SQLException("Database insertion failed!");
        }

        // Gets the new request's ID
        st = dbConnection.prepareStatement("SELECT MAX(ID) FROM OBSERVATION_REQUEST");
        ResultSet rs = st.executeQuery();
        rs.next();
        req.setId(rs.getLong(1));
        rs.close();
        st.close();

        // Adds the request's sensors
        for (Sensor s : req.getSensors()) {
            st = dbConnection.prepareStatement(
                    "INSERT INTO REQUEST_SENSORS(OBSERVATION_REQUEST, SENSOR) "
                    + "VALUES(?, ?)");
            st.setLong(1, req.getId());
            st.setString(2, s.toUpperString());

            result = st.executeUpdate();
            st.close();

            if (result <= 0) {
                dbConnection.rollback();
                throw new SQLException("Database insertion failed!");
            }
        }

        dbConnection.commit();
    }

    private void addSensorsToRequestObject(ObservationRequest obsReq)
            throws SQLException {
        PreparedStatement st = dbConnection.prepareStatement(
                "SELECT SENSOR FROM REQUEST_SENSORS WHERE OBSERVATION_REQUEST=?");
        st.setLong(1, obsReq.getId());
        ResultSet rs = st.executeQuery();

        while (rs.next()) {
            obsReq.addSensor(rs.getString("SENSOR"));
        }

        rs.close();
        st.close();
    }

    public void addTarget(Target target) throws SQLException {
        PreparedStatement st = dbConnection.prepareStatement("INSERT INTO TARGET "
                + "(NAME) VALUES(?)");
        st.setString(1, target.getName());
        int result = st.executeUpdate();
        st.close();
        if (result <= 0) {
            throw new SQLException("Database target insertion failed!");
        }

        // Gets the new target's ID
        st = dbConnection.prepareStatement("SELECT MAX(ID) FROM TARGET");
        ResultSet rs = st.executeQuery();
        rs.next();
        target.setId(rs.getLong(1));
        rs.close();
        st.close();

        savePositionsOfTarget(target);

        dbConnection.commit();
    }

    @Override
    public void close() throws SQLException {
        dbConnection.close();
    }

    public void deleteRequest(ObservationRequest req) throws SQLException {
        PreparedStatement st = dbConnection.prepareStatement(
                "DELETE FROM OBSERVATION_REQUEST WHERE ID = ?");
        st.setLong(1, req.getId());
        int result = st.executeUpdate();
        st.close();
        if (result <= 0) {
            throw new SQLException("Database delete failed!");
        }

        dbConnection.commit();
    }

    public void deleteTarget(Target target) throws SQLException {
        PreparedStatement st = dbConnection.prepareStatement(
                "DELETE FROM TARGET WHERE ID = ?");
        st.setLong(1, target.getId());
        int result = st.executeUpdate();
        st.close();
        if (result <= 0) {
            throw new SQLException("Database delete failed!");
        }

        dbConnection.commit();
    }

    public void editRequest(ObservationRequest req) throws SQLException {
        // Adds the request tuple and checks the result
        PreparedStatement st = dbConnection.prepareStatement(
                "UPDATE OBSERVATION_REQUEST "
                + "SET CLIENT=?, EARLIEST_TIME=?, LATEST_TIME=?, "
                + "MIN_DURATION=?, NEED_TO_PLAN=? "
                + "WHERE ID=?");
        st.setString(1, req.getClient());
        st.setTimestamp(2, Timestamp.valueOf(req.getEarliestTime()));
        st.setTimestamp(3, Timestamp.valueOf(req.getLatestTime()));
        st.setLong(4, req.getMinDuration(true));
        st.setBoolean(5, req.getNeedToPlan());
        st.setLong(6, req.getId());
        int result = st.executeUpdate();
        st.close();
        if (result <= 0) {
            throw new SQLException("Database insertion failed!");
        }

        // Removes the request's old sensors
        st = dbConnection.prepareStatement("DELETE FROM REQUEST_SENSORS WHERE OBSERVATION_REQUEST=?");
        st.setLong(1, req.getId());
        result = st.executeUpdate();
        st.close();
        if (result <= 0) {
            dbConnection.rollback();
            throw new SQLException("Database insertion failed!");
        }

        // Adds the request's sensors
        for (Sensor s : req.getSensors()) {
            st = dbConnection.prepareStatement(
                    "INSERT INTO REQUEST_SENSORS(OBSERVATION_REQUEST, SENSOR) "
                    + "VALUES(?, ?)");
            st.setLong(1, req.getId());
            st.setString(2, s.toUpperString());
            result = st.executeUpdate();
            st.close();
            if (result <= 0) {
                dbConnection.rollback();
                throw new SQLException("Database insertion failed!");
            }
        }

        dbConnection.commit();
    }

    public List<ObservationRequest> getAllRequests(List<Target> targets)
            throws SQLException {
        PreparedStatement st = dbConnection.prepareStatement(
                "SELECT * FROM OBSERVATION_REQUEST "
                + "WHERE ID <> ALL(SELECT CLUSTER FROM REQUEST_CLUSTER) "
                + "ORDER BY EARLIEST_TIME");
        ResultSet rs = st.executeQuery();

        List<ObservationRequest> requests = new ArrayList<>();
        while (rs.next()) {
            ObservationRequest obsReq = new ObservationRequest(
                    rs.getLong("ID"), rs.getString("CLIENT"),
                    rs.getTimestamp("EARLIEST_TIME"), rs.getTimestamp("LATEST_TIME"),
                    rs.getInt("MIN_DURATION"), rs.getBoolean("NEED_TO_PLAN"));
            long targetId = rs.getLong("TARGET");
            Target target
                    = targets.stream().filter(t -> (t.getId() == targetId))
                    .findFirst().get();
            obsReq.setTarget(target);
            addSensorsToRequestObject(obsReq);
            requests.add(obsReq);
        }

        rs.close();
        st.close();
        return requests;
    }

    public List<Target> getAllTargets() throws SQLException {
        PreparedStatement st = dbConnection.prepareStatement(
                "SELECT * FROM TARGET ORDER BY NAME");
        ResultSet rs = st.executeQuery();

        List<Target> targets = new ArrayList<>();

        while (rs.next()) {
            Target target = new Target(
                    rs.getLong("ID"), rs.getString("NAME"));
            addPositionstoTarget(target);
            targets.add(target);
        }

        rs.close();
        st.close();
        return targets;
    }

    public List<UAV> getAllUAVs() throws SQLException {
        PreparedStatement st = dbConnection.prepareStatement(
                "SELECT * FROM UAV JOIN UAV_MODEL ON UAV.TYPE=UAV_MODEL.TYPE");
        ResultSet rs = st.executeQuery();

        List<UAV> uavs = new ArrayList<>();
        Set<Airport> airports = new HashSet<>();
        while (rs.next()) {
            UAV uav = new UAV(rs.getLong("ID"), rs.getString("TYPE"),
                    rs.getString("NAME"),
                    rs.getDouble("MAX_OPERABILITY_DISTANCE"),
                    rs.getDouble("CRUISING_SPEED"),
                    rs.getDouble("CRUISING_ALTITUDE"),
                    rs.getDouble("OPERATING_TIME"),
                    rs.getDouble("FUEL_CAPACITY"),
                    rs.getDouble("FUEL_LEVEL"),
                    rs.getDouble("CONSUMPTION")
            );
            addAvailabilitiesToUAV(uav, airports);
            uavs.add(uav);
        }

        rs.close();
        st.close();
        return uavs;
    }

    public List<ObservationRequest> getFilteredRequests(
            LocalDate date, CoordBounds area) throws SQLException {
        PreparedStatement st = dbConnection.prepareStatement(
                "SELECT OBSERVATION_REQUEST.ID OBSID, CLIENT, EARLIEST_TIME, "
                + "LATEST_TIME, MIN_DURATION, NEED_TO_PLAN, TARGET.ID TRGID, NAME "
                + "FROM OBSERVATION_REQUEST JOIN TARGET ON OBSERVATION_REQUEST.TARGET=TARGET.ID "
                + "WHERE (OBSERVATION_REQUEST.EARLIEST_TIME)::DATE=? AND "
                + "? >= ALL(SELECT POSITION_LAT FROM TARGET_POINT WHERE TARGET_POINT.TARGET=TARGET.ID) AND "
                + "? <= ALL(SELECT POSITION_LAT FROM TARGET_POINT WHERE TARGET_POINT.TARGET=TARGET.ID) AND "
                + "? >= ALL(SELECT POSITION_LNG FROM TARGET_POINT WHERE TARGET_POINT.TARGET=TARGET.ID) AND "
                + "? <= ALL(SELECT POSITION_LNG FROM TARGET_POINT WHERE TARGET_POINT.TARGET=TARGET.ID) AND "
                + "OBSERVATION_REQUEST.ID <> ALL(SELECT CLUSTER FROM REQUEST_CLUSTER) "
                + "ORDER BY EARLIEST_TIME");
        st.setDate(1, Date.valueOf(date));
        st.setDouble(2, area.getMaxLatitude());
        st.setDouble(3, area.getMinLatitude());
        st.setDouble(4, area.getMaxLongitude());
        st.setDouble(5, area.getMinLongitude());
        ResultSet rs = st.executeQuery();

        List<ObservationRequest> requests = new ArrayList<>();
        while (rs.next()) {
            ObservationRequest obsReq = new ObservationRequest(
                    rs.getLong("OBSID"),
                    rs.getString("CLIENT"),
                    rs.getTimestamp("EARLIEST_TIME"),
                    rs.getTimestamp("LATEST_TIME"),
                    rs.getInt("MIN_DURATION"),
                    rs.getBoolean("NEED_TO_PLAN"));
            Target target = new Target(
                    rs.getLong("TRGID"),
                    rs.getString("NAME"));
            addPositionstoTarget(target);
            obsReq.setTarget(target);
            addSensorsToRequestObject(obsReq);
            requests.add(obsReq);
        }

        rs.close();
        st.close();
        return requests;
    }

    public void renameTarget(Target target) throws SQLException {
        // Adds the request tuple and checks the result
        PreparedStatement st = dbConnection.prepareStatement(
                "UPDATE TARGET SET NAME=? WHERE ID=?");
        st.setString(1, target.getName());
        st.setLong(2, target.getId());
        int result = st.executeUpdate();
        st.close();
        if (result <= 0) {
            throw new SQLException("Database insertion failed!");
        }

        dbConnection.commit();
    }

    private void savePositionsOfTarget(Target target) throws SQLException {
        for (int i = 0; i < target.getPositionsSize(); i++) {
            Coordinates position = target.getPositionAt(i);
            PreparedStatement st = dbConnection.prepareStatement("INSERT INTO TARGET_POINT "
                    + "(TARGET, INDEX, POSITION_LAT, POSITION_LNG) "
                    + "VALUES (?, ?, ?, ?)");
            st.setLong(1, target.getId());
            st.setInt(2, i);
            st.setDouble(3, position.getLatitude());
            st.setDouble(4, position.getLongitude());
            int result = st.executeUpdate();
            st.close();
            if (result <= 0) {
                dbConnection.rollback();
                throw new SQLException("Database coordinates insertion failed!");
            }
        }
    }

    public List<Airport> getAllAirports() throws SQLException {
        PreparedStatement st = dbConnection.prepareStatement(
                "SELECT * FROM AIRPORT");

        ResultSet rs = st.executeQuery();
        List<Airport> airports = new ArrayList<>();
        while (rs.next()) {
            airports.add(new Airport(rs.getString("ID"), rs.getString("NAME"),
                    rs.getDouble("POSITION_LAT"), rs.getDouble("POSITION_LNG"),
                    rs.getDouble("ALTITUDE"),
                    rs.getDouble("END_TAKEOFF_LAT"), rs.getDouble("END_TAKEOFF_LNG"),
                    rs.getDouble("BEGIN_LANDING_LAT"), rs.getDouble("BEGIN_LANDING_LNG"),
                    rs.getLong("TAKEOFF_DURATION"),
                    rs.getLong("LANDING_DURATION")));
        }
        return airports;
    }
}
