/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.model;

import com.lynden.gmapsfx.javascript.object.LatLong;

/**
 *
 * @author marco
 */
public class CoordBounds {

    private final double minLatitude;
    private final double maxLatitude;
    private final double minLongitude;
    private final double maxLongitude;

    public CoordBounds(LatLong northWest, LatLong southEast) {
        minLatitude = southEast.getLatitude();
        maxLatitude = northWest.getLatitude();
        minLongitude = northWest.getLongitude();
        maxLongitude = southEast.getLongitude();
    }

    public double getMinLatitude() {
        return minLatitude;
    }

    public double getMaxLatitude() {
        return maxLatitude;
    }

    public double getMinLongitude() {
        return minLongitude;
    }

    public double getMaxLongitude() {
        return maxLongitude;
    }
}
