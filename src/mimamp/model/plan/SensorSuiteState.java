/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.model.plan;

import java.time.LocalDateTime;
import mimamp.model.Airport;

/**
 *
 * @author marco
 */
public class SensorSuiteState extends AgentState {

    private final SensorSuiteOnUAV suite;

    public SensorSuiteState(SensorSuiteOnUAV suite, Airport airport, LocalDateTime beginTime) {
        super(airport.getPosition(), beginTime);
        this.suite = suite;
    }
}
