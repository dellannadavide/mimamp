package mimamp.model.plan;

import mimamp.model.*;
import mimamp.model.plan.actions.DurativeUAVAction;
import mimamp.model.plan.actions.DurativeSensorSuiteAction;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import mimamp.model.plan.actions.FlyThroughTarget;
import mimamp.model.plan.actions.Landing;
import mimamp.model.plan.actions.Loiter;
import mimamp.model.plan.actions.WaitOnGround;

/**
 *
 * @author marco
 */
public class Plan {

    private final String planning_model;
    private long id;
    private String name;
    private final Set<UAVAvailability> initialState;
    private Map<UAV, List<DurativeUAVAction>> uavPlanMap;
    private Map<SensorSuiteOnUAV, List<DurativeSensorSuiteAction>> suitePlanMap;
    private Map<UAV, LocalDateTime> startTimes;

    public Plan(Mission m, String planning_model) {
        this.id = -1;
        this.name = "Mission nr. " + m.getId() + " planned on " + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE);
        this.initialState = m.getInitialState();
        this.uavPlanMap = new HashMap<>();
        this.suitePlanMap = new HashMap<>();
        this.startTimes = new HashMap<>();
        this.planning_model = planning_model;
        m.getUAVsAvailability().stream()
                .forEach(uavAv -> {
                    UAV uav = uavAv.getUAV();
                    uavPlanMap.put(uav, new ArrayList<>());
                    if(planning_model.equals(PlanningModels.NUMERIC))
                        startTimes.put(uav, m.getFirstUsefulTakeoffTime(uavAv));

                    SensorSuiteOnUAV suite = new SensorSuiteOnUAV(uavAv.getSensorSuite(), uav);
                    suitePlanMap.put(suite, new ArrayList<>());
                });
    }

    public void addSensorSuiteAction(SensorSuiteOnUAV suite, DurativeSensorSuiteAction action) {
        List<DurativeSensorSuiteAction> actions = suitePlanMap.get(suite);
        actions.add(action);
    }

    public void addUAVAction(UAV uav, DurativeUAVAction action) {
        List<DurativeUAVAction> actions = uavPlanMap.get(uav);
        actions.add(action);
    }

    public List<DurativeUAVAction> getActionsOf(UAV uav) {
        return uavPlanMap.get(uav);
    }

    public List<DurativeSensorSuiteAction>
            getActionsOf(SensorSuiteOnUAV suite) {
        return suitePlanMap.get(suite);
    }

    public Airport getAirportOf(UAV uav) {
        return initialState.stream()
                .filter(uavAv -> uavAv.getUAV().equals(uav))
                .findAny()
                .get()
                .getAirport();
    }

    public Set<Airport> getAirports() {
        return initialState.stream()
                .map(uavAv -> uavAv.getAirport())
                .distinct()
                .collect(Collectors.toSet());
    }

    public long getId() {
        return id;
    }

    public LocalDateTime getLastTimeFor(SensorSuiteOnUAV suite) {
        List<DurativeSensorSuiteAction> actions = suitePlanMap.get(suite);
        if (actions.isEmpty()) {
            return startTimes.get(suite.getUAV());
        } else {
            return actions.get(actions.size() - 1).getEndOfAction();
        }
    }

    public LocalDateTime getLastTimeFor(UAV uav) {
        List<DurativeUAVAction> actions = uavPlanMap.get(uav);
        if (actions.isEmpty()) {
            if (planning_model.equals(PlanningModels.TEMPORAL))
                System.out.println("Qualcosa di sbagliato, sei in un metodo che dovrebbe essere usato da un  modello numerico ma è stato usato da uno temporale");
            return startTimes.get(uav);
        } else {
            return actions.get(actions.size() - 1).getEndOfAction();
        }
    }
    public LocalDateTime getLastTimeFor(UAV uav, LocalDateTime lastTime) {
        List<DurativeUAVAction> actions = uavPlanMap.get(uav);
        if (actions.isEmpty()) {
            if (planning_model.equals(PlanningModels.TEMPORAL))
                startTimes.put(uav, lastTime);
            return startTimes.get(uav);
        } else {
            return actions.get(actions.size() - 1).getEndOfAction();
        }
    }


    public String getName() {
        return name;
    }

    public LocalDateTime getStartTimeOf(UAV uav) {
        return startTimes.get(uav);
    }

    public Set<SensorSuiteOnUAV> getSuites() {
        return suitePlanMap.keySet();
    }

    public Set<UAV> getUAVs() {
        return uavPlanMap.keySet();
    }

    public void printPlan() {
        uavPlanMap.keySet().forEach(uav -> printPlanFor(uav));
        suitePlanMap.keySet().forEach(suite -> printPlanFor(suite));
    }

    private void printPlanFor(UAV uav) {
        UAVState state = new UAVState(uav, getAirportOf(uav), startTimes.get(uav));
        System.out.println("Plan for UAV " + uav);

        List<DurativeUAVAction> actions = uavPlanMap.get(uav);
        actions.stream().forEachOrdered(action -> {
            System.out.println(action.toString().replace('\n', ' '));
            action.execute(state);
        });
        System.out.println(actions.get(actions.size() - 1).getEndOfAction()
                .format(DateTimeFormatter.ISO_LOCAL_TIME));
        System.out.println();
    }

    private void printPlanFor(SensorSuiteOnUAV suite) {
        UAV uav = suite.getUAV();
        SensorSuiteState state = new SensorSuiteState(
                suite, getAirportOf(uav), startTimes.get(uav));
        System.out.println("Plan for suite " + suite.getSensorSuite() + ", mounted on " + uav);

        List<DurativeSensorSuiteAction> actions = suitePlanMap.get(suite);
        actions.stream().forEachOrdered(action -> {
            System.out.println(action.toString().replace('\n', ' '));
            action.execute(state);
        });
        System.out.println(actions.get(actions.size() - 1).getEndOfAction()
                .format(DateTimeFormatter.ISO_LOCAL_TIME));
        System.out.println();
    }

    public void setId(long id) {
        this.id = id;
        this.name = "Mission nr. " + id + " planned on " + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE);
    }

    public Set<UAVAvailability> getInitialStates() {
        return initialState;
    }

    public long getAvailabilityIdOf(UAV uav) {
        return initialState.stream()
                .filter(uavAv -> uavAv.getUAV().equals(uav))
                .findFirst()
                .get()
                .getId();
    }

    public String getKMLPathOf(UAV uav) {
        StringBuilder builder = new StringBuilder();
        UAVState state = new UAVState(uav, getAirportOf(uav), startTimes.get(uav));
        Coordinates position = state.getPosition();
        builder
                .append(position.getLongitude()).append(',')
                .append(position.getLatitude()).append(',')
                .append("0");
        List<DurativeUAVAction> actions = uavPlanMap.get(uav);
        for (DurativeUAVAction action : actions) {
            if (action instanceof WaitOnGround
                    || action instanceof Loiter) {
                continue;
            }
            if (action instanceof FlyThroughTarget) {
                FlyThroughTarget through = ((FlyThroughTarget) action);
                List<Coordinates> path = through.getPathArrayCoord();
                if (through.isForward()) {
                    path.stream().forEach((c) -> {
                        builder.append(' ')
                                .append(c.getLongitude()).append(',')
                                .append(c.getLatitude()).append(',')
                                .append(4 * uav.getCruisingAltitude());
                    });
                } else {
                    for (int i = path.size() - 1; i >= 0; i--) {
                        Coordinates c = path.get(i);
                        builder.append(' ')
                                .append(c.getLongitude()).append(',')
                                .append(c.getLatitude()).append(',')
                                .append(4 * uav.getCruisingAltitude());
                    }
                }
                action.execute(state);
//                position = state.getPosition();
            } else {
                action.execute(state);
                position = state.getPosition();
                builder.append(' ')
                        .append(position.getLongitude()).append(',')
                        .append(position.getLatitude()).append(',')
                        .append(action instanceof Landing ? "0" : 4 * uav.getCruisingAltitude());
            }
        }
        return builder.toString();
    }

    public boolean isObservedForward(ObservationRequest observationRequest) {
        List<DurativeUAVAction> allActions = new ArrayList<>();
        uavPlanMap.values().forEach(list -> allActions.addAll(list));
        return allActions.stream()
                .filter(a -> a instanceof FlyThroughTarget)
                .map(a -> (FlyThroughTarget) a)
                .filter(m -> m.getRequest().equals(observationRequest))
                .findAny()
                .get()
                .isForward();
    }
}
