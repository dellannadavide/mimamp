/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.model.plan.actions;

import java.time.Duration;
import java.time.LocalDateTime;
import mimamp.model.ObservationRequest;
import mimamp.model.plan.SensorSuiteState;

/**
 *
 * @author marco
 */
public class Monitor extends DurativeSensorSuiteAction {

    private final ObservationRequest request;
    private final Duration duration;

    public Monitor(LocalDateTime beginOfAction, ObservationRequest request) {
        super(beginOfAction);
        this.request = request;
        this.duration = Duration.ofMillis(request.getMinDuration(true));
    }

    public Monitor(LocalDateTime beginOfAction, ObservationRequest request, long duration) {
        super(beginOfAction);
        this.request = request;
        this.duration = Duration.ofMillis(duration);
    }

    @Override
    public void execute(SensorSuiteState state) {
        state.setTime(getEndOfAction());
    }

    @Override
    public Duration getDuration() {
        return duration;
    }

    public ObservationRequest getObservationRequest() {
        return request;
    }

    @Override
    public String toString() {
        return String.format("%s - %s (%d seconds)\nMonitoring target %s\nUsing sensors %s",
                getBeginOfActionStr(), getEndOfActionStr(), getDuration().getSeconds(),
                request.getTargetName(), request.getSensors());
    }

    public long getObservationRequestId() {
        return request.getId();
    }
}
