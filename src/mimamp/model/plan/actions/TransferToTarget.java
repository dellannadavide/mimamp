/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.model.plan.actions;

import java.time.Duration;
import java.time.LocalDateTime;
import mimamp.model.Coordinates;
import mimamp.model.Target;
import mimamp.model.plan.UAVState;

/**
 *
 * @author marco
 */
public class TransferToTarget extends DurativeUAVAction {

    private final Coordinates prevLocation;
    private final double speed;
    private final Target target;
    private boolean fromFirstPoint;
    private final Duration duration;

    public TransferToTarget(LocalDateTime beginOfAction, Target target,
            Coordinates prevLocation, Coordinates nextLocation, double speed) {
        super(beginOfAction);
        this.prevLocation = prevLocation;
        this.speed = speed;
        this.target = target;
        fromFirstPoint = (target.getFirstPosition() == nextLocation);
        double distance = prevLocation.distanceFrom(nextLocation);
        this.duration = Duration.ofMillis(Math.round(distance / speed));
    }
    public TransferToTarget(LocalDateTime beginOfAction, Target target,
                            Coordinates prevLocation, Coordinates nextLocation, double speed, long duration) {
        super(beginOfAction);
        this.prevLocation = prevLocation;
        this.speed = speed;
        this.target = target;
        fromFirstPoint = (target.getFirstPosition() == nextLocation);
        this.duration = Duration.ofMillis(duration);
    }

    @Override
    public void execute(UAVState state) {
        state.moveToPosition(fromFirstPoint ? target.getFirstPosition() : target.getLastPosition());
        state.setTime(getEndOfAction());
    }

    public void setFromFirstPoint(boolean fromFirstPoint) {
        this.fromFirstPoint = fromFirstPoint;
    }

    @Override
    public Duration getDuration() {
        return duration;
    }

    @Override
    public String toString() {
        return String.format("%s - %s (%d seconds)\nTransfering to target:\n%s",
                getBeginOfActionStr(), getEndOfActionStr(), getDuration().getSeconds(),
                target);
    }

    public double getLatitude() {
        return prevLocation.getLatitude();
    }

    public double getLongitude() {
        return prevLocation.getLongitude();
    }

    public double getSpeed() {
        return speed;
    }

    public long getTargetId() {
        return target.getId();
    }

    public boolean isFromFirstPoint() {
        return fromFirstPoint;
    }
}
