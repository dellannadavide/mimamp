/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.model.plan.actions;

import java.time.Duration;
import java.time.LocalDateTime;
import mimamp.model.Coordinates;
import mimamp.model.plan.SensorSuiteState;

/**
 *
 * @author marco
 */
public class Transfer extends DurativeSensorSuiteAction {

    private final Duration duration;
    private final Coordinates nextLocation;

    public Transfer(LocalDateTime beginOfAction, Duration duration, Coordinates nextLocation) {
        super(beginOfAction);
        this.duration = duration;
        this.nextLocation = nextLocation;
    }

    @Override
    public void execute(SensorSuiteState state) {
        state.moveToPosition(nextLocation);
        state.setTime(getEndOfAction());
    }

    @Override
    public Duration getDuration() {
        return duration;
    }

    @Override
    public String toString() {
        return String.format("%s - %s (%d seconds)\nBeing transfered to:\n%s",
                getBeginOfActionStr(), getEndOfActionStr(), getDuration().getSeconds(),
                nextLocation);
    }

    public double getLatitude() {
        return nextLocation.getLatitude();
    }

    public double getLongitude() {
        return nextLocation.getLongitude();
    }
}
