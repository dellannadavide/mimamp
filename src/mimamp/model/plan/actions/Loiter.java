/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.model.plan.actions;

import java.time.Duration;
import java.time.LocalDateTime;
import mimamp.controller.encoding.Encoder;
import mimamp.model.plan.UAVState;

/**
 *
 * @author marco
 */
public class Loiter extends DurativeUAVAction {

    private final Duration duration;
    private final boolean no;

    private Loiter(LocalDateTime beginOfAction, boolean no, Duration duration) {
        super(beginOfAction);
        this.duration = duration;
        this.no = no;
    }

    public Loiter(LocalDateTime beginOfAction, Duration duration) {
        this(beginOfAction, false, duration);
    }
    public Loiter(LocalDateTime beginOfAction, long duration) {
        this(beginOfAction, false, Duration.ofMillis(duration));
    }

    public Loiter(LocalDateTime beginOfAction, boolean no) {
        this(beginOfAction, no, Duration.ofMillis(Encoder.getTimeLoiter(true)));
    }

    @Override
    public void execute(UAVState state) {
        state.useFuelFor(getDuration());
        state.setTime(getEndOfAction());
    }

    @Override
    public Duration getDuration() {
        return duration;
    }

    public boolean isNo() {
        return no;
    }

    @Override
    public String toString() {
        return getBeginOfActionStr() + "\nLoiter on this place";
    }
}
