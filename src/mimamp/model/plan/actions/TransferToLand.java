/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.model.plan.actions;

import java.time.Duration;
import java.time.LocalDateTime;
import mimamp.model.Airport;
import mimamp.model.Coordinates;
import mimamp.model.plan.UAVState;

/**
 *
 * @author marco
 */
public class TransferToLand extends DurativeUAVAction {
    private final Coordinates prevLocation;
    private final double speed;
    private final Airport airport;
    private final Duration duration;

    public TransferToLand(LocalDateTime beginOfAction, Airport airport,
            Coordinates prevLocation, double speed) {
        super(beginOfAction);
        this.prevLocation = prevLocation;
        this.speed = speed;
        this.airport = airport;
        double distance = prevLocation.distanceFrom(airport.getBeginOfLanding());
        this.duration = Duration.ofMillis(Math.round(distance / speed));

    }

    public TransferToLand(LocalDateTime beginOfAction, Airport airport,
                          Coordinates prevLocation, double speed, long duration) {
        super(beginOfAction);
        this.prevLocation = prevLocation;
        this.speed = speed;
        this.airport = airport;
        this.duration = Duration.ofMillis(duration);
    }
    
    @Override
    public void execute(UAVState state) {
        state.moveToPosition(airport.getBeginOfLanding());
        state.setTime(getEndOfAction());
    }

    @Override
    public Duration getDuration() {
        return duration;
    }
    
    @Override
    public String toString() {
        return String.format("%s - %s (%d seconds)\nTransfering to airport:\n%s",
                getBeginOfActionStr(), getEndOfActionStr(), getDuration().getSeconds(),
                airport);
    }

    public double getLatitude() {
        return prevLocation.getLatitude();
    }

    public double getLongitude() {
        return prevLocation.getLongitude();
    }

    public double getSpeed() {
        return speed;
    }

    public String getAirportId() {
        return airport.getId();
    }
}
