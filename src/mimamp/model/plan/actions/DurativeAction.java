package mimamp.model.plan.actions;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author marco
 */
public abstract class DurativeAction implements Comparable<DurativeAction> {

    private final LocalDateTime beginOfAction;
    private final DateTimeFormatter formatter;

    public DurativeAction(LocalDateTime beginOfAction) {
        this.beginOfAction = beginOfAction;
        formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
    }

    @Override
    public int compareTo(DurativeAction action) {
        return this.beginOfAction.compareTo(action.beginOfAction);
    }

    public LocalDateTime getBeginOfAction() {
        return beginOfAction;
    }

    public String getBeginOfActionStr() {
        return formatter.format(beginOfAction);
    }

    public abstract Duration getDuration();

    public LocalDateTime getEndOfAction() {
        return beginOfAction.plus(getDuration());
    }

    public String getEndOfActionStr() {
        return formatter.format(getEndOfAction());
    }
}
