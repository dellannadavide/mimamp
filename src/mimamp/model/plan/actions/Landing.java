/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.model.plan.actions;

import java.time.Duration;
import java.time.LocalDateTime;
import mimamp.model.Airport;
import mimamp.model.plan.UAVState;

/**
 *
 * @author marco
 */
public class Landing extends DurativeUAVAction {
    private final Airport airport;
    private final boolean prev;
    private final Duration duration;

    public Landing(LocalDateTime beginOfAction, Airport airport, boolean prev) {
        super(beginOfAction);
        this.airport = airport;
        this.prev = prev;
        this.duration = Duration.ofMillis(airport.getLandingDuration(true));
    }
    public Landing(LocalDateTime beginOfAction, Airport airport, boolean prev, long duration) {
        super(beginOfAction);
        this.airport = airport;
        this.prev = prev;
        this.duration = Duration.ofMillis(duration);
    }

    @Override
    public void execute(UAVState state) {
        state.moveToPosition(airport.getPosition());
        state.setOnFly(false);
        state.setTime(getEndOfAction());
    }

    @Override
    public Duration getDuration() {
        return duration;
    }
    
    public boolean isPrev() {
        return prev;
    }
    
    @Override
    public String toString() {
        return String.format("%s - %s (%d seconds)\nLanding to airport:\n %s",
                getBeginOfActionStr(), getEndOfActionStr(), getDuration().getSeconds(),
                airport);
    }

    public String getAirportId() {
        return airport.getId();
    }
}
