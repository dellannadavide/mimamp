/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.model.plan.actions;

import java.time.LocalDateTime;
import mimamp.model.plan.SensorSuiteState;

/**
 *
 * @author marco
 */
public abstract class DurativeSensorSuiteAction extends DurativeAction {

    public DurativeSensorSuiteAction(LocalDateTime beginOfAction) {
        super(beginOfAction);
    }

    public abstract void execute(SensorSuiteState state);
}
