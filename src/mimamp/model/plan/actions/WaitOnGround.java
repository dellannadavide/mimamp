/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.model.plan.actions;

import java.time.Duration;
import java.time.LocalDateTime;

import mimamp.controller.encoding.NumericEncoder;
import mimamp.model.plan.UAVState;

/**
 *
 * @author marco
 */
public class WaitOnGround extends DurativeUAVAction {

    public WaitOnGround(LocalDateTime beginOfAction) {
        super(beginOfAction);
    }

    @Override
    public void execute(UAVState state) {
        state.setTime(getEndOfAction());
    }

    @Override
    public Duration getDuration() {
        return Duration.ofMillis(NumericEncoder.getTimeWaitOnGround(true));
    }
    
    @Override
    public String toString() {
        return getBeginOfActionStr() + "\nWaiting on ground";
    }
}
