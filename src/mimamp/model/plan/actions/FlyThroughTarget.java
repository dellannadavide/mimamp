/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.model.plan.actions;

import com.lynden.gmapsfx.javascript.object.LatLong;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import mimamp.model.Coordinates;
import mimamp.model.ObservationRequest;
import mimamp.model.plan.UAVState;

/**
 *
 * @author marco
 */
public class FlyThroughTarget extends DurativeUAVAction {

    private final boolean forward;
    private final ObservationRequest request;
    private final Duration duration;

    public FlyThroughTarget(LocalDateTime beginOfAction,
            ObservationRequest request, Coordinates startPosition) {
        super(beginOfAction);
        this.request = request;
        this.forward = request.isFirstPosition(startPosition);
        this.duration = request.getMinDurationObj();
    }

    public FlyThroughTarget(LocalDateTime beginOfAction,
                            ObservationRequest request, Coordinates startPosition, long duration) {
        super(beginOfAction);
        this.request = request;
        this.forward = request.isFirstPosition(startPosition);
        this.duration = Duration.ofMillis(duration);
    }

    @Override
    public void execute(UAVState state) {
        state.moveToPosition(forward ? request.getTargetLastPosition()
                : request.getTargetFirstPosition());
        state.setTime(getEndOfAction());
    }

    @Override
    public Duration getDuration() {
        return duration;
    }
    
    public LatLong[] getPathArray() {
        return request.getPathArray();
    }
    
    public boolean isForward() {
        return forward;
    }

    @Override
    public String toString() {
        return getBeginOfAction().format(DateTimeFormatter.ISO_LOCAL_TIME)
                + " - Flying through target " + request.getTargetName()
                + ",\nfrom " + (forward ? request.getTargetFirstPosition() : request.getTargetLastPosition())
                + " to " + (forward ? request.getTargetLastPosition() : request.getTargetFirstPosition())
                + " (" + getDuration().getSeconds() + " secs)";
    }

    public long getRequestId() {
        return request.getId();
    }

    public List<Coordinates> getPathArrayCoord() {
        return request.getPathArrayCoord();
    }

    public ObservationRequest getRequest() {
        return request;
    }
}
