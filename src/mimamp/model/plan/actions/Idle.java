/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.model.plan.actions;

import java.time.Duration;
import java.time.LocalDateTime;
import mimamp.model.plan.SensorSuiteState;

/**
 *
 * @author marco
 */
public class Idle extends DurativeSensorSuiteAction {

    private final Duration duration;

    public Idle(LocalDateTime beginOfAction, Duration duration) {
        super(beginOfAction);
        this.duration = duration;
    }

    public Idle(LocalDateTime beginOfAction, int durationMillis) {
        this(beginOfAction, Duration.ofMillis(durationMillis));
    }

    @Override
    public void execute(SensorSuiteState state) {
        state.setTime(getEndOfAction());
    }

    @Override
    public Duration getDuration() {
        return duration;
    }

    @Override
    public String toString() {
        return getBeginOfActionStr() + "\nIdle";
    }
}
