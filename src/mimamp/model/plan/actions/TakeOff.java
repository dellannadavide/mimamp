/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.model.plan.actions;

import java.time.Duration;
import java.time.LocalDateTime;
import mimamp.model.Airport;
import mimamp.model.plan.UAVState;

/**
 *
 * @author marco
 */
public class TakeOff extends DurativeUAVAction {

    private final Airport airport;
    private final boolean succ;
    private final Duration duration;

    public TakeOff(LocalDateTime beginOfAction, Airport airport, boolean succ) {
        super(beginOfAction);
        this.airport = airport;
        this.succ = succ;
        this.duration = Duration.ofMillis(airport.getTakeoffDuration(true));
    }
    public TakeOff(LocalDateTime beginOfAction, Airport airport, boolean succ, long duration) {
        super(beginOfAction);
        this.airport = airport;
        this.succ = succ;
        this.duration = Duration.ofMillis(duration);
    }

    @Override
    public Duration getDuration() {
        return duration;
    }

    @Override
    public void execute(UAVState state) {
        state.setOnFly(true);
        state.moveToPosition(airport.getEndOfTakeoff());
        state.setTime(getEndOfAction());
    }

    public boolean isSucc() {
        return succ;
    }

    @Override
    public String toString() {
        return String.format("%s - %s (%d seconds)\nTaking off from airport:\n%s",
                getBeginOfActionStr(), getEndOfActionStr(), getDuration().getSeconds(),
                airport);
    }

    public String getAirportId() {
        return airport.getId();
    }
}
