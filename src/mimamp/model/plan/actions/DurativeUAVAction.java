/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.model.plan.actions;

import java.time.LocalDateTime;
import mimamp.model.plan.UAVState;

/**
 *
 * @author marco
 */
public abstract class DurativeUAVAction extends DurativeAction {

    public DurativeUAVAction(LocalDateTime beginOfAction) {
        super(beginOfAction);
    }

    public abstract void execute(UAVState state);
}
