/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.model.plan;

import java.time.LocalDateTime;
import mimamp.model.Coordinates;

/**
 *
 * @author marco
 */
class AgentState {

    private Coordinates position;
    private LocalDateTime time;

    AgentState(Coordinates position, LocalDateTime beginTime) {
        this.position = position;
        this.time = beginTime;
    }

    public Coordinates getPosition() {
        return position;
    }

    public void moveToPosition(Coordinates newPosition) {
        this.position = newPosition;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }
}
