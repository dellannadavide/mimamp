/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.model.plan;

import java.util.Objects;
import mimamp.model.SensorSuite;
import mimamp.model.UAV;

/**
 *
 * @author marco
 */
public class SensorSuiteOnUAV {

    private final SensorSuite suite;
    private final UAV uav;

    public SensorSuiteOnUAV(SensorSuite suite, UAV uav) {
        this.suite = suite;
        this.uav = uav;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj instanceof SensorSuiteOnUAV
                && (this == obj || ((SensorSuiteOnUAV) obj).getUAVId() == this.getUAVId());
    }

    SensorSuite getSensorSuite() {
        return suite;
    }
    
    public String getUAVDescription() {
        return uav + ", mounting suite " + suite;
    }

    long getUAVId() {
        return uav.getId();
    }

    public UAV getUAV() {
        return uav;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.suite);
        hash = 71 * hash + Objects.hashCode(this.uav);
        return hash;
    }
    
    @Override
    public String toString() {
        return "Suite " + suite;
    }
}
