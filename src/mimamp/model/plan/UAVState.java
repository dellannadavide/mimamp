/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.model.plan;

import java.time.Duration;
import java.time.LocalDateTime;
import mimamp.model.Airport;
import mimamp.model.Coordinates;
import mimamp.model.UAV;

/**
 *
 * @author marco
 */
public class UAVState extends AgentState {
    private boolean onFly;
    private final UAV uav;
    
    public UAVState(UAV uav, Airport airport, LocalDateTime beginTime) {
        super(airport.getPosition(), beginTime);
        this.onFly = false;
        this.uav = uav;
    }

    @Override
    public void moveToPosition(Coordinates newPosition) {
        super.moveToPosition(newPosition);
        uav.useFuelForLength(getPosition().distanceFrom(newPosition));
    }

    public void setOnFly(boolean onFly) {
        this.onFly = onFly;
    }

    public void useFuelFor(Duration duration) {
        uav.useFuelForDuration(duration);
    }
}
