package mimamp.model;

import com.lynden.gmapsfx.javascript.JavascriptObject;
import com.lynden.gmapsfx.javascript.object.LatLong;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A time-bounded request to observe a certain Target using a certain Sensor.
 */
public class ObservationRequest implements PDDLObject {

    /**
     * An unique identifier.
     */
    long id;
    /**
     * The client that is creating this request.
     */
    String client;
    /**
     * The Target to observe.
     */
    Target target;
    /**
     * The Sensor to observe the Target with.
     */
    private Set<Sensor> sensors;
    /**
     * The observation's earliest time.
     */
    LocalDateTime earliestTime;
    /**
     * The observation's latest time.
     */
    LocalDateTime latestTime;
    /**
     * The minimum observation duration.
     */
    private Duration minDuration;
    /**
     * <code>true</code> if this request has already been took in charge.
     */
    private boolean needToPlan;

    /**
     * @param id An unique identifier
     * @param client The client that is creating this request
     * @param target The Target to observe
     * @param earliestTime The observation's earliest time
     * @param latestTime The observation's latest time
     * @param minDuration The minimum observation duration
     * @param needToPlan <code>true</code> if this request has already been took
     * in charge
     *
     * @throws IllegalArgumentException if the time-bound is not within a single
     * calendar day
     */
    public ObservationRequest(long id, String client, Target target,
            LocalDateTime earliestTime, LocalDateTime latestTime,
            Duration minDuration, boolean needToPlan) {
        if (earliestTime == null || latestTime == null) {
            earliestTime = LocalDate.now().atStartOfDay();
            latestTime = LocalDate.now().atTime(23, 59, 59, 0);
        } else if (earliestTime.getDayOfMonth() != latestTime.getDayOfMonth()) {
            throw new IllegalArgumentException(
                    "Time interval must be within a single day!");
        }

        this.id = id;
        this.client = client;
        this.target = target;
        this.sensors = new HashSet<>();
        this.earliestTime = earliestTime;
        this.latestTime = latestTime;
        this.minDuration = minDuration;
        this.needToPlan = needToPlan;
    }

    public ObservationRequest(long id, String client,
            Timestamp earliestTimeStamp, Timestamp latestTimeStamp,
            int minDuration, boolean needToPlan) {
        this(id, client, null,
                earliestTimeStamp.toLocalDateTime(),
                latestTimeStamp.toLocalDateTime(),
                Duration.ofMillis(minDuration), needToPlan);
    }

    public ObservationRequest(Target target) {
        this(-1, "", target, null, null, Duration.ofMillis(1000), true);
    }

    public void addSensor(String sensorStr) {
        sensors.add(new Sensor(Sensor.SensorType.valueOf(sensorStr)));
    }

    public void addSensor(Sensor sensor) {
        sensors.add(sensor);
    }

    /**
     * Seconds.
     *
     * @param uav
     * @return
     */
    public long bestTimeFor(UAV uav) {
        return Math.round(target.getLength() / uav.getCruisingSpeed(false));
    }

    public double distanceFrom(ObservationRequest obsReq) {
        if (!obsReq.targetIsPoint()) {
            throw new IllegalArgumentException(
                    "Can't calculate a distance from a target line!");
        }
        return obsReq.getTargetPosition()
                .distanceFrom(target.getFirstPosition());
    }

    @Override
    public boolean equals(Object o) {
        return this == o
                || (o != null && o instanceof ObservationRequest
                && ((ObservationRequest) o).id == this.id);
    }

    public String getClient() {
        return client;
    }

    public LocalDate getDate() {
        return earliestTime.toLocalDate();
    }

    public String getDetailString() {
        if (targetIsPoint()) {
            return String.format("Req. nr. %d, by client '%s'\n"
                    + "At coordinates %s\n"
                    + "With sensors: %s\n"
                    + "On %s, %s - %s\n"
                    + "During at least %d seconds\n"
                    + "%s planned",
                    id, client, target.getPosition(), getSensorsString(),
                    earliestTime.toLocalDate(), earliestTime.toLocalTime(),
                    latestTime.toLocalTime(), minDuration.getSeconds(),
                    needToPlan ? "Not yet" : "Already");
        } else {
            return String.format("Req. nr. %d, by client '%s'\n"
                    + "Path from %s\n  to %s\n"
                    + "With sensors: %s\n"
                    + "On %s, %s - %s\n"
                    + "During at least %d seconds\n"
                    + "%s planned",
                    id, client, target.getFirstPosition(), target.getLastPosition(),
                    getSensorsString(), earliestTime.toLocalDate(),
                    earliestTime.toLocalTime(), latestTime.toLocalTime(),
                    minDuration.getSeconds(), needToPlan ? "Not yet" : "Already");
        }
    }

    public LocalDateTime getEarliestTime() {
        return earliestTime;
    }

    public String getEarliestTimeStr() {
        return earliestTime.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
    }

    public long getId() {
        return id;
    }

    public LocalDateTime getLatestTime() {
        return latestTime;
    }

    public String getLatestTimeStr() {
        return latestTime.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
    }

    public Duration getMinDurationObj() {
        return minDuration;
    }
    public long getMinDuration(boolean millisec) {
        return millisec ? minDuration.toMillis() : minDuration.getSeconds();
    }
    /*public long getMinDurationMillis() {
        return minDuration.toMillis();
    }*/

    /*public long getMinDurationSec() {
        return minDuration.getSeconds();
    }*/

    boolean getNeedToPlan() {
        return needToPlan;
    }

    public LatLong[] getPathArray() {
        return target.getPathArray();
    }
    
    public String getSensorColor() {
        return sensors.stream().findFirst().get().getColor();
    }

    public String getKMLSensorColor() {
        return sensors.stream().findFirst().get().getKMLColor();
    }

    public Set<Sensor> getSensors() {
        return sensors;
    }

    private String getSensorsString() {
        StringBuilder str = new StringBuilder();
        sensors.stream().forEach(s -> str.append(s).append(" "));
        return str.toString();
    }

    public Target getTarget() {
        return target;
    }

    public JavascriptObject getTargetShape() {
        return target.getShape();
    }

    double getTargetMostFarPositionFrom(Coordinates position) {
        return target.getMostFarPositionFrom(position);
    }

    public String getTargetName() {
        return target.getName();
    }

    public Coordinates getTargetFirstPosition() {
        return target.getFirstPosition();
    }

    public Coordinates getTargetLastPosition() {
        return target.getLastPosition();
    }

    public double getTargetLength() {
        target.calculateLength();
        return target.getLength();
    }

    double getTargetLessFarBoundary(Coordinates position) {
        return target.getLessFarBoundary(position);
    }

    public Coordinates getTargetPosition() {
        return target.getPosition();
    }

    public LatLong getTargetLatLongPosition() {
        return getTargetPosition().toLatLong();
    }

    long getTargetId() {
        return target.getId();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    public boolean isFirstPosition(Coordinates startPosition) {
        return target.isFirstPosition(startPosition);
    }

    public boolean targetIsPoint() {
        return target.isPoint();
    }

    @Override
    public String pddl() {
        return "obsReq" + id;
    }

    public String pddlSite() {
        return target.pddl();
    }

    public String pddlLastSite() {
        return target.pddlLast();
    }

    public void removeSensor(String sensorStr) {
        sensors.remove(new Sensor(Sensor.SensorType.valueOf(sensorStr)));
    }

    public void setClient(String client) {
        this.client = client;
    }

    public void setEarliestTime(LocalDateTime earliestTime) {
        this.earliestTime = earliestTime;
    }

    void setId(long id) {
        this.id = id;
    }

    public void setLatestTime(LocalDateTime latestTime) {
        this.latestTime = latestTime;
    }

    public void setMinDurationSec(long minDurationSec) {
        this.minDuration = Duration.ofSeconds(minDurationSec);
    }

    public void setTargetShape(JavascriptObject jso) {
        target.setShape(jso);
    }

    public void setTarget(Target target) {
        this.target = target;
    }

    boolean timeCompatibleWith(ObservationRequest req) {
        ObservationRequest first, second;
        if (earliestTime.isBefore(req.earliestTime)) {
            first = this;
            second = req;
        } else {
            first = req;
            second = this;
        }

        if (first.latestTime.isAfter(second.earliestTime)) {
            Duration duration;
            if (first.latestTime.isBefore(second.latestTime)) {
                // [__first__]
                //     [____second____]
                duration = Duration.between(second.earliestTime, first.latestTime);
            } else {
                // [_____first_____]
                //      [_second_]
                duration = Duration.between(second.earliestTime, second.latestTime);
            }
            return duration.compareTo(first.minDuration) > 0
                    && duration.compareTo(second.minDuration) > 0;
        }
        return false;
    }

    public boolean timeIncompatibleWith(UAV uav) {
        long flyTimeSec = bestTimeFor(uav);
        long durationSec = minDuration.getSeconds();
        return durationSec < 0.85 * flyTimeSec
                || durationSec > 1.15 * flyTimeSec;
    }

    @Override
    public String toString() {
        return id + " - target '" + target + "' with sensors " + getSensorsString();
    }

    public List<Coordinates> getPathArrayCoord() {
        return target.getPositions();
    }
}
