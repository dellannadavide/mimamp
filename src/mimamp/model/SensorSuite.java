package mimamp.model;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A suite of Sensors an UAV can be available with.
 */
public enum SensorSuite implements PDDLObject {
    S_CAMERA, S_EO, S_HYPERSPECTRAL, S_IR, S_RADAR,
    SUITE1, SUITE2, SUITE3, SUITE4, SUITE5, SUITE6, SUITE7;

    private Set<Sensor.SensorType> getSuite() {
        Set<Sensor.SensorType> suite = new HashSet<>();
        switch (this) {
            case S_CAMERA:
                suite.add(Sensor.SensorType.CAMERA);
                break;
            case S_EO:
                suite.add(Sensor.SensorType.EO);
                break;
            case S_HYPERSPECTRAL:
                suite.add(Sensor.SensorType.HYPERSPECTRAL);
                break;
            case S_IR:
                suite.add(Sensor.SensorType.IR);
                break;
            case S_RADAR:
                suite.add(Sensor.SensorType.RADAR);
                break;
            case SUITE1:
                suite.add(Sensor.SensorType.EO);
                suite.add(Sensor.SensorType.IR);
                break;
            case SUITE2:
                suite.add(Sensor.SensorType.EO);
                suite.add(Sensor.SensorType.RADAR);
                break;
            case SUITE3:
                suite.add(Sensor.SensorType.EO);
                suite.add(Sensor.SensorType.HYPERSPECTRAL);
                break;
            case SUITE4:
                suite.add(Sensor.SensorType.HYPERSPECTRAL);
                suite.add(Sensor.SensorType.RADAR);
                break;
            case SUITE5:
                suite.add(Sensor.SensorType.EO);
                suite.add(Sensor.SensorType.IR);
                suite.add(Sensor.SensorType.RADAR);
                break;
            case SUITE6:
                suite.add(Sensor.SensorType.EO);
                suite.add(Sensor.SensorType.HYPERSPECTRAL);
                suite.add(Sensor.SensorType.RADAR);
                break;
            case SUITE7:
                suite.add(Sensor.SensorType.EO);
                suite.add(Sensor.SensorType.HYPERSPECTRAL);
                suite.add(Sensor.SensorType.IR);
                suite.add(Sensor.SensorType.RADAR);
                break;
        }
        return suite;
    }

    boolean contains(Set<Sensor> sensors) {
        return getSuite().containsAll(
                sensors.stream()
                .map(s -> s.getType())
                .collect(Collectors.toList()));
    }

    @Override
    public String pddl() {
        return name();
    }
}
