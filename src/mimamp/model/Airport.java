package mimamp.model;

import java.time.Duration;
import java.util.Objects;

/**
 * An Airport, eg a place where an inactive UAV belongs to.
 */
public class Airport implements PDDLObject {

    /**
     * International Air Transport Association (IATA) airport code.
     */
    private final String id;
    /**
     * The human readable name.
     */
    private String name;
    /**
     * The Airport's position on the Earth, corresponding to the takeoff's
     * beginning point and to the landing's ending point.
     */
    private final Coordinates position;
    /**
     * The Airport's altitude, in meters above sea level (MASL).
     */
    private final double altitude;
    /**
     * The coordinates where takeoffs must start.
     */
    private final Coordinates endOfTakeoff;
    /**
     * The coordinates where landing must end.
     */
    private final Coordinates beginOfLanding;
    /**
     * The takeoff duration.
     */
    private final Duration takeoffDuration;
    /**
     * The landing duration.
     */
    private final Duration landingDuration;

    /**
     * @param id International Air Transport Association (IATA) airport code
     * @param name The human readable name
     * @param positionLat The Airport's position on the Earth (latitude)
     * @param positionLng The Airport's position on the Earth (longitude)
     * @param altitude The Airport's altitude, in meters above sea level (MASL)
     * @param endOfTakeoffLat The coordinates where takeoffs must start (latitude)
     * @param endOfTakeoffLng The coordinates where takeoffs must start (longitude)
     * @param beginOfLandingLat The coordinates where landing must end (latitude)
     * @param beginOfLandingLng The coordinates where landing must end (longitude)
     * @param takeoffDuration The takeoff duration
     * @param landingDuration The landing duration
     *
     * @throws IllegalArgumentException If the ID is not a valid IATA code
     */
    public Airport(String id, String name,
            double positionLat, double positionLng, double altitude,
            double endOfTakeoffLat, double endOfTakeoffLng,
            double beginOfLandingLat, double beginOfLandingLng,
            long takeoffDuration, long landingDuration) {
        if (!id.matches("[A-Z]{3}")) {
            throw new IllegalArgumentException("IATA code '" + id + "' is not valid!");
        }

        this.id = id;
        this.name = name;
        this.position = new Coordinates(positionLat, positionLng);
        this.altitude = altitude;
        this.endOfTakeoff = new Coordinates(endOfTakeoffLat, endOfTakeoffLng);
        this.beginOfLanding = new Coordinates(beginOfLandingLat, beginOfLandingLng);
        this.takeoffDuration = Duration.ofMillis(takeoffDuration);
        this.landingDuration = Duration.ofMillis(landingDuration);
    }

    @Override
    public boolean equals(Object obj) {
        return this == obj || (obj != null && obj instanceof Airport
                && ((Airport) obj).id.equals(this.id));
    }

    public Coordinates getBeginOfLanding() {
        return beginOfLanding;
    }

    public long getDistanceFromBeginOfLanding(Coordinates c) {
        return c.distanceFrom(beginOfLanding);
    }

    public long getDistanceFromCenter(Coordinates c) {
        return c.distanceFrom(position);
    }

    public long getDistanceFromEndOfTakeoff(Coordinates c) {
        return c.distanceFrom(endOfTakeoff);
    }

    public Coordinates getEndOfTakeoff() {
        return endOfTakeoff;
    }

    public String getId() {
        return id;
    }

    /**
     * @return The duration of the landing on this airport
     * the random increment of at most 2% is introduced to avoid segmentation fault of colin planner when same values
     */
    public long getLandingDuration(boolean millis) {
        if(millis)
            return landingDuration.toMillis()+(long)(Math.random()*(landingDuration.toMillis()*0.04));
        else
            return landingDuration.getSeconds()+(long)(Math.random()*(landingDuration.getSeconds()*0.04));
    }

    /**
     * @return The length of this airport's landing path (meters)
     */
    public long getLandingLength() {
        return beginOfLanding.distanceFrom(position);
    }

    public Coordinates getPosition() {
        return position;
    }

    /**
     * @return The duration of the takeoff from this airport
     * the random increment of at most 2% is introduced to avoid segmentation fault of colin planner when same values
     */
    public long getTakeoffDuration(boolean millis) {
        if(millis)
            return takeoffDuration.toMillis()+(long)(Math.random()*(takeoffDuration.toMillis()*0.04));
        else
            return takeoffDuration.getSeconds()+(long)(Math.random()*(takeoffDuration.getSeconds()*0.04));
    }

    /**
     * @return The length of this airport's takeoff path (meters)
     */
    public long getTakeoffLength() {
        return position.distanceFrom(endOfTakeoff);
    }

    /**
     * @return The distance between the airport's end of takeoff site and its
     * begin of landing site, in meters
     */
    public long getTakeoffToLandingLength() {
        return endOfTakeoff.distanceFrom(beginOfLanding);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.id);
        return hash;
    }

    /**
     * @return The airport's PDDL name
     */
    @Override
    public String pddl() {
        return "airport-" + id;
    }

    /**
     * @return The airport begin of landing position's PDDL name
     */
    public String pddlBeginLanding() {
        return "airport-" + id + "-site-begin-landing";
    }

    /**
     * @return The airport position's PDDL name
     */
    public String pddlCenter() {
        return "airport-" + id + "-site";
    }

    /**
     * @return The airport end of takeoff position's PDDL name
     */
    public String pddlEndTakeoff() {
        return "airport-" + id + "-site-end-takeoff";
    }

    @Override
    public String toString() {
        return name;
    }
}
