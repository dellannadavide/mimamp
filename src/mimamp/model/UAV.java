package mimamp.model;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

/**
 * An Unmanned Aerial Vehicle (UAV), eventually linked to its availabilities.
 */
public class UAV implements PDDLObject {

    /**
     * An unique identifier.
     */
    private final long id;
    /**
     * The model (Falco, Sky-Y, C-Fly).
     */
    private final String model;
    /**
     * The human readable name.
     */
    private final String name;
    /**
     * The maximum operability distance, in meters.
     */
    private final double maxOperabilityDistance;
    /**
     * The average consumption, in l/m.
     */
    private final double consumption;
    /**
     * The cruising speed, in m/ms.
     */
    private final double cruisingSpeed;
    /**
     * The cruising altitude, in meters.
     */
    private final double cruisingAltitude;
    /**
     * The operating time, in hours.
     */
    private final double operatingTime;
    /**
     * The maximum fuel capacity, in liters.
     */
    private final double fuelCapacity;
    /**
     * The actual fuel level, as capacity percentage.
     */
    private double fuelLevel;
    /**
     * The list of availability of this UAV.
     */
    private final List<UAVAvailability> availabilities;

    /**
     * @param id An unique identifier
     * @param name The human readable name
     * @param maxOperabilityDistance The maximum operability distance
     */
    UAV(long id, String model, String name, double maxOperabilityDistance,
            double cruisingSpeed, double cruisingAltitude, double operatingTime,
            double fuelCapacity, double fuelLevel, double consumption) {
        this.id = id;
        this.model = model;
        this.name = name;
        this.maxOperabilityDistance = maxOperabilityDistance;
        this.cruisingSpeed = cruisingSpeed;
        this.cruisingAltitude = cruisingAltitude;
        this.operatingTime = operatingTime;
        this.fuelCapacity = fuelCapacity;
        this.fuelLevel = fuelLevel;
        this.consumption = consumption;
        this.availabilities = new ArrayList<>();
    }

    void addAvailability(UAVAvailability availability) {
        availabilities.add(availability);
    }

    public boolean canHandle(ObservationRequest request) {
        return availabilities.stream()
                .anyMatch(availability -> availability.canHandle(request));
    }

    @Override
    public boolean equals(Object o) {
        return this == o
                || (o != null && o instanceof UAV && ((UAV) o).id == this.id);
    }

    public List<UAVAvailability> getAvailabilities() {
        return availabilities;
    }

    public UAVAvailability getAvailabilityFor(ObservationRequest req) {
        return availabilities.stream()
                .filter(availability -> availability.canHandle(req))
                .findFirst()
                .orElse(null);
    }

    public double getConsumption() {
        return consumption;
    }

    public double getCruisingSpeed(boolean millis) {
        /*by default cruisingSpeed is in m/ms */
        return millis ? cruisingSpeed: cruisingSpeed*1000;
    }

    public double getFuelLiters() {
        return fuelCapacity * fuelLevel;
    }

    public long getId() {
        return id;
    }

    double getMaxOperabilityDistance() {
        return maxOperabilityDistance;
    }

    public long getTimeUnitDistance() {
        return Math.round(1 / cruisingSpeed);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public String pddl() {
        return "uav" + id;
    }

    @Override
    public String toString() {
        return model + " N" + id;
    }

    public void useFuelForLength(long takeoffLength) {
        double litersUsed = consumption * takeoffLength;
        double percUsed = litersUsed / fuelCapacity;
        // fuelLevel -= percUsed;
    }

    public void useFuelForDuration(Duration duration) {
        useFuelForLength(Math.round(cruisingSpeed * duration.toMillis()));
    }

    public int getCruisingAltitude() {
        return (int) Math.round(cruisingAltitude);
    }
}
