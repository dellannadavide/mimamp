package mimamp.model;

import mimamp.controller.planning.Planners;

import java.io.File;
import java.util.*;

/**
 * Created by davide on 22/10/16.
 */
public final class Domains {
    private static String DOMAINS_FOLDER = "domains/";
    /**
     * I nomi dei file dei domini
     */
    public static String SMAT = "domain.pddl";
    public static String THESIS_NUM = "SmatF2_domain_clean_grounded_NOADL.pddl";
    public static String THESIS_TEMP = "SmatF2_domain_clean_temp_grounded_NOADL.pddl";
    public static String THESIS_TEMP_TFD = "SmatF2_domain_clean_temp_grounded_TDF.pddl";
    public static String THESIS_NUM_FREE = "SmatF2_domain_clean_grounded_free_target_NOADL.pddl";
    public static String THESIS_TEMP_FREE = "SmatF2_domain_clean_temp_grounded_free_target_NOADL.pddl";
    public static String THESIS_TEMP_TFD_FREE = "SmatF2_domain_clean_temp_grounded_free_target_TDF.pddl";

    /**
     * Stringhe per la visualizzazione grafica
     */
    private static final String STR_SMAT = "SMAT-F2";
    private static final String STR_THESIS_NUM = "THESIS_GROUNDED_NUM";
    private static final String STR_THESIS_TEMP = "THESIS_GROUNDED_TEMP";
    private static final String STR_THESIS_TEMP_TFD = "THESIS_GROUNDED_TEMP_TDF";
    private static final String STR_THESIS_NUM_FREE = "THESIS_GROUNDED_NUM_FREE";
    private static final String STR_THESIS_TEMP_FREE = "THESIS_GROUNDED_TEMP_FREE";
    private static final String STR_THESIS_TEMP_TFD_FREE = "THESIS_GROUNDED_TEMP_TDF_FREE";

    /**
     * Mappa che associa le stringhe visualizzate graficamente ai file pddl contenenti i domini
     */
    private static final Map<String, String> domains;
    static
    {
        domains = new HashMap<String, String>();
        domains.put(STR_SMAT, SMAT);
        domains.put(STR_THESIS_NUM, THESIS_NUM);
        domains.put(STR_THESIS_TEMP, THESIS_TEMP);
        domains.put(STR_THESIS_TEMP_TFD, THESIS_TEMP_TFD);
        domains.put(STR_THESIS_NUM_FREE, THESIS_NUM_FREE);
        domains.put(STR_THESIS_TEMP_FREE, THESIS_TEMP_FREE);
        domains.put(STR_THESIS_TEMP_TFD_FREE, THESIS_TEMP_TFD_FREE);
    }

    /**
     * Mappe che associano le stringhe dei domini visualizzate graficamente a un booleano
     * che identifica la possibilità di usare una certa unità di misura o no
     */
    private static final Map<String, Boolean> allows_seconds;
    private static final Map<String, Boolean> allows_milliseconds;
    static
    {
        /*seconds*/
        allows_seconds = new HashMap<String, Boolean>();
        allows_seconds.put(STR_SMAT, false);
        allows_seconds.put(STR_THESIS_NUM, true);
        allows_seconds.put(STR_THESIS_TEMP, true);
        allows_seconds.put(STR_THESIS_TEMP_TFD, true);
        allows_seconds.put(STR_THESIS_NUM_FREE, true);
        allows_seconds.put(STR_THESIS_TEMP_FREE, true);
        allows_seconds.put(STR_THESIS_TEMP_TFD_FREE, true);
        /*milliseconds*/
        allows_milliseconds = new HashMap<String, Boolean>();
        allows_milliseconds.put(STR_SMAT, true);
        allows_milliseconds.put(STR_THESIS_NUM, true);
        allows_milliseconds.put(STR_THESIS_TEMP, true);
        allows_milliseconds.put(STR_THESIS_TEMP_TFD, true);
        allows_milliseconds.put(STR_THESIS_NUM_FREE, true);
        allows_milliseconds.put(STR_THESIS_TEMP_FREE, true);
        allows_milliseconds.put(STR_THESIS_TEMP_TFD_FREE, true);    }

    public static String getDomain(String domain) {
        return domains.get(domain);
    }

    public static Collection<String> getDomains(String planning_model, String planner) {
        switch(planning_model.toUpperCase()) {
            case PlanningModels.NUMERIC:
                switch (planner) {
                    case Planners.COLIN_NUMERIC:
                    case Planners.METRICFF:
                    case Planners.LPG:
                        return Arrays.asList(STR_SMAT, STR_THESIS_NUM);
                    case Planners.POPF2_NUMERIC: /*POPF2 doesn't support ADL, SMAT domain contains them*/
                        return Arrays.asList(STR_THESIS_NUM);
                }
                break;
            case PlanningModels.TEMPORAL:
                    switch (planner) {
                        case Planners.COLIN_TEMPORAL:
                        case Planners.POPF2_TEMPORAL:
                            return  Arrays.asList(STR_THESIS_TEMP);
                        case Planners.TFD: /*TFD doesn't support timed-initial-literals and continuous numeric change*/
                            return Arrays.asList(STR_THESIS_TEMP_TFD);
                    }
                break;
        }
        return Arrays.asList();
    }

    public static Collection<String> getDomainsFree(String planning_model, String planner) {
        switch(planning_model.toUpperCase()) {
            case PlanningModels.NUMERIC:
                return Arrays.asList(STR_THESIS_NUM_FREE);
            case PlanningModels.TEMPORAL:
                switch (planner) {
                    case Planners.TFD: /*TFD doesn't support timed-initial-literals and continuous numeric change*/
                        return Arrays.asList(STR_THESIS_TEMP_TFD_FREE);
                    case Planners.COLIN_TEMPORAL:
                    case Planners.POPF2_TEMPORAL:
                        return  Arrays.asList(STR_THESIS_TEMP_FREE);

                }
                break;
        }
        return Arrays.asList();
    }

    public static String getDomainsFolder() {
        return DOMAINS_FOLDER;
    }

    public static String getDomainsFolderAbsolutePath() {
        return new File(DOMAINS_FOLDER).getAbsolutePath();
    }

    public static boolean getSecondsAvailable(String domain) {
        return allows_seconds.get(domain);
    }

    public static boolean getMilliSecondsAvailable(String domain) {
        return allows_milliseconds.get(domain);
    }

    public static boolean isTFDDomain(String domain) {
        return domain.equalsIgnoreCase(THESIS_TEMP_TFD) || domain.equalsIgnoreCase(THESIS_TEMP_TFD_FREE);
    }
    public static boolean isSMATDomain(String domain) {
        return domain.equalsIgnoreCase(SMAT);
    }

    public static boolean isFreeDomain(String domain) {
        return domain.equalsIgnoreCase(THESIS_NUM_FREE) || domain.equalsIgnoreCase(THESIS_TEMP_TFD_FREE) || domain.equalsIgnoreCase(THESIS_TEMP_FREE);
    }
}
