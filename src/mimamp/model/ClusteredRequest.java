/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.model;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * A cluster of two or more compatible ObservationRequests.
 */
public class ClusteredRequest extends ObservationRequest {
    /** The ObservationRequests included in this cluster. */
    Set<ObservationRequest> representedRequests;

    private ClusteredRequest(long id, String client, Target target,
            LocalDateTime earliestTime, LocalDateTime latestTime, Duration minDuration,
            boolean needToPlan, Set<Sensor> sensors,
            Set<ObservationRequest> representedRequests) {
        super(id, client, target, earliestTime, latestTime, minDuration, needToPlan);
        sensors.forEach(s -> addSensor(s));
        this.representedRequests = representedRequests;
    }

    /**
     * @param cluster The observation requests to group on the returned cluster
     * @return A ClusteredRequest that includes the parameter set
     */
    static ClusteredRequest clusterOf(Set<ObservationRequest> cluster) {
        // All requests must be points
        if (!cluster.stream().allMatch(obsReq -> obsReq.targetIsPoint())) {
            throw new IllegalArgumentException(
                    "Only requests with target points can be clustered!");
        }

        // Finds a representative
        ObservationRequest representative = cluster.stream().findFirst().get();

        // Earliest time of the cluster is the latest earliest time of the represented requests.
        LocalDateTime earliest
                = cluster.stream().max((req1, req2)
                        -> req1.earliestTime.compareTo(req2.earliestTime)).get().earliestTime;
        // Latest time of the cluster is the earliest latest time of the represented requests.
        LocalDateTime latest
                = cluster.stream().min((req1, req2)
                        -> req1.latestTime.compareTo(req2.latestTime)).get().latestTime;
        // Minimum duration of the cluster is the sum of all represented requests' minimum durations.
        boolean millis = true;
        Duration minDuration = Duration.ofMillis(
                cluster.stream()
                .mapToLong(req -> req.getMinDuration(millis))
                .sum());

        // The coordinates of the cluster is the midpoint of the represented requests' positions
        double trgLatitude = cluster.stream()
                .mapToDouble(req -> req.target.getFirstPosition().getLatitude())
                .average().getAsDouble();
        double trgLongitude = cluster.stream()
                .mapToDouble(req -> req.target.getFirstPosition().getLongitude())
                .average().getAsDouble();
        
        // Builds a human-understandable cluster's target name.
        StringBuilder trgName = new StringBuilder("cluster(");
        cluster.stream()
                .map(obsReq -> obsReq.getTargetName())
                .forEach(trg -> trgName.append(trg).append(", "));
        trgName.deleteCharAt(trgName.length() - 1);
        trgName.deleteCharAt(trgName.length() - 1);
        trgName.append(')');
        
        // Creates the fake target...
        Target target = new Target(representative.getTargetId(),
                trgName.toString(), trgLatitude, trgLongitude);
        
        Set<Sensor> sensors = representative.getSensors();

        // Creates the "real" clustered request, using a private constructor.
        return new ClusteredRequest(representative.id, representative.client,
                target, earliest, latest, minDuration, true, sensors, cluster);
    }

    public Set<ObservationRequest> getRepresentedRequests() {
        return representedRequests;
    }

    public void setRepresentedRequests(Set<ObservationRequest> representedRequests) {
        this.representedRequests = representedRequests;
    }

    @Override
    public String pddl() {
        return "cluster" + getId();
    }
}
