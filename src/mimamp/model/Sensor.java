package mimamp.model;

import java.util.Objects;
import java.util.Set;

/**
 * A sensor that is capable to observe targets.
 */
public class Sensor {

    public static enum SensorType {

        CAMERA, EO, HYPERSPECTRAL, IR, RADAR;

        public boolean dataFusionIncompatible(Set<SensorType> sensors) {
            if (sensors.isEmpty()) {
                return false;
            } else if (sensors.size() > 1) {
                return true;
            }

            switch (this) {
                case CAMERA:
                case HYPERSPECTRAL:
                    return true;
                case EO:
                    return sensors.contains(IR) || sensors.contains(RADAR);
                case IR:
                    return sensors.contains(EO) || sensors.contains(RADAR);
                case RADAR:
                    return sensors.contains(EO) || sensors.contains(IR);
                default:
                    throw new IllegalStateException();
            }
        }

        private String getColor() {
            switch (this) {
                case CAMERA:
                    return "#0000FF";
                case EO:
                    return "#008000";
                case HYPERSPECTRAL:
                    return "#FFD700";
                case IR:
                    return "#4B0082";
                case RADAR:
                    return "#00FFFF";
            }
            throw new IllegalStateException();
        }

        private String getKMLColor() {
            switch (this) {
                case CAMERA:
                    return "FFFF0000";
                case EO:
                    return "FF008000";
                case HYPERSPECTRAL:
                    return "FF00D7FF";
                case IR:
                    return "FF82004B";
                case RADAR:
                    return "FFFFFF00";
            }
            throw new IllegalStateException();
        }
    }

    public static enum SensorModel {

        GENERIC;
    }

    /**
     * The Sensor type
     */
    private SensorType type;
    /**
     * The Sensor model
     */
    private SensorModel model;

    /**
     * @param type The Sensor type
     * @param model The Sensor model
     */
    public Sensor(SensorType type, SensorModel model) {
        this.type = type;
        this.model = model;
    }

    /**
     * Automatically sets the model as generic.
     *
     * @param type The Sensor type
     */
    public Sensor(SensorType type) {
        this(type, SensorModel.GENERIC);
    }

    public SensorType getType() {
        return type;
    }

    @Override
    public String toString() {
        return type.name().toLowerCase();
    }

    String toUpperString() {
        return type.name();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Sensor) {
            Sensor s = (Sensor) o;
            return s.type == this.type
                    && s.model == this.model;
        } else {
            return false;
        }
    }

    String getColor() {
        return type.getColor();
    }

    String getKMLColor() {
        return type.getKMLColor();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.type);
        hash = 79 * hash + Objects.hashCode(this.model);
        return hash;
    }
}
