package mimamp.model;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import mimamp.model.plan.SensorSuiteOnUAV;

/**
 * A Mission Requirement Definition (MRD).
 */
public class Mission {

    /** The hardcoded cluster radius, in meters. */
    public final static int CLUSTER_RADIUS = 1500;

    /** The database-assigned ID. */
    private long id;

    /**
     * The UAVs that must be used during the mission
     * (with the suite of sensors loaded and the airport where they are lcoated)
     *
     */
    private Set<UAVAvailability> uavsAvailability;

    /**
     * The ObservationRequests took in
     * charge by this mission
     *
     */
    private Set<ObservationRequest> observationRequests;

    /**
     * The assignment of the ObservationRequests to the UAVs, made by the
     * operator. The keySet corresponds to all the ObservationRequests took in
     * charge by this mission; the values correspond to all the UAVs that have
     * to be used by the mission, the suite of sensors loaded and the airports
     * where they are located.
     */
    private Map<ObservationRequest, UAVAvailability> assignments;
    /**
     * The constraints between pairs of ObservationRequests.
     */
    private Set<MultiObs> multiObs;
    /**
     * The earliest time of the window within the mission has to be performed.
     */
    private LocalDateTime earliestTime;
    /**
     * The latest time of the window within the mission has to be performed.
     */
    private LocalDateTime latestTime;
    /**
     * The maximum duration of the mission.
     */
    private Duration maxDuration;

    public Mission(LocalDate date) {
        this.earliestTime = this.latestTime = date.atStartOfDay();
    }

    /**
     * @param pddlAirport the PDDL representation of an airport
     * @return The Airport object represented by the parameter string
     */
    public Airport decodeAirport(String pddlAirport) {
       String arptId = pddlAirport.substring(8, 11);
        Airport a = null;
        for(UAVAvailability uavAv: uavsAvailability)
            if (uavAv.getAirport().getId().equals(arptId)) {
                a = uavAv.getAirport();
                break;
            }
        return a;
    }

    /**
     * @param pddlLocation The PDDL representation of a location
     * @return  The Coordinates object represented by the parameter string
     */
    public Coordinates decodeLocation(String pddlLocation) {
        if (pddlLocation.startsWith("TARGETEND")) {
            long trgId = Long.parseLong(pddlLocation.substring(9));
            return observationRequests.stream()
                    .map(obsReq -> obsReq.getTarget())
                    .filter(target -> target.getId() == trgId)
                    .findAny()
                    .get()
                    .getLastPosition();
        } else if (pddlLocation.startsWith("TARGET")) {
            long trgId = Long.parseLong(pddlLocation.substring(6));
            return observationRequests.stream()
                    .map(obsReq -> obsReq.getTarget())
                    .filter(target -> target.getId() == trgId)
                    .findAny()
                    .get()
                    .getFirstPosition();
        } else {
            return decodeAirport(pddlLocation).getEndOfTakeoff();
        }
    }

    /**
     * @param pddlRequest The PDDL representation of an ObservationRequest
     * @return The ObservationRequest object represented by the parameter string
     */
    public ObservationRequest decodeObservationRequest(String pddlRequest) {
        long reqId;
        if (pddlRequest.startsWith("CLUSTER")) {
            reqId = Long.parseLong(pddlRequest.substring(7));
        } else {
            reqId = Long.parseLong(pddlRequest.substring(6));
        }
        return observationRequests.stream()
                .filter(obsReq -> obsReq.getId() == reqId)
                .findAny()
                .get();
    }

    /**
     * @param pddlTarget The PDDL representation of a target
     * @return The Target object represented by the parameter string
     */
    public Target decodeTarget(String pddlTarget) {
        long trgId;
        if (pddlTarget.startsWith("OBSREQ")) {
            trgId = Long.parseLong(pddlTarget.substring(6));
        } else {
            trgId = Long.parseLong(pddlTarget.substring(7));
        }
        return observationRequests.stream()
                .filter(obsReq -> obsReq.getId() == trgId)
                .findAny()
                .get()
                .getTarget();
    }

    /**
     * @param pddlUav The PDDL representation of a UAV
     * @return The UAV object represented by the parameter string
     */
    public UAV decodeUav(String pddlUav) {
        long uavId = Long.parseLong(pddlUav.substring(3));
        return uavsAvailability.stream()
                .map(uavAv -> uavAv.getUAV())
                .filter(uav -> uav.getId() == uavId)
                .findAny()
                .get();
    }

    public Map<ObservationRequest, UAVAvailability> getAssignments() {
        return assignments;
    }

    public UAVAvailability getAssignment(ObservationRequest obsReq) {
        return assignments.get(obsReq);
    }

    public LocalDateTime getEarliestTime() {
        return earliestTime;
    }

    /*
     * Heuristic for (Time-mission ?uav) and (Delta-time ?uav).
     */
    public LocalDateTime getFirstUsefulTakeoffTime(UAVAvailability uavAv) {
        if(assignments!=null) {
            UAV uav = uavAv.getUAV();
            LocalDateTime minTime = assignments.keySet().stream()
                    // tutte le richieste
                    .filter(obsReq
                            // che riguardano questo UAV
                            -> assignments.get(obsReq).getUAV().equals(uav)
                            // ma non sono secondi parametri di multiObs
                            && multiObs.stream().noneMatch(mOb
                            -> mOb.getRequest2().equals(obsReq)))
                    .map((ObservationRequest obsReq) -> {
                        double distance = obsReq.getTargetLessFarBoundary(uavAv.getAirportEndOfTakeOff());
                        long timeToReach = Math.round(distance / uav.getCruisingSpeed(true));
                        return obsReq.getEarliestTime()
                                .minus(timeToReach, ChronoUnit.MILLIS)
                                .minus(uavAv.getAirportTakeoffDuration(true), ChronoUnit.MILLIS);
                    })
                    .min((time1, time2) -> time1.compareTo(time2))
                    .orElse(earliestTime);
            return earliestTime.isAfter(minTime) ? earliestTime : minTime;
        }
        else {
            /*assignment is null -> no heuristic for initial uavs time-mission can be calculated
            (no initial information on assignments is available)*/
            return earliestTime;
        }


    }

    public long getId() {
        return id;
    }

    public Set<UAVAvailability> getInitialState() {
        /*return assignments.values().stream()
                .distinct()
                .collect(Collectors.toSet());*/
        return uavsAvailability.stream()
                .distinct()
                .collect(Collectors.toSet());
    }

    LocalDateTime getLatestTime() {
        return latestTime;
    }

    public int getRequestsNumber() {
        return observationRequests.size();
    }

    public SensorSuiteOnUAV getSensorSuiteOn(UAV uav) {
        SensorSuite suite = uavsAvailability.stream()
                .filter(uavAv -> uavAv.getUAV().equals(uav))
                .map(uavAv -> uavAv.getSensorSuite())
                .findAny()
                .get();
        return new SensorSuiteOnUAV(suite, uav);
    }

    public Duration getMaxDuration() {
        return maxDuration;
    }

    public Set<MultiObs> getMultiObs() {
        return multiObs;
    }
    
    public Set<ObservationRequest> getRequests() {
        return observationRequests;
    }

    /**
     * Replaces clusterable requests with the corresponding cluster, using the
     * Quality-Threshold algorithm.
     */
    public void QTClustering() {
        /*TODO: modify this code to allow clustering also when no assignment is specified*/
        if(assignments!=null) {
            Set<ObservationRequest> freeRequests = assignments.keySet().stream()
                    .filter(obsReq -> {
                        return obsReq.targetIsPoint()
                                && multiObs.stream().noneMatch(mOb
                                -> obsReq.equals(mOb.getRequest1())
                                || obsReq.equals(mOb.getRequest2()));
                    })
                    .collect(Collectors.toSet());
            Set<Set<ObservationRequest>> clusters = new HashSet<>();

            while (!freeRequests.isEmpty()) {
                Map<ObservationRequest, Set<ObservationRequest>> tempClusters
                        = new HashMap<>();
                freeRequests.stream().forEach((ObservationRequest i) -> {
                    Set<ObservationRequest> set = new HashSet<>();
                    set.add(i);
                    freeRequests.stream()
                            .filter((ObservationRequest j) -> {
                                return i != j
                                        && i.distanceFrom(j) <= CLUSTER_RADIUS
                                        && i.getSensors().equals(j.getSensors())
                                        && assignments.get(i).equals(assignments.get(j))
                                        && i.timeCompatibleWith(j);
                            })
                            .forEach(j -> set.add(j));
                    tempClusters.put(i, set);
                });

                Set<ObservationRequest> maxCluster = tempClusters.values().stream()
                        .max((set1, set2) -> set1.size() - set2.size())
                        .get();
                clusters.add(maxCluster);
                freeRequests.removeAll(maxCluster);
            }

            clusters.stream().forEach((Set<ObservationRequest> cluster) -> {
                // Per ogni cluster di dimensione > 1, eliminare le relative obsreq,
                // crearne una nuova ed aggiungerla.
                if (cluster.size() > 1) {
                    UAVAvailability uavAv = assignments.get(
                            cluster.stream().findFirst().get());
                    cluster.stream().forEach(req -> assignments.remove(req));
                    ObservationRequest clusteredRequest
                            = ClusteredRequest.clusterOf(cluster);
                    assignments.put(clusteredRequest, uavAv);
                }
            });
        }
    }

    public void setUAVsAvailability(Set<UAVAvailability> availabilities) {
        this.uavsAvailability = availabilities;
    }
    public Set<UAVAvailability> getUAVsAvailability() {
        return uavsAvailability;
    }


    public void setObservationRequests(Set<ObservationRequest> obsreqs) {
        this.observationRequests = obsreqs;
    }
    public Set<ObservationRequest> getObservationRequests() {
        return observationRequests;
    }

    public void setAssignments(Map<ObservationRequest, UAVAvailability> assignments) {
        this.assignments = assignments;
    }

    public void setEarliestTime(LocalTime time) {
        earliestTime = LocalDateTime.of(earliestTime.toLocalDate(), time);
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setLatestTime(LocalTime time) {
        latestTime = LocalDateTime.of(latestTime.toLocalDate(), time);
    }

    public void setMaxDuration(Duration maxDuration) {
        this.maxDuration = maxDuration;
    }

    public void setMultiObs(Set<MultiObs> multiObs) {
        this.multiObs = multiObs;
    }

    /**
     * @return A Map that associates to all UAVs the number of requests
     * associated with them
     */
    public Map<UAV, Integer> createNumTrgMap() {
        Map<UAV, Integer> m = new HashMap<>();
        if(assignments!=null) {
            assignments.keySet().stream()
                    .forEach(obsReq -> {
                        UAV uav = assignments.get(obsReq).getUAV();
                        if (!m.containsKey(uav)) {
                            m.put(uav, 1);
                        } else {
                            m.put(uav, 1 + m.get(uav));
                        }
                    });
        }
        return m;
    }

    public boolean hasNotMultiObs() {
        return multiObs.isEmpty();
    }
}
