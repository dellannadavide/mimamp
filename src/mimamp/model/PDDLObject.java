/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.model;

/**
 *
 * @author marco
 */
public interface PDDLObject {
    /**
     * @return a decodeable PDDL name of the implementing object
     */
    String pddl();
}
