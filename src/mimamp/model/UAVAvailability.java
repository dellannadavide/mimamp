package mimamp.model;

import java.time.LocalDate;

/**
 * A time-bounded availability of an UAV into an Airport, with a set of
 * installed Sensors.
 */
public class UAVAvailability {

    /**
     * An unique identifier.
     */
    private final long id;
    /**
     * The subject UAV.
     */
    private final UAV uav;
    /**
     * The SensorSuite installed on the UAV.
     */
    private final SensorSuite sensorSuite;
    /**
     * The Airport where the UAV is located.
     */
    private final Airport airport;
    /**
     * The beginning date of this availability.
     */
    private final LocalDate beginDate;
    /**
     * The ending date of this availability.
     */
    private final LocalDate endingDate;

    /**
     * @param uav The subject UAV
     * @param sensorSuite The SensorSuite installed on the UAV
     * @param airport The airport where the UAV is located
     * @param beginDate The beginning date of this availability
     * @param endingDate The ending date of this availability
     */
    UAVAvailability(long id, UAV uav, String sensorSuite, Airport airport,
            LocalDate beginDate, LocalDate endingDate) {
        this.id = id;
        this.uav = uav;
        this.sensorSuite = SensorSuite.valueOf(sensorSuite);
        this.airport = airport;
        this.beginDate = beginDate;
        this.endingDate = endingDate;
    }

    /**
     * @param request The {@link ObservationRequest} to check
     * @return <code>true</code> if the UAV can handle the parameter request on
     * this availability period
     */
    boolean canHandle(ObservationRequest request) {
        double targetDistance = request.getTargetMostFarPositionFrom(airport.getPosition());
        return (request.getDate().isAfter(beginDate) || request.getDate().isEqual(beginDate))
                && (request.getDate().isBefore(endingDate) || request.getDate().isEqual(endingDate))
                && targetDistance <= uav.getMaxOperabilityDistance()
                && sensorSuite.contains(request.getSensors());
    }
    
    public boolean containsDate(LocalDate date) {
        return (date.isAfter(beginDate) || date.isEqual(beginDate))
                && (date.isBefore(endingDate) || date.isEqual(endingDate));
    }

    @Override
    public boolean equals(Object o) {
        return this == o
                || (o != null && o instanceof UAVAvailability
                && ((UAVAvailability) o).id == this.id);
    }

    public Airport getAirport() {
        return airport;
    }

    public Coordinates getAirportEndOfTakeOff() {
        return airport.getEndOfTakeoff();
    }

    long getAirportTakeoffDuration(boolean millis) {
        return airport.getTakeoffDuration(millis);
    }

    public long getId() {
        return id;
    }

    public SensorSuite getSensorSuite() {
        return sensorSuite;
    }

    public UAV getUAV() {
        return uav;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }
    
    public String pddlSensorSuite() {
        return sensorSuite.pddl();
    }
    
    @Override
    public String toString() {
        return uav + " (" + sensorSuite + "), on airport " + airport;
    }
}
