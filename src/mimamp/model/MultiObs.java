package mimamp.model;

/**
 * A temporal constraint among a pairs of ObservationRequests.
 */
public class MultiObs {

    public enum Constraint {

        BEFORE, OVERLAP;
    }

    /**
     * The first ObservationRequest involved.
     */
    private final ObservationRequest request1;
    /**
     * The second ObservationRequest involved.
     */
    private final ObservationRequest request2;
    /**
     * The type of constraint represented.
     */
    private final Constraint constraint;
    
    public MultiObs(ObservationRequest request1, ObservationRequest request2,
            Constraint constraint) {
        this.request1 = request1;
        this.request2 = request2;
        this.constraint = constraint;
    }

    public String getConstraint() {
        return constraint.name().toLowerCase();
    }

    public ObservationRequest getRequest1() {
        return request1;
    }

    public ObservationRequest getRequest2() {
        return request2;
    }

    public String pddlRequest1() {
        return request1.pddl();
    }

    public String pddlRequest2() {
        return request2.pddl();
    }
}
