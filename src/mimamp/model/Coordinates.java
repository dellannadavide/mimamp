/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mimamp.model;

import com.lynden.gmapsfx.javascript.object.LatLong;
import java.util.Locale;

/**
 *
 * @author marco
 */
public class Coordinates {

    private final double latitude;
    private final double longitude;

    public Coordinates(double latitude, double longitude) {
        // Maximum precision is 5 decimal digits.
        this.latitude = Double.parseDouble(String.format(Locale.ENGLISH, "%.5f", latitude));
        this.longitude = Double.parseDouble(String.format(Locale.ENGLISH, "%.5f", longitude));
    }

    public Coordinates(LatLong latLng) {
        this(latLng.getLatitude(), latLng.getLongitude());
    }

    /**
     * @param coord The coordinates to calculate the distance from
     * @return The distance between this Coordinates and the parameter ones, in meters
     */
    public long distanceFrom(Coordinates coord) {
        double dLat = (coord.getLatitude() - latitude) * Math.PI / 180;
        double dLon = (coord.getLongitude() - longitude) * Math.PI / 180;

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(latitude * Math.PI / 180)
                * Math.cos(coord.getLatitude() * Math.PI / 180)
                * Math.sin(dLon / 2) * Math.sin(dLon / 2);

        return Math.round(12742000 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)));
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Coordinates) {
            Coordinates c = (Coordinates) o;
            return c.latitude - this.latitude <= 0.0001
                    && c.longitude - this.longitude <= 0.0001;
        }
        return false;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.latitude) ^ (Double.doubleToLongBits(this.latitude) >>> 32));
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.longitude) ^ (Double.doubleToLongBits(this.longitude) >>> 32));
        return hash;
    }

    public LatLong toLatLong() {
        return new LatLong(latitude, longitude);
    }

    @Override
    public String toString() {
        return String.format(Locale.ENGLISH, "%.5f° lat, %.5f° lng",
                latitude, longitude);
    }
}
