(define (problem MRD)
  (:domain SmatF2)
  (:objects
	obsReq67 obsReq68 obsReq69 obsReq70 obsReq71 obsReq72 - Point
	obsReq75 obsReq74 - Target
	uav6 uav4 - Uav
	airport-CUF - Airport
	airport-CUF-site airport-CUF-site-begin-landing airport-CUF-site-end-takeoff target90 targetEnd90 target85 target88 target87 target83 target86 target84 target89 targetEnd89 - Site2D
	S_EO SUITE2 - SensorSuite
  )

  (:init
	(= (time-wait-on-ground) 100000)
	(= (time-loiter) 100000)
	(= (time_between_take_off) 300000)
	(= (total-fly-time) 0)
	(= (min_delta_time) 999999995904)
	(= (max_time_mission) 0)

	(= (NumberTrgCovered) 0)
	(= (MinNumTrgt) 0)

	(site_target target90 obsReq75)
	(= (Time-min-obs obsReq75) 2200000)
	(= (Earliest-time-start-obs-target obsReq75) 2400000)
	(= (Latest-time-end-obs-target obsReq75) 12600000)
	(unvisited_t obsReq75)
	(uncovered obsReq75)
	(= (Time-mission-start-obs-target obsReq75) 0)
	(= (Time-mission-end-obs-target obsReq75) 0)
	(site_target targetEnd90 obsReq75)
	(not_same target90 targetEnd90)
	(site_target target85 obsReq67)
	(= (Time-min-obs obsReq67) 90000)
	(= (Earliest-time-start-obs-target obsReq67) 3600000)
	(= (Latest-time-end-obs-target obsReq67) 21600000)
	(unvisited_t obsReq67)
	(uncovered obsReq67)
	(= (Time-mission-start-obs-target obsReq67) 0)
	(= (Time-mission-end-obs-target obsReq67) 0)
	(site_target target88 obsReq68)
	(= (Time-min-obs obsReq68) 300000)
	(= (Earliest-time-start-obs-target obsReq68) 3600000)
	(= (Latest-time-end-obs-target obsReq68) 21600000)
	(unvisited_t obsReq68)
	(uncovered obsReq68)
	(= (Time-mission-start-obs-target obsReq68) 0)
	(= (Time-mission-end-obs-target obsReq68) 0)
	(site_target target87 obsReq69)
	(= (Time-min-obs obsReq69) 60000)
	(= (Earliest-time-start-obs-target obsReq69) 3600000)
	(= (Latest-time-end-obs-target obsReq69) 21600000)
	(unvisited_t obsReq69)
	(uncovered obsReq69)
	(= (Time-mission-start-obs-target obsReq69) 0)
	(= (Time-mission-end-obs-target obsReq69) 0)
	(site_target target83 obsReq70)
	(= (Time-min-obs obsReq70) 60000)
	(= (Earliest-time-start-obs-target obsReq70) 3600000)
	(= (Latest-time-end-obs-target obsReq70) 21600000)
	(unvisited_t obsReq70)
	(uncovered obsReq70)
	(= (Time-mission-start-obs-target obsReq70) 0)
	(= (Time-mission-end-obs-target obsReq70) 0)
	(site_target target86 obsReq71)
	(= (Time-min-obs obsReq71) 60000)
	(= (Earliest-time-start-obs-target obsReq71) 3600000)
	(= (Latest-time-end-obs-target obsReq71) 21600000)
	(unvisited_t obsReq71)
	(uncovered obsReq71)
	(= (Time-mission-start-obs-target obsReq71) 0)
	(= (Time-mission-end-obs-target obsReq71) 0)
	(site_target target84 obsReq72)
	(= (Time-min-obs obsReq72) 60000)
	(= (Earliest-time-start-obs-target obsReq72) 3600000)
	(= (Latest-time-end-obs-target obsReq72) 21600000)
	(unvisited_t obsReq72)
	(uncovered obsReq72)
	(= (Time-mission-start-obs-target obsReq72) 0)
	(= (Time-mission-end-obs-target obsReq72) 0)
	(site_target target89 obsReq74)
	(= (Time-min-obs obsReq74) 900000)
	(= (Earliest-time-start-obs-target obsReq74) 2400000)
	(= (Latest-time-end-obs-target obsReq74) 5400000)
	(unvisited_t obsReq74)
	(uncovered obsReq74)
	(= (Time-mission-start-obs-target obsReq74) 0)
	(= (Time-mission-end-obs-target obsReq74) 0)
	(site_target targetEnd89 obsReq74)
	(not_same target89 targetEnd89)

	(osservabile obsReq75)
	(= (observable obsReq75) 2400000)
	(osservabile obsReq68)
	(= (observable obsReq68) 3600000)
	(osservabile obsReq69)
	(= (observable obsReq69) 3600000)
	(osservabile obsReq71)
	(= (observable obsReq71) 3600000)
	(osservabile obsReq72)
	(= (observable obsReq72) 3600000)
	(osservabile obsReq74)
	(= (observable obsReq74) 2400000)

	(site_for_begin_takeOff airport-CUF airport-CUF-site)
	(site_for_end_takeOff airport-CUF airport-CUF-site-end-takeoff)
	(site_for_landing airport-CUF airport-CUF-site-begin-landing)
	(site_after_landing airport-CUF airport-CUF-site)
	(= (time-last-take-off airport-CUF) 0)
	(= (Distance2D airport-CUF-site airport-CUF-site) 0)
	(= (Distance2D airport-CUF-site airport-CUF-site-begin-landing) 4407)
	(= (Distance2D airport-CUF-site airport-CUF-site-end-takeoff) 4460)
	(= (Distance2D airport-CUF-site-begin-landing airport-CUF-site) 4407)
	(= (Distance2D airport-CUF-site-begin-landing airport-CUF-site-begin-landing) 0)
	(= (Distance2D airport-CUF-site-begin-landing airport-CUF-site-end-takeoff) 8852)
	(= (Distance2D airport-CUF-site-end-takeoff airport-CUF-site) 4460)
	(= (Distance2D airport-CUF-site-end-takeoff airport-CUF-site-begin-landing) 8852)
	(= (Distance2D airport-CUF-site-end-takeoff airport-CUF-site-end-takeoff) 0)
	(= (time_required airport-CUF-site airport-CUF-site uav6) 0.0)
	(= (time_required airport-CUF-site airport-CUF-site-begin-landing uav6) 88140.0)
	(= (time_required airport-CUF-site airport-CUF-site-end-takeoff uav6) 89200.0)
	(= (time_required airport-CUF-site-begin-landing airport-CUF-site uav6) 88140.0)
	(= (time_required airport-CUF-site-begin-landing airport-CUF-site-begin-landing uav6) 0.0)
	(= (time_required airport-CUF-site-begin-landing airport-CUF-site-end-takeoff uav6) 177040.0)
	(= (time_required airport-CUF-site-end-takeoff airport-CUF-site uav6) 89200.0)
	(= (time_required airport-CUF-site-end-takeoff airport-CUF-site-begin-landing uav6) 177040.0)
	(= (time_required airport-CUF-site-end-takeoff airport-CUF-site-end-takeoff uav6) 0.0)
	(= (time_required airport-CUF-site airport-CUF-site uav4) 0.0)
	(= (time_required airport-CUF-site airport-CUF-site-begin-landing uav4) 95804.3)
	(= (time_required airport-CUF-site airport-CUF-site-end-takeoff uav4) 96956.5)
	(= (time_required airport-CUF-site-begin-landing airport-CUF-site uav4) 95804.3)
	(= (time_required airport-CUF-site-begin-landing airport-CUF-site-begin-landing uav4) 0.0)
	(= (time_required airport-CUF-site-begin-landing airport-CUF-site-end-takeoff uav4) 192434.8)
	(= (time_required airport-CUF-site-end-takeoff airport-CUF-site uav4) 96956.5)
	(= (time_required airport-CUF-site-end-takeoff airport-CUF-site-begin-landing uav4) 192434.8)
	(= (time_required airport-CUF-site-end-takeoff airport-CUF-site-end-takeoff uav4) 0.0)
	(= (Distance2D airport-CUF-site-begin-landing target88) 92209)
	(= (Distance2D target88 airport-CUF-site-begin-landing) 92209)
	(= (Distance2D airport-CUF-site-end-takeoff target88) 83365)
	(= (Distance2D target88 airport-CUF-site-end-takeoff) 83365)
	(= (Distance2D airport-CUF-site target88) 87803)
	(= (Distance2D target88 airport-CUF-site) 87803)
	(= (time_required airport-CUF-site-begin-landing target88 uav6) 1844180.0)
	(= (time_required target88 airport-CUF-site-begin-landing uav6) 1844180.0)
	(= (time_required airport-CUF-site-end-takeoff target88 uav6) 1667300.0)
	(= (time_required target88 airport-CUF-site-end-takeoff uav6) 1667300.0)
	(= (time_required airport-CUF-site target88 uav6) 1756060.0)
	(= (time_required target88 airport-CUF-site uav6) 1756060.0)
	(= (time_required airport-CUF-site-begin-landing target88 uav4) 2004543.5)
	(= (time_required target88 airport-CUF-site-begin-landing uav4) 2004543.5)
	(= (time_required airport-CUF-site-end-takeoff target88 uav4) 1812282.6)
	(= (time_required target88 airport-CUF-site-end-takeoff uav4) 1812282.6)
	(= (time_required airport-CUF-site target88 uav4) 1908760.9)
	(= (time_required target88 airport-CUF-site uav4) 1908760.9)
	(= (Distance2D airport-CUF-site-begin-landing target83) 82546)
	(= (Distance2D target83 airport-CUF-site-begin-landing) 82546)
	(= (Distance2D airport-CUF-site-end-takeoff target83) 73772)
	(= (Distance2D target83 airport-CUF-site-end-takeoff) 73772)
	(= (Distance2D airport-CUF-site target83) 78218)
	(= (Distance2D target83 airport-CUF-site) 78218)
	(= (time_required airport-CUF-site-begin-landing target83 uav6) 1650920.0)
	(= (time_required target83 airport-CUF-site-begin-landing uav6) 1650920.0)
	(= (time_required airport-CUF-site-end-takeoff target83 uav6) 1475440.0)
	(= (time_required target83 airport-CUF-site-end-takeoff uav6) 1475440.0)
	(= (time_required airport-CUF-site target83 uav6) 1564360.0)
	(= (time_required target83 airport-CUF-site uav6) 1564360.0)
	(= (time_required airport-CUF-site-begin-landing target83 uav4) 1794478.3)
	(= (time_required target83 airport-CUF-site-begin-landing uav4) 1794478.3)
	(= (time_required airport-CUF-site-end-takeoff target83 uav4) 1603739.1)
	(= (time_required target83 airport-CUF-site-end-takeoff uav4) 1603739.1)
	(= (time_required airport-CUF-site target83 uav4) 1700391.3)
	(= (time_required target83 airport-CUF-site uav4) 1700391.3)
	(= (Distance2D airport-CUF-site-begin-landing target85) 103880)
	(= (Distance2D target85 airport-CUF-site-begin-landing) 103880)
	(= (Distance2D airport-CUF-site-end-takeoff target85) 95346)
	(= (Distance2D target85 airport-CUF-site-end-takeoff) 95346)
	(= (Distance2D airport-CUF-site target85) 99563)
	(= (Distance2D target85 airport-CUF-site) 99563)
	(= (time_required airport-CUF-site-begin-landing target85 uav6) 2077600.0)
	(= (time_required target85 airport-CUF-site-begin-landing uav6) 2077600.0)
	(= (time_required airport-CUF-site-end-takeoff target85 uav6) 1906920.0)
	(= (time_required target85 airport-CUF-site-end-takeoff uav6) 1906920.0)
	(= (time_required airport-CUF-site target85 uav6) 1991260.0)
	(= (time_required target85 airport-CUF-site uav6) 1991260.0)
	(= (time_required airport-CUF-site-begin-landing target85 uav4) 2258260.9)
	(= (time_required target85 airport-CUF-site-begin-landing uav4) 2258260.9)
	(= (time_required airport-CUF-site-end-takeoff target85 uav4) 2072739.1)
	(= (time_required target85 airport-CUF-site-end-takeoff uav4) 2072739.1)
	(= (time_required airport-CUF-site target85 uav4) 2164413.0)
	(= (time_required target85 airport-CUF-site uav4) 2164413.0)
	(= (Distance2D airport-CUF-site-begin-landing target89) 51812)
	(= (Distance2D target89 airport-CUF-site-begin-landing) 51812)
	(= (Distance2D airport-CUF-site-end-takeoff target89) 43393)
	(= (Distance2D target89 airport-CUF-site-end-takeoff) 43393)
	(= (Distance2D airport-CUF-site target89) 47689)
	(= (Distance2D target89 airport-CUF-site) 47689)
	(= (time_required airport-CUF-site-begin-landing target89 uav6) 1036240.0)
	(= (time_required target89 airport-CUF-site-begin-landing uav6) 1036240.0)
	(= (time_required airport-CUF-site-end-takeoff target89 uav6) 867860.0)
	(= (time_required target89 airport-CUF-site-end-takeoff uav6) 867860.0)
	(= (time_required airport-CUF-site target89 uav6) 953780.0)
	(= (time_required target89 airport-CUF-site uav6) 953780.0)
	(= (time_required airport-CUF-site-begin-landing target89 uav4) 1126347.8)
	(= (time_required target89 airport-CUF-site-begin-landing uav4) 1126347.8)
	(= (time_required airport-CUF-site-end-takeoff target89 uav4) 943326.1)
	(= (time_required target89 airport-CUF-site-end-takeoff uav4) 943326.1)
	(= (time_required airport-CUF-site target89 uav4) 1036717.4)
	(= (time_required target89 airport-CUF-site uav4) 1036717.4)
	(= (Distance2D airport-CUF-site-begin-landing targetEnd89) 68107)
	(= (Distance2D targetEnd89 airport-CUF-site-begin-landing) 68107)
	(= (Distance2D airport-CUF-site-end-takeoff targetEnd89) 60116)
	(= (Distance2D targetEnd89 airport-CUF-site-end-takeoff) 60116)
	(= (Distance2D airport-CUF-site targetEnd89) 64220)
	(= (Distance2D targetEnd89 airport-CUF-site) 64220)
	(= (time_required airport-CUF-site-begin-landing targetEnd89 uav6) 1362140.0)
	(= (time_required targetEnd89 airport-CUF-site-begin-landing uav6) 1362140.0)
	(= (time_required airport-CUF-site-end-takeoff targetEnd89 uav6) 1202320.0)
	(= (time_required targetEnd89 airport-CUF-site-end-takeoff uav6) 1202320.0)
	(= (time_required airport-CUF-site targetEnd89 uav6) 1284400.0)
	(= (time_required targetEnd89 airport-CUF-site uav6) 1284400.0)
	(= (time_required airport-CUF-site-begin-landing targetEnd89 uav4) 1480587.0)
	(= (time_required targetEnd89 airport-CUF-site-begin-landing uav4) 1480587.0)
	(= (time_required airport-CUF-site-end-takeoff targetEnd89 uav4) 1306869.6)
	(= (time_required targetEnd89 airport-CUF-site-end-takeoff uav4) 1306869.6)
	(= (time_required airport-CUF-site targetEnd89 uav4) 1396087.0)
	(= (time_required targetEnd89 airport-CUF-site uav4) 1396087.0)
	(= (Distance2D airport-CUF-site-begin-landing target86) 78362)
	(= (Distance2D target86 airport-CUF-site-begin-landing) 78362)
	(= (Distance2D airport-CUF-site-end-takeoff target86) 69757)
	(= (Distance2D target86 airport-CUF-site-end-takeoff) 69757)
	(= (Distance2D airport-CUF-site target86) 74140)
	(= (Distance2D target86 airport-CUF-site) 74140)
	(= (time_required airport-CUF-site-begin-landing target86 uav6) 1567240.0)
	(= (time_required target86 airport-CUF-site-begin-landing uav6) 1567240.0)
	(= (time_required airport-CUF-site-end-takeoff target86 uav6) 1395140.0)
	(= (time_required target86 airport-CUF-site-end-takeoff uav6) 1395140.0)
	(= (time_required airport-CUF-site target86 uav6) 1482800.0)
	(= (time_required target86 airport-CUF-site uav6) 1482800.0)
	(= (time_required airport-CUF-site-begin-landing target86 uav4) 1703521.7)
	(= (time_required target86 airport-CUF-site-begin-landing uav4) 1703521.7)
	(= (time_required airport-CUF-site-end-takeoff target86 uav4) 1516456.5)
	(= (time_required target86 airport-CUF-site-end-takeoff uav4) 1516456.5)
	(= (time_required airport-CUF-site target86 uav4) 1611739.1)
	(= (time_required target86 airport-CUF-site uav4) 1611739.1)
	(= (Distance2D airport-CUF-site-begin-landing target87) 84185)
	(= (Distance2D target87 airport-CUF-site-begin-landing) 84185)
	(= (Distance2D airport-CUF-site-end-takeoff target87) 75372)
	(= (Distance2D target87 airport-CUF-site-end-takeoff) 75372)
	(= (Distance2D airport-CUF-site target87) 79829)
	(= (Distance2D target87 airport-CUF-site) 79829)
	(= (time_required airport-CUF-site-begin-landing target87 uav6) 1683700.0)
	(= (time_required target87 airport-CUF-site-begin-landing uav6) 1683700.0)
	(= (time_required airport-CUF-site-end-takeoff target87 uav6) 1507440.0)
	(= (time_required target87 airport-CUF-site-end-takeoff uav6) 1507440.0)
	(= (time_required airport-CUF-site target87 uav6) 1596580.0)
	(= (time_required target87 airport-CUF-site uav6) 1596580.0)
	(= (time_required airport-CUF-site-begin-landing target87 uav4) 1830108.7)
	(= (time_required target87 airport-CUF-site-begin-landing uav4) 1830108.7)
	(= (time_required airport-CUF-site-end-takeoff target87 uav4) 1638521.7)
	(= (time_required target87 airport-CUF-site-end-takeoff uav4) 1638521.7)
	(= (time_required airport-CUF-site target87 uav4) 1735413.0)
	(= (time_required target87 airport-CUF-site uav4) 1735413.0)
	(= (Distance2D airport-CUF-site-begin-landing target90) 67960)
	(= (Distance2D target90 airport-CUF-site-begin-landing) 67960)
	(= (Distance2D airport-CUF-site-end-takeoff target90) 60080)
	(= (Distance2D target90 airport-CUF-site-end-takeoff) 60080)
	(= (Distance2D airport-CUF-site target90) 64131)
	(= (Distance2D target90 airport-CUF-site) 64131)
	(= (time_required airport-CUF-site-begin-landing target90 uav6) 1359200.0)
	(= (time_required target90 airport-CUF-site-begin-landing uav6) 1359200.0)
	(= (time_required airport-CUF-site-end-takeoff target90 uav6) 1201600.0)
	(= (time_required target90 airport-CUF-site-end-takeoff uav6) 1201600.0)
	(= (time_required airport-CUF-site target90 uav6) 1282620.0)
	(= (time_required target90 airport-CUF-site uav6) 1282620.0)
	(= (time_required airport-CUF-site-begin-landing target90 uav4) 1477391.3)
	(= (time_required target90 airport-CUF-site-begin-landing uav4) 1477391.3)
	(= (time_required airport-CUF-site-end-takeoff target90 uav4) 1306087.0)
	(= (time_required target90 airport-CUF-site-end-takeoff uav4) 1306087.0)
	(= (time_required airport-CUF-site target90 uav4) 1394152.2)
	(= (time_required target90 airport-CUF-site uav4) 1394152.2)
	(= (Distance2D airport-CUF-site-begin-landing targetEnd90) 136338)
	(= (Distance2D targetEnd90 airport-CUF-site-begin-landing) 136338)
	(= (Distance2D airport-CUF-site-end-takeoff targetEnd90) 127525)
	(= (Distance2D targetEnd90 airport-CUF-site-end-takeoff) 127525)
	(= (Distance2D airport-CUF-site targetEnd90) 131933)
	(= (Distance2D targetEnd90 airport-CUF-site) 131933)
	(= (time_required airport-CUF-site-begin-landing targetEnd90 uav6) 2726760.0)
	(= (time_required targetEnd90 airport-CUF-site-begin-landing uav6) 2726760.0)
	(= (time_required airport-CUF-site-end-takeoff targetEnd90 uav6) 2550500.0)
	(= (time_required targetEnd90 airport-CUF-site-end-takeoff uav6) 2550500.0)
	(= (time_required airport-CUF-site targetEnd90 uav6) 2638660.0)
	(= (time_required targetEnd90 airport-CUF-site uav6) 2638660.0)
	(= (time_required airport-CUF-site-begin-landing targetEnd90 uav4) 2963869.6)
	(= (time_required targetEnd90 airport-CUF-site-begin-landing uav4) 2963869.6)
	(= (time_required airport-CUF-site-end-takeoff targetEnd90 uav4) 2772282.6)
	(= (time_required targetEnd90 airport-CUF-site-end-takeoff uav4) 2772282.6)
	(= (time_required airport-CUF-site targetEnd90 uav4) 2868108.7)
	(= (time_required targetEnd90 airport-CUF-site uav4) 2868108.7)
	(= (Distance2D airport-CUF-site-begin-landing target84) 77712)
	(= (Distance2D target84 airport-CUF-site-begin-landing) 77712)
	(= (Distance2D airport-CUF-site-end-takeoff target84) 69275)
	(= (Distance2D target84 airport-CUF-site-end-takeoff) 69275)
	(= (Distance2D airport-CUF-site target84) 73586)
	(= (Distance2D target84 airport-CUF-site) 73586)
	(= (time_required airport-CUF-site-begin-landing target84 uav6) 1554240.0)
	(= (time_required target84 airport-CUF-site-begin-landing uav6) 1554240.0)
	(= (time_required airport-CUF-site-end-takeoff target84 uav6) 1385500.0)
	(= (time_required target84 airport-CUF-site-end-takeoff uav6) 1385500.0)
	(= (time_required airport-CUF-site target84 uav6) 1471720.0)
	(= (time_required target84 airport-CUF-site uav6) 1471720.0)
	(= (time_required airport-CUF-site-begin-landing target84 uav4) 1689391.3)
	(= (time_required target84 airport-CUF-site-begin-landing uav4) 1689391.3)
	(= (time_required airport-CUF-site-end-takeoff target84 uav4) 1505978.3)
	(= (time_required target84 airport-CUF-site-end-takeoff uav4) 1505978.3)
	(= (time_required airport-CUF-site target84 uav4) 1599695.7)
	(= (time_required target84 airport-CUF-site uav4) 1599695.7)

	(current_site uav6 airport-CUF-site)
	(ready_to_takeoff uav6 airport-CUF)
	(= (Time-take-off uav6 airport-CUF) 242565)
	(= (Time-landing uav6 airport-CUF) 304904)
	(not-already-transfered uav6)
	(= (fuel_required_loiter uav6) 1)
	(= (NumberTrgCoveredUav uav6) 0)
	(= (Time-mission uav6) 0)
	(= (Delta-Time uav6) 0)
	(current_site uav4 airport-CUF-site)
	(ready_to_takeoff uav4 airport-CUF)
	(= (Time-take-off uav4 airport-CUF) 246077)
	(= (Time-landing uav4 airport-CUF) 306537)
	(not-already-transfered uav4)
	(= (fuel_required_loiter uav4) 1)
	(= (NumberTrgCoveredUav uav4) 0)
	(= (Time-mission uav4) 0)
	(= (Delta-Time uav4) 0)

	(= (Distance2D target88 target88) 0)
	(= (time_required target88 target88 uav6) 0.0)
	(= (time_required target88 target88 uav4) 0.0)
	(= (Distance2D target88 target83) 17411)
	(= (time_required target88 target83 uav6) 348220.0)
	(= (time_required target88 target83 uav4) 378500.0)
	(= (Distance2D target88 target85) 24163)
	(= (time_required target88 target85 uav6) 483260.0)
	(= (time_required target88 target85 uav4) 525282.6)
	(= (Distance2D target88 target89) 46231)
	(= (time_required target88 target89 uav6) 924620.0)
	(= (time_required target88 target89 uav4) 1005021.7)
	(= (Distance2D target88 targetEnd89) 43189)
	(= (time_required target88 targetEnd89 uav6) 863780.0)
	(= (time_required target88 targetEnd89 uav4) 938891.3)
	(= (Distance2D target88 target86) 26248)
	(= (time_required target88 target86 uav6) 524960.0)
	(= (time_required target88 target86 uav4) 570608.7)
	(= (Distance2D target88 target87) 13932)
	(= (time_required target88 target87 uav6) 278640.0)
	(= (time_required target88 target87 uav4) 302869.6)
	(= (Distance2D target88 target90) 44948)
	(= (time_required target88 target90 uav6) 898960.0)
	(= (time_required target88 target90 uav4) 977130.4)
	(= (Distance2D target88 targetEnd90) 44481)
	(= (time_required target88 targetEnd90 uav6) 889620.0)
	(= (time_required target88 targetEnd90 uav4) 966978.3)
	(= (Distance2D target88 target84) 31366)
	(= (time_required target88 target84 uav6) 627320.0)
	(= (time_required target88 target84 uav4) 681869.6)
	(= (Distance2D target83 target88) 17411)
	(= (time_required target83 target88 uav6) 348220.0)
	(= (time_required target83 target88 uav4) 378500.0)
	(= (Distance2D target83 target83) 0)
	(= (time_required target83 target83 uav6) 0.0)
	(= (time_required target83 target83 uav4) 0.0)
	(= (Distance2D target83 target85) 41187)
	(= (time_required target83 target85 uav6) 823740.0)
	(= (time_required target83 target85 uav4) 895369.6)
	(= (Distance2D target83 target89) 32472)
	(= (time_required target83 target89 uav6) 649440.0)
	(= (time_required target83 target89 uav4) 705913.0)
	(= (Distance2D target83 targetEnd89) 26037)
	(= (time_required target83 targetEnd89 uav6) 520740.0)
	(= (time_required target83 targetEnd89 uav4) 566021.7)
	(= (Distance2D target83 target86) 8841)
	(= (time_required target83 target86 uav6) 176820.0)
	(= (time_required target83 target86 uav4) 192195.7)
	(= (Distance2D target83 target87) 3481)
	(= (time_required target83 target87 uav6) 69620.0)
	(= (time_required target83 target87 uav4) 75673.9)
	(= (Distance2D target83 target90) 27754)
	(= (time_required target83 target90 uav6) 555080.0)
	(= (time_required target83 target90 uav4) 603347.8)
	(= (Distance2D target83 targetEnd90) 58453)
	(= (time_required target83 targetEnd90 uav6) 1169060.0)
	(= (time_required target83 targetEnd90 uav4) 1270717.4)
	(= (Distance2D target83 target84) 13974)
	(= (time_required target83 target84 uav6) 279480.0)
	(= (time_required target83 target84 uav4) 303782.6)
	(= (Distance2D target85 target88) 24163)
	(= (time_required target85 target88 uav6) 483260.0)
	(= (time_required target85 target88 uav4) 525282.6)
	(= (Distance2D target85 target83) 41187)
	(= (time_required target85 target83 uav6) 823740.0)
	(= (time_required target85 target83 uav4) 895369.6)
	(= (Distance2D target85 target85) 0)
	(= (time_required target85 target85 uav6) 0.0)
	(= (time_required target85 target85 uav4) 0.0)
	(= (Distance2D target85 target89) 65276)
	(= (time_required target85 target89 uav6) 1305520.0)
	(= (time_required target85 target89 uav4) 1419043.5)
	(= (Distance2D target85 targetEnd89) 66053)
	(= (time_required target85 targetEnd89 uav6) 1321060.0)
	(= (time_required target85 targetEnd89 uav4) 1435934.8)
	(= (Distance2D target85 target86) 49885)
	(= (time_required target85 target86 uav6) 997700.0)
	(= (time_required target85 target86 uav4) 1084456.5)
	(= (Distance2D target85 target87) 37738)
	(= (time_required target85 target87 uav6) 754760.0)
	(= (time_required target85 target87 uav4) 820391.3)
	(= (Distance2D target85 target90) 67858)
	(= (time_required target85 target90 uav6) 1357160.0)
	(= (time_required target85 target90 uav4) 1475173.9)
	(= (Distance2D target85 targetEnd90) 38033)
	(= (time_required target85 targetEnd90 uav6) 760660.0)
	(= (time_required target85 targetEnd90 uav4) 826804.3)
	(= (Distance2D target85 target84) 55117)
	(= (time_required target85 target84 uav6) 1102340.0)
	(= (time_required target85 target84 uav4) 1198195.7)
	(= (Distance2D target89 target88) 46231)
	(= (time_required target89 target88 uav6) 924620.0)
	(= (time_required target89 target88 uav4) 1005021.7)
	(= (Distance2D target89 target83) 32472)
	(= (time_required target89 target83 uav6) 649440.0)
	(= (time_required target89 target83 uav4) 705913.0)
	(= (Distance2D target89 target85) 65276)
	(= (time_required target89 target85 uav6) 1305520.0)
	(= (time_required target89 target85 uav4) 1419043.5)
	(= (Distance2D target89 target89) 0)
	(= (time_required target89 target89 uav6) 0.0)
	(= (time_required target89 target89 uav4) 0.0)
	(= (Distance2D target89 targetEnd89) 18017)
	(= (time_required target89 targetEnd89 uav6) 360340.0)
	(= (time_required target89 targetEnd89 uav4) 391673.9)
	(= (Distance2D target89 target86) 26858)
	(= (time_required target89 target86 uav6) 537160.0)
	(= (time_required target89 target86 uav4) 583869.6)
	(= (Distance2D target89 target87) 34890)
	(= (time_required target89 target87 uav6) 697800.0)
	(= (time_required target89 target87 uav4) 758478.3)
	(= (Distance2D target89 target90) 18611)
	(= (time_required target89 target90 uav6) 372220.0)
	(= (time_required target89 target90 uav4) 404587.0)
	(= (Distance2D target89 targetEnd90) 90190)
	(= (time_required target89 targetEnd90 uav6) 1803800.0)
	(= (time_required target89 targetEnd90 uav4) 1960652.2)
	(= (Distance2D target89 target84) 25901)
	(= (time_required target89 target84 uav6) 518020.0)
	(= (time_required target89 target84 uav4) 563065.2)
	(= (Distance2D target86 target88) 26248)
	(= (time_required target86 target88 uav6) 524960.0)
	(= (time_required target86 target88 uav4) 570608.7)
	(= (Distance2D target86 target83) 8841)
	(= (time_required target86 target83 uav6) 176820.0)
	(= (time_required target86 target83 uav4) 192195.7)
	(= (Distance2D target86 target85) 49885)
	(= (time_required target86 target85 uav6) 997700.0)
	(= (time_required target86 target85 uav4) 1084456.5)
	(= (Distance2D target86 target89) 26858)
	(= (time_required target86 target89 uav6) 537160.0)
	(= (time_required target86 target89 uav4) 583869.6)
	(= (Distance2D target86 targetEnd89) 17423)
	(= (time_required target86 targetEnd89 uav6) 348460.0)
	(= (time_required target86 targetEnd89 uav4) 378760.9)
	(= (Distance2D target86 target86) 0)
	(= (time_required target86 target86 uav6) 0.0)
	(= (time_required target86 target86 uav4) 0.0)
	(= (Distance2D target86 target87) 12316)
	(= (time_required target86 target87 uav6) 246320.0)
	(= (time_required target86 target87 uav4) 267739.1)
	(= (Distance2D target86 target90) 19084)
	(= (time_required target86 target90 uav6) 381680.0)
	(= (time_required target86 target90 uav4) 414869.6)
	(= (Distance2D target86 targetEnd90) 66322)
	(= (time_required target86 targetEnd90 uav6) 1326440.0)
	(= (time_required target86 targetEnd90 uav4) 1441782.6)
	(= (Distance2D target86 target84) 5268)
	(= (time_required target86 target84 uav6) 105360.0)
	(= (time_required target86 target84 uav4) 114521.7)
	(= (Distance2D target87 target88) 13932)
	(= (time_required target87 target88 uav6) 278640.0)
	(= (time_required target87 target88 uav4) 302869.6)
	(= (Distance2D target87 target83) 3481)
	(= (time_required target87 target83 uav6) 69620.0)
	(= (time_required target87 target83 uav4) 75673.9)
	(= (Distance2D target87 target85) 37738)
	(= (time_required target87 target85 uav6) 754760.0)
	(= (time_required target87 target85 uav4) 820391.3)
	(= (Distance2D target87 target89) 34890)
	(= (time_required target87 target89 uav6) 697800.0)
	(= (time_required target87 target89 uav4) 758478.3)
	(= (Distance2D target87 targetEnd89) 29419)
	(= (time_required target87 targetEnd89 uav6) 588380.0)
	(= (time_required target87 targetEnd89 uav4) 639543.5)
	(= (Distance2D target87 target86) 12316)
	(= (time_required target87 target86 uav6) 246320.0)
	(= (time_required target87 target86 uav4) 267739.1)
	(= (Distance2D target87 target87) 0)
	(= (time_required target87 target87 uav6) 0.0)
	(= (time_required target87 target87 uav4) 0.0)
	(= (Distance2D target87 target90) 31152)
	(= (time_required target87 target90 uav6) 623040.0)
	(= (time_required target87 target90 uav4) 677217.4)
	(= (Distance2D target87 targetEnd90) 55565)
	(= (time_required target87 targetEnd90 uav6) 1111300.0)
	(= (time_required target87 targetEnd90 uav4) 1207934.8)
	(= (Distance2D target87 target84) 17453)
	(= (time_required target87 target84 uav6) 349060.0)
	(= (time_required target87 target84 uav4) 379413.0)
	(= (Distance2D target90 target88) 44948)
	(= (time_required target90 target88 uav6) 898960.0)
	(= (time_required target90 target88 uav4) 977130.4)
	(= (Distance2D target90 target83) 27754)
	(= (time_required target90 target83 uav6) 555080.0)
	(= (time_required target90 target83 uav4) 603347.8)
	(= (Distance2D target90 target85) 67858)
	(= (time_required target90 target85 uav6) 1357160.0)
	(= (time_required target90 target85 uav4) 1475173.9)
	(= (Distance2D target90 target89) 18611)
	(= (time_required target90 target89 uav6) 372220.0)
	(= (time_required target90 target89 uav4) 404587.0)
	(= (Distance2D target90 targetEnd89) 1815)
	(= (time_required target90 targetEnd89 uav6) 36300.0)
	(= (time_required target90 targetEnd89 uav4) 39456.5)
	(= (Distance2D target90 target86) 19084)
	(= (time_required target90 target86 uav6) 381680.0)
	(= (time_required target90 target86 uav4) 414869.6)
	(= (Distance2D target90 target87) 31152)
	(= (time_required target90 target87 uav6) 623040.0)
	(= (time_required target90 target87 uav4) 677217.4)
	(= (Distance2D target90 target90) 0)
	(= (time_required target90 target90 uav6) 0.0)
	(= (time_required target90 target90 uav4) 0.0)
	(= (Distance2D target90 targetEnd90) 85100)
	(= (time_required target90 targetEnd90 uav6) 1702000.0)
	(= (time_required target90 targetEnd90 uav4) 1850000.0)
	(= (Distance2D target90 target84) 14764)
	(= (time_required target90 target84 uav6) 295280.0)
	(= (time_required target90 target84 uav4) 320956.5)
	(= (Distance2D target84 target88) 31366)
	(= (time_required target84 target88 uav6) 627320.0)
	(= (time_required target84 target88 uav4) 681869.6)
	(= (Distance2D target84 target83) 13974)
	(= (time_required target84 target83 uav6) 279480.0)
	(= (time_required target84 target83 uav4) 303782.6)
	(= (Distance2D target84 target85) 55117)
	(= (time_required target84 target85 uav6) 1102340.0)
	(= (time_required target84 target85 uav4) 1198195.7)
	(= (Distance2D target84 target89) 25901)
	(= (time_required target84 target89 uav6) 518020.0)
	(= (time_required target84 target89 uav4) 563065.2)
	(= (Distance2D target84 targetEnd89) 13277)
	(= (time_required target84 targetEnd89 uav6) 265540.0)
	(= (time_required target84 targetEnd89 uav4) 288630.4)
	(= (Distance2D target84 target86) 5268)
	(= (time_required target84 target86 uav6) 105360.0)
	(= (time_required target84 target86 uav4) 114521.7)
	(= (Distance2D target84 target87) 17453)
	(= (time_required target84 target87 uav6) 349060.0)
	(= (time_required target84 target87 uav4) 379413.0)
	(= (Distance2D target84 target90) 14764)
	(= (time_required target84 target90 uav6) 295280.0)
	(= (time_required target84 target90 uav4) 320956.5)
	(= (Distance2D target84 targetEnd90) 70341)
	(= (time_required target84 targetEnd90 uav6) 1406820.0)
	(= (time_required target84 targetEnd90 uav4) 1529152.2)
	(= (Distance2D target84 target84) 0)
	(= (time_required target84 target84 uav6) 0.0)
	(= (time_required target84 target84 uav4) 0.0)
	(= (Distance2D targetEnd89 target88) 43189)
	(= (time_required targetEnd89 target88 uav6) 863780.0)
	(= (time_required targetEnd89 target88 uav4) 938891.3)
	(= (Distance2D targetEnd89 target83) 26037)
	(= (time_required targetEnd89 target83 uav6) 520740.0)
	(= (time_required targetEnd89 target83 uav4) 566021.7)
	(= (Distance2D targetEnd89 target85) 66053)
	(= (time_required targetEnd89 target85 uav6) 1321060.0)
	(= (time_required targetEnd89 target85 uav4) 1435934.8)
	(= (Distance2D targetEnd89 target89) 18017)
	(= (time_required targetEnd89 target89 uav6) 360340.0)
	(= (time_required targetEnd89 target89 uav4) 391673.9)
	(= (Distance2D targetEnd89 targetEnd89) 0)
	(= (time_required targetEnd89 targetEnd89 uav6) 0.0)
	(= (time_required targetEnd89 targetEnd89 uav4) 0.0)
	(= (Distance2D targetEnd89 target86) 17423)
	(= (time_required targetEnd89 target86 uav6) 348460.0)
	(= (time_required targetEnd89 target86 uav4) 378760.9)
	(= (Distance2D targetEnd89 target87) 29419)
	(= (time_required targetEnd89 target87 uav6) 588380.0)
	(= (time_required targetEnd89 target87 uav4) 639543.5)
	(= (Distance2D targetEnd89 target90) 1815)
	(= (time_required targetEnd89 target90 uav6) 36300.0)
	(= (time_required targetEnd89 target90 uav4) 39456.5)
	(= (Distance2D targetEnd89 targetEnd90) 83561)
	(= (time_required targetEnd89 targetEnd90 uav6) 1671220.0)
	(= (time_required targetEnd89 targetEnd90 uav4) 1816543.5)
	(= (Distance2D targetEnd89 target84) 13277)
	(= (time_required targetEnd89 target84 uav6) 265540.0)
	(= (time_required targetEnd89 target84 uav4) 288630.4)
	(= (Distance2D targetEnd90 target88) 44481)
	(= (time_required targetEnd90 target88 uav6) 889620.0)
	(= (time_required targetEnd90 target88 uav4) 966978.3)
	(= (Distance2D targetEnd90 target83) 58453)
	(= (time_required targetEnd90 target83 uav6) 1169060.0)
	(= (time_required targetEnd90 target83 uav4) 1270717.4)
	(= (Distance2D targetEnd90 target85) 38033)
	(= (time_required targetEnd90 target85 uav6) 760660.0)
	(= (time_required targetEnd90 target85 uav4) 826804.3)
	(= (Distance2D targetEnd90 target89) 90190)
	(= (time_required targetEnd90 target89 uav6) 1803800.0)
	(= (time_required targetEnd90 target89 uav4) 1960652.2)
	(= (Distance2D targetEnd90 targetEnd89) 83561)
	(= (time_required targetEnd90 targetEnd89 uav6) 1671220.0)
	(= (time_required targetEnd90 targetEnd89 uav4) 1816543.5)
	(= (Distance2D targetEnd90 target86) 66322)
	(= (time_required targetEnd90 target86 uav6) 1326440.0)
	(= (time_required targetEnd90 target86 uav4) 1441782.6)
	(= (Distance2D targetEnd90 target87) 55565)
	(= (time_required targetEnd90 target87 uav6) 1111300.0)
	(= (time_required targetEnd90 target87 uav4) 1207934.8)
	(= (Distance2D targetEnd90 target90) 85100)
	(= (time_required targetEnd90 target90 uav6) 1702000.0)
	(= (time_required targetEnd90 target90 uav4) 1850000.0)
	(= (Distance2D targetEnd90 targetEnd90) 0)
	(= (time_required targetEnd90 targetEnd90 uav6) 0.0)
	(= (time_required targetEnd90 targetEnd90 uav4) 0.0)
	(= (Distance2D targetEnd90 target84) 70341)
	(= (time_required targetEnd90 target84 uav6) 1406820.0)
	(= (time_required targetEnd90 target84 uav4) 1529152.2)

	(before obsReq71 obsReq70)
	(= (observable obsReq70) 999999995904)
	(before obsReq70 obsReq67)
	(= (observable obsReq67) 999999995904)
  )

  (:goal
	(and
	  (covered obsReq75)
	  (covered obsReq67)
	  (covered obsReq68)
	  (covered obsReq69)
	  (covered obsReq70)
	  (covered obsReq71)
	  (covered obsReq72)
	  (covered obsReq74)
	  (landed uav6 airport-CUF)
	  (landed uav4 airport-CUF)
	  (< (- (max_time_mission) (min_delta_time)) 14000000)
	)
  )

  (:metric
minimize (total-fly-time)
)
)
