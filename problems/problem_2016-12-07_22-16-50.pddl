(define (problem MRD)
  (:domain SmatF2)
  (:objects
	obsReq110 - Point
	obsReq111 obsReq112 obsReq113 - Target
	uav5 uav7 - Uav
	airport-CUF - Airport
	airport-CUF-site airport-CUF-site-begin-landing airport-CUF-site-end-takeoff target88 target89 targetEnd89 target90 targetEnd90 target109 targetEnd109 - Site2D
	SUITE5 SUITE1 - SensorSuite
  )

  (:init
	(= (NumberTrgCovered) 0)
	(= (MinNumTrgt) 2.0)

	(site_target target88 obsReq110)
	(= (Time-min-obs obsReq110) 300)
	(unvisited_t obsReq110)
	(osservabile obsReq110)
	(uncovered obsReq110)
	(site_target target89 obsReq111)
	(= (Time-min-obs obsReq111) 948)
	(unvisited_t obsReq111)
	(osservabile obsReq111)
	(uncovered obsReq111)
	(site_target targetEnd89 obsReq111)
	(site_target target90 obsReq112)
	(= (Time-min-obs obsReq112) 1956)
	(unvisited_t obsReq112)
	(osservabile obsReq112)
	(uncovered obsReq112)
	(site_target targetEnd90 obsReq112)
	(site_target target109 obsReq113)
	(= (Time-min-obs obsReq113) 1863)
	(unvisited_t obsReq113)
	(osservabile obsReq113)
	(uncovered obsReq113)
	(site_target targetEnd109 obsReq113)

	(observable obsReq111)
	(observable obsReq112)
	(observable obsReq113)

	(site_for_begin_takeOff airport-CUF airport-CUF-site)
	(site_for_end_takeOff airport-CUF airport-CUF-site-end-takeoff)
	(site_for_landing airport-CUF airport-CUF-site-begin-landing)
	(site_after_landing airport-CUF airport-CUF-site)
	(= (time_required airport-CUF-site airport-CUF-site uav5) 0.0)
	(= (time_required airport-CUF-site airport-CUF-site-begin-landing uav5) 95.8)
	(= (time_required airport-CUF-site airport-CUF-site-end-takeoff uav5) 97.0)
	(= (time_required airport-CUF-site-begin-landing airport-CUF-site uav5) 95.8)
	(= (time_required airport-CUF-site-begin-landing airport-CUF-site-begin-landing uav5) 0.0)
	(= (time_required airport-CUF-site-begin-landing airport-CUF-site-end-takeoff uav5) 192.4)
	(= (time_required airport-CUF-site-end-takeoff airport-CUF-site uav5) 97.0)
	(= (time_required airport-CUF-site-end-takeoff airport-CUF-site-begin-landing uav5) 192.4)
	(= (time_required airport-CUF-site-end-takeoff airport-CUF-site-end-takeoff uav5) 0.0)
	(= (time_required airport-CUF-site airport-CUF-site uav7) 0.0)
	(= (time_required airport-CUF-site airport-CUF-site-begin-landing uav7) 88.1)
	(= (time_required airport-CUF-site airport-CUF-site-end-takeoff uav7) 89.2)
	(= (time_required airport-CUF-site-begin-landing airport-CUF-site uav7) 88.1)
	(= (time_required airport-CUF-site-begin-landing airport-CUF-site-begin-landing uav7) 0.0)
	(= (time_required airport-CUF-site-begin-landing airport-CUF-site-end-takeoff uav7) 177.0)
	(= (time_required airport-CUF-site-end-takeoff airport-CUF-site uav7) 89.2)
	(= (time_required airport-CUF-site-end-takeoff airport-CUF-site-begin-landing uav7) 177.0)
	(= (time_required airport-CUF-site-end-takeoff airport-CUF-site-end-takeoff uav7) 0.0)
	(= (time_required airport-CUF-site-begin-landing target89 uav5) 1126.3)
	(= (time_required target89 airport-CUF-site-begin-landing uav5) 1126.3)
	(= (time_required airport-CUF-site-end-takeoff target89 uav5) 943.3)
	(= (time_required target89 airport-CUF-site-end-takeoff uav5) 943.3)
	(= (time_required airport-CUF-site target89 uav5) 1036.7)
	(= (time_required target89 airport-CUF-site uav5) 1036.7)
	(= (time_required airport-CUF-site-begin-landing target89 uav7) 1036.2)
	(= (time_required target89 airport-CUF-site-begin-landing uav7) 1036.2)
	(= (time_required airport-CUF-site-end-takeoff target89 uav7) 867.9)
	(= (time_required target89 airport-CUF-site-end-takeoff uav7) 867.9)
	(= (time_required airport-CUF-site target89 uav7) 953.8)
	(= (time_required target89 airport-CUF-site uav7) 953.8)
	(= (time_required airport-CUF-site-begin-landing targetEnd89 uav5) 1480.6)
	(= (time_required targetEnd89 airport-CUF-site-begin-landing uav5) 1480.6)
	(= (time_required airport-CUF-site-end-takeoff targetEnd89 uav5) 1306.9)
	(= (time_required targetEnd89 airport-CUF-site-end-takeoff uav5) 1306.9)
	(= (time_required airport-CUF-site targetEnd89 uav5) 1396.1)
	(= (time_required targetEnd89 airport-CUF-site uav5) 1396.1)
	(= (time_required airport-CUF-site-begin-landing targetEnd89 uav7) 1362.1)
	(= (time_required targetEnd89 airport-CUF-site-begin-landing uav7) 1362.1)
	(= (time_required airport-CUF-site-end-takeoff targetEnd89 uav7) 1202.3)
	(= (time_required targetEnd89 airport-CUF-site-end-takeoff uav7) 1202.3)
	(= (time_required airport-CUF-site targetEnd89 uav7) 1284.4)
	(= (time_required targetEnd89 airport-CUF-site uav7) 1284.4)
	(= (time_required airport-CUF-site-begin-landing target88 uav5) 2004.5)
	(= (time_required target88 airport-CUF-site-begin-landing uav5) 2004.5)
	(= (time_required airport-CUF-site-end-takeoff target88 uav5) 1812.3)
	(= (time_required target88 airport-CUF-site-end-takeoff uav5) 1812.3)
	(= (time_required airport-CUF-site target88 uav5) 1908.8)
	(= (time_required target88 airport-CUF-site uav5) 1908.8)
	(= (time_required airport-CUF-site-begin-landing target88 uav7) 1844.2)
	(= (time_required target88 airport-CUF-site-begin-landing uav7) 1844.2)
	(= (time_required airport-CUF-site-end-takeoff target88 uav7) 1667.3)
	(= (time_required target88 airport-CUF-site-end-takeoff uav7) 1667.3)
	(= (time_required airport-CUF-site target88 uav7) 1756.1)
	(= (time_required target88 airport-CUF-site uav7) 1756.1)
	(= (time_required airport-CUF-site-begin-landing target90 uav5) 1477.4)
	(= (time_required target90 airport-CUF-site-begin-landing uav5) 1477.4)
	(= (time_required airport-CUF-site-end-takeoff target90 uav5) 1306.1)
	(= (time_required target90 airport-CUF-site-end-takeoff uav5) 1306.1)
	(= (time_required airport-CUF-site target90 uav5) 1394.2)
	(= (time_required target90 airport-CUF-site uav5) 1394.2)
	(= (time_required airport-CUF-site-begin-landing target90 uav7) 1359.2)
	(= (time_required target90 airport-CUF-site-begin-landing uav7) 1359.2)
	(= (time_required airport-CUF-site-end-takeoff target90 uav7) 1201.6)
	(= (time_required target90 airport-CUF-site-end-takeoff uav7) 1201.6)
	(= (time_required airport-CUF-site target90 uav7) 1282.6)
	(= (time_required target90 airport-CUF-site uav7) 1282.6)
	(= (time_required airport-CUF-site-begin-landing targetEnd90 uav5) 2963.9)
	(= (time_required targetEnd90 airport-CUF-site-begin-landing uav5) 2963.9)
	(= (time_required airport-CUF-site-end-takeoff targetEnd90 uav5) 2772.3)
	(= (time_required targetEnd90 airport-CUF-site-end-takeoff uav5) 2772.3)
	(= (time_required airport-CUF-site targetEnd90 uav5) 2868.1)
	(= (time_required targetEnd90 airport-CUF-site uav5) 2868.1)
	(= (time_required airport-CUF-site-begin-landing targetEnd90 uav7) 2726.8)
	(= (time_required targetEnd90 airport-CUF-site-begin-landing uav7) 2726.8)
	(= (time_required airport-CUF-site-end-takeoff targetEnd90 uav7) 2550.5)
	(= (time_required targetEnd90 airport-CUF-site-end-takeoff uav7) 2550.5)
	(= (time_required airport-CUF-site targetEnd90 uav7) 2638.7)
	(= (time_required targetEnd90 airport-CUF-site uav7) 2638.7)
	(= (time_required airport-CUF-site-begin-landing target109 uav5) 1127.0)
	(= (time_required target109 airport-CUF-site-begin-landing uav5) 1127.0)
	(= (time_required airport-CUF-site-end-takeoff target109 uav5) 943.4)
	(= (time_required target109 airport-CUF-site-end-takeoff uav5) 943.4)
	(= (time_required airport-CUF-site target109 uav5) 1037.1)
	(= (time_required target109 airport-CUF-site uav5) 1037.1)
	(= (time_required airport-CUF-site-begin-landing target109 uav7) 1036.8)
	(= (time_required target109 airport-CUF-site-begin-landing uav7) 1036.8)
	(= (time_required airport-CUF-site-end-takeoff target109 uav7) 868.0)
	(= (time_required target109 airport-CUF-site-end-takeoff uav7) 868.0)
	(= (time_required airport-CUF-site target109 uav7) 954.1)
	(= (time_required target109 airport-CUF-site uav7) 954.1)
	(= (time_required airport-CUF-site-begin-landing targetEnd109 uav5) 2361.5)
	(= (time_required targetEnd109 airport-CUF-site-begin-landing uav5) 2361.5)
	(= (time_required airport-CUF-site-end-takeoff targetEnd109 uav5) 2200.5)
	(= (time_required targetEnd109 airport-CUF-site-end-takeoff uav5) 2200.5)
	(= (time_required airport-CUF-site targetEnd109 uav5) 2277.7)
	(= (time_required targetEnd109 airport-CUF-site uav5) 2277.7)
	(= (time_required airport-CUF-site-begin-landing targetEnd109 uav7) 2172.5)
	(= (time_required targetEnd109 airport-CUF-site-begin-landing uav7) 2172.5)
	(= (time_required airport-CUF-site-end-takeoff targetEnd109 uav7) 2024.4)
	(= (time_required targetEnd109 airport-CUF-site-end-takeoff uav7) 2024.4)
	(= (time_required airport-CUF-site targetEnd109 uav7) 2095.5)
	(= (time_required targetEnd109 airport-CUF-site uav7) 2095.5)

	(current_site uav5 airport-CUF-site)
	(ready_to_takeoff uav5 airport-CUF)
	(= (Time-take-off uav5 airport-CUF) 244)
	(= (Time-landing uav5 airport-CUF) 306)
	(at 0 (= (start_next uav5) 0.01))
	(current_site uav7 airport-CUF-site)
	(ready_to_takeoff uav7 airport-CUF)
	(= (Time-take-off uav7 airport-CUF) 240)
	(= (Time-landing uav7 airport-CUF) 303)
	(at 0 (= (start_next uav7) 0.01))

	(= (time_required target89 target89 uav5) 0.0)
	(= (time_required target89 target89 uav7) 0.0)
	(= (time_required target89 targetEnd89 uav5) 391.7)
	(= (time_required target89 targetEnd89 uav7) 360.3)
	(= (time_required target89 target88 uav5) 1005.0)
	(= (time_required target89 target88 uav7) 924.6)
	(= (time_required target89 target90 uav5) 404.6)
	(= (time_required target89 target90 uav7) 372.2)
	(= (time_required target89 targetEnd90 uav5) 1960.7)
	(= (time_required target89 targetEnd90 uav7) 1803.8)
	(= (time_required target89 target109 uav5) 8.7)
	(= (time_required target89 target109 uav7) 8.0)
	(= (time_required target89 targetEnd109 uav5) 1818.6)
	(= (time_required target89 targetEnd109 uav7) 1673.1)
	(= (time_required target88 target89 uav5) 1005.0)
	(= (time_required target88 target89 uav7) 924.6)
	(= (time_required target88 targetEnd89 uav5) 938.9)
	(= (time_required target88 targetEnd89 uav7) 863.8)
	(= (time_required target88 target88 uav5) 0.0)
	(= (time_required target88 target88 uav7) 0.0)
	(= (time_required target88 target90 uav5) 977.1)
	(= (time_required target88 target90 uav7) 899.0)
	(= (time_required target88 targetEnd90 uav5) 967.0)
	(= (time_required target88 targetEnd90 uav7) 889.6)
	(= (time_required target88 target109 uav5) 999.0)
	(= (time_required target88 target109 uav7) 919.1)
	(= (time_required target88 targetEnd109 uav5) 1167.3)
	(= (time_required target88 targetEnd109 uav7) 1073.9)
	(= (time_required target90 target89 uav5) 404.6)
	(= (time_required target90 target89 uav7) 372.2)
	(= (time_required target90 targetEnd89 uav5) 39.5)
	(= (time_required target90 targetEnd89 uav7) 36.3)
	(= (time_required target90 target88 uav5) 977.1)
	(= (time_required target90 target88 uav7) 899.0)
	(= (time_required target90 target90 uav5) 0.0)
	(= (time_required target90 target90 uav7) 0.0)
	(= (time_required target90 targetEnd90 uav5) 1850.0)
	(= (time_required target90 targetEnd90 uav7) 1702.0)
	(= (time_required target90 target109 uav5) 409.1)
	(= (time_required target90 target109 uav7) 376.4)
	(= (time_required target90 targetEnd109 uav5) 1996.3)
	(= (time_required target90 targetEnd109 uav7) 1836.6)
	(= (time_required target109 target89 uav5) 8.7)
	(= (time_required target109 target89 uav7) 8.0)
	(= (time_required target109 targetEnd89 uav5) 395.5)
	(= (time_required target109 targetEnd89 uav7) 363.9)
	(= (time_required target109 target88 uav5) 999.0)
	(= (time_required target109 target88 uav7) 919.1)
	(= (time_required target109 target90 uav5) 409.1)
	(= (time_required target109 target90 uav7) 376.4)
	(= (time_required target109 targetEnd90 uav5) 1955.3)
	(= (time_required target109 targetEnd90 uav7) 1798.9)
	(= (time_required target109 target109 uav5) 0.0)
	(= (time_required target109 target109 uav7) 0.0)
	(= (time_required target109 targetEnd109 uav5) 1810.0)
	(= (time_required target109 targetEnd109 uav7) 1665.2)
	(= (time_required targetEnd89 target89 uav5) 391.7)
	(= (time_required targetEnd89 target89 uav7) 360.3)
	(= (time_required targetEnd89 targetEnd89 uav5) 0.0)
	(= (time_required targetEnd89 targetEnd89 uav7) 0.0)
	(= (time_required targetEnd89 target88 uav5) 938.9)
	(= (time_required targetEnd89 target88 uav7) 863.8)
	(= (time_required targetEnd89 target90 uav5) 39.5)
	(= (time_required targetEnd89 target90 uav7) 36.3)
	(= (time_required targetEnd89 targetEnd90 uav5) 1816.5)
	(= (time_required targetEnd89 targetEnd90 uav7) 1671.2)
	(= (time_required targetEnd89 target109 uav5) 395.5)
	(= (time_required targetEnd89 target109 uav7) 363.9)
	(= (time_required targetEnd89 targetEnd109 uav5) 1957.4)
	(= (time_required targetEnd89 targetEnd109 uav7) 1800.8)
	(= (time_required targetEnd90 target89 uav5) 1960.7)
	(= (time_required targetEnd90 target89 uav7) 1803.8)
	(= (time_required targetEnd90 targetEnd89 uav5) 1816.5)
	(= (time_required targetEnd90 targetEnd89 uav7) 1671.2)
	(= (time_required targetEnd90 target88 uav5) 967.0)
	(= (time_required targetEnd90 target88 uav7) 889.6)
	(= (time_required targetEnd90 target90 uav5) 1850.0)
	(= (time_required targetEnd90 target90 uav7) 1702.0)
	(= (time_required targetEnd90 targetEnd90 uav5) 0.0)
	(= (time_required targetEnd90 targetEnd90 uav7) 0.0)
	(= (time_required targetEnd90 target109 uav5) 1955.3)
	(= (time_required targetEnd90 target109 uav7) 1798.9)
	(= (time_required targetEnd90 targetEnd109 uav5) 1363.8)
	(= (time_required targetEnd90 targetEnd109 uav7) 1254.7)
	(= (time_required targetEnd109 target89 uav5) 1818.6)
	(= (time_required targetEnd109 target89 uav7) 1673.1)
	(= (time_required targetEnd109 targetEnd89 uav5) 1957.4)
	(= (time_required targetEnd109 targetEnd89 uav7) 1800.8)
	(= (time_required targetEnd109 target88 uav5) 1167.3)
	(= (time_required targetEnd109 target88 uav7) 1073.9)
	(= (time_required targetEnd109 target90 uav5) 1996.3)
	(= (time_required targetEnd109 target90 uav7) 1836.6)
	(= (time_required targetEnd109 targetEnd90 uav5) 1363.8)
	(= (time_required targetEnd109 targetEnd90 uav7) 1254.7)
	(= (time_required targetEnd109 target109 uav5) 1810.0)
	(= (time_required targetEnd109 target109 uav7) 1665.2)
	(= (time_required targetEnd109 targetEnd109 uav5) 0.0)
	(= (time_required targetEnd109 targetEnd109 uav7) 0.0)

	(before obsReq113 obsReq110)
  )

  (:goal
	(and
	  (covered obsReq110)
	  (covered obsReq111)
	  (covered obsReq112)
	  (covered obsReq113)
	  (landed uav5 airport-CUF)
	  (landed uav7 airport-CUF)
	)
  )

  (:metric
minimize (total-time)
)
)
